import 'zone.js/dist/zone-node';
import { enableProdMode } from '@angular/core';
import * as express from 'express';
import { join } from 'path';
import * as cors from "cors";
import * as compression from 'compression';
import * as xhr2 from 'xhr2';
import * as cookieParser from 'cookie-parser'

// Mocking window/MediaDeviceInfo for zxing-scanner
const domino = require('domino');

const fs = require('fs');
const template = fs.readFileSync('dist/browser/index.html').toString();
const win = domino.createWindow(template);
global['window'] = win;
global['MediaDeviceInfo'] = win.MediaDeviceInfo;

xhr2.prototype._restrictedHeaders.cookie = false;


// Express server
const app = express();
app.use(cookieParser());
app.use(compression());
var router = express.Router();

enableProdMode();

//options for cors midddleware
const options: cors.CorsOptions = {
  allowedHeaders: ["Origin", "X-Requested-With", "Content-Type", "Accept", "X-Access-Token"],
  credentials: true,
  methods: "GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE",
  preflightContinue: false
};

//use cors middleware
router.use(cors(options));

//use cors middleware
router.use(cors(options));

//enable pre-flight
router.options("*", cors(options));

const PORT = process.env.PORT || 4000;
const DIST_FOLDER = join(process.cwd(), 'dist/browser');

// * NOTE :: leave this as require() since this file is built Dynamically from webpack
const { AppServerModuleNgFactory, LAZY_MODULE_MAP, ngExpressEngine, provideModuleMap } = require('./dist/server/main');

// Our Universal express-engine (found @ https://github.com/angular/universal/tree/master/modules/express-engine)
app.engine('html', ngExpressEngine({
  bootstrap: AppServerModuleNgFactory,
  providers: [
    provideModuleMap(LAZY_MODULE_MAP)
  ]
}));

app.set('view engine', 'html');
app.set('views', DIST_FOLDER);

// Example Express Rest API endpoints
// app.get('/api/**', (req, res) => { });
// Serve static files from /browser
app.get('*.*', express.static(DIST_FOLDER, {
  maxAge: '1y'
}));

// All regular routes use the Universal engine
// All regular routes use the Universal engine
// app.get('*', (req, res) => {
//   console.log( "-------" + req.hostname);
//   console.log( "-------" + req.originalUrl);
//   res.render('index', { req });
// });
app.get('*', (req, res) => {
  console.time(`GET: ${req.originalUrl}`);
  // console.log("cookies------" + JSON.stringify(req.cookies) + "- path  " + req.originalUrl)

  if (req.originalUrl === '/') {
    if (JSON.stringify(req.cookies).length > 20) {
      // console.log('cookies exist')
      var session_id = JSON.stringify(req.cookies).substring(15);
      // console.log('cookies exist::' + session_id)
      req.originalUrl = req.originalUrl + '/pageRedirect'+ '?session-id=' + session_id.substring(0, session_id.length - 2);;
    } else {
      // console.log('cookies does not exist')
      req.originalUrl = req.originalUrl + '/selectcity'
    }
  }
  res.render(
    'index', {
    req: req,
    res: res,
    providers: [
      {
        provide: 'REQUEST', useValue: (req)
      },
      {
        provide: 'RESPONSE', useValue: (res)
      },
    ]
  },
    (err, html) => {
      // console.timeEnd(`GET: ${req.originalUrl}`);
      if (!!err) throw err;
      res.send(html);
    }
  );
});

// Start up the Node server
app.listen(PORT, () => {
  console.log(`Node Express server listening on http://localhost:${PORT}`);
});
