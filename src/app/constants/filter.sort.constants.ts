export const storeSortOptions = [
  { value: 'POPULARITY', displayText: 'Popularity'},
  { value: 'DISTANCE', displayText: 'Distance'},
  { value: 'RATING', displayText: 'Rating'}
];

export const marketTypeFilter = [
  'Retail',
  'Wholesale',
  'All'
];

export const distanceFilter = [
  {key: 5, label: '0-5 Km'},
  {key: 10, label: 'Upto 10 Km'},
  {key: 15, label: 'Upto 15 Km'},
];

export const productSortOptions = [
  { value: 'POPULARITY', displayText: 'Popularity'},
  { value: 'NEWARRIVALS', displayText: 'New Arrivals'},
  { value: 'PRICELOWER', displayText: 'Low to High'},
  { value: 'PRICEUPPER', displayText: 'High To Low'},
  { value: 'DISTANCE', displayText: 'Distance'}
];

export const offerFilterForProducts = [
  {key: 10, label: '10% and above'},
  {key: 20, label: '20% and above'},
  {key: 30, label: '30% and above'},
  {key: 40, label: '40% and above'},
  {key: 50, label: '50% and above'},
];
