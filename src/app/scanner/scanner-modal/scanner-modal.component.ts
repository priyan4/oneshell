import { Component, OnInit } from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {BarcodeData} from '@app/scanner/scanner.service';

@Component({
  selector: 'app-scanner-modal',
  template: `
    <div class="close-div">
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <app-os-barcode-scanner
        (scanSuccess)="onScanSuccess($event)"></app-os-barcode-scanner>
    </div>
  `,
  styleUrls: ['./scanner-modal.component.scss']
})
export class ScannerModalComponent implements OnInit {
  constructor(public activeModal: NgbActiveModal) {}
  ngOnInit() {
  }
  onScanSuccess(value: BarcodeData) {
    this.activeModal.dismiss('scan success');
    console.log("Scan Success");
    if(value.table_no != null){
      window.open(`/${[`business/so`, value.business_city, value.business_id, value.business_name, value.table_no].join('/')}`, '_self');
    } else {
      window.open(`/${[`business`, value.business_city, value.business_id, value.business_name].join('/')}`, '_self');
    }
  }
}
