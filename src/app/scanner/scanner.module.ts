import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScannerModalComponent } from './scanner-modal/scanner-modal.component';
import { ZXingScannerModule } from '@zxing/ngx-scanner';
import {OsBarcodeScannerComponent} from '@app/scanner/os-barcode-scanner/os-barcode-scanner.component';



@NgModule({
  declarations: [ScannerModalComponent, OsBarcodeScannerComponent],
  imports: [
    CommonModule,
    ZXingScannerModule
  ],
  exports: [ScannerModalComponent, OsBarcodeScannerComponent],
  entryComponents: [ScannerModalComponent]
})
export class ScannerModule { }
