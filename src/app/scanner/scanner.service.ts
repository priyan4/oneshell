import { Injectable } from '@angular/core';
import {tap} from 'rxjs/operators';
import {of} from 'rxjs';
import {Blowfish} from 'javascript-blowfish';
import {MyHttpClient} from '@app/services/http.helper';
import {environment as env} from '@env/environment';
const BASE_URL = env.serverUrl;

@Injectable({
  providedIn: 'root'
})
export class ScannerService {
  private encryptionKey: string;
  constructor(private httpClient: MyHttpClient) { }
  retrieveEncryptionKey() {
    if (!this.encryptionKey) {
      return this.httpClient.get(BASE_URL + '/v1/web/customerService/getEncryptionKey', {responseType: 'text'}).pipe(
        tap(res => this.encryptionKey = res as string)
      );
    }
    return of(this.encryptionKey);
  }

  extractBusinessDetails(encryptionKey: string, dataToDecrypt: string) {
    const decryptedData = this.decrypt(encryptionKey, dataToDecrypt);
    if (!decryptedData) { return; }
    const extractedData = this.extract(decryptedData);
    if (extractedData.link_to_invoke === BUSINESS_PAGE) {
      return extractedData;
    }
  }
  private decrypt(encryptionKey: string, dataToDecrypt: string) {
    try {
      const blowfish = new Blowfish(encryptionKey);
      const base64 = blowfish.base64Decode(dataToDecrypt)
      const decryptedDataWithZeros = blowfish.decrypt(base64);
      const decryptedData = blowfish.trimZeros(decryptedDataWithZeros);
      return decryptedData;
    } catch (e) {
      console.error('Error while decrypting', e);
    }
  }
  private extract(decrypted: string): BarcodeData {
    const decryptedArr = decrypted.split('::');
    return {
      link_to_invoke: decryptedArr[0],
      business_id: decryptedArr[1],
      business_city: decryptedArr[2],
      partner_id: decryptedArr[3],
      partner_city: decryptedArr[4],
      business_name: decryptedArr[5],
      table_no: decryptedArr[6]
    } as BarcodeData;
  }
}
const BUSINESS_PAGE = 'BUSINESS_PAGE';
export interface BarcodeData {
  link_to_invoke: string;
  business_id: string;
  business_city: string;
  partner_id: string;
  partner_city: string;
  business_name: string;
  table_no: string;
}
