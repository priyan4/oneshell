import { Component, EventEmitter, OnInit, Output} from '@angular/core';
import {BarcodeData, ScannerService} from '@app/scanner/scanner.service';
import { BarcodeFormat } from '@zxing/library';

@Component({
  selector: 'app-os-barcode-scanner',
  template: `
    <div [hidden]="!permission">
      <zxing-scanner [formats]="allowedFormats"
                     [enable]="enable"
                     (camerasFound)="onCamerasFound($event)"
                     (camerasNotFound)="onCamerasNotFound($event)"
                     (scanSuccess)="onScanSuccess($event)"
                     (permissionResponse)="onPermissionResponse($event)">
      </zxing-scanner>
    </div>
    <div *ngIf="!permission" class="alert alert-danger" role="alert">
      This page has been blocked from accessing your camera. Please allow camera access from settings.
    </div>
  `,
  styleUrls: ['./os-barcode-scanner.component.scss']
})
export class OsBarcodeScannerComponent  implements OnInit {
  @Output()
  scanSuccess = new EventEmitter<BarcodeData>();
  allowedFormats: BarcodeFormat[];
  enable: boolean;
  permission = true;

  constructor(private scannerService: ScannerService) {
  }
  ngOnInit(): void {
    this.allowedFormats = [BarcodeFormat.QR_CODE, BarcodeFormat.EAN_13, BarcodeFormat.CODE_128, BarcodeFormat.DATA_MATRIX];
    this.enable = true;
  }
  onCamerasFound(value) {
    console.log('onCamerasFound', value)
  }
  onCamerasNotFound(value) {
    console.log('onCamerasNotFound', value)
  }
  onScanSuccess(encrypted) {
    this.scannerService.retrieveEncryptionKey().subscribe(encryptionKey => {
      const extractedData = this.scannerService.extractBusinessDetails(encryptionKey, encrypted);
      if (extractedData) {
        this.enable = false;
        this.scanSuccess.emit(extractedData);
      }
    });
  }
  onPermissionResponse(value) {
    this.permission = value;
  }
 }
