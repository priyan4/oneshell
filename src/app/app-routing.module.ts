import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from '../app/home-page/home-page.component';
import { MoviesComponent } from '../app/pages/movies/movies.component';
import { MovieDetailsComponent } from '../app/pages/movies/movie-details/movie-details.component';
import { DynamicLinkComponent } from '@app/core/dynamic-link/dynamic-link.component';
import { EventsComponent } from '../app/pages/events/events.component';
import { EventsDetailsComponent } from '../app/pages/events/events-details/events-details.component';
import { CouponsComponent } from '../app/pages/coupons/coupons.component';
import { NotFoundComponent } from '@app/pages/not-found/not-found.component';
import { HealthComponent } from '../app/pages/health/health.component';
import { BanquetHallsComponent } from './pages/banquet-halls/banquet-halls.component';
import { OrdersComponent } from './pages/orders/orders.component';
import { OrderDetailsComponent } from './pages/orders/order-details/order-details.component';
import { HealthDepartmentsComponent } from './pages/health-departments/health-departments.component';
import { StoreComponent } from '../app/pages/store/store.component';
import { OffersComponent } from './pages/offers/offers.component';
import { NewStoresComponent } from './pages/new-stores/new-stores.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { StaticPageWrapperComponent } from './pages/static-page-wrapper/static-page-wrapper.component';
import { ActivitiesComponent } from './pages/activities/activities.component';
import { ActivitiesDetailComponent } from './pages/activities/activities-detail/activities-detail.component';
import { HomeDeliveryBrowseCategoriesComponent } from './pages/home-delivery/home-delivery-browse-categories/home-delivery-browse-categories.component';
import { DeliveryOrderDetailsComponent } from './pages/orders/delivery-order-details/delivery-order-details.component';
import {HOME} from '@app/constants';
import { LocationComponent } from './pages/location/location.component';
import { StaticPageComponent } from './static-page/static-page.component';
// import { BaseRedirectionComponent } from './base-redirection/base-redirection.component';

const routes: Routes = [
  {
    path: HOME,
    component: HomePageComponent
  },
  {
    path: 'selectcity',
    component: StaticPageComponent
  },
  {
    path: 'movies/:city/:tab',
    component: MoviesComponent
  },
  {
    path: 'movie/:city/:name',
    component: MovieDetailsComponent
  },
  {
    path: 'stores/:city',
    component: StoreComponent
  },
  { path: 'business', loadChildren: () => import('./business-page/business-page.module').then(m => m.BusinessPageModule) },
  { path: 'realEstate', loadChildren: () => import('./real-estate/real-estate.module').then(m => m.RealEstateModule) },
  { path: 'products', loadChildren: () => import('./products-page/products-page.module').then(m => m.ProductsPageModule) },
  { path: 'service', loadChildren: () => import('./service-page/service-page.module').then(m => m.ServicePageModule) },
  { path: 'recommendations', loadChildren: () => import('./recommendations-page/recommendations-page.module').then(m => m.RecommendationsPageModule) },
  { path: 'viewCart', loadChildren: () => import('./view-cart/view-cart.module').then(m => m.ViewCartModule) },
  { path: 'hall', loadChildren: () => import('./halls-page/halls-page.module').then(m => m.HallsPageModule) },
  {
    path: 'events/:city/:type',
    component: EventsComponent
  },
  {
    path: 'event/:city/:type/:id/:name',
    component: EventsDetailsComponent
  },
  {
    path: 'activities/:city/:type',
    component: ActivitiesComponent
  },
  {
    path: 'activity/detail/:city/:type/:id/:name',
    component: ActivitiesDetailComponent
  },
  {
    path: 'offers/:city/:type',
    component: OffersComponent
  },
  {
    path: 'offers/:city',
    redirectTo: 'offers/:city/ALL'
  },
  {
    path: 'coupons/:type',
    component: CouponsComponent
  },
  { path: 'product', loadChildren: () => import('./product/product.module').then(m => m.ProductModule) },
  {
    path: 'health/:city',
    component: HealthComponent
  },
  {
    path: 'stores/n/:city',
    component: NewStoresComponent
  },
  {
    path: 'halls/:city',
    component: BanquetHallsComponent
  },
  {
    path: 'orders/:type/:page',
    component: OrdersComponent
  },
  {
    path: 'order/service/details/:orderId/:businessId/:businessCity',
    component: OrderDetailsComponent
  },
  {
    path: 'order/delivery/details/:orderId/:businessId/:businessCity',
    component: DeliveryOrderDetailsComponent
  },
  {
    path: 'health/d/:city',
    component: HealthDepartmentsComponent
  },
  {
    path: 'homeDelivery/categories/:city',
    component: HomeDeliveryBrowseCategoriesComponent
  },
  {
    path: ':type/:city/:code',
    component: DynamicLinkComponent
  },
  {
    path: 'static-page/:page',
    component: StaticPageWrapperComponent
  },
  { path: 'profile', loadChildren: () => import('./profile/profile.module').then(m => m.ProfileModule) },
  {
    path: 'user',
    loadChildren: () =>
      import('./feature-modules/authentication/authentication.module')
        .then(module => module.AuthenticationModule),
  },
  {
    path: 'contact-us',
    component: ContactUsComponent
  },
  {
    path: 'location',
    component: LocationComponent
  },
  {
    path: '',
    component: HomePageComponent,
    pathMatch: 'full'
  },
  {
    path: ':city',
    component: HomePageComponent
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }


