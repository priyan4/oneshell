import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BusinessPageRoutingModule } from './business-page-routing.module';
import {BusinessDetailsComponent} from './business-details/business-details.component';
import {SharedModule} from '@app/shared/shared-module.module';
import {CarouselModule} from 'ngx-bootstrap';
import {FormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FeaturedStoresComponent} from './featured-stores/featured-stores.component';
import {NgxPaginationModule} from 'ngx-pagination';
import {BusinessListingCategoryComponent} from './store-listing-category/store-listing-category.component';
import {BusinessListingSearchComponent} from './store-listing-search/store-listing-search.component';
import {CoreModule} from '@app/core/core.module';
import {MatCardModule} from '@angular/material/card';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import {MatCheckboxModule, MatRadioModule, MatSelectModule} from '@angular/material';
import {MatTabsModule} from '@angular/material/tabs';
import {OsShareButtonModule} from '@app/os-share-button/os-share-button.module';
import { OtherDetailsComponent } from './business-details/other-details/other-details.component';
@NgModule({
  declarations: [BusinessDetailsComponent, FeaturedStoresComponent, BusinessListingCategoryComponent, BusinessListingSearchComponent,OtherDetailsComponent],
  imports: [
    CommonModule,
    BusinessPageRoutingModule,
    SharedModule,
    FormsModule,
    CarouselModule,
    NgbModule,
    NgxPaginationModule,
    OsShareButtonModule,
    CoreModule,
    MatCardModule,
    MatSlideToggleModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatCheckboxModule,
    MatSelectModule,
    MatTabsModule
  ]
})
export class BusinessPageModule { }
