import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessListingSearchComponent } from './store-listing-search.component';

describe('StoreListingSearchComponent', () => {
  let component: BusinessListingSearchComponent;
  let fixture: ComponentFixture<BusinessListingSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessListingSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessListingSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
