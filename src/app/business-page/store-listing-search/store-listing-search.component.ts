import { Component, OnInit, ViewChild, ElementRef, TemplateRef, Inject, PLATFORM_ID, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { environment as env } from '@env/environment';
import { ContextDataService } from '@app/services/context-data.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { NgForm } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { distanceFilter, marketTypeFilter, storeSortOptions } from '@app/constants/filter.sort.constants';
import { Subject } from 'rxjs';
import { CitiesService } from '@app/services/cities.service';
import { isPlatformBrowser } from '@angular/common';
import { debounceTime, takeWhile, tap } from 'rxjs/operators';
import { maxOfArray } from '@app/utils/math.utils';
import { MatSelectChange } from '@angular/material/typings/select';
import { MatRadioChange } from '@angular/material/typings/radio';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../../services/http.helper';

const BASE_URL = env.serverUrl;


@Component({
  selector: 'app-store-listing-search',
  templateUrl: './store-listing-search.component.html',
  styleUrls: ['./store-listing-search.component.scss']
})
export class BusinessListingSearchComponent implements OnInit, OnDestroy {
  storeCompleteList: any[] = [];
  storeSortOptions: { displayText: string, value: string }[] = storeSortOptions;
  selectedSortOption: string;
  currentDay = '';
  nextToken = 1;
  fromScreen = '';
  isFullyLoaded = false;
  paginationSize = 10;
  listingHeader = 'Stores';
  total_rating: any;
  total_rating_fixed: number;
  total_rating_float: boolean;
  total_rating_total: number;
  listAdvItems: Array<any> = [];
  days: any = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
  displayName = '';
  keyword = '';
  selectedTag = '';
  city = '';
  baseUrl: string = env.webUrl;

  config: any;
  pageNumber: number;
  pageStart: number;
  pageSize: number;
  pageEnd: number;
  totalItems: number;
  showData = true;

  dealModalRef: BsModalRef;
  showDealError = false;
  chosenStore: any;

  destroyed: boolean;
  @ViewChild('confirmDealDetailsShare', { static: false }) confirmDealDetailsTpl: ElementRef;
  shareDealModalRef: BsModalRef;
  currentActiveModalRef: BsModalRef;

  marketTypeFilterOptions: { label: string, value: boolean }[] = [];
  distanceFilterOptions: { label: string, key: number, value: boolean }[] = [];
  locationFilterOptions: { label: string, value: boolean }[] = [];

  filterChange = new Subject();
  readonly filterType = {
    market: 'market',
    distance: 'distance',
    location: 'location'
  };
  filterDetailsInMobile: { selected: string } = { selected: this.filterType.market };
  get currentActiveFilterType() {
    switch (this.filterDetailsInMobile.selected) {
      case this.filterType.market: return this.marketTypeFilterOptions;
      case this.filterType.distance: return this.distanceFilterOptions;
      case this.filterType.location: return this.locationFilterOptions;
    }
  }

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private contextData: ContextDataService,
    private modalService: BsModalService,
    private snackBar: MatSnackBar,
    private citiesService: CitiesService,
    private api: MyHttpClient,
    @Inject(PLATFORM_ID) private platformId: any
  ) {
    this.currentDay = [
      'Sunday',
      'Monday',
      'Tuesday',
      'Wednesday',
      'Thursday',
      'Friday',
      'Saturday'
    ][new Date().getDay()];
  }

  ngOnInit() {

    this.city = this.route.snapshot.paramMap.get('city');
    this.keyword = this.route.snapshot.paramMap.get('keyword');
    this.selectedTag = this.route.snapshot.paramMap.get('category');

    if (this.route.snapshot.paramMap.get('display') == null) {
      this.displayName = this.route.snapshot.paramMap.get('keyword');
    }
    else {
      this.displayName = this.route.snapshot.paramMap.get('display');
    }

    this.pageSize = 12;
    this.pageNumber = parseInt(((this.route.snapshot.paramMap.get('page') != null) ? this.route.snapshot.paramMap.get('page') : '1'));
    this.populateFilters();
    this.selectedSortOption = this.storeSortOptions[0].value;

    this.getStoreList();

    this.listenToFilterChange();
  }

  setHeightOfImage() {
    return (window.innerHeight / 4) + 'px';
  }

  repeatRating(rating: any, feedback: any) {
    if (typeof (rating) !== 'undefined') {
      this.total_rating = (rating / feedback).toFixed(1);
      this.total_rating_fixed = parseInt(this.total_rating.toString().split('.')[0]);
      this.total_rating_float = parseInt(this.total_rating.toString().split('.')[1]) > 0;
      if (this.total_rating_float) {
        this.total_rating_total = 5 - this.total_rating_fixed - 1;
      }
      else {
        this.total_rating_total = 5 - this.total_rating_fixed;
      }
      return true;
    } else {
      return false;
    }
  }
  /*
  
    getStoreAdvertisement() {
  
      console.log('inside store adv method');
  
      let params: any;
      let URL: string;
  
      params = new HttpParams()
        .set('business_city', this.city)
        .set('next_token', String(this.nextToken))
        .set('keyword', this.keyword)
        .set('selected_tag', this.selectedTag)
        .set('customer_id', localStorage.getItem('customer_id'))
        .set('location', this.city);
  
      URL = '/v1/web/searchAll/getAdvBusiness';
  
      this.api.get(URL, params).subscribe(
        response => {
          console.log(response);
  
          this.storeCompleteList = this.storeCompleteList.concat(response.business_list);
          this.getStoreList();
        },
        err => {
          console.log('error' + err);
        }
      );
    }
  */

  private populateFilters() {
    this.marketTypeFilterOptions = marketTypeFilter.map(l => {
      return { label: l, value: false };
    });
    this.distanceFilterOptions = distanceFilter.map(l => {
      return { key: l.key, label: l.label, value: false };
    });
    if (isPlatformBrowser(this.platformId)) {
      this.citiesService.getLocationByCity(this.citiesService.activeCity).pipe(
        tap(res => {
          this.locationFilterOptions = res.sort().map(l => {
            return { label: l, value: false };
          });
        })
      ).subscribe();
    }
  }

  getStoreList() {
    const filterKeys = this.prepareFilterKeys();
    const data = {
      business_city: this.city,
      sort_type: this.selectedSortOption.toLowerCase(),
      page_number: this.pageNumber,
      page_size: this.pageSize,
      keyword: this.keyword,
      selected_tag: this.selectedTag,
      filterKeys
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    this.api.post<any>(BASE_URL + "/v1/web/searchAll/browse", data, httpOptions).subscribe(
      response => {
        this.totalItems = response.total_count;

        this.config = {
          currentPage: this.pageNumber,
          itemsPerPage: this.pageSize,
          totalItems: this.totalItems
        };

        this.pageStart = 1 + (this.pageNumber - 1) * this.pageSize;

        if ((this.totalItems - this.pageStart) > this.pageSize) {
          this.pageEnd = (this.pageNumber * this.pageSize);
        } else {
          this.pageEnd = this.totalItems;
        }

        const currentDate = new Date();
        const dayName = this.days[currentDate.getDay()];

        const dd = String(currentDate.getDate()).padStart(2, '0');
        const mm = String(currentDate.getMonth() + 1).padStart(2, '0');
        const yyyy = currentDate.getFullYear();

        const dateString = yyyy + '-' + mm + '-' + dd + 'T';
        let timing: Map<string, any>;



        for (const business of response.business_list) {

          // Rating
          if (business.number_of_feedbacks != 0) {
            const rating = business.total_ratings / business.number_of_feedbacks;
            business.rating_display_number = rating.toFixed(1);
          } else {
            business.rating_display_number = 0;
          }
          business.rating_display_description = '(' + business.number_of_feedbacks + ' reviews)';

          if (business.oneshell_home_delivery) {
            this.checkOffline(business, dateString);
          } else {
            business.show_offline = false;
          }

          timing = business.business_timings;

          for (const [key, value] of Object.entries(timing)) {

            if (key === dayName) {

              const timingObj = value;

              const startDateTimeString = dateString + timingObj.start_time + ':00';
              const endDateTimeString = dateString + timingObj.end_time + ':00';

              const startDateTime = new Date(startDateTimeString);
              const endDateTime = new Date(endDateTimeString);

              if (timingObj.start_time === '24 hrs') {
                business.is_open = 'Open';
                business.timings = '24 hrs';
              } else if (currentDate.getTime() >= startDateTime.getTime() && currentDate.getTime() <= endDateTime.getTime()) {

                if (timingObj.break_end_time === '- -') {
                  business.is_open = 'Open';
                } else {

                  const breakStartDateTimeString = dateString + timingObj.break_start_time + ':00';
                  const breakEndDateTimeString = dateString + timingObj.break_end_time + ':00';

                  const breakStartDateTime = new Date(breakStartDateTimeString);
                  const breakEndDateTime = new Date(breakEndDateTimeString);

                  if (currentDate.getTime() >= breakEndDateTime.getTime() && currentDate.getTime() <= breakStartDateTime.getTime()) {
                    business.is_open = 'Open';
                  } else {
                    business.is_open = 'Closed';
                  }
                }
                business.timings = startDateTime.toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true }) + ' - ' + endDateTime.toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
              } else {
                business.is_open = 'Closed';
                business.timings = startDateTime.toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true }) + ' - ' + endDateTime.toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
              }
            }
          }
        }

        this.storeCompleteList = response.business_list || [];
        this.storeCompleteList.forEach(store => store.urlBusinessName = store.business_name.replace(/\s+/g, '-').replace(/[^a-zA-Z0-9-]/g, ''))
        this.nextToken = Number(response.next_token);
        if ((Number(response.next_token) - (this.nextToken)) != this.paginationSize) {
          this.isFullyLoaded = true;
        }

      }, err => console.error(err)
    );
  }

  private prepareFilterKeys() {
    const distance = maxOfArray(this.distanceFilterOptions.filter(o => o.value).map(o => o.key));
    const marketTypes = this.marketTypeFilterOptions.filter(o => o.value).map(o => o.label);
    const locations = this.locationFilterOptions.filter(o => o.value).map(o => o.label);
    return {
      distance,
      market_type: marketTypes.length ? marketTypes : undefined,
      locations: locations.length ? locations : undefined
    };
  }

  private listenToFilterChange(): void {
    this.filterChange.pipe(
      takeWhile(() => !this.destroyed),
      debounceTime(500),
      tap(() => this.getStoreList())
    ).subscribe();
  }

  isAnyFilterSelected(): boolean {
    return !!this.distanceFilterOptions.filter(o => o.value).length
      || !!this.marketTypeFilterOptions.filter(o => o.value).length
      || (this.showLocationFilter() && !!this.locationFilterOptions.filter(o => o.value).length);
  }

  clearAllFilters(): void {
    this.distanceFilterOptions.forEach(o => o.value = false);
    this.marketTypeFilterOptions.forEach(o => o.value = false);
    this.locationFilterOptions.forEach(o => o.value = false);
    this.filterChange.next();
  }

  showLocationFilter(): boolean {
    return !!this.locationFilterOptions.length;
  }

  openPopup(template: TemplateRef<any>) {
    this.currentActiveModalRef = this.modalService.show(template);
  }

  pageChange(newPage: number) {
    this.showData = false;
    if (this.route.snapshot.paramMap.get('display') == null) {
      window.location.href = this.baseUrl + '/business/s' + '/' + this.city + '/' + this.selectedTag + '/' + this.keyword + '/' + newPage;
    } else {
      window.location.href = this.baseUrl + '/business/s' + '/' + this.city + '/' + this.selectedTag + '/' + this.keyword + '/' + this.displayName + '/' + newPage;
    }
  }

  checkOffline(businessItem: any, dateString: any) {

    if (businessItem.is_offline) {
      businessItem.show_offline = true;
    }

    if (businessItem.delivery_start_time != null && businessItem.delivery_end_time != null) {
      const startDateTimeString = dateString + businessItem.delivery_start_time + ':00';
      const endDateTimeString = dateString + businessItem.delivery_end_time + ':00';

      const startDateTime = new Date(startDateTimeString);
      const endDateTime = new Date(endDateTimeString);
      const currentDate = new Date();

      if (startDateTime.getTime() > endDateTime.getTime()) {
        if (currentDate.getTime() > startDateTime.getTime()) {
          businessItem.show_offline = false;
        } else {
          businessItem.show_offline = true;
        }
      } else if (endDateTime.getTime() > startDateTime.getTime()) {
        if (currentDate.getTime() > startDateTime.getTime() && currentDate.getTime() < endDateTime.getTime()) {
          businessItem.show_offline = false;
        } else {
          businessItem.show_offline = true;
        }
      }
    }
  }

  onStoreClicked(store: any) {
    window.location.href = this.baseUrl + '/business/' + store.business_city + '/' + store.business_id + '/' + store.business_name;
  }

  showDealTemplate(template: TemplateRef<any>, store: any, event: any) {
    event.stopImmediatePropagation();
    this.chosenStore = store;
    if (!this.contextData.isUserLoggedIn) {
      this.dealModalRef = this.modalService.show(template);
    } else {
      this.shareDealModalRef = this.modalService.show(this.confirmDealDetailsTpl);
    }
  }

  hideDealTemplate() {
    this.showDealError = false;
    this.dealModalRef.hide();
  }

  hideShareDealTemplate() {
    this.shareDealModalRef.hide();
  }

  onSubmit(form: NgForm) {
    if (form.value.name === '' || form.value.number === '') {
      this.showDealError = true;
    } else {
      this.showDealError = false;
      this.hideDealTemplate();
      this.postDealDetails(form.value.name, form.value.number);
    }
  }

  onShareAlertClicked() {
    this.hideShareDealTemplate();
    this.postDealDetails('', '');
  }

  postDealDetails(name: string, phNo: string) {
    const reqBody = {
      business_id: this.chosenStore.business_id,
      business_city: this.chosenStore.business_city,
      customer_name: name,
      customer_phone_number: phNo,
      category_display_name: this.displayName
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    this.api.post<any>(BASE_URL + "/v1/web/customerService/enquiry/deal/getBusinessDeal", reqBody, httpOptions).subscribe(
      response => {
        if (response.success) {
          this.snackBar.open('Successfully Submitted Details!', '', {
            duration: 2000,
          });
        }
      },
      err => {
        console.error(err);
      }
    );
  }
  onSortOptionsChange(event: MatSelectChange | MatRadioChange) {
    this.getStoreList();
  }
  ngOnDestroy() {
    this.destroyed = true;
  }
}