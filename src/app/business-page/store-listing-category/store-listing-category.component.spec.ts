import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessListingCategoryComponent } from './store-listing-category.component';

describe('BusinessListingCategoryComponent', () => {
  let component: BusinessListingCategoryComponent;
  let fixture: ComponentFixture<BusinessListingCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessListingCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessListingCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
