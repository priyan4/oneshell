import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { environment as env } from '@env/environment';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../../services/http.helper';

const BASE_URL = env.serverUrl;

@Component({
  selector: 'app-featured-stores',
  templateUrl: './featured-stores.component.html',
  styleUrls: ['./featured-stores.component.scss']
})
export class FeaturedStoresComponent implements OnInit {

  storeList: Array<any> = [];
  nextToken: number = 1;
  isFullyLoaded: boolean = false;
  paginationSize: number = 10;
  requestedPage: string = '';
  categories: Array<String> = [];
  city: string;
  customer_id: string;
  type: string;

  config: any;
  pageNumber: number;
  pageStart: number;
  pageSize: number;
  pageEnd: number;
  totalItems: number = 0;

  baseUrl: string = env.webUrl;
  showData: boolean = true;

  constructor(
    private route: ActivatedRoute,
    private api: MyHttpClient,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
  }

  ngOnInit() {
    this.storeList = [];
    this.categories.push("health_care_level1");
    this.customer_id = "c0000000000000";

    this.city = this.activatedRoute.snapshot.paramMap.get("city");
    this.type = this.activatedRoute.snapshot.paramMap.get("type");

    this.pageSize = 12;
    this.pageNumber = parseInt(((this.activatedRoute.snapshot.paramMap.get("page") != null) ? this.activatedRoute.snapshot.paramMap.get("page") : '1'));


    this.getPremiumBusinessCount();

    if ("General" === this.type) {
      this.getGeneralBusinessList();
    } else {
      this.getHealthBusinessADVList();
    }
  }

  getPremiumBusinessCount() {

    const params = new HttpParams();
    const reqData = params
      .set("business_city", this.city)
      .set("category", this.type);

    this.api.get<any>(BASE_URL + '/v1/web/homePage/getBusinessAdvCount',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
        response => {

          this.totalItems = response.total_count;

          this.config = {
            currentPage: this.pageNumber,
            itemsPerPage: this.pageSize,
            totalItems: this.totalItems
          };

          this.pageStart = 1 + (this.pageNumber - 1) * this.pageSize;

          if ((this.totalItems - this.pageStart) > this.pageSize)
            this.pageEnd = (this.pageNumber * this.pageSize);
          else
            this.pageEnd = this.totalItems;
        }
      );
  }

  pageChange(newPage: number) {
    this.showData = false;
    if ("General" === this.type) {
      window.location.href = this.baseUrl + '/business/ps/General/' + this.city + "/" + newPage;
    } else {
      window.location.href = this.baseUrl + '/business/ps/Health/' + this.city + "/" + newPage;
    }
  }

  getGeneralBusinessList() {

    let data: any;

    data = {
      business_city: this.city,
      //below is not being used
      customer_gender: "All",
      //below is not being used
      customer_age: 30,
      page_number: this.pageNumber,
      page_size: 12,
      query: 0,
      customer_id: this.customer_id,
      category: "General",
      is_home_page: false
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    this.api.post<any>(BASE_URL + "/v1/web/homePage/getPremiumBusiness", data, httpOptions).subscribe(
      response => {

        console.log(response);
        this.storeList = this.storeList.concat(response.business_list);
        for (let i = 0; i < this.storeList.length; i++) {
          this.storeList[i]['urlBusinessName'] = this.storeList[i]['business_name'].replace(/\s+/g, "-").replace(/[^a-zA-Z0-9-]/g, "");
        }

      }, (err) => {
        console.log('error ' + err);
        //show no data found message
      })
  }

  getHealthBusinessADVList() {

    let reqBody = {
      business_city: this.city,
      page_number: this.pageNumber,
      page_size: 12,
      is_home_page: false,
      customer_id: this.customer_id,
      query: 0,
      category: 'Health',
      customer_gender: 'All'
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    this.api.post<any>(BASE_URL + "/v1/web/homePage/getPremiumBusiness", reqBody, httpOptions).subscribe(
      response => {

        console.log(response);
        this.storeList = this.storeList.concat(response.business_list);
        for (let i = 0; i < this.storeList.length; i++) {
          this.storeList[i]['urlBusinessName'] = this.storeList[i]['business_name'].replace(/\s+/g, "-").replace(/[^a-zA-Z0-9-]/g, "");
        }
      }, (err) => {
        console.log('error ' + err);
        //show no data found message
      })
  }

  setHeightOfImage() {
    return (window.innerHeight / 4) + 'px';
  }
}
