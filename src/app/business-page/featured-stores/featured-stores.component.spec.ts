import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeaturedStoresComponent } from './featured-stores.component';

describe('FeaturedStoresComponent', () => {
  let component: FeaturedStoresComponent;
  let fixture: ComponentFixture<FeaturedStoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeaturedStoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeaturedStoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
