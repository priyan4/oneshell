import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {BusinessDetailsComponent} from './business-details/business-details.component';
import {FeaturedStoresComponent} from './featured-stores/featured-stores.component';
import {BusinessListingCategoryComponent} from './store-listing-category/store-listing-category.component';
import {BusinessListingSearchComponent} from './store-listing-search/store-listing-search.component';
import { OtherDetailsComponent } from './business-details/other-details/other-details.component';


const routes: Routes = [
  { path: 'l/:city/:category/:level/:categorydisplayname/:prevPage/:page', component: BusinessListingCategoryComponent },
  { path: 's/:city/:category/:keyword/:display/:page', component: BusinessListingSearchComponent },
  { path: 's/:city/:category/:keyword/:page', component: BusinessListingSearchComponent },
  { path: 'ps/:type/:city', component: FeaturedStoresComponent },
  { path: 'ps/:type/:city/:page', component: FeaturedStoresComponent },
  { path: ':type/:city/:id/:name/:tableNo', component: BusinessDetailsComponent },
  { path: ':city/:id/:name', component: BusinessDetailsComponent },
  { path: ':type/:city/:id/:name', component: BusinessDetailsComponent },
  { path: 'p/:city/:id/:name/:productid/:category', component: BusinessDetailsComponent },
  { path: 'info',component:OtherDetailsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BusinessPageRoutingModule { }
