import { Component, OnInit, EventEmitter, Output, TemplateRef, ViewChild,HostListener } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subject, Subscription } from 'rxjs';
import { debounceTime, delay, distinctUntilChanged, filter, switchMap, takeWhile, tap } from 'rxjs/operators';
import { LoginDataService } from '@app/shared/login-data.service';
import { Title, Meta } from '@angular/platform-browser';
import { environment as env } from '@env/environment';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { BusinessDetailsService } from '@app/business-page/services/business-details.service';
import { ContextDataService } from '@app/services/context-data.service';
import { LoginService } from '@app/login/login.service';
import { DeviceDetectorService } from 'ngx-device-detector';
import { StoreCartButtonComponent } from '@app/core/components/store-cart-button/store-cart-button.component';
import { SelfOrderCartButtonComponent } from '@app/core/components/self-order-cart-button/self-order-cart-button.component';
import { MatSlideToggleChange } from '@angular/material';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LocationComponent } from '@app/pages/location/location.component';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../../../services/http.helper';
import { } from 'google-maps';
import { Location } from '@angular/common';
declare var google: any;
const BASE_URL = env.serverUrl;

@Component({
  selector: 'app-other-details',
  templateUrl: './other-details.component.html',
  styleUrls: ['./other-details.component.scss']
})
export class OtherDetailsComponent implements OnInit {

  @Output() loginType = new EventEmitter();
  activeCategory: string = '';
  nextToken: number = 1;
  listState: string = '';
  isFullyLoaded: boolean = false;
  businessProfile: any;
  businesslat: any;
  businesslng: any;
  couponLists: any;
  businessObj: any;
  paymentMethods: any;
  servicesList: any[];
  offersList: any[];
  businessCourse: any;
  paymentMethodsCount: boolean;
  businessCourseCount: boolean = false;
  todaysSpecialCount: boolean;
  menucardCount: boolean;
  todaySpecial: any;
  menuCards: any;
  specialists: any;
  professionals: any;
  total_rating: any;
  total_rating_fixed: number = 0;
  total_rating_float: boolean;
  total_rating_total: number = 0;
  homeServiceVendorObj: any;
  phone_number: String;
  alternate_phone_number: String;
  requestedPage: string;
  showCallListModal: boolean;
  isHomeServiceEnabled: boolean = false;
  productsList: any[];
  additionalBrowseList: any;
  additionalBrowseBuyMore: any;
  browse_categories: any;
  browse_categoriesCount: boolean;
  homeServiceCategories: any;
  productsListCount: number = 0;
  isOneShellHomeDelivery: boolean = false;
  restaurantProductList: Array<any> = [];
  isVegSelected: boolean = false;
  menuType: string;

  config: any;
  pageNumber: number;
  pageStart: number;
  pageSize: number;
  pageEnd: number;
  totalItems: number;
  deliveryProfile: any;
  isOffline: boolean = false;
  achievementsList: any[];
  workHistoryList: any[];
  workHistory: any;

  sharableSocialMediaPlatforms = ['facebook', 'twitter', 'linkedin', 'google',
    'telegram', 'messenger', 'whatsapp', 'email', 'tumblr'];

  customer_city: any;
  customer_dellivery_radius: any;
  deliverystatus: string;
  isLocationAvail: boolean;
  deliverstatusaction: string;
  userAddress: string = '';
  message: any;
  subscription: Subscription;
  addToCartProcessed = true;
  get customer_id(): string {
    return this.contextData.loggedInCustomerId;
  }
  get share_link(): string {
    return this.businessProfile && this.businessProfile.share_link;
  }
  currentRate = 0;
  product: any;
  showProductPopUp: boolean = false;
  days: any = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];

  isProdsAvailable: boolean = false;

  isGrocery: boolean = false;
  isFashion: boolean = false;
  isRestaurant: boolean = false;
  isOther: boolean = false;
  isRestaurantWithDelivery: boolean = false;
  isStore: boolean = false;
  levelCategories = [];
  selectedLevelCategory: any;

  searchTxt: string = '';
  search$: Subject<any> = new Subject();
  searchResultsList: Array<any> = [];
  url: string;
  showSearchDialog: boolean = false;

  profCurrentDayTime: string = '';
  sunTime: string = '';
  monTime: string = '';
  tuesTime: string = '';
  wedTime: string = '';
  thurTime: string = '';
  friTime: string = '';
  satTime: string = '';
  showAdditionalTimings: boolean = false;
  shownGroup = null;
  business_id: string;
  business_name: string;
  city: string;
  business_city: string;
  product_id: string;
  category: string;

  imageUrls: Array<string> = [];
  title: string;
  description: string;
  keywords: string;
  animate: boolean = false;

  baseUrl: string = env.webUrl;
  currentMenuLevel3: string;

  tabNo: number = 0;
  tabNo2:number = 1;
  mobileView: boolean = false;

  public latitude: number;
  public longitude: number;
  public currentaddress: any;
  public formatedaddress: any;

  get isSubscribed(): boolean {
    return this.businessProfile && this.businessProfile.is_subscribed;
  }

  private destroyed: boolean;

  modalRef: BsModalRef;
  hotelServicesList: Array<any> = [];
  moreDetails = 'More Details >';

  tableNo: string;
  isSelfOrderAvailable: boolean = false;

  private cartChanged$: Subject<any> = new Subject<any>();

  @ViewChild(StoreCartButtonComponent, { static: false }) private storeCartBtn: StoreCartButtonComponent;
  @ViewChild(SelfOrderCartButtonComponent, { static: false }) private selfOrderCartBtn: SelfOrderCartButtonComponent;
  activeItem: Array<any>;

  constructor(private api: MyHttpClient, private router: Router,
    private data: LoginDataService, private activatedRoute: ActivatedRoute,
    private metaTagService: Meta, private titleService: Title, private modalService: BsModalService,
    private service: BusinessDetailsService,
    public contextData: ContextDataService,
    private _loginService: LoginService,
    private deviceService: DeviceDetectorService,
    private modalServicengb: NgbModal,
    private location: Location
  ) {

    this.subscription = this.service.getMessage().subscribe(message => {
      if (message) {
        this.placechanged();
      } else {
        console.log("error in Subject");
      }
    });
  }


  ngOnInit() {
    var info_detail =JSON.parse(localStorage.getItem("info_details"));
    if(info_detail.id){
      this.business_id = info_detail.id
    }
    if(info_detail.name){
      this.business_name = info_detail.name
    }
    if(info_detail.city){
      this.city = info_detail.city;
      this.business_city = info_detail.city;
    }
    if(info_detail.product_id){
      this.product_id=info_detail.product_id;
    }
    if(info_detail.category){
      this.category=info_detail.category;
    }
    if(info_detail.tableno){
      this.tableNo=info_detail.tableno;
    }
    
    this.getBusinessProfile();
    this.getSecondLevelCategories();
    if (this.contextData.isUserLoggedIn) {
      this.getBusinessRatingByCustomer();
    }
    this.addSearchListener();

    if (this.product_id != null) {
      this.getPremiumProductDetails();
    }
    if (this.deviceService.isMobile()) {
      this.sharableSocialMediaPlatforms = ['facebook', 'whatsapp', 'telegram', 'messenger',
        'google', 'twitter', 'linkedin', 'email', 'tumblr'];
    }
    if(window.screen.width < 576 || window.innerWidth < 576) {
      this.mobileView = true;
    }
    else {
      // window.location.href = this.baseUrl + '/business/' + this.city + '/' + this.business_id + '/' + this.business_name;
      this.mobileView = false;
      this.location.back();
    }

  }
  @HostListener('window:resize')
  onResize() {
    if(window.screen.width < 576 || window.innerWidth < 576) {
      this.tabNo = 0;
      this.tabNo2=1;
      this.mobileView = true;
    }
    else {
      // window.location.href = this.baseUrl + '/business/' + this.city + '/' + this.business_id + '/' + this.business_name;
      this.mobileView = false;
      this.location.back();
    }
  }

  ngAfterViewInit(): void {
    const loadMap = document.createElement('script');
    loadMap.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBhU34uK5dLXDUM1PDVWBWJ6A1CBYCBcKw';
    loadMap.type = 'text/javascript';
    document.body.appendChild(loadMap);
    document.body.removeChild(loadMap);
  }

  showOffersModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  showWorkHistoryModal(template: TemplateRef<any>, workHistoryObj: any) {
    this.modalRef = this.modalService.show(template);
    this.workHistory = workHistoryObj;
  }

  addPageMeta() {
    this.metaTagService.addTags([
      { name: 'Title', content: this.title },
      { name: 'description', content: this.description },
      { name: 'keywords', content: this.keywords },
      { name: 'robots', content: 'index, follow' },
      { name: 'author', content: 'OneShell' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { charset: 'UTF-8' }
    ]);
    this.titleService.setTitle(this.title);
  }

  getBusinessProfile() {
    if (this.activeCategory !== this.listState) {
      this.nextToken = 1;
      this.isFullyLoaded = false;
      this.offersList = [];
      this.hotelServicesList = [];
    }
    const params = new HttpParams()
      .set('business_city', this.business_city)
      .set('business_id', this.business_id)
      .set('customer_id', this.customer_id);

      this.service.getBusinessProfile(params).subscribe(response => {
      this.businessProfile = response;

      this.isSelfOrderAvailable = this.tableNo != null && this.businessProfile.is_self_order_enabled;
      this.listenToCartChange();

      this.businesslat = response.latitude;
      this.businesslng = response.longitude;

      this.isOneShellHomeDelivery = this.businessProfile.oneshell_home_delivery;

      this.title = this.businessProfile.title;
      this.description = this.businessProfile.description;
      this.keywords = this.businessProfile.keywords;
      this.hotelServicesList = this.businessProfile.hotel_service_list;

      this.addPageMeta();

      this.paymentMethods = this.businessProfile.payment_methods;
      this.paymentMethodsCount = this.paymentMethods.length != 0;
      this.todaysSpecialCount = this.businessProfile.specials.length != 0;
      this.menucardCount = this.businessProfile.menucard_images.length != 0;
      this.servicesList = this.businessProfile.services;
      this.offersList = this.businessProfile.offers;
      this.todaySpecial = this.businessProfile.specials;
      this.menuCards = this.businessProfile.menucard_images;
      this.specialists = this.businessProfile.specialist;
      this.professionals = this.businessProfile.professionals;
      this.additionalBrowseList = this.businessProfile.browse_category_response;
      this.phone_number = this.businessProfile.phone_number;
      this.alternate_phone_number = this.businessProfile.alternate_phone_number;
      this.isHomeServiceEnabled = this.businessProfile.is_home_service_enabled;
      this.imageUrls = this.businessProfile.business_image_urls;
      this.achievementsList = this.businessProfile.achievement_response;
      this.workHistoryList = this.businessProfile.work_history_response;

      for (var i = 0; i < this.workHistoryList.length; i++) {
        var workImagesList: any;
        workImagesList = this.workHistoryList[i].secondary_image_urls;
        workImagesList.splice(0, 0, this.workHistoryList[i].primary_image_url);
        this.workHistoryList[i].imageUrls = workImagesList;
      }

      this.isProdsAvailable = this.businessProfile.number_of_products > 0;

      for (var i = 0; i < this.businessProfile.level1_categories.length; i++) {

        if (this.businessProfile.level1_categories[i])
          if (this.businessProfile.level1_categories[i].name.includes("grocery") ||
            this.businessProfile.level1_categories[i].name.includes("ayurvedic") ||
            this.businessProfile.level1_categories[i].name.includes("organic") ||
            this.businessProfile.level1_categories[i].name.includes("pets") ||
            this.businessProfile.level1_categories[i].name.includes("pooja") ||
            this.businessProfile.level1_categories[i].name.includes("wholesale") ||
            this.businessProfile.level1_categories[i].name.includes("sweet")
          ) {
            this.isGrocery = true;
            this.isOther = false;
            this.isFashion = false;
            this.additionalBrowseBuyMore = this.additionalBrowseList[3];
            this.additionalBrowseList = this.additionalBrowseList.slice(0, 3);
            break;
          } else if (this.businessProfile.level1_categories[i].name.includes("fashion") ||
            this.businessProfile.level1_categories[i].name.includes("optical") ||
            this.businessProfile.level1_categories[i].name.includes("sport") ||
            this.businessProfile.level1_categories[i].name.includes("jewellery")) {
            this.isFashion = true;
            this.isGrocery = false;
            this.isOther = false;
            break;
          } else if (this.businessProfile.level1_categories[i].name.includes("food")) {
            this.isRestaurant = true;
            break;
          } else {
            this.isOther = true;
            this.isFashion = false;
            this.isGrocery = false;
          }
      }

      this.isRestaurantWithDelivery = this.isRestaurant && this.isOneShellHomeDelivery;
      if (this.isRestaurantWithDelivery) {
        // this.paginationRedirectionUrl = this.baseUrl + '/products/c/' + this.currentCity + '/' + 'Business Category Products' + '/' + this.businessId + '/' + this.businessCity + '/' + this.level2DisplayName + '/' + this.category + '/' + this.level3Category + '/' + this.index;
        this.getLevel3Categories();
        this.isStore = false;
      } else {
        this.isStore = true;
        this.getBusinessCourse();
        this.trendingProducts();
        if (this.isHomeServiceEnabled) {
          this.homeServiceCategories = this.businessProfile.services_list;
          this.getHomeServiceVendorDetails();
        }

        if (this.professionals.length != 0) {
          for (var i = 0; i < this.professionals.length; i++) {
            this.getProfessionalFormattedTiming(this.professionals[i]);
          }
        }

        if (this.specialists.length != 0) {
          for (var i = 0; i < this.specialists.length; i++) {
            this.getProfessionalFormattedTiming(this.specialists[i]);
          }
        }

        if (this.additionalBrowseList.length) {
          for (let i = 0; i < this.additionalBrowseList.length; i++) {
            this.getBrowseOffersLevelsCategories(this.additionalBrowseList[i]);
          }
        }
      }

      if (this.businessProfile.oneshell_home_delivery) {
        this.getBusinessDeliveryProfile();
      } else {
        this.isOffline = false;
      }

      console.log("Offline Flag : " + this.isOffline);

      // Rating
      if (this.businessProfile.number_of_feedbacks != 0) {
        var rating = this.businessProfile.total_ratings / this.businessProfile.number_of_feedbacks;
        this.businessProfile.rating_display_number = rating.toFixed(1);
      } else {
        this.businessProfile.rating_display_number = 0;
      }
      this.businessProfile.rating_display_description = '(' + this.businessProfile.number_of_feedbacks + ' reviews)';

      this.total_rating = ((this.businessProfile.total_ratings / this.businessProfile.number_of_feedbacks).toFixed(1));
      if (this.total_rating == 'NaN') {
        this.total_rating = 0;
      }
      this.total_rating_fixed = parseInt(this.total_rating.toString().split('.')[0]);
      this.total_rating_float = parseInt(this.total_rating.toString().split('.')[1]) > 0;
      if (this.total_rating_float)
        this.total_rating_total = 5 - this.total_rating_fixed - 1;
      else
        this.total_rating_total = 5 - this.total_rating_fixed;

      /*
    Business Timing logic
    */
      var currentDate = new Date();
      var dayName = this.days[currentDate.getDay()];

      var dd = String(currentDate.getDate()).padStart(2, '0');
      var mm = String(currentDate.getMonth() + 1).padStart(2, '0');
      var yyyy = currentDate.getFullYear();

      var dateString = yyyy + '-' + mm + '-' + dd + 'T';
      let timing: Map<String, any>;

      timing = this.businessProfile.business_timings;

      for (let [key, value] of Object.entries(timing)) {
        if (key === dayName) {

          let timingObj = value;

          var startDateTimeString = dateString + timingObj.start_time + ':00';
          var endDateTimeString = dateString + timingObj.end_time + ':00';

          let startDateTime = new Date(startDateTimeString);
          let endDateTime = new Date(endDateTimeString);

          if (timingObj.start_time === '24 hrs') {
            this.businessProfile.is_open = 'Open';
            this.businessProfile.timings = '24 hrs';
          } else if (currentDate.getTime() >= startDateTime.getTime() && currentDate.getTime() <= endDateTime.getTime()) {

            if (timingObj.break_end_time === '- -') {
              this.businessProfile.is_open = 'Open';
            } else {

              var breakStartDateTimeString = dateString + timingObj.break_start_time + ':00';
              var breakEndDateTimeString = dateString + timingObj.break_end_time + ':00';

              let breakStartDateTime = new Date(breakStartDateTimeString);
              let breakEndDateTime = new Date(breakEndDateTimeString);

              if (currentDate.getTime() >= breakEndDateTime.getTime() && currentDate.getTime() <= breakStartDateTime.getTime()) {
                this.businessProfile.is_open = 'Open';
              } else {
                this.businessProfile.is_open = 'Closed';
              }
            }
            this.businessProfile.timings = startDateTime.toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true }) + ' - ' + endDateTime.toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
          } else {
            this.businessProfile.is_open = 'Closed';
            this.businessProfile.timings = startDateTime.toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true }) + ' - ' + endDateTime.toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
          }
        }
      }
    },
      err => {
        console.error(err);
      }
    );
  }

  getBusinessCourse() {
    const reqData = new HttpParams()
      .set("business_city", this.business_city)
      .set("business_id", this.business_id);

    this.api.get<any>(BASE_URL + '/v1/web/customerService/getBusinessCourses',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
        response => {
          this.businessCourse = response;
          this.businessCourseCount = (this.businessCourse.length !== 0);
        },
        err => {
          console.log("error" + err);
        }
      );
  }

  trendingProducts() {
    const reqData = new HttpParams()
      .set("business_city", this.business_city)
      .set("business_id", this.business_id)
      .set('customer_id', this.contextData.customerProfile.customer_id)
      .set('customer_city', this.contextData.customerProfile.customer_city)
      .set("is_home_page", "false")
      .set("page_number", "1")
      .set("page_size", "10");

    this.api.get<any>(BASE_URL + '/v1/web/customerService/getTrendingProductsInBusiness',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
        response => {
          this.productsList = response.product_list;
          for (let i = 0; i < this.productsList.length; i++) {
            const name = this.productsList[i]['name'].replace(/\s+/g, '-').replace(/[^a-zA-Z0-9-]/g, '');
            this.productsList[i]['urlProductName'] = name;
            this.productsList[i]['productNavigationUrl'] = this.baseUrl + '/product/' + this.productsList[i].business_city + '/' + 'Business' + '/' + this.productsList[i].business_id + '/' + this.productsList[i].category_level3.name + '/' + this.productsList[i].product_id + '/' + this.productsList[i].urlProductName;
          }
        },
        err => {
          console.log('error' + err);
        }
      );
  }

  getSecondLevelCategories() {
    const reqData = new HttpParams()
      .set("business_city", this.business_city)
      .set("business_id", this.business_id);

    this.api.get<any>(BASE_URL + '/v1/web/customerService/getLevel2Categories',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
        response => {
          this.browse_categories = response;
          this.browse_categoriesCount = (this.browse_categories.length != 0);
        },
        err => {
          console.log("error" + err);
        }
      );
  }

  getHomeServiceVendorDetails() {
    const reqData = new HttpParams()
      .set("business_city", this.business_city)
      .set("business_id", this.business_id);

    this.api.get<any>(BASE_URL + '/v1/web/customer/orders/getBusinessHomeServiceDetails',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
        response => {
          this.homeServiceVendorObj = response;
        },
        err => {
          console.log("error" + err);
        }
      );
  }

  getPremiumProductDetails() {
    const reqBody = {
      business_id: this.business_id,
      business_city: this.business_city,
      customer_id: this.contextData.customerProfile.customer_id,
      customer_city: this.contextData.customerProfile.customer_city,
      product_id: this.product_id,
      category: this.category
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    this.api.post<any>(BASE_URL + "/v1/web/customerService/getProductDetails", reqBody, httpOptions).pipe(
      tap(res => {
        this.product = res;
        this.showProductPopUp = true;
      })
    ).subscribe(() => undefined, err => console.error(err));
  }

  getBrowseOffersLevelsCategories(c: any) {
    const reqData = new HttpParams()
      .set("business_city", this.business_city)
      .set("business_id", this.business_id)
      .set("offer_category_name", c.name);

    this.api.get<any>(BASE_URL + '/v1/web/customerService/getLevel1CategoriesByStoreOfferCategory',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
        response => {
          if (response && response.length) {
            c.first_category = response[0].name;
          }
        },
        err => {
          console.log("error" + err);
        }
      );
  }

  handleHomeServiceBooking() {
    this.router.navigate(['/service/h/store/booking/', this.city, this.business_id, this.business_city, this.business_name],
      { state: { data: this.homeServiceVendorObj } });
  }

  openCallListModal() {
    this.showCallListModal = true;
    this.handleBusinessEnquiry();
  }

  closeCallListModal() {
    this.showCallListModal = false;
  }
  closeFormModal() {
    this.showProductPopUp = false;
  }
  showRatingModel(template: TemplateRef<any>) {
    if (this.contextData.isUserLoggedIn) {
      this.modalRef = this.modalService.show(template);
    } else {
      this._loginService.openLoginModal();
    }
  }
  hideRatingModel() {
    this.modalRef.hide();
  }

  showSpamModel() {
    this.service.openSpamModel();
  }

  submitRating() {
    this.hideRatingModel();
    const reqBody = {
      business_id: this.business_id,
      business_city: this.business_city,
      customer_id: this.contextData.customerProfile.customer_id,
      city: this.contextData.customerProfile.customer_city,
      rating: this.currentRate
    };
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    this.api.post<any>(BASE_URL + "/v1/web/customerService/rateBusiness", reqBody, httpOptions).subscribe(
      response => {
        this.getBusinessRatingByCustomer();
      },
      err => {
        console.log("error" + err);
      }
    );
  }

  handleBusinessEnquiry() {
    let reqBody;
    if (localStorage.getItem("username")) {
      reqBody = {
        phone_number: localStorage.getItem("username"),
        business_id: this.business_id,
        business_city: this.business_city,
      };
    } else {
      reqBody = {
        phone_number: "0000000000",
        business_id: this.business_id,
        business_city: this.business_city,
      };
    }

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    this.api.post<any>(BASE_URL + "/v1/web/customerService/enquiry/trackBusinessEnquiry", reqBody, httpOptions).subscribe(res => undefined,
      err => console.log('error' + err));
  }

  handleSearchChange(event) {
    this.search$.next(this.searchTxt);
    if (event.target.id != "txtSearch") {
      this.searchResultsList = [];
    }
  }

  ngOnDestroy() {
    this.destroyed = true;
    this.subscription.unsubscribe();
  }

  addSearchListener() {
    this.search$.pipe(
      takeWhile(() => !this.destroyed),
      distinctUntilChanged(),
      switchMap((searchText: string) => {
        this.searchResultsList = [];
        const params = new HttpParams();
        const reqData = params
          .set("keyword", this.searchTxt)
          .set("business_id", this.business_id)
          .set("business_city", this.business_city)
          .set("size", "20");
        return this.api.get<any>(BASE_URL + '/v1/web/autoComplete/product/getStringsByBusiness',
          { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData });
      })
    ).subscribe((response) => {
      if (response) {
        this.searchResultsList.push(...response);
      }
    });
  }

  getBusinessRatingByCustomer() {
    const reqBody = {
      business_id: this.business_id,
      business_city: this.business_city,
      customer_id: this.contextData.customerProfile.customer_id,
      city: this.contextData.customerProfile.customer_city
    };
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    this.api.post<any>(BASE_URL + "/v1/web/customerService/getRatingsForBusiness", reqBody, httpOptions).subscribe(
      response => {

        this.currentRate = response;
      },
      err => {
        console.log("error" + err);
      }
    );
  }

  showSearchModal() {
    this.showSearchDialog = true;
  }

  hideSearchModal() {
    this.showSearchDialog = false;
  }

  getProfessionalFormattedTiming(profResponse: any) {
    const currentDate = new Date();
    const dayName = this.days[currentDate.getDay()];
    let timing: Map<string, any>;
    timing = profResponse.availability;

    for (const [key, value] of Object.entries(timing)) {
      const timingObj = value;
      let formattedStr = '';
      for (let i = 0; i < timingObj.length; i++) {
        if (timingObj[i].start_time === '24 hrs') {
          formattedStr += '24 hrs';
          break;
        } else if (timingObj[i].start_time === 'Closed') {
          formattedStr += 'Holiday';
          break;
        } else {
          formattedStr += timingObj[i].start_time + ' to ' + timingObj[i].end_time;
        }
        if (i !== (timingObj.length - 1)) {
          formattedStr += ', ';
        }
      }
      if (key === dayName) {
        profResponse.profCurrentDayTime = formattedStr;
      }
      if ('sunday' === key) {
        this.sunTime = 'Sun' + "\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0" + formattedStr;
      } else if ('monday' === key) {
        this.monTime = 'Mon' + "\xa0\xa0\xa0\xa0\xa0\xa0\xa0" + formattedStr;
      } else if ('tuesday' === key) {
        this.tuesTime = 'Tue' + "\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0" + formattedStr;
      } else if ('wednesday' === key) {
        this.wedTime = 'Wed' + "\xa0\xa0\xa0\xa0\xa0\xa0\xa0" + formattedStr;
      } else if ('thursday' === key) {
        this.thurTime = 'Thu' + "\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0" + formattedStr;
      } else if ('friday' === key) {
        this.friTime = 'Fri' + "\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0" + formattedStr;
      } else if ('saturday' === key) {
        this.satTime = 'Sat' + "\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0" + formattedStr;
      }
    }
  }
  oncurrentTimingClicked(group) {
    if (this.isGroupShown(group)) {
      this.shownGroup = null;
    } else {
      this.shownGroup = group;
    }
  }
  isGroupShown(group) {
    return this.shownGroup === group;
  }
  onSubscribe() {
    this.animate = true;
    this.service.subscribeToBusiness(this.business_id, this.business_city).pipe(
      delay(1000),
      tap(subscribed => this.businessProfile.is_subscribed = subscribed),
      tap(subscribed => this.animate = false),
    ).subscribe();
  }

  onQuantityChange(product: any) {
    this.cartChanged$.next();
  }

  private listenToCartChange() {
    if (this.isSelfOrderAvailable) {
      this.cartChanged$.pipe(
        debounceTime(1000),
        takeWhile(() => !this.destroyed),
        filter(() => !!this.selfOrderCartBtn),
        tap(() => this.selfOrderCartBtn.refreshBusinessCartCount())
      ).subscribe();
    } else {
      this.cartChanged$.pipe(
        debounceTime(1000),
        takeWhile(() => !this.destroyed),
        filter(() => !!this.storeCartBtn),
        tap(() => this.storeCartBtn.refreshBusinessCartCount())
      ).subscribe();
    }
  }

  checkOffline() {

    if (this.customer_dellivery_radius != undefined && this.customer_dellivery_radius <= this.deliveryProfile.delivery_kms_range) {
      this.isOffline = false;
    } else {
      this.isOffline = true;
    }

    if (!this.isOffline && this.deliveryProfile.is_offline) {
      this.isOffline = true;
    }

    if (!this.isOffline && this.deliveryProfile.delivery_start_time != null && this.deliveryProfile.delivery_end_time != null) {

      var currentDate = new Date();

      var dd = String(currentDate.getDate()).padStart(2, '0');
      var mm = String(currentDate.getMonth() + 1).padStart(2, '0');
      var yyyy = currentDate.getFullYear();

      var dateString = yyyy + '-' + mm + '-' + dd + 'T';

      var startDateTimeString = dateString + this.deliveryProfile.delivery_start_time + ':00';
      var endDateTimeString = dateString + this.deliveryProfile.delivery_end_time + ':00';

      var startDateTime = new Date(startDateTimeString);
      var endDateTime = new Date(endDateTimeString);

      if (startDateTime.getTime() > endDateTime.getTime()) {
        if (currentDate.getTime() > startDateTime.getTime()) {
          this.isOffline = false;
        } else {
          this.isOffline = true;
        }
      } else if (endDateTime.getTime() > startDateTime.getTime()) {
        if (currentDate.getTime() > startDateTime.getTime() && currentDate.getTime() < endDateTime.getTime()) {
          this.isOffline = false;
        } else {
          this.isOffline = true;
        }
      }
    }

    if (this.isOffline) {
      this.deliverystatus = "Currently not delivering to: ";
      this.deliverstatusaction = "CHANGE";
    }
    else {
      this.deliverystatus = " Delivering to: ";
      this.deliverstatusaction = "CHANGE";
    }
    if (!this.isLocationAvail) {
      this.deliverystatus = "Please provide Delivery location to determine Product /  Service Availability";
      this.deliverstatusaction = "LOCATION";
      this.isOffline = true;
    }
  }

  checkSelfOrderOffline() {

    if (this.customer_dellivery_radius != undefined && this.customer_dellivery_radius <= this.deliveryProfile.self_order_kms_range) {
      this.isOffline = false;
    } else {
      this.isOffline = true;
    }

    if (this.isOffline) {
      this.deliverystatus = "Currently not Serving to: ";
      this.deliverstatusaction = "";
    }
    else {
      this.deliverystatus = " Serving to: ";
      this.deliverstatusaction = "";
    }
    if (!this.isLocationAvail) {
      this.deliverystatus = "Please set your current location to place order";
      this.deliverstatusaction = "LOCATION";
      this.isOffline = true;
    }
  }

  getLevel3Categories() {
    const reqData = new HttpParams()
      .set('business_city', this.business_city)
      .set('business_id', this.business_id)
      .set('category', "restaurant_level2")
      .set('type', "home_delivery");

    this.api.get<any>(BASE_URL + '/v1/web/customerService/getRestaurantMenuCategories',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
        response => {
          this.levelCategories = response;
          for (let i = 0; i < this.levelCategories.length; i++) {
            // this.levelCategories[i].href_link = this.baseUrl + '/products/c/' + this.currentCity + '/' + 'Business Category Products' + '/' + this.businessId + '/' + this.businessCity + '/' + this.level2DisplayName + '/' + this.category + '/' + this.levelCategories[i].name + '/' + i + '/' + '1';
            this.currentMenuLevel3 = this.levelCategories[0];
            this.getProductsByLevel2Category(this.levelCategories[0]);
          }
        },
        err => {
          console.error(err);
        }
      );
  }

  getProductsByLevel2Category(level3Category: any) {
    this.selectedLevelCategory = level3Category;
    this.restaurantProductList = [];
    var leve3PropertiesFilters = [];
    var level3PropertyFilter: any = null;
    if (this.isVegSelected) {
      level3PropertyFilter = {
        property_name: 'veg',
        property_type: 'tag_properties',
      }
      leve3PropertiesFilters.push(level3PropertyFilter);
    }
    const businessProductFilters = {
      category: level3Category.name,
      properties: leve3PropertiesFilters
    };

    const data = {
      business_city: this.business_city,
      page_number: 1,
      page_size: 100,
      business_id: this.business_id,
      level1_or_level2_category: 'restaurant_level2',
      customer_id: this.contextData.customerProfile.customer_id,
      customer_city: this.contextData.customerProfile.customer_city,
      category_level: 'LEVEL2',
      table_no: this.tableNo,
      businessProductFilters
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    this.api.post<any>(BASE_URL + "/v1/web/customerService/getRestaurantProductsByLevel2AndLevel3CategoriesInBusiness", data, httpOptions).subscribe(
      response => {

        this.restaurantProductList = response.product_list;
        this.menuType = level3Category.display_name;

        for (let i = 0; i < this.restaurantProductList.length; i++) {
          this.restaurantProductList[i].urlProductName = this.restaurantProductList[i].name.replace(/\s+/g, '-').replace(/[^a-zA-Z0-9-]/g, '');
          if (this.isSelfOrderAvailable) {
            this.restaurantProductList[i].product_href = this.baseUrl + '/product/selfOrder/' + this.restaurantProductList[i].business_city + '/' + 'Business' + '/' + this.restaurantProductList[i].business_id + '/' + this.restaurantProductList[i].category_level3.name + '/' + this.restaurantProductList[i].product_id + '/' + this.restaurantProductList[i].urlProductName + '/' + this.tableNo;
          } else {
            this.restaurantProductList[i].product_href = this.baseUrl + '/product/' + this.restaurantProductList[i].business_city + '/' + 'Business' + '/' + this.restaurantProductList[i].business_id + '/' + this.restaurantProductList[i].category_level3.name + '/' + this.restaurantProductList[i].product_id + '/' + this.restaurantProductList[i].urlProductName;
          }
        }

        this.totalItems = response.total_count;
        this.config = {
          currentPage: this.pageNumber,
          itemsPerPage: this.pageSize,
          totalItems: this.totalItems
        };

        this.pageStart = 1 + (this.pageNumber - 1) * this.pageSize;

        if ((this.totalItems - this.pageStart) > this.pageSize)
          this.pageEnd = (this.pageNumber * this.pageSize);
        else
          this.pageEnd = this.totalItems;
      },
      err => {
        console.log('error' + err);
      }
    );
  }

  getBusinessDeliveryProfile() {
    const reqData = new HttpParams()
      .set('business_city', this.business_city)
      .set('business_id', this.business_id);

    this.api.get<any>(BASE_URL + '/v1/web/customer/cart/getBusinessDeliveryProfile',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
        response => {
          this.deliveryProfile = response
          this.getdistance();
        },
        err => console.error(err)
      );
  }

  showMenu(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }
  selectMenu(menu) {
    this.modalRef.hide();
    this.currentMenuLevel3 = menu;
    this.getProductsByLevel2Category(menu);
  }

  populateGetProductsByLevel2Category(menu) {
    this.currentMenuLevel3 = menu;
    this.getProductsByLevel2Category(menu);
  }

  onToggle(event: MatSlideToggleChange) {
    this.isVegSelected = event.checked;
    console.log("Veg value : " + this.isVegSelected);
    this.getProductsByLevel2Category(this.currentMenuLevel3);
  }
  openLocationModal(template?: TemplateRef<any>) {
    if(this.isSelfOrderAvailable) {
      this.setCurrentPosition(template);
    }
    else {
      const modalRef = this.modalServicengb.open(LocationComponent);
    }
  }

  getdistance() {
    let localaddress: any;
    localaddress = JSON.parse(localStorage.getItem("userAddress"));
    if (localaddress) {
      this.isLocationAvail = true;
      if (localaddress.address != null) {
        this.userAddress = localaddress.address;
      }
      let data: any;
      data = {
        from_latitude: localaddress.latitude,
        from_longitude: localaddress.longitude,
        to_latitude: this.businesslat,
        to_longitude: this.businesslng
      };

      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      };

      this.api.post<any>(BASE_URL + "/v1/web/customer/maps/getDistance", data, httpOptions).subscribe(
        response => {
          this.customer_dellivery_radius = response;
          if (this.isSelfOrderAvailable) {
            this.checkSelfOrderOffline();
          } else {
            this.checkOffline();
          }
        }, (err) => {
          console.log('error ' + err);
          //show no data found message
        })
    }
    else {
      this.isLocationAvail = false;
      if (this.isSelfOrderAvailable) {
        this.checkSelfOrderOffline();
      } else {
        this.checkOffline();
      }
    }
  }

  placechanged() {
    this.getdistance();
  }



  public setCurrentPosition(template: TemplateRef<any>) {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.autocompleteonload(this.latitude, this.longitude);
      },()=> {
        const modalRef = this.modalServicengb.open(template);
      },{
        enableHighAccuracy: true,
        timeout: 1000,
        maximumAge: 0
      });
    }
    else {
      alert("Your Browser doesnt support Location capture")
    }
  }

  public autocompleteonload(lat, lng) {
    let geocoder = new google.maps.Geocoder();
    let latlng = new google.maps.LatLng(lat, lng);
    let request = {
      location: latlng
    }

    geocoder.geocode(request, (results, status) => {
      if (status == 'OK') {
        this.currentaddress = results[0].address_components;
        this.formatedaddress = results[0].formatted_address;
        this.autocompleteAddress(this.currentaddress);
      }
      else {
        alert('Geocode was not successful for the following reason: ' + status);
      }
    });
    if (this.currentaddress != null || this.currentaddress != undefined) {
      this.autocompleteAddress(this.currentaddress);
    }
  }

  public autocompleteAddress(result) {

    var newItem = {
      'latitude': this.latitude,
      'longitude': this.longitude,
      'address': this.formatedaddress
    };

    localStorage.setItem('userAddress', JSON.stringify(newItem));
    this.getdistance();

  }
  checkUserLogin() {
    if (!this.contextData.isUserLoggedIn) {
      this._loginService.openLoginModal();
    }
  }


}
