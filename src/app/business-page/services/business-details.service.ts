import { Injectable } from '@angular/core';
import {LoginService} from '@app/login/login.service';
import {ContextDataService} from '@app/services/context-data.service';
import {Observable, of, Subject} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {SpamModalComponent} from '@app/core/details-footer-mobile/modals/spam-modal/spam-modal.component';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { environment as env } from '@env/environment';
import { MyHttpClient } from '../../services/http.helper';

const BASE_URL = env.serverUrl;

@Injectable({
  providedIn: 'root'
})
export class BusinessDetailsService {
  private subject = new Subject<any>();

  constructor(private loginService: LoginService, private contextData: ContextDataService,
              private api: MyHttpClient,
              private modalService: NgbModal) { }

  subscribeToBusiness(businessId: string, businessCity: string): Observable<boolean> {
    if (!this.contextData.isUserLoggedIn) {
      this.loginService.openLoginModal();
      return of(false);
    }
    const reqBody = {
      customer_id: this.contextData.customerProfile.customer_id,
      city: this.contextData.customerProfile.customer_city,
      name: this.contextData.customerProfile.name,
      age: this.contextData.customerProfile.age,
      gender: this.contextData.customerProfile.age,
      business_id: businessId,
      business_city: businessCity
    };
    
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.api.post<any>(BASE_URL + "/v1/web/customerService/subscribeBusiness", reqBody, httpOptions).pipe(
      map(data => data.success)
    );
  }
  
  openSpamModel() {
    if (!this.contextData.isUserLoggedIn) {
      this.loginService.openLoginModal();
      return;
    }
    this.modalService.open(SpamModalComponent, { centered: true });
  }

  getBusinessProfile(reqData: HttpParams): Observable<any> {
    return this.api.get<any>(BASE_URL + '/v1/web/customerService/getBusinessProfile',
    { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).pipe(
      tap(res => this.contextData.populateBusinessProfile(res))
    );
  }

  sendMessage(message: string) {
      this.subject.next({ text: message });
  }

  clearMessage() {
      this.subject.next();
  }

  getMessage(): Observable<any> {
      return this.subject.asObservable();
  }
}