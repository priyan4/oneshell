export interface FormValidationErrors {
    [controlName: string]: Array<{
        type: string;
        message: string;
    }>;
}
