import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatSnackBar } from "@angular/material/snack-bar";
import { environment as env } from '@env/environment';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../services/http.helper';

const BASE_URL = env.serverUrl;

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {

  contact: {
    name: string,
    email: string,
    phoneNumber?: number,
    subject: string,
    message: string
  };

  showErrorText: boolean = false;

  constructor(private api: MyHttpClient, private snackBar: MatSnackBar) { }

  ngOnInit() {
  }
  onSubmit(form: NgForm) {

    if (form.value.username === '' || form.value.email === '' || form.value.city === '' || form.value.subject === '' || form.value.message === '') {
      this.showErrorText = true;
    } else {
      const reqBody = {
        name: form.value.username,
        email: form.value.email,
        phoneNumber: form.value.number,
        city: form.value.city,
        subject: form.value.subject,
        message: form.value.message
      };
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      };

      this.api.post<any>(BASE_URL + "/v1/web/customerService/enquiry/saveCustomerFeedbackDetails", reqBody, httpOptions).subscribe(
        response => {
          if (response.success) {
            this.snackBar.open('Successfully Submitted Details!', '', {
              duration: 2000,
            });
            form.reset();
          }
        },
        err => {
          console.log('error' + err);
        }
      );
    }
  }
}