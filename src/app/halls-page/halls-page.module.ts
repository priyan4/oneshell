import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HallsPageRoutingModule } from './halls-page-routing.module';
import {HallDetailsComponent} from '@app/halls-page/hall-details/hall-details.component';
import {CalendarModule} from '@syncfusion/ej2-angular-calendars';
import {SharedModule} from '@app/shared/shared-module.module';
import {NgbRatingModule, NgbPopoverModule} from '@ng-bootstrap/ng-bootstrap';
import {MatTabsModule} from '@angular/material/tabs';
import { NgxGalleryModule } from 'ngx-gallery';
import {MatDatepickerModule, MatNativeDateModule} from '@angular/material';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { MatSelectModule} from '@angular/material';
import { OsShareButtonModule } from '@app/os-share-button/os-share-button.module';

@NgModule({
  declarations: [HallDetailsComponent],
  imports: [
    CommonModule,
    HallsPageRoutingModule,
    SharedModule,
    CalendarModule,
    NgbRatingModule,
    MatTabsModule,
    NgxGalleryModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatInputModule,
    FormsModule,
    MatSelectModule,
    NgbPopoverModule,
    OsShareButtonModule,
  ]
})
export class HallsPageModule { }
