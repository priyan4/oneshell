import { Component, ElementRef, OnInit, ViewChild,TemplateRef, ViewEncapsulation, EventEmitter, Output, HostListener } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { NgbCalendar, NgbDateStruct, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CalendarComponent } from '@syncfusion/ej2-angular-calendars';
import { LoginDataService } from '@app/shared/login-data.service';
import { Title, Meta } from '@angular/platform-browser';
import { environment as env } from '@env/environment';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../../services/http.helper';
import { LoginService } from '@app/login/login.service';
import { BusinessDetailsService } from '@app/business-page/services/business-details.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ContextDataService } from '@app/services/context-data.service';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';
import {FormControl} from '@angular/forms';
import { delay, tap } from 'rxjs/operators';

const BASE_URL = env.serverUrl;

@Component({
  selector: 'app-hall-details',
  templateUrl: './hall-details.component.html',
  styleUrls: ['./hall-details.component.scss'],
})
export class HallDetailsComponent implements OnInit {

  @Output() loginType = new EventEmitter();
  @ViewChild('ejCalendar', { static: false }) ejCalendar: CalendarComponent;

  businessProfile: any;
  hallProfile: any;
  businessObj: any;
  customer_city: any;
  customer_id: any;
  requestedPage: string;
  paymentMethods: any;
  imagesList: any;
  paymentMethodsCount: boolean;
  menuCards: any;
  menucardCount: boolean;
  days: any = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
  servicesList: any;
  offersList: any;
  servicesListCount: boolean;
  offersListCount: boolean;
  phone_number: String;
  alternate_phone_number: String;
  isNonVegAllowed: boolean = false;
  total_rating: any;
  total_rating_fixed: number = 0;
  total_rating_float: boolean;
  total_rating_total: number = 0;
  disableDates = [];
  tabNo: number = 0;
  imgGalleryTitle: string;


  public minDate: Date = new Date();
  public maxDate: Date = new Date("10/16/2020");
  public value: any = new Date();
  public minEnqDate: Date = new Date();

  model: NgbDateStruct;
  showCallListModal: boolean;
  showImagesPopUp: boolean = false;
  showImageUrls: any;
  showRatingModalDialog: Boolean = false;
  currentRate = 0;
  public multiSelect: Boolean = true;

  business_id: string;
  business_name: string;
  city: string;

  title: string;
  description: string;
  keywords: string;
  modalRef: BsModalRef;

  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];
  openGallery: boolean = false;
  from: any;
  to: any;
  fromTime: string;
  toTime: string;
  timings: any = [
      {value:'00:00',viewValue:'00:00'},      
      {value:'00:30',viewValue:'00:30'},      
      {value:'01:00',viewValue:'01:00'},     
      {value:'01:30',viewValue:'01:30'},      
      {value:'02:00',viewValue:'02:00'},      
      {value:'02:30',viewValue:'02:30'},      
      {value:'03:00',viewValue:'03:00'},      
      {value:'03:30',viewValue:'03:30'},      
      {value:'04:00',viewValue:'04:00'},      
      {value:'04:30',viewValue:'04:30'},      
      {value:'05:00',viewValue:'05:00'},      
      {value:'05:30',viewValue:'05:30'},      
      {value:'06:00',viewValue:'06:00'},      
      {value:'06:30',viewValue:'06:30'},     
      {value:'07:00',viewValue:'07:00'},      
      {value:'07:30',viewValue:'07:30'},      
      {value:'08:00',viewValue:'08:00'},      
      {value:'08:30',viewValue:'08:30'},      
      {value:'09:00',viewValue:'09:00'},      
      {value:'09:30',viewValue:'09:30'},      
      {value:'10:00',viewValue:'10:00'},      
      {value:'10:30',viewValue:'10:30'},      
      {value:'11:00',viewValue:'11:00'},      
      {value:'11:30',viewValue:'11:30'},      
      {value:'12:00',viewValue:'12:00'},      
      {value:'12:30',viewValue:'12:30'},      
      {value:'13:00',viewValue:'13:00'},      
      {value:'13:30',viewValue:'13:30'},      
      {value:'14:00',viewValue:'14:00'},      
      {value:'14:30',viewValue:'14:30'},      
      {value:'15:00',viewValue:'15:00'},      
      {value:'15:30',viewValue:'15:30'},      
      {value:'16:00',viewValue:'16:00'},      
      {value:'16:30',viewValue:'16:30'},      
      {value:'17:00',viewValue:'17:00'},      
      {value:'17:30',viewValue:'17:30'},      
      {value:'18:00',viewValue:'18:00'},      
      {value:'18:30',viewValue:'18:30'},      
      {value:'19:00',viewValue:'19:00'},      
      {value:'19:30',viewValue:'19:30'},      
      {value:'20:00',viewValue:'20:00'},      
      {value:'20:30',viewValue:'20:30'},      
      {value:'21:00',viewValue:'21:00'},      
      {value:'21:30',viewValue:'21:30'},      
      {value:'22:00',viewValue:'22:00'},      
      {value:'22:30',viewValue:'22:30'},      
      {value:'23:00',viewValue:'23:00'},      
      {value:'23:30',viewValue:'23:30'},
  ];
  comments: any;
  hallUrl: any = [];
  bookingDetails: any = [];
  showToaster: boolean = false;
  animate: boolean;
  get share_link(): string {
    return this.businessProfile && this.businessProfile.share_link;
  }

  get isSubscribed(): boolean {
    return this.businessProfile && this.businessProfile.is_subscribed;
  }

  sharableSocialMediaPlatforms = ['facebook', 'twitter', 'linkedin', 'google',
  'telegram', 'messenger', 'whatsapp', 'email', 'tumblr'];

  constructor(private api: MyHttpClient, private router: Router, private _elementRef: ElementRef,
    private service: BusinessDetailsService,private modalService: BsModalService,public contextData: ContextDataService,
    private calendar: NgbCalendar, private modal: NgbModal, private data: LoginDataService,
    private activatedRoute: ActivatedRoute, private metaTagService: Meta, private titleService: Title, private loginservice: LoginService) {

  }

  onSubscribe() {
    this.animate = true;
    this.service.subscribeToBusiness(this.business_id, this.city).pipe(
      delay(1000),
      tap(subscribed => this.businessProfile.is_subscribed = subscribed),
      tap(subscribed => this.animate = false),
    ).subscribe();
  }

  ngOnInit() {

    this.business_id = this.activatedRoute.snapshot.paramMap.get("id");
    this.business_name = this.activatedRoute.snapshot.paramMap.get("name");
    this.city = this.activatedRoute.snapshot.paramMap.get("city");

    this.getBusinessProfile();

    this.galleryOptions = [
      {
          width: '600px',
          height: '400px',
          thumbnailsColumns: 4,
          preview: false,
          imageAnimation: NgxGalleryAnimation.Slide
      },
      // max-width 800
      {
          breakpoint: 767,
          width: '100%',
          height: '350px',
          imagePercent: 80,
          thumbnailsPercent: 20,
          thumbnailsMargin: 10,
          thumbnailMargin: 10
      },
      // max-width 400
      {
          breakpoint: 767,
          thumbnails: false
      }
  ];
  
  }


  addPageMeta() {

    this.metaTagService.addTags([
      { name: 'Title', content: this.title },
      { name: 'description', content: this.description },
      { name: 'keywords', content: this.keywords },
      { name: 'robots', content: 'index, follow' },
      { name: 'author', content: 'OneShell' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { charset: 'UTF-8' }
    ]);

    this.titleService.setTitle(this.title);
  }
  @HostListener('window:resize')
  onResize() {
    if(window.screen.width < 576 || window.innerWidth < 576) {
      this.tabNo = 0;
    }
  }
  
  getBusinessProfile() {
    const reqData = new HttpParams()
      .set("business_city", this.city)
      .set("business_id", this.business_id)
      .set("customer_id", this.customer_id);

    this.api.get<any>(BASE_URL + '/v1/web/customerService/getBusinessProfile',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
        response => {

          console.log(response);
          this.businessProfile = response;

          this.title = this.businessProfile.title;
          this.description = this.businessProfile.description;
          this.keywords = this.businessProfile.keywords;

          this.addPageMeta();

          this.getBookedDates();
          if (this.businessProfile.business_image_urls.length > 0) {
            this.imagesList = this.businessProfile.business_image_urls;
            this.imagesList.splice(0, 0, this.businessProfile.primary_image_url);
          } else {
            this.imagesList = this.businessProfile.primary_image_url;
          }

          this.hallUrl = this.businessProfile.business_image_urls && this.businessProfile.business_image_urls.map((item:string)=> {return item;})
          // this.hallUrl.push(this.businessProfile.primary_image_url);
          this.paymentMethods = this.businessProfile.payment_methods;
          this.paymentMethodsCount = this.paymentMethods.length != 0;
          this.menucardCount = this.businessProfile.menucard_images.length != 0;
          this.servicesList = this.businessProfile.services;
          this.servicesListCount = this.servicesList.length != 0;
          this.offersList = this.businessProfile.offers;
          this.menuCards = this.businessProfile.menucard_images;


          this.offersListCount = this.offersList.length != 0;
          this.phone_number = this.businessProfile.phone_number;
          this.alternate_phone_number = this.businessProfile.alternate_phone_number;
          this.hallProfile = this.businessProfile.hall_profile;


          if (this.hallProfile != null) {
          }
          if (this.hallProfile != null && this.hallProfile.food_types.length > 0) {
            for (var i = 0; i < this.hallProfile.food_types.length; i++) {
              if ("nonveg" == this.hallProfile.food_types[i]) {
                this.isNonVegAllowed = true;
              }
            }
          }

          this.total_rating = ((this.businessProfile.total_ratings / this.businessProfile.number_of_feedbacks).toFixed(1));
          if (this.total_rating == 'NaN') {
            this.total_rating = 0;
          }
          this.total_rating_fixed = parseInt(this.total_rating.toString().split('.')[0]);
          this.total_rating_float = parseInt(this.total_rating.toString().split('.')[1]) > 0;
          if (this.total_rating_float)
            this.total_rating_total = 5 - this.total_rating_fixed - 1;
          else
            this.total_rating_total = 5 - this.total_rating_fixed;

          /*
          Business Timing logic
          */
          var currentDate = new Date();
          var dayName = this.days[currentDate.getDay()];

          var dd = String(currentDate.getDate()).padStart(2, '0');
          var mm = String(currentDate.getMonth() + 1).padStart(2, '0');
          var yyyy = currentDate.getFullYear();

          var dateString = yyyy + '-' + mm + '-' + dd + 'T';
          let timing: Map<String, any>;

          timing = this.businessProfile.business_timings;

          for (let [key, value] of Object.entries(timing)) {
            if (key === dayName) {

              let timingObj = value;

              var startDateTimeString = dateString + timingObj.start_time + ':00';
              var endDateTimeString = dateString + timingObj.end_time + ':00';

              let startDateTime = new Date(startDateTimeString);
              let endDateTime = new Date(endDateTimeString);

              if (currentDate.getTime() >= startDateTime.getTime() && currentDate.getTime() <= endDateTime.getTime()) {

                if (timingObj.break_end_time === '- -') {
                  this.businessProfile.is_open = 'Open';
                } else {

                  var breakStartDateTimeString = dateString + timingObj.break_start_time + ':00';
                  var breakEndDateTimeString = dateString + timingObj.break_end_time + ':00';

                  let breakStartDateTime = new Date(breakStartDateTimeString);
                  let breakEndDateTime = new Date(breakEndDateTimeString);

                  if (currentDate.getTime() >= breakEndDateTime.getTime() && currentDate.getTime() <= breakStartDateTime.getTime()) {
                    this.businessProfile.is_open = 'Open';
                  } else {
                    this.businessProfile.is_open = 'Closed';
                  }
                }
              } else {
                this.businessProfile.is_open = 'Closed';
              }
              this.businessProfile.timings = startDateTime.toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true }) + ' - ' + endDateTime.toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
            }
          }

        },
        err => {
          console.log("error" + err);
        }
      );
  }

  getBookedDates() {

    const reqData = {}
    reqData["business_id"] = this.businessProfile.business_id
    reqData["business_city"] = this.businessProfile.business_city
    reqData["city"] = this.businessProfile.business_city
    reqData["year"] = new Date().getFullYear();
    reqData["month"] = new Date().getMonth() + 1;

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    this.api.post<any>(BASE_URL + "/v1/web/customer/banquetHalls/getHallBookedDetailsForMonthYear", reqData, httpOptions).subscribe(
      response => {

        if (response.length > 0) {
          this.disableDates = [];
          this.bookingDetails = response.map((item)=> {return item});
          response.map((res) => {
            let date: any = res.date;
            console.log(date);
            let dateFormat = date.split('/');
            let newFormat = dateFormat[2] + '/' + dateFormat[1] + '/' + dateFormat[0];
            let newValueFormat = dateFormat[1] + '/' + dateFormat[0] + '/' + dateFormat[2];

            let newDate = new Date();
            let todayDate = newDate.getDate();
            let todayMonth = newDate.getMonth() + 1;
            let todayYear = newDate.getFullYear();

            let fullDate = todayMonth + '/' + todayDate + '/' + todayYear;
            let fullMaxDate = todayMonth + '/' + todayDate + '/' + todayYear + 1;

            console.log(fullDate);
            this.minDate = new Date(fullDate);
            this.maxDate = new Date(fullMaxDate);
            this.value = new Date(fullDate);


            // new Date("10/19/2019");
            this.disableDates.push(newFormat);
          })
          console.log(this.disableDates);
          this.disabledDate(event);
        }
        else {
          this.bookingDetails = []
        }

      },
      err => {
        console.log("error" + err);
      }
    );
    

  }

  onNavigate(event) {
    console.log(event);
    const reqData = {}
    reqData["business_id"] = this.businessProfile.business_id
    reqData["business_city"] = this.businessProfile.business_city
    reqData["city"] = this.businessProfile.business_city
    reqData["year"] = new Date(event.date).getFullYear();
    reqData["month"] = new Date(event.date).getMonth() + 1;

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    this.api.post<any>(BASE_URL + "/v1/web/customer/banquetHalls/getHallBookedDetailsForMonthYear", reqData, httpOptions).subscribe(
      response => {

        if (response.length > 0) {
          this.disableDates = [];
          this.bookingDetails = response.map((item)=> {return item});
          response.map((res) => {
            let date: any = res.date;
            console.log(date);
            let dateFormat = date.split('/');
            let newFormat = dateFormat[2] + '/' + dateFormat[1] + '/' + dateFormat[0];
            // let newValueFormat = dateFormat[1] + '/' + dateFormat[0] + '/' + dateFormat[2];

            let newDate = new Date(event.date);
            let todayDate = newDate.getDate();
            let todayMonth = newDate.getMonth() + 1;
            let todayYear = newDate.getFullYear();

            let fullDate = todayMonth + '/' + todayDate + '/' + todayYear;
            // let fullMaxDate = todayMonth + '/' + todayDate + '/' + todayYear + 1;

            // console.log(fullDate);
            // this.minDate = new Date(fullDate);
            // this.maxDate = new Date(fullMaxDate);
            this.value = new Date(fullDate);
            if (event.event.target.classList.contains('e-next')) {
              //incrementing the month while clicking the next icon
              date = new Date(event.date.setMonth(event.date.getMonth() + 1));
            }
            if (event.event.target.classList.contains('e-prev')) {
              //decrementing the month while clicking the previous icon
              date = new Date(event.date.setMonth(event.date.getMonth() - 1));
            }
            if (event.view == 'Month') {
              this.ejCalendar.navigateTo(event.view, new Date('' + date));
            }

            this.disabledDate(event);
            // new Date("10/19/2019");
            this.disableDates.push(newFormat);
          })
          console.log(this.disableDates);
          this.disabledDate(event);
        }
        else {
          this.bookingDetails = [];
        }

      },
      err => {
        console.log("error" + err);
      }
    );

  }

  disabledDate(args): void {
    let span: HTMLElement;
    span = document.createElement('span');
    //Use "e-icons" class name to load Essential JS 2 built-in icons.
    span.setAttribute('class', 'e-icons highlight-day');

    for (let i = 0; i < this.disableDates.length; i++) {
      if (+ new Date(args.date) == + new Date(this.disableDates[i])) {
        if (args.element == undefined) {
          if( args.target) {
            args.target.appendChild(span);
            args.target.setAttribute('title', 'Already Booked!');
            args.target.className = 'special';
            args.isDisabled = true;
          }
        }
        else {
          args.element.appendChild(span);
          args.element.setAttribute('title', 'Already Booked!');
          args.element.className = 'special';
          // console.log(args.date);
          args.isDisabled = true;
        }

      }
    }

  }

  datePicker() {
    console.log('test')
    this.model = this.calendar.getToday();
  }

  openCallListModal() {
    this.showCallListModal = true;
    this.handleBusinessEnquiry();
  }

  closeCallListModal() {
    this.showCallListModal = false;
  }

  handleBusinessEnquiry() {

    if (localStorage.getItem("username")) {
      let reqBody = {
        phone_number: localStorage.getItem("username"),
        business_id: this.business_id,
        business_city: this.city
      };

      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      };

      this.api.post<any>(BASE_URL + "/v1/web/customerService/enquiry/trackBusinessEnquiry", reqBody, httpOptions)
        .subscribe(
          response => {
            console.log("updation success: " + response.success);
          },
          err => {
            console.log("error" + err);
          }
        );
    }

  }

  closeImagesFormModal() {
    this.showImagesPopUp = false;
  }

  onHallProfileImagesClicked() {
    this.showImageUrls = [];
    if (this.businessProfile.business_image_urls.length > 0) {
      this.showImageUrls = this.businessProfile.business_image_urls;
      this.showImageUrls.splice(0, 0, this.businessProfile.primary_image_url);
    } else {
      this.showImageUrls.splice(0, 0, this.businessProfile.primary_image_url);
    }
    this.showImagesPopUp = true;
  }

  onHallProfileACImagesClicked() {
    this.showImageUrls = [];
    if (this.hallProfile.ac_room_images.length > 0) {
      this.showImageUrls = this.hallProfile.ac_room_images;
      this.showImagesPopUp = true;
    }
  }

  onHallProfileNonACImagesClicked() {
    this.showImageUrls = [];
    if (this.hallProfile.non_ac_room_images.length > 0) {
      this.showImageUrls = this.hallProfile.non_ac_room_images;
      this.showImagesPopUp = true;
    }
  }

  selectedDate(data) {
    console.log(data.value)
  }

  getDisabledDates(year: any, month: any) {
    let reqBody = {
      city: localStorage.getItem("activeCity"),
      business_id: this.business_id,
      business_city: this.city,
      year: 2019,
      month: 10
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    this.api.post<any>(BASE_URL + "/v1/web/customer/banquetHalls/getHallBookedDatesForMonthYear", reqBody, httpOptions).subscribe(
      response => {
        this.disableDates = response;
      },
      err => {
        console.log("error" + err);
      }
    );
  }

  showRatingModel(template: TemplateRef<any>) {
    if (this.contextData.isUserLoggedIn) {
      this.modalRef = this.modalService.show(template);
    } else {
      this.loginservice.openLoginModal();
    }
  }
  showSpamModel() {
    this.service.openSpamModel();
  }

  hideRatingModel() {
    this.modalRef.hide();
  }

  submitRating() {
    this.hideRatingModel();
    let reqBody = {
      business_id: this.business_id,
      business_city: this.city,
      customer_id: this.contextData.customerProfile.customer_id,
      city: this.contextData.customerProfile.customer_city,
      rating: this.currentRate
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    this.api.post<any>(BASE_URL + "/v1/web/customerService/rateBusiness", reqBody, httpOptions).subscribe(
      response => {
      },
      err => {
        console.log("error" + err);
      }
    );
  }
  closeGallery() {
    this.openGallery = false;
  }
  openImgGallery(type:string){
    if(type === 'hall') {
      this.galleryImages = this.hallUrl.map((item:any)=> {
        return {
          small: item,
          medium: item,
          big: item,
        }
    });
    this.imgGalleryTitle = "Hall";
    }
   else if(type === 'ac') {
      this.galleryImages = this.hallProfile.ac_room_images.map((item:any)=> {
        return {
          small: item,
          medium: item,
          big: item,
        }
    });
    this.imgGalleryTitle = "AC Rooms";
    }
    else if(type === 'nonac') {
      this.galleryImages = this.hallProfile.non_ac_room_images.map((item:any)=> {
        return {
          small: item,
          medium: item,
          big: item,
        }
    });
    this.imgGalleryTitle = "Non AC Rooms";
    }
    this.openGallery = true;
  }
  update() {
    this.hallProfile.ac_room_images.push(this.hallProfile.ac_room_images[0]);
    this.hallProfile.ac_room_images.push(this.hallProfile.ac_room_images[0]);
    this.hallProfile.ac_room_images.push(this.hallProfile.ac_room_images[0]);

  }

  sendEnquiry() {
    if(this.contextData.isUserLoggedIn) {
      let fromDate: string = this.from.getDate().toString() + '/' + (this.from.getMonth() + 1).toString()  + '/' + this.from.getFullYear().toString();
      let toDate: string = this.to.getDate().toString()  + '/' + (this.to.getMonth() + 1).toString()  + '/' + this.to.getFullYear().toString();
      
      let reqBody = {
        business_id: this.business_id,
        business_city: this.city,
        customer_id: this.contextData.customerProfile.customer_id,
        customer_city: this.contextData.customerProfile.customer_city,
        customer_name: this.contextData.customerProfile.name,
        customer_phone_number: this.contextData.customerProfile.phone_number,
        comments: this.comments,
        start_date: fromDate,
        end_date: toDate,
        start_time: this.fromTime,
        end_time: this.toTime,
        partner_profile_id: this.businessProfile.partner_profile_id,
        partner_city: this.businessProfile.partner_city,
        business_name: this.business_name,
      };
  
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      }; 
      
      this.api.post<any>(BASE_URL + "/v1/web/customer/banquetHalls/postHallEnquiryRequest", reqBody, httpOptions).subscribe(
        response => {
          console.log("sucess");
          if(response.success) {
            this.comments = "";
            this.fromTime = undefined;
            this.toTime = undefined;
            this.from = undefined;
            this.to = undefined;
            this.showMessage();
          }
        },
        err => {
          console.log("error" + err);
        }
      );

    }
    else {
      this.loginservice.openLoginModal();
    }
  }
  showMessage(): void {
    this.showToaster = true;
    setTimeout(() => {
      this.showToaster = false;
    }, 5000);
  }
}
