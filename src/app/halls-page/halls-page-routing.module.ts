import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HallDetailsComponent} from '@app/halls-page/hall-details/hall-details.component';

const routes: Routes = [
  {
    path: ':city/:id/:name',
    component: HallDetailsComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HallsPageRoutingModule { }
