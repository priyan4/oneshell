import { Component, OnInit, OnDestroy, TemplateRef, NgZone } from '@angular/core';
import { CitySelectionPopupService } from "@app/core/city-selection-popup/city-selection-popup.service";
import { DeviceDetectorService } from 'ngx-device-detector';
import { CitiesService, City, CityType } from '@app/services/cities.service';
import { Subscription, Observable } from 'rxjs';
import { Title, Meta } from '@angular/platform-browser';
import { environment as env } from '@env/environment';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../services/http.helper';
import { tap, debounceTime, distinctUntilChanged, filter, switchMap } from 'rxjs/operators';
import { UrlNavigationService } from '@app/services/url-navigation.service';
import { NgbTypeaheadSelectItemEvent, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TitleCasePipe } from '@angular/common';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { } from 'google-maps';
declare var google: any;
const BASE_URL = env.serverUrl;

@Component({
  selector: 'app-static-page',
  templateUrl: './static-page.component.html',
  styleUrls: ['./static-page.component.scss']
})
export class StaticPageComponent implements OnInit, OnDestroy {
  deviceInfo: any;
  browser: string;
  browser_version: any;
  os: string;
  os_version: any;
  device: string;
  messages: any[] = [];
  subscription: Subscription;
  text_data: any;
  page_title: string;

  public latitude: number;
  public longitude: number;
  public currentaddress: any;
  public formatedaddress: any;
  title = 'OneShell - The Local Connect App, Local Business Search, Local Product Search, Local Professionals Search, City Offers, Health, Movies, Events, Activities, Social, Real Estate, Function Halls, Home Service, Shopping, Home Delivery';
  description = 'OneShell - The Local Connect App, India\'s Real Time Local Search, provides Best City Offers, Happening Local Events, Home Service Booking, Home Delivery from Local stores, Movies, Activities, restaurants, Real Estate and Function Halls. You can also View Products From Local Stores, Offers from Local Businesses, Book Home Service from your Service Providers, View Health Tips From Doctors, Restaurant Menu and Todays Special, Real Estate Site Plans along with Booking Status, Real Estate Floor Plans, Function Halls Availability';
  keywords = 'OneShell, local search, local product search, local store search, local service search, local offer search, local business search, food, grocery, medicines, shopping, movies, hotels, services, health, professionals, real estate, function halls availability, function halls availability, health tips, health deals, events, activities, hotels, automobiles, appointments, coupons, search plus services, local businesses, online yellow pages, Local Search directory, city yellow pages';

  searchTxt:string;
  showAllCities: boolean;
  popularCities: City[] = [];
  otherCities: City[] = [];
  citySearchText: string;
  areaSearchText: string;
  private allCities: City[] = [];
  private readonly numberOfOtherCitiesShownByDefault = 4;

  get otherCitiesFilteredBasedOnShowAllCitiesFlag() {
    return (this.showAllCities || !this.isOtherCitiesSizeHigherThanDefault) ? this.otherCities
      : this.otherCities.slice(0, this.numberOfOtherCitiesShownByDefault);
  }

  get isOtherCitiesSizeHigherThanDefault() {
    return this.otherCities.length > this.numberOfOtherCitiesShownByDefault;
  }

  get activeCityObject(): City {
    return this.allCities.find(c => c.city_name === this.citiesService.activeCity);
  }
  constructor(private api: MyHttpClient,
    private citySelectionPopup: CitySelectionPopupService,
    private deviceService: DeviceDetectorService,
    public citiesService: CitiesService,
    private metaTagService: Meta,
    private urlNavigation: UrlNavigationService,
    public titleCasePipe: TitleCasePipe,
    private titleService: Title,
    private modalServicengb: NgbModal,
    private zone: NgZone,
    private snackBar:MatSnackBar) {


    this.subscription = this.citiesService.getMessage().subscribe(message => {
      if (message) {
        this.postuserdata();
      } else {
        console.log("error in Subject");
      }
    });

  }

  ngOnInit() {
    this.populateCities();
    this.addPageMeta();
    this.epicFunction();
    this.getdata();
    this.page_title = 'THE LOCAL CONNECT APP';
  }

  onCitySelection(city: City): void {
    this.setLocationDetails(city.city_name);
    this.sendMessage();
  }

  private setLocationDetails(cityName: string, areaName?: string) {
    this.citiesService.setActiveCity(cityName);
    this.citiesService.setActiveArea(areaName);
    this.closeDialogAndNavigateToHome(cityName);
  }

  sendMessage(): void {
    // send message to subscribers via observable subject
    this.citiesService.sendMessage('City is Selected');
}
private closeDialogAndNavigateToHome(cityName: string) {
  this.urlNavigation.homePage(cityName);
}

  private populateCities() {
    this.citiesService.getCities().pipe(
      tap(cities => this.allCities = cities),
      tap(() => this.popularCities = this.allCities.filter(c => c.type === CityType.popular)),
      tap(() => this.otherCities = this.allCities.filter(c => c.type === CityType.other))
    ).subscribe();
  }

  searchcity(term){
   
    this.citiesService.searchCities(term).subscribe((res:any)=>{
      console.log(res);
      if(res.length>0){
        this.onCitySelection(res[0])
      }
      else{
       this.snackBarfun();
      }
    })
  }

  snackBarfun(){
    const config = new MatSnackBarConfig();
    config.duration = 2000;
    config.verticalPosition = 'top';
    config.horizontalPosition = 'center';
    this.zone.run(() => {
      this.snackBar.open('Sorry, We are yet to be there, Please choose from the Available cities', '', config);
    });
   
  }
  searchCity = (text$: Observable<string>) => text$.pipe(
    debounceTime(300),
    distinctUntilChanged(),
    filter(text => !!text),
    switchMap(term => this.citiesService.searchCities(term))
)


public setCurrentPosition() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition((position) => {
      this.latitude = position.coords.latitude;
      this.longitude = position.coords.longitude;
      this.autocompleteonload(this.latitude, this.longitude);
    },()=> {
      // const modalRef = this.modalServicengb.open(template);
    },{
      enableHighAccuracy: true,
      timeout: 1000,
      maximumAge: 0
    });
  }
  else {
    alert("Your Browser doesnt support Location capture")
  }
}

ngAfterViewInit(): void {
  const loadMap = document.createElement('script');
  loadMap.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBhU34uK5dLXDUM1PDVWBWJ6A1CBYCBcKw';
  loadMap.type = 'text/javascript';
  document.body.appendChild(loadMap);
  document.body.removeChild(loadMap);
}

public autocompleteonload(lat, lng) {
  let geocoder = new google.maps.Geocoder();
  let latlng = new google.maps.LatLng(lat, lng);
  let request = {
    location: latlng
  }

  geocoder.geocode(request, (results, status) => {
    if (status == 'OK') {
      this.currentaddress = results[0].address_components;
      this.formatedaddress = results[0].formatted_address;
      console.log(this.formatedaddress)
        this.searchcity(this.formatedaddress[0].long_name);
    }
    else {
      alert('Geocode was not successful for the following reason: ' + status);
    }
  });
 
}







  addPageMeta() {

    this.metaTagService.addTags([
      { name: 'Title', content: this.title },
      { name: 'description', content: this.description },
      { name: 'keywords', content: this.keywords },
      { name: 'robots', content: 'index, follow' },
      { name: 'author', content: 'OneShell' },
      { charset: 'UTF-8' }
    ]);
    this.titleService.setTitle(this.title);
  }


  postuserdata() {
    let data: any;

    data = {
      default_city: this.citiesService.activeCity,
      user_data: {
        agent: this.deviceInfo,
        browser: this.deviceInfo.browser,
        browser_version: this.deviceInfo.browser_version,
        os: this.deviceInfo.os,
        os_version: this.deviceInfo.os_version,
        device: this.device
      }
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    this.api.post<any>(BASE_URL + "/v1/web/customer/saveAnonymousUser", data, httpOptions).subscribe(
      response => {

        console.log(response);

      }, (err) => {
        console.log('error ' + err);
        //show no data found message
      })
  }

  
  citySearchResultFormatter = (city: City): string => this.titleCaseFormatter(city && city.city_name);

  titleCaseFormatter = (name: string): string => this.titleCasePipe.transform(name);

  onCitySelectionFromSearchResults = (searchResult: NgbTypeaheadSelectItemEvent): void => searchResult && searchResult.item && this.onCitySelection(searchResult.item);

  onAreaSelectionFromSearchResults = (searchResult: NgbTypeaheadSelectItemEvent): void => searchResult && searchResult.item && this.onAreaSelection(searchResult.item);

  getUrlToDisplay = (city: City): string => this.citiesService.activeCity === city.city_name ? city.selected_image_url : city.image_url;

  showAreaSelection = (): boolean => (this.citiesService.activeCity && (this.activeCityObject && this.activeCityObject.is_locations_enabled));

  private onAreaSelection(areaName: string) {
    this.setLocationDetails(this.citiesService.activeCity, areaName);
  }


  epicFunction() {
    this.deviceInfo = this.deviceService.getDeviceInfo();
    this.browser = this.deviceInfo.browser;
    this.browser_version = this.deviceInfo.browser_version;
    this.os = this.deviceInfo.os;
    this.os_version = this.deviceInfo.os_version;
    const isMobile = this.deviceService.isMobile();
    const isTablet = this.deviceService.isTablet();
    const isDesktopDevice = this.deviceService.isDesktop();
    this.device = isDesktopDevice ? 'Desktop' : isTablet ? 'Tablet' : isMobile ? 'Mobile' : 'unknown';

  }

  getdata() {
    this.text_data = [
      {
        imageurl: 'https://firebasestorage.googleapis.com/v0/b/oneshell-d3a18.appspot.com/o/web_assets%2Fstatic_home_page%2FShopping_1.png?alt=media&token=6fe8a816-e4ed-436c-9c10-eea9b8251bab',
        title: 'Shopping',
        description: 'Shop for Fashion, Grocery, Electronics, Food & More from your Local Stores Online'
      },
      {
        imageurl: 'https://firebasestorage.googleapis.com/v0/b/oneshell-d3a18.appspot.com/o/web_assets%2Fstatic_home_page%2FHome%20Service_2.png?alt=media&token=8bdc3f15-d89b-4f6f-ae39-cfbe3b7787f5',
        title: 'HOME SERVICE',
        description: 'Get Electricians, Plumbers, Carpenters, Doctors and Other Service professionals on a Tap!'
      },
      {
        imageurl: 'https://firebasestorage.googleapis.com/v0/b/oneshell-d3a18.appspot.com/o/web_assets%2Fstatic_home_page%2FEntertainment_3.png?alt=media&token=1a125392-da64-45f8-8661-7b5762cd1a23',
        title: 'ENTERTAINMENT',
        description: 'Keep yourself updated with the Local Happenings like Movies, Events, Activities & More on a tap!'
      },
      {
        imageurl: 'https://firebasestorage.googleapis.com/v0/b/oneshell-d3a18.appspot.com/o/web_assets%2Fstatic_home_page%2FReal%20Estate_4.png?alt=media&token=3e26c80b-0ffb-4211-9199-e1ab47161b9f',
        title: 'REAL ESTATE & BANQUET',
        description: 'Browse for Real Estate, Availability of Banquet Hall, Resorts & more'
      }
    ]
  }

  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.subscription.unsubscribe();
  }

}