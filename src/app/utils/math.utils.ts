export const maxOfArray = (numArray) => numArray && numArray.length ? Math.max(...numArray) : undefined ;
