import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ViewCartRoutingModule} from './view-cart-routing.module';
import {ViewCartComponent} from './view-cart.component';
import {StoreCartComponent} from './store-cart/store-cart.component';
import {MatCardModule} from '@angular/material/card';
import {MatDividerModule} from '@angular/material/divider';
import {SharedModule} from '@app/shared/shared-module.module';
import {MatButtonModule} from '@angular/material/button';
import {CoreModule} from '@app/core/core.module';
import { PlaceOrderComponent } from './place-order/place-order.component';
import {SelfOrderComponent} from './self-order/self-order.component';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import {MatDatepickerModule, MatRadioModule, MatNativeDateModule} from '@angular/material';
import {FormsModule} from '@angular/forms';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import {MatTabsModule} from '@angular/material/tabs';
import { NgxImageCompressService } from 'ngx-image-compress';

@NgModule({
  declarations: [ViewCartComponent, StoreCartComponent, PlaceOrderComponent, SelfOrderComponent],
  imports: [
    CommonModule,
    ViewCartRoutingModule,
    MatCardModule,
    MatDividerModule,
    MatButtonModule,
    SharedModule,
    CoreModule,
    MatTooltipModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    FormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    NgxMaterialTimepickerModule,
    MatTabsModule
  ],
  providers: [
    NgxImageCompressService
  ]
})
export class ViewCartModule {
}
