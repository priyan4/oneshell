import { Component, ElementRef, OnInit, TemplateRef, ViewChild, Inject, forwardRef, PLATFORM_ID } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CartService } from '@app/services/cart.service';
import { tap } from 'rxjs/operators';
import { BusinessDetailsService } from '@app/business-page/services/business-details.service';
import { ContextDataService } from '@app/services/context-data.service';
import { environment as env } from '@env/environment';
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { DeviceDetectorService } from "ngx-device-detector";
import { NgxMaterialTimepickerTheme } from "ngx-material-timepicker/src/app/material-timepicker/models/ngx-material-timepicker-theme.interface";
import { FileUploadService } from '../../shared/file-upload-service';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../../services/http.helper';
import { NgxImageCompressService } from 'ngx-image-compress';
import { isPlatformBrowser } from '@angular/common';
const BASE_URL = env.serverUrl;

@Component({
  selector: 'app-place-order',
  templateUrl: './place-order.component.html',
  styleUrls: ['./place-order.component.scss']
})
export class PlaceOrderComponent implements OnInit {

  private businessId: string;
  private businessCity: string;
  businessProfile: any;
  private destroyed: boolean;
  baseUrl = env.webUrl;
  time = { hour: 13, minute: 30 };

  checkoutResponse: any;
  availOffers: any = [];
  filterMobileToggle: boolean = false;
  deliveryAddress: {
    name: string;
    phoneNumber: number,
    email?: string,
    address: { line1: string, line2?: string }
  };
  deliveryDate: Date;
  deliveryTime: string;
  paymentOptions = {
    upi: 'Pay by UPI on Delivery',
    cod: 'Cash On Delivery'
  };
  paymentMethod = this.paymentOptions.upi;
  previewUrl: any;
  fileName: any;
  localUrl: any;

  @ViewChild('uploadImage', { static: false }) uploadImage: any;
  isPrimaryImageUpdated: boolean = false;
  isSecondaryImageUpdated: boolean = false;
  trackLastUpdatedFile: any = '';
  downloadFullPath: string = '';
  localCompressedURl: any;
  secondaryImageArray: any[] = [];
  secondaryImageUrls: string[] = [];
  prescription_image_url: any[] = [];
  isPrescriptionRequired: boolean = false;
  isPrescriptionAllow: boolean ;

  get businessName() {
    return this.businessProfile && this.businessProfile.business_name;
  }

  get businessCartProducts(): any[] {
    this.availOffers = [];
    if (this.checkoutResponse && this.checkoutResponse.free_delivery_offer_details_list && this.checkoutResponse.free_delivery_offer_details_list.length){
    Array.prototype.push.apply(this.availOffers, this.checkoutResponse.free_delivery_offer_details_list);
    }
    if (this.checkoutResponse && this.checkoutResponse.bill_offer_details_list && this.checkoutResponse.bill_offer_details_list.length){
      Array.prototype.push.apply(this.availOffers, this.checkoutResponse.bill_offer_details_list);
    }
    return this.checkoutResponse && this.checkoutResponse.business_products_cart_response;

  }

  @ViewChild('placeOrderStatus', { static: false }) placeOrderStatusTpl: ElementRef;
  placeOrderResponse: any;
  private placeOrderStatusModalRef: BsModalRef;

  timePickerTheme: NgxMaterialTimepickerTheme = {
    container: {
      buttonColor: '#dc3545'
    },
    dial: {
      dialActiveColor: '#dc3545'
    },
    clockFace: {
      clockFaceTimeActiveColor: 'dc3545',
      clockHandColor: '#dc3545'
    }
  };

  constructor(private activatedRoute: ActivatedRoute,
    private cartService: CartService,
    private businessDetailsService: BusinessDetailsService,
    private contextData: ContextDataService,
    private modalService: BsModalService,
    public deviceService: DeviceDetectorService,
    private sharedApi: FileUploadService,
    private api: MyHttpClient,
    private imageCompress: NgxImageCompressService,
    @Inject(PLATFORM_ID) private platformId: any
    // @Inject(forwardRef(() => NgxImageCompressService)) public imageCompress: NgxImageCompressService
  ) { }

  ngOnInit() {
    this.businessId = this.activatedRoute.snapshot.paramMap.get('business_id');
    this.businessCity = this.activatedRoute.snapshot.paramMap.get('business_city');
    this.populateBusinessProfile();
    this.populateDefaultDeliveryAddress();
    this.populateCheckOutResponse();
    this.getPrescription();
  }

  onPlaceOrder() {
    if (this.isPrescriptionRequired){
    if (this.secondaryImageArray.length) {
      this.sharedApi.uploadImage(this.secondaryImageArray).subscribe(
        (data) => {
          console.log(data);
          this.onSaveApi(data.url);
        }, error => {
          console.log("error in uploading imges to firebase");
        });
    }
  }
  else {
    this.placeOrderAfterUploadCheck();
  }

  }

  private populateCheckOutResponse() {
    this.cartService.getCheckoutResponse(this.businessId, this.businessCity).pipe(
      tap(res => this.checkoutResponse = res)
    ).subscribe();
  }

  private populateDefaultDeliveryAddress() {
    this.deliveryAddress = {
      name: this.contextData.customerProfile.name,
      phoneNumber: this.contextData.customerProfile.phone_number,
      email: this.contextData.customerProfile.email_id,
      address: { line1: '', line2: '' }
    };
  }

  private populateBusinessProfile() {
    const params = new HttpParams()
      .set('business_city', this.businessCity)
      .set('business_id', this.businessId)
      .set('customer_id', this.contextData.customerProfile.customer_id);
    this.businessDetailsService.getBusinessProfile(params).subscribe(response => {
      this.businessProfile = response;
    });
  }

  private showPlaceOrderStatusModal() {
    if (this.placeOrderResponse.success) {
      window.location.href = this.baseUrl + '/order/delivery/details/' + this.placeOrderResponse.order_id + '/' + this.businessId + '/' + this.businessCity;
      return;
    }
    this.placeOrderStatusModalRef = this.modalService.show(this.placeOrderStatusTpl);
  }

  onOk() {
    this.placeOrderStatusModalRef.hide();
    this.placeOrderResponse = undefined;
  }

  uploadAddItemImage(event) {
    const file = event.target.files[0];
    this.trackLastUpdatedFile = file;
    this.fileName = file['name'];
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.localUrl = event.target.result;
        this.trackLastUpdatedFile = this.sharedApi.compressFile(this.localUrl, this.fileName, this.uploadImage, this.trackLastUpdatedFile);
      }
      reader.readAsDataURL(event.target.files[0]);
    }
  }
  deletePreview() {
    this.uploadImage.nativeElement.src = "";
  }

  uploadAddItemImageMultiple(event) {
    let file: any;
    file = event.target.files;
    this.isSecondaryImageUpdated = true;
    if (event.target.files) {
      Object.values(file).forEach(fileObj => {
        this.toCompressPrimarySecondary(event, fileObj['name'], fileObj);
      });
    }
  }

  private toCompressPrimarySecondary(event, fileName, fileObj?: any) {
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.localUrl = event.target.result;
      this.compressFile(this.localUrl, fileName);
    }
    reader.readAsDataURL(fileObj);

  }

  private compressFile(image, fileName) {
    const file = image;
    this.trackLastUpdatedFile = file;
    var orientation = -1;
    this.imageCompress.compressFile(image, orientation, 50, 50).then(data => {
      this.localCompressedURl = data;
      this.secondaryImageUrls.push(this.localCompressedURl);
      let arr = data.split(','),
        mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]),
        n = bstr.length,
        u8arr = new Uint8Array(n);

      while (n--) {
        u8arr[n] = bstr.charCodeAt(n);
      }
      const imageFile = new File([u8arr], fileName, { type: 'image/jpeg' });
      this.secondaryImageArray.push(imageFile);
      this.PrescriptionAllow();
    });
  }

  deleteSecondaryImage(index: number) {
    this.secondaryImageUrls.splice(index, 1);
    this.secondaryImageArray.splice(index, 1);
  }

  getPrescription() {

    const data = {
      business_id: this.businessId,
      business_city: this.businessCity
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    this.api.post<any>(BASE_URL + "/v1/web/customer/cart/isPrescriptionRequired", data, httpOptions).subscribe(
      response => {
        console.log(response);
        this.isPrescriptionRequired = response ? true : false;
      }
    );

  }
  onSaveApi(value) {
    this.prescription_image_url.push(value);
    console.log(this.prescription_image_url);
    if (this.prescription_image_url.length == this.secondaryImageArray.length){
      this.placeOrderAfterUploadCheck();
    }
  }
  PrescriptionAllow(){
    if(this.isPrescriptionRequired){
      this.isPrescriptionAllow = this.secondaryImageArray.length ? true:false;
    }
    else {
      this.isPrescriptionAllow = true;
    }
  }

  placeOrderAfterUploadCheck() {
    this.cartService.placeOrder(this.businessId, this.businessCity, this.businessProfile.phone_number,
      this.checkoutResponse, 'Default', this.deliveryAddress,
      this.paymentMethod, this.deliveryDate, this.deliveryTime, this.prescription_image_url).pipe(
        tap(res => this.placeOrderResponse = res),
        tap(res => this.showPlaceOrderStatusModal())
      ).subscribe();
  }
}
