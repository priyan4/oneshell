import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewCartComponent } from './view-cart.component';
import {StoreCartComponent} from './store-cart/store-cart.component';
import {PlaceOrderComponent} from './place-order/place-order.component';
import {SelfOrderComponent} from './self-order/self-order.component';
import {UserLoggedInGuard} from '@app/core/guard/user-logged-in-guard.service';

const routes: Routes = [
  {
    path: '',
    component: ViewCartComponent,
    canActivate: [UserLoggedInGuard]
  },
  {
    path: ':business_city/:business_id',
    component: StoreCartComponent,
    canActivate: [UserLoggedInGuard]
  },
  {
    path: ':business_city/:business_id/placeOrder',
    component: PlaceOrderComponent,
  },
  // temporary routing will be replaced
  {
    path: ':business_city/:business_id/:business_name/:table_no/selfOrder',
    component:SelfOrderComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewCartRoutingModule { }
