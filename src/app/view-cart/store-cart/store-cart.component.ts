import { Component, ElementRef, OnDestroy, OnInit, TemplateRef, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CartService } from '@app/services/cart.service';
import { filter, takeWhile, tap } from 'rxjs/operators';
import { BusinessDetailsService } from '@app/business-page/services/business-details.service';
import { ContextDataService } from '@app/services/context-data.service';
import { Subject } from 'rxjs';
import { environment as env } from '@env/environment';
import { forkJoin } from 'rxjs';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { UrlNavigationService } from "@app/services/url-navigation.service";
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../../services/http.helper';

const BASE_URL = env.serverUrl;

@Component({
  selector: 'app-store-cart',
  templateUrl: './store-cart.component.html',
  styleUrls: ['./store-cart.component.scss']
})
export class StoreCartComponent implements OnInit, OnDestroy {

  businessId: string;
  businessCity: string;
  businessCartProducts: any[];
  private businessProfile: any;
  subTotal: number;
  private cartChanged$: Subject<any> = new Subject<any>();
  private destroyed: boolean;
  baseUrl = env.webUrl;

  outOfStockItems: any[];
  disableCheckOut: boolean;
  businessCartCount: number;
  deliveryProfile: any;
  isOffline: boolean = false;
  showToaster: boolean = false;
  mobileView: boolean;

  get businessName() {
    return this.businessProfile && this.businessProfile.business_name;
  }

  get validBusinessCartProducts() {
    return this.businessCartProducts ? this.businessCartProducts.filter(bc => !!bc.quantity) : [];
  }
  @HostListener('window:resize') onResize() {
    if(window.screen.width < 768 || window.innerWidth < 768) {
      this.mobileView = true;
    }
    else {
      this.mobileView = false;
    
    }
  }
  @ViewChild('outOfStockPopupTpl', { static: false }) outOfStockPopupTpl: ElementRef;
  bsModalRef: BsModalRef;

  constructor(private activatedRoute: ActivatedRoute,
    private cartService: CartService,
    private businessDetailsService: BusinessDetailsService,
    private contextData: ContextDataService,
    private modalService: BsModalService,
    private urlNavigation: UrlNavigationService,
    private api: MyHttpClient) { }

  ngOnInit() {
    this.businessId = this.activatedRoute.snapshot.paramMap.get('business_id');
    this.businessCity = this.activatedRoute.snapshot.paramMap.get('business_city');
    this.populateBusinessCartProductsAndBusinessProfile();

    this.getBusinessDeliveryProfile();
    this.populateSubTotal();
    this.populateBusinessCartCount()
    this.listenToCartChange();
    if(window.screen.width < 768 || window.innerWidth < 768) {
      this.mobileView = true;
    }
    else {
      this.mobileView = false;
    }
  }

  ngOnDestroy(): void {
    this.destroyed = true;
  }

  continueShopping(){
  var data=JSON.parse(localStorage.getItem("business_list_detail"));
      window.location.href =   this.baseUrl + '/business/l/' + data.city + '/' + data.category + '/'+ data.level + '/' + data.displayname + '/' + 'SERVICE' + '/' + '1';

  }

  checkout() {
    this.disableCheckOut = true;
    if(this.subTotal > 0 && this.deliveryProfile && (this.subTotal >= this.deliveryProfile.minimum_order)) {
      this.cartService.verifyCheckout(this.businessId, this.businessCity).pipe(
        tap(res => {
          if (res && res.length) {
            this.disableCheckOut = true;
            this.showOutOfStockModal(res);
          } else {
            this.urlNavigation.placeOrderPage(this.businessCity, this.businessId);
          }
        })
      ).subscribe();
    }
    
    else {
     this.showMessage();
    }
  }
  showMessage(): void {
    this.showToaster = true;
    setTimeout(() => {
      this.showToaster = false;
    }, 5000);
  }
  onDelete(product) {
    this.cartService.deleteCartProduct(product, this.businessId).pipe(
      tap(() => this.removeDeletedProduct(product)),
      tap(() => this.cartChanged$.next())
    ).subscribe();
  }

  private removeDeletedProduct(product) {
    this.businessCartProducts = product.variable_product_id !== 'vp1'
      ? this.businessCartProducts.filter(cp => !(cp.product_id === product.product_id && cp.variable_product_id === product.variable_product_id))
      : this.businessCartProducts.filter(cp => (cp.product_id !== product.product_id));
  }

  private populateBusinessCartProductsAndBusinessProfile() {
    const params = new HttpParams()
      .set('business_city', this.businessCity)
      .set('business_id', this.businessId)
      .set('customer_id', this.contextData.customerProfile.customer_id);
    forkJoin(this.businessDetailsService.getBusinessProfile(params),
      this.cartService.getBusinessCartProducts(this.businessId, this.businessCity)).pipe(
        tap(arr => this.businessProfile = arr[0]),
        tap(arr => {
          this.businessCartProducts = arr[1];
          this.businessCartProducts.forEach(product => {
           
            product.business_id = this.businessProfile.business_id;
            product.business_city = this.businessProfile.business_city;
            product.business_name = this.businessProfile.business_name;

            var propertyVal = '';
            if (product.chosen_properties != null && product.chosen_properties.length > 0) {
              for (var i = 0; i < product.chosen_properties.length; i++) {
                if (i == 0) {
                  propertyVal += product.chosen_properties[i].value;
                } else {
                  propertyVal += ' | ' + product.chosen_properties[i].value;
                }
              }
            }
            if (product.additional_property_list != null && product.additional_property_list.length > 0) {
              for (var i = 0; i < product.additional_property_list.length; i++) {
                if (propertyVal != '') {
                  propertyVal += product.additional_property_list[i].name;
                } else {
                  propertyVal += ' | ' + product.additional_property_list[i].name;
                }
              }
            }
            product.property_value = propertyVal;

            console.log("product.property_value:"+product.property_value);
          });
        })
      ).subscribe();
  }

  private populateBusinessCartCount() {
    this.cartService.getBusinessCartCount(this.businessId, this.businessCity).pipe(
      tap(count => this.businessCartCount = count)
    ).subscribe();
  }

  private populateSubTotal() {
    this.cartService.getProductsCartSubTotal(this.businessId, this.businessCity).pipe(
      tap(res => this.subTotal = res)
    ).subscribe();
  }

  onQuantityChange(product: any) {
    this.cartChanged$.next();
  }

  private listenToCartChange() {
    this.cartChanged$.pipe(
      takeWhile(() => !this.destroyed),
      tap(() => this.onCartChange())
    ).subscribe();
  }

  private onCartChange() {
    this.disableCheckOut = false;
    this.populateSubTotal();
    this.populateBusinessCartCount();
  }

  private showOutOfStockModal(notInStock: any[]) {
    this.outOfStockItems = this.businessCartProducts.filter(prod => notInStock.map(p => p.product_id).includes(prod.product_id));
    this.bsModalRef = this.modalService.show(this.outOfStockPopupTpl);
  }

  calculateRealPrice(cartProduct: any) {
    const actualPrice = cartProduct.actual_discount ? cartProduct.offer_price_double : cartProduct.actual_price_double;
    return (actualPrice * (cartProduct.quantity || 0)).toFixed(2);
  }

  calculateActualPrice(cartProduct: any) {
    return (cartProduct.actual_price_double * (cartProduct.quantity || 0)).toFixed(2);
  }

  getBusinessDeliveryProfile() {
    const reqData = new HttpParams()
      .set('business_city', this.businessCity)
      .set('business_id', this.businessId);
    
    this.api.get<any>(BASE_URL + '/v1/web/customer/cart/getBusinessDeliveryProfile',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
      response => {
        this.deliveryProfile = response
        this.checkOffline(this.deliveryProfile);
      },
      err => console.error(err)
    );
  }

  checkOffline(businessItem: any) {

    if (businessItem.is_offline) {
      this.isOffline = true;
    }

    if (businessItem.delivery_start_time != null && businessItem.delivery_end_time != null) {

      var currentDate = new Date();

      var dd = String(currentDate.getDate()).padStart(2, '0');
      var mm = String(currentDate.getMonth() + 1).padStart(2, '0');
      var yyyy = currentDate.getFullYear();

      var dateString = yyyy + '-' + mm + '-' + dd + 'T';

      var startDateTimeString = dateString + businessItem.delivery_start_time + ':00';
      var endDateTimeString = dateString + businessItem.delivery_end_time + ':00';

      var startDateTime = new Date(startDateTimeString);
      var endDateTime = new Date(endDateTimeString);

      if (startDateTime.getTime() > endDateTime.getTime()) {
        if (currentDate.getTime() > startDateTime.getTime()) {
          this.isOffline = false;
        } else {
          this.isOffline = true;
        }
      } else if (endDateTime.getTime() > startDateTime.getTime()) {
        if (currentDate.getTime() > startDateTime.getTime() && currentDate.getTime() < endDateTime.getTime()) {
          this.isOffline = false;
        } else {
          this.isOffline = true;
        }
      }
    }

    if(this.isOffline) {
      this.disableCheckOut = true;
    }
  }
}
