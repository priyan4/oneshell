import { Component, ElementRef, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CartService } from '@app/services/cart.service';
import { filter, takeWhile, tap } from 'rxjs/operators';
import { HttpParams } from '@angular/common/http';
import { BusinessDetailsService } from '@app/business-page/services/business-details.service';
import { ContextDataService } from '@app/services/context-data.service';
import { Subject } from 'rxjs';
import { environment as env } from '@env/environment';
import { forkJoin } from 'rxjs';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { UrlNavigationService } from "@app/services/url-navigation.service";
import { MyHttpClient } from '../../services/http.helper';

@Component({
  selector: 'app-self-order',
  templateUrl: './self-order.component.html',
  styleUrls: ['./self-order.component.scss']
})

export class SelfOrderComponent implements OnInit, OnDestroy {

  businessId: string;
  businessCity: string;
  businessName: string;
  businessCartProducts: any[];
  baseUrl = env.webUrl;
  businessProfile: any;
  subTotal: number;
  businessCartCount: number;
  disableCheckOut: boolean;
  private cartChanged$: Subject<any> = new Subject<any>();
  private destroyed: boolean;
  validBusinessCartProducts: any;
  checkoutResponse: any;
  orderId: string;
  placedProducts: any;
  placeOrderResponse: any;
  tableNo: any;
  isSelfOrderAvailable: boolean = true;
  isOffline: boolean = false;
  showSummary: boolean = false;
  validBusinessPlacedSelfOrder: any = [];
  tabNo: number = 0;
  orderPlaced: boolean = false;
  availOffers:any = [];
  filterMobileToggle: boolean = false;

  constructor(private activatedRoute: ActivatedRoute,
    private cartService: CartService,
    private businessDetailsService: BusinessDetailsService,
    private contextData: ContextDataService,
    private modalService: BsModalService,
    private urlNavigation: UrlNavigationService,
    private api: MyHttpClient) { }

  ngOnInit() {
    this.businessId = this.activatedRoute.snapshot.paramMap.get('business_id');
    this.businessCity = this.activatedRoute.snapshot.paramMap.get('business_city');
    this.businessName = this.activatedRoute.snapshot.paramMap.get('business_name');
    this.tableNo = this.activatedRoute.snapshot.paramMap.get('table_no');
    
    this.populateSelfOrderCartProductsAndBusinessProfile();
    this.populateSubTotal();
    this.populateBusinessCartCount();
    this.listenToCartChange();
    this.getOrderId();
    // Summary API calls
    this.getPlacedProducts();
    this.populateCheckOutResponse();
  }

  // Mock API Call => to be replaced with below commented lines

  private populateSelfOrderCartProductsAndBusinessProfile() {
    const params = new HttpParams()
      .set('business_city', this.businessCity)
      .set('business_id', this.businessId)
      .set('customer_id', this.contextData.customerProfile.customer_id);
    forkJoin(this.businessDetailsService.getBusinessProfile(params),
      this.cartService.getSelfOrderCartProducts(this.businessId, this.businessCity, this.tableNo)).pipe(
        tap(arr => this.businessProfile = arr[0]),
        tap(arr => {
          this.businessCartProducts = arr[1];
          this.businessCartProducts.forEach(product => {
            product.business_id = this.businessProfile.business_id;
            product.business_city = this.businessProfile.business_city;
            product.business_name = this.businessProfile.business_name;

            var propertyVal = undefined;
            if (product.chosen_properties != null && product.chosen_properties.length > 0) {
              for (var i = 0; i < product.chosen_properties; i++) {
                if (i == 0) {
                  propertyVal += product.chosen_properties[i];
                } else {
                  propertyVal += ' | ' + product.chosen_properties[i];
                }
              }
            }
            if (product.additional_property_list != null && product.additional_property_list.length > 0) {
              for (var i = 0; i < product.additional_property_list; i++) {
                propertyVal += ' | ' + product.chosen_properties[i];
              }
            }
            product.property_value = propertyVal;

          }
          );
          this.validBusinessCartProducts = this.businessCartProducts ? this.businessCartProducts.filter(bc => !!bc.self_order_quantity) : [];
        })
      ).subscribe();
  }

  private getOrderId() {
    this.cartService.getSelfOrderId(this.businessId,this.businessCity,this.tableNo).pipe(
      tap(res => this.orderId = res.order_id)
    ).subscribe();
  }

  private populateBusinessCartCount() {
    this.cartService.getSelfOrderBusinessCartCount(this.businessId, this.businessCity, this.tableNo).pipe(
      tap(count => this.businessCartCount = count)
    ).subscribe();
  }


  private populateCheckOutResponse() {
    this.cartService.getSelfOrderSummaryResponse(this.businessId, this.businessCity, this.tableNo).pipe(
      tap(res => this.prepareOffers(res))
    ).subscribe();
  }

  private prepareOffers(res) {
    this.checkoutResponse = res
    this.availOffers=[];
    Array.prototype.push.apply(this.availOffers, this.checkoutResponse.bill_offer_details_list);
  }

  private populateSubTotal() {
    this.cartService.getSelfOrderProductsSubTotal(this.businessId, this.businessCity, this.tableNo).pipe(
      tap(res => this.subTotal = res)
    ).subscribe();
  }

  private getPlacedProducts() {
    this.cartService.getSelfOrderPlacedProducts(this.businessId, this.businessCity, this.tableNo).pipe(
      tap(res =>{ 
          this.placedProducts = res;
          this.prepareAdditionalProperties(res);
          this.populateCheckOutResponse()
        })
    ).subscribe();
  }



  finishEating() {
    this.cartService.getCompletedOrder(this.businessId, this.businessCity,this.businessProfile.phone_number,this.tableNo,this.orderId,this.businessProfile.partner_city,this.businessProfile.partner_profile_id).pipe(
      tap(res => 
        {
          if (res) {
            window.location.href = this.baseUrl + '/' + this.businessCity
          }
        }
        )
    ).subscribe();
  }


  // place your oder => order  btn
  placeOrder() {
    this.cartService.placeSelfOrder(this.businessId, this.businessCity, this.businessProfile.phone_number,this.tableNo,this.orderId,this.businessProfile.partner_city,this.businessProfile.partner_profile_id).pipe(
        tap(res =>{
          if(res.success) {
            this.getPlacedProducts();
            this.tabNo = 1;
            this.orderPlaced = true;
          }
        })
      ).subscribe();
  }

  // process the placed products

  private prepareAdditionalProperties(res: any) {
    this.placeOrderResponse = res
    this.placeOrderResponse.forEach(product => {

      var propertyVal = undefined;
      if (product.chosen_properties != null && product.chosen_properties.length > 0) {
        for (var i = 0; i < product.chosen_properties; i++) {
          if (i == 0) {
            propertyVal += product.chosen_properties[i];
          } else {
            propertyVal += ' | ' + product.chosen_properties[i];
          }
        }
      }
      if (product.additional_property_list != null && product.additional_property_list.length > 0) {
        for (var i = 0; i < product.additional_property_list; i++) {
          propertyVal += ' | ' + product.chosen_properties[i];
        }
      }
      product.property_value = propertyVal;
    }
    );
    this.validBusinessPlacedSelfOrder = this.placeOrderResponse ? this.placeOrderResponse.filter(order => !!order.quantity) : [];
    this.showSummary = this.validBusinessPlacedSelfOrder.length ? true : false;
  }

  // add and delete products

  private listenToCartChange() {
    this.cartChanged$.pipe(
      takeWhile(() => !this.destroyed),
      tap(() => this.onCartChange())
    ).subscribe();
  }

  private onCartChange() {
    this.disableCheckOut = false;
    this.populateSubTotal();
    this.populateBusinessCartCount();
  }

  onQuantityChange(product: any) {
    this.cartChanged$.next();
  }

  onDelete(product) {
    this.cartService.deleteCartProduct(product, this.businessId, this.tableNo).pipe(
      tap(() => this.removeDeletedProduct(product)),
      tap(() => this.cartChanged$.next())
    ).subscribe();
  }

  private removeDeletedProduct(product) {
    this.validBusinessCartProducts = product.variable_product_id !== 'vp1'
      ? this.validBusinessCartProducts.filter(cp => !(cp.product_id === product.product_id && cp.variable_product_id === product.variable_product_id))
      : this.validBusinessCartProducts.filter(cp => (cp.product_id !== product.product_id));
  }

  calculateRealPrice(cartProduct: any) {
    const actualPrice = cartProduct.actual_discount ? cartProduct.offer_price_double : cartProduct.actual_price_double;
    return (actualPrice * (cartProduct.self_order_quantity || 0)).toFixed(2);
  }

  calculateSummaryItemsRealPrice(cartProduct: any) {
    const actualPrice = cartProduct.actual_discount ? cartProduct.offer_price_double : cartProduct.actual_price_double;
    return (actualPrice * (cartProduct.quantity || 0)).toFixed(2);
  }


  ngOnDestroy(): void {
    this.destroyed = true;
  }

}