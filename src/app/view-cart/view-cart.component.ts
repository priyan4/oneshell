import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { environment as env } from '@env/environment';
import { ContextDataService } from '@app/services/context-data.service';
import {tap} from 'rxjs/operators';
import {CartService} from '@app/services/cart.service';
import { CitiesService } from '@app/services/cities.service';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../services/http.helper';

const BASE_URL = env.serverUrl;

@Component({
  selector: 'app-view-cart',
  templateUrl: './view-cart.component.html',
  styleUrls: ['./view-cart.component.scss']
})
export class ViewCartComponent implements OnInit {

  cartBusinessesList: any;
  baseUrl: string = env.webUrl;
  activeCity: string;

  constructor(private activatedRoute: ActivatedRoute,
              private api: MyHttpClient,
              private router: Router, private contextData: ContextDataService,
              private cartService: CartService, public citiesService: CitiesService) { }

  ngOnInit() {
    this.activeCity = this.citiesService.activeCity;
    this.getCartBusinesses();
  }

  onViewCart(cartBusiness) {
    window.open(`${this.baseUrl}/viewCart/${cartBusiness.business_city}/${cartBusiness.business_id}`,
      '_self');
  }

  getCartBusinesses() {
    const params = new HttpParams();

    let reqData;
    reqData = params.set("customer_id", this.contextData.customerProfile.customer_id)
      .set("customer_city", this.contextData.customerProfile.customer_city);

    this.api.get<any>(BASE_URL + '/v1/web/customer/cart/getCompleteCart',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
        response => {

      this.cartBusinessesList = response;
      for (let business of this.cartBusinessesList) {
        business.urlBusinessName = business.business_name.replace(/\s+/g, "-").replace(/[^a-zA-Z0-9-]/g, "");
      }
      console.log(this.cartBusinessesList);

    }, (err) => {
      console.log('error ' + err);
      //show no data found message
    })
  }

  onDeleteCartClicked(businessCartItem: any) {
    const data = {
      business_id: businessCartItem.business_id,
      business_city: businessCartItem.business_city,
      customer_id: this.contextData.customerProfile.customer_id,
      customer_city: this.contextData.customerProfile.customer_city,
    };
    
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    this.api.post<any>(BASE_URL + "/v1/web/customer/cart/removeProductsFromBusinessCart", data, httpOptions).pipe(
      tap(() => this.getCartBusinesses()),
      tap(() => this.cartService.refreshCompleteCartCount())
    ).subscribe( response => undefined, err => console.error(err));
  }

}