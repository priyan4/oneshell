import { Component, ElementRef, HostListener, Inject, OnDestroy, OnInit, PLATFORM_ID, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { environment as env } from '@env/environment';
import { BusinessDetailsService } from '@app/business-page/services/business-details.service';
import { ContextDataService } from '@app/services/context-data.service';
import { trigger } from '@angular/animations';
import { fadeIn } from '../../../animations/animations';
import { Subject } from 'rxjs';
import { debounceTime, filter, takeWhile, tap, distinctUntilChanged, switchMap, map } from 'rxjs/operators';
import { StoreCartButtonComponent } from '@app/core/components/store-cart-button/store-cart-button.component';
import { offerFilterForProducts, productSortOptions } from '@app/constants/filter.sort.constants';
import { isPlatformBrowser } from '@angular/common';
import { CitiesService, City } from '@app/services/cities.service';
import { ProductDataServiceService } from '@app/products-page/services/product-data-service.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { debounce } from '@app/utils/decorators';
import { FOOTER_HEIGHT } from '@app/constants/style.constants';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../../services/http.helper';

const BASE_URL = env.serverUrl;

@Component({
  selector: 'app-products-list',
  animations: [
    trigger('fadeIn', fadeIn())
  ],
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss'],
  host: {
    '(document:click)': 'handleSearchChange($event)',
  },
})
export class ProductsListComponent implements OnInit, OnDestroy {

  private cartChanged$: Subject<any> = new Subject<any>();
  private destroyed: boolean;
  @ViewChild(StoreCartButtonComponent, { static: false }) private storeCartBtn: StoreCartButtonComponent;
  detailPage: boolean;

  constructor(private route: ActivatedRoute,
    private api: MyHttpClient,
    private router: Router,
    private productDataServiceService: ProductDataServiceService,
    private businessDetailsService: BusinessDetailsService,
    private modalService: BsModalService,
    private contextData: ContextDataService,
    private citiesService: CitiesService,
    @Inject(PLATFORM_ID) private platformId: any) {

  }

  productList: Array<any> = [];
  nextToken = 1;
  requestedPage = '';
  level2Category = '';
  paginationSize = 10;
  isFullyLoaded = false;
  allLevel3Category: any;
  levelCategories = [];
  selectedCategory: string;
  currentCity: any;
  type: ProductListType;
  category: string;
  keyword: string;
  level: string;
  businessId: string;
  businessCity: string;
  businessName: string;
  displayName: string;
  level3Category: string;
  level1Category: string;
  index: number;
  showBusinessName = false;
  baseUrl: string = env.webUrl;
  showAvailableForDeliveryTxt: boolean;

  searchTxt = '';
  search$: Subject<any> = new Subject();
  searchResultsList: Array<any> = [];
  url: string;

  paginationRedirectionUrl: string;

  config: any;
  pageNumber: number;
  pageStart: number;
  pageSize: number;
  pageEnd: number;
  totalItems: number;
  categoryLevel: string;
  level2DisplayName: string;

  isPaginationEnabled: boolean;
  currentBreadCrumbDisplayName: string;
  productNavigationUrl: string;
  showCategories = false;
  showData = true;
  oneShellHomeDelivery = false;
  deliveryProfile: any;
  isOffline = false;
  productListType = ProductListType;

  @ViewChild('categoryBtn', { static: false }) categoryBtn: ElementRef;
  readonly categorySlideIdPrefix = 'category_slide_';
  currentActiveModalRef: BsModalRef;

  filterChange = new Subject();
  offerFilterOptions: { label: string, key: number, value: boolean }[] = [];
  brandsFilterOptions: { label: string, value: boolean }[] = [];
  locationFilterOptions: { label: string, value: boolean }[] = [];
  selectedSortOption: string;
  productSortOptions: { displayText: string, value: string }[] = productSortOptions;
  readonly filterType = {
    offer: 'offer',
    brand: 'brand',
    location: 'location'
  };
  filterDetailsInMobile: { selected: string } = { selected: this.filterType.offer };
  get currentActiveFilterType() {
    switch (this.filterDetailsInMobile.selected) {
      case this.filterType.offer: return this.offerFilterOptions;
      case this.filterType.brand: return this.brandsFilterOptions;
      case this.filterType.location: return this.locationFilterOptions;
    }
  }
  hideFilterAndSortBarInMobile: boolean;
  showFilters: boolean;
  showSort: boolean;

  ngOnInit() {
    this.pageSize = 40;

    this.currentCity = this.route.snapshot.paramMap.get('city');
    this.type = this.route.snapshot.paramMap.get('type') as ProductListType;
    this.businessId = this.route.snapshot.paramMap.get('businessid');
    this.businessCity = this.route.snapshot.paramMap.get('businesscity');
    this.category = this.route.snapshot.paramMap.get('category');
    this.keyword = this.route.snapshot.paramMap.get('keyword');
    this.level3Category = this.route.snapshot.paramMap.get('leve3category');
    this.level1Category = this.route.snapshot.paramMap.get('level1category');
    this.index = Number(this.route.snapshot.paramMap.get('index'));
    this.level = this.route.snapshot.paramMap.get('level');
    this.level2DisplayName = this.route.snapshot.paramMap.get('displayname');
    this.pageNumber = parseInt(((this.route.snapshot.paramMap.get('page') != null) ? this.route.snapshot.paramMap.get('page') : '1'));

    this.displayName = this.keyword;
    this.allLevel3Category = {
      name: 'all_level3',
      display_name: 'All',
      level: 'LEVEL3'
    };
    this.selectedSortOption = this.productSortOptions[0].value;

    if (this.type === ProductListType.Search || this.type === ProductListType.Trending ||
      this.type === ProductListType.healthProducts ||
      this.type === ProductListType.businessSearch ||
      this.type === ProductListType.businessCategoryProducts) {
      this.populateFilters();
      this.listenToFilterChange();
      this.showFilters = true;
    } else {
      this.showFilters = false;
    }

    if (this.type === ProductListType.Search ||
      this.type === ProductListType.healthProducts ||
      this.type === ProductListType.businessSearch ||
      this.type === ProductListType.businessCategoryProducts) {
      this.showSort = true;
    } else {
      this.showSort = false;
    }

    this.getShortBusinessDetails();

    if (this.type === ProductListType.Search) {
      this.isPaginationEnabled = true;
      this.showBusinessName = true;
      this.showAvailableForDeliveryTxt = true;
      this.currentBreadCrumbDisplayName = this.keyword;
      this.paginationRedirectionUrl = this.baseUrl + '/products/' + this.currentCity + '/' + 'Search' + '/' + this.category + '/' + this.keyword;
      this.refreshProductsBasedOnType();
    }

    if (this.type === ProductListType.Trending) {
      this.isPaginationEnabled = true;
      this.showBusinessName = true;
      this.showAvailableForDeliveryTxt = true;
      this.currentBreadCrumbDisplayName = 'Trending';
      this.paginationRedirectionUrl = this.baseUrl + '/products/ct/' + this.currentCity + '/' + 'Trending' + '/' + this.category + '/' + this.index;
      this.refreshProductsBasedOnType();
    }

    if (this.type === ProductListType.healthProducts) {
      this.isPaginationEnabled = true;
      this.showBusinessName = true;
      this.showAvailableForDeliveryTxt = true;
      this.currentBreadCrumbDisplayName = 'Healthy Deals';
      this.paginationRedirectionUrl = this.baseUrl + '/products/' + this.currentCity + '/' + 'Health Products' + '/' + this.category + '/' + this.keyword + '/' + 'LEVEL1';
      this.refreshProductsBasedOnType();
    }

    if (this.type === ProductListType.businessSearch) {
      this.isPaginationEnabled = true;
      this.currentBreadCrumbDisplayName = this.keyword;
      this.paginationRedirectionUrl = this.baseUrl + '/products/' + this.currentCity + '/' + 'Business Search' + '/' + this.businessId + '/' + this.businessName + '/' + this.category + '/' + this.keyword;
      this.refreshProductsBasedOnType();
    }

    if (this.type === ProductListType.businessTrending) {
      this.isPaginationEnabled = true;
      this.currentBreadCrumbDisplayName = 'Trending';
      this.paginationRedirectionUrl = this.baseUrl + '/products/t/' + this.currentCity + '/' + 'Business Trending' + '/' + this.businessId + '/' + this.businessCity + '/' + 'Trending Products';
      this.getBusinessTrendingProductsCount();
      this.getMoreTrendingProducts();
    }

    if (this.type === ProductListType.businessCategoryProducts) {
      this.isPaginationEnabled = true;
      this.currentBreadCrumbDisplayName = this.route.snapshot.paramMap.get('displayname');
      this.paginationRedirectionUrl = this.baseUrl + '/products/c/' + this.currentCity + '/' + 'Business Category Products' + '/' + this.businessId + '/' + this.businessCity + '/' + this.level2DisplayName + '/' + this.category + '/' + this.level3Category + '/' + this.index;
      this.refreshProductsBasedOnType();
    }

    if (this.type === ProductListType.businessOfferProducts) {
      this.currentBreadCrumbDisplayName = this.keyword;
      this.isPaginationEnabled = true;
      this.paginationRedirectionUrl = this.baseUrl + '/products/o/c/' + this.currentCity + '/' + 'Business Offer Products' + '/' + this.businessId + '/' + this.businessCity + '/' + this.category + '/' + this.keyword + '/' + this.level1Category + '/' + this.index;
      this.getProductsByAdditionalBrowseOffersCategory();
    }

    this.populateBusinessName();
    this.listenToCartChange();
    this.addSearchListener();

    this.detailPage = false;
  }

  @HostListener('window:scroll')
  @debounce()
  scrollHandler() {
    this.hideFilterAndSortBarInMobile =
      (window.innerHeight + window.pageYOffset + FOOTER_HEIGHT) >= document.body.offsetHeight;
  }

  private listenToFilterChange(): void {
    this.filterChange.pipe(
      takeWhile(() => !this.destroyed),
      debounceTime(500),
      tap(() => this.refreshProductsBasedOnType())
    ).subscribe();
  }

  private refreshProductsBasedOnType() {
    switch (this.type) {
      case ProductListType.Search: this.getProductList(); break;
      case ProductListType.healthProducts: this.getProductsByAllLevels(); break;
      case ProductListType.Trending: this.getCityTrendingProductList(); break;
      case ProductListType.businessSearch: this.getBusinessProductList(); break;
      case ProductListType.businessCategoryProducts: this.getProductsByLevel2Category(); break;
    }
  }

  showLocationFilter(): boolean {
    return !!this.locationFilterOptions.length;
  }
  private prepareProductFilters() {
    const offers = this.offerFilterOptions.filter(o => o.value).map(o => o.key);
    const brands = this.brandsFilterOptions.filter(o => o.value).map(o => o.label);
    const locations = this.locationFilterOptions.filter(o => o.value).map(o => o.label);
    return {
      offers: offers.length ? offers : undefined,
      brands: brands.length ? brands : undefined,
      locations: locations.length ? locations : undefined
    };
  }

  isAnyFilterSelected(): boolean {
    return !!this.offerFilterOptions.filter(o => o.value).length
      || !!this.brandsFilterOptions.filter(o => o.value).length
      || (this.showLocationFilter() && !!this.locationFilterOptions.filter(o => o.value).length);
  }

  clearAllFilters(): void {
    this.offerFilterOptions.forEach(o => o.value = false);
    this.brandsFilterOptions.forEach(o => o.value = false);
    this.locationFilterOptions.forEach(o => o.value = false);
    this.filterChange.next();
  }

  private populateFilters() {
    this.offerFilterOptions = offerFilterForProducts.map(l => {
      return { key: l.key, label: l.label, value: false };
    });
    if (isPlatformBrowser(this.platformId)) {
      if ([ProductListType.Search, ProductListType.healthProducts].includes(this.type)) {
        this.citiesService.getCities().pipe(
          map(res => res.find(c => c.city_name === this.citiesService.activeCity)),
          filter((activeCityObj: City) => !!activeCityObj && activeCityObj.is_locations_enabled),
          switchMap(() => this.citiesService.getLocationByCity(this.citiesService.activeCity)),
          tap(res => {
            this.locationFilterOptions = res.sort().map(l => {
              return { label: l, value: false };
            });
          })
        ).subscribe();
      }

      if (this.type === ProductListType.Search) {
        this.productDataServiceService.getBrandsByProductsCategory(this.currentCity, this.category).pipe(
          tap(res => {
            this.brandsFilterOptions = res.map(l => {
              return { label: l, value: false };
            });
          })
        ).subscribe();
      } else if (this.type === ProductListType.businessSearch || this.type === ProductListType.businessCategoryProducts) {
        var brandCategory;
        if ('all_level3' === this.level3Category) {
          brandCategory = this.category;
        } else {
          brandCategory = this.level3Category;
        }

        this.productDataServiceService.getBrandsByProductsCategoryInBusiness(this.businessId, this.currentCity, brandCategory).pipe(
          tap(res => {
            this.brandsFilterOptions = res.map(l => {
              return { label: l, value: false };
            });
          })
        ).subscribe();
      }
    }
  }

  ngOnDestroy(): void {
    this.destroyed = true;
  }

  showBreadcrumb(): boolean {
    const type = this.type && this.type.toLowerCase();
    return ['Business Trending', 'Business Category Products', 'Business Offer Products', 'Business Search', 'Trending', 'Search', 'Health Products']
      .map(v => v.toLowerCase())
      .includes(type);
  }

  showAddToCart(): boolean {
    const type = this.type && this.type.toLowerCase();
    return ['Business Trending', 'Business Category Products', 'Business Offer Products', 'Business Search']
      .map(v => v.toLowerCase())
      .includes(type);
  }

  getBusinessTrendingProductsCount() {

    const data = {
      business_city: this.businessCity,
      business_id: this.businessId,
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    this.api.post<any>(BASE_URL + "/v1/customer/pagination/web/getBusinessTrendingProductsCount", data, httpOptions).subscribe(
      response => {


        this.totalItems = response.total_count;
        this.config = {
          currentPage: this.pageNumber,
          itemsPerPage: this.pageSize,
          totalItems: this.totalItems
        };

        this.pageStart = 1 + (this.pageNumber - 1) * this.pageSize;

        if ((this.totalItems - this.pageStart) > this.pageSize) {
          this.pageEnd = (this.pageNumber * this.pageSize);
        } else {
          this.pageEnd = this.totalItems;
        }
      }
    );
  }

  getMoreTrendingProducts() {
    const reqData = new HttpParams()
      .set('business_city', this.businessCity)
      .set('business_id', this.businessId)
      .set('customer_id', this.contextData.customerProfile.customer_id)
      .set('customer_city', this.contextData.customerProfile.customer_city)
      .set('is_home_page', 'false')
      .set('page_number', String(this.pageNumber))
      .set('page_size', '12');

    this.api.get<any>(BASE_URL + '/v1/web/customerService/getTrendingProductsInBusiness',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
        response => {
          this.productList = this.productList.concat(response.product_list);
          for (let i = 0; i < this.productList.length; i++) {
            this.productList[i].urlProductName = this.productList[i].name.replace(/\s+/g, '-').replace(/[^a-zA-Z0-9-]/g, '');
            this.productList[i].productNavigationUrl = this.baseUrl + '/product/' + this.businessCity + '/' + 'Business Trending' + '/' + this.businessId + '/' + this.productList[i].category_level3.name + '/' + this.productList[i].product_id + '/' + this.productList[i].urlProductName + '/' + this.index + '/' + this.pageNumber;
          }
          if ((Number(response.next_token) - (this.nextToken)) !== this.paginationSize) {
            this.isFullyLoaded = true;
          }
          this.nextToken = Number(response.next_token);
        }, err => console.error(err));
  }

  getProductsByLevel2Category() {
    const productFilters = this.prepareProductFilters();
    const businessProductFilters = {
      category: this.level3Category,
      offers: productFilters.offers,
      brands: productFilters.brands
    };
    this.categoryLevel = 'LEVEL2';
    const data = {
      business_city: this.businessCity,
      page_number: this.pageNumber,
      page_size: this.pageSize,
      business_id: this.businessId,
      level1_or_level2_category: this.category,
      customer_id: this.contextData.customerProfile.customer_id,
      customer_city: this.contextData.customerProfile.customer_city,
      category_level: this.categoryLevel,
      businessProductFilters,
      sort_type: this.selectedSortOption,
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    this.api.post<any>(BASE_URL + "/v1/web/customerService/getProductsByLevel2AndLevel3CategoriesInBusiness", data, httpOptions).subscribe(
      response => {

        this.productList = response.product_list;
        for (let i = 0; i < this.productList.length; i++) {
          this.productList[i].urlProductName = this.productList[i].name.replace(/\s+/g, '-').replace(/[^a-zA-Z0-9-]/g, '');
          this.productList[i].productNavigationUrl = this.baseUrl + '/product/' + this.businessCity + '/' + 'Business Category Products' + '/' + this.businessId + '/' + this.productList[i].category_level3.name + '/' + this.productList[i].product_id + '/' + this.productList[i].urlProductName + '/' + this.level3Category + '/' + this.index + '/' + this.pageNumber;
        }
        this.totalItems = response.total_count;
        this.config = {
          currentPage: this.pageNumber,
          itemsPerPage: this.pageSize,
          totalItems: this.totalItems
        };

        this.pageStart = 1 + (this.pageNumber - 1) * this.pageSize;

        if ((this.totalItems - this.pageStart) > this.pageSize) {
          this.pageEnd = (this.pageNumber * this.pageSize);
        } else {
          this.pageEnd = this.totalItems;
        }
      },
      err => console.error(err)
    );
    this.levelCategories = [];
    this.getLevel3Categories();
  }

  getBusinessProductList() {
    this.productList = [];
    const productFilters = this.prepareProductFilters();
    const businessProductFilters = {
      offers: productFilters.offers,
      brands: productFilters.brands
    };
    const data = {
      business_city: this.currentCity,
      business_id: this.businessId,
      page_number: this.pageNumber,
      page_size: this.pageSize,
      category: this.category,
      keyword: this.keyword,
      customer_id: this.contextData.customerProfile.customer_id,
      customer_city: this.contextData.customerProfile.customer_city,
      sort_type: this.selectedSortOption,
      businessProductFilters
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    this.api.post<any>(BASE_URL + "/v1/web/customerService/searchProductsInBusiness", data, httpOptions).subscribe(
      response => {

        this.productList = response.product_list;
        for (let i = 0; i < this.productList.length; i++) {
          this.productList[i].urlProductName = this.productList[i].name.replace(/\s+/g, '-').replace(/[^a-zA-Z0-9-]/g, '');
          this.productList[i].productNavigationUrl = this.baseUrl + '/product/' + this.productList[i].business_city + '/' + 'Business Search' + '/' + this.businessId + '/' + this.productList[i].category_level3.name + '/' + this.productList[i].product_id + '/' + this.productList[i].urlProductName + '/' + this.category + '/' + this.keyword + '/' + '0' + '/' + this.pageNumber;
        }

        this.totalItems = response.total_count;
        this.config = {
          currentPage: this.pageNumber,
          itemsPerPage: this.pageSize,
          totalItems: this.totalItems
        };

        this.pageStart = 1 + (this.pageNumber - 1) * this.pageSize;

        if ((this.totalItems - this.pageStart) > this.pageSize) {
          this.pageEnd = (this.pageNumber * this.pageSize);
        } else {
          this.pageEnd = this.totalItems;
        }
      },
      err => console.error(err)
    );
  }

  getLevel3Categories() {
    const reqData = new HttpParams()
      .set('business_city', this.businessCity)
      .set('business_id', this.businessId)
      .set('category', this.category);

    this.api.get<any>(BASE_URL + '/v1/web/customerService/getLevel3Categories',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
        response => {

          this.levelCategories.push(this.allLevel3Category);
          for (const cat of response) {
            this.levelCategories.push(cat);
          }
          for (let i = 0; i < this.levelCategories.length; i++) {
            this.levelCategories[i].href_link = this.baseUrl + '/products/c/' + this.currentCity + '/' + 'Business Category Products' + '/' + this.businessId + '/' + this.businessCity + '/' + this.level2DisplayName + '/' + this.category + '/' + this.levelCategories[i].name + '/' + i + '/' + '1';
          }
          if (this.levelCategories.length > 1) {
            this.showCategories = true;
          }
        },
        err => {
          console.error(err);
        }
      );
  }

  getBrowseOffersLevelsCategories() {
    const reqData = new HttpParams()
      .set('business_city', this.businessCity)
      .set('business_id', this.businessId)
      .set('offer_category_name', this.category);

    this.api.get<any>(BASE_URL + '/v1/web/customerService/getLevel1CategoriesByStoreOfferCategory',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
        response => {
          this.levelCategories = response;
          for (let i = 0; i < this.levelCategories.length; i++) {
            this.levelCategories[i].href_link = this.baseUrl + '/products/o/c/' + this.currentCity + '/' + 'Business Offer Products' + '/' + this.businessId + '/' + this.businessCity + '/' + this.category + '/' + this.keyword + '/' + this.levelCategories[i].name + '/' + i + '/' + '1';
          }
          if (this.levelCategories.length > 1) {
            this.showCategories = true;
          }
        },
        err => {
          console.error(err);
        }
      );
  }

  getProductsByLevel1Category() {
    const businessProductFilters = {
      // category: level3Category
    };
    const data = {
      business_city: localStorage.getItem('listingBusinessCity'),
      next_token: String(this.nextToken),
      business_id: localStorage.getItem('listingBusinessId'),
      level1_or_level2_category: localStorage.getItem('level2Category'),
      customer_id: this.contextData.customerProfile.customer_id,
      customer_city: this.contextData.customerProfile.customer_city,
      category_level: 'LEVEL2',
      businessProductFilters
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    this.api.post<any>(BASE_URL + "/v1/web/customerService/web/getProductsByLevel2AndLevel3CategoriesInBusiness", data, httpOptions).subscribe(
      response => {

        this.productList = this.productList.concat(response.product_list);
        for (let i = 0; i < this.productList.length; i++) {
          this.productList[i].urlProductName = this.productList[i].name.replace(/\s+/g, '-').replace(/[^a-zA-Z0-9-]/g, '');
        }

        this.totalItems = response.total_count;
        this.config = {
          currentPage: this.pageNumber,
          itemsPerPage: this.pageSize,
          totalItems: this.totalItems
        };

        this.pageStart = 1 + (this.pageNumber - 1) * this.pageSize;

        if ((this.totalItems - this.pageStart) > this.pageSize) {
          this.pageEnd = (this.pageNumber * this.pageSize);
        } else {
          this.pageEnd = this.totalItems;
        }

      },
      err => {
        console.log('error' + err);
      }
    );
  }

  getProductsByAdditionalBrowseOffersCategory() {

    let data: any;

    let businessProductFilters: any;

    businessProductFilters = {
      category: this.category
    };

    data = {
      business_city: this.businessCity,
      page_number: this.pageNumber,
      page_size: this.pageSize,
      business_id: this.businessId,
      level1_or_level2_category: this.level1Category,
      category_level: 'LEVEL1',
      customer_id: this.contextData.customerProfile.customer_id,
      customer_city: this.contextData.customerProfile.customer_city,
      businessProductFilters
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    this.api.post<any>(BASE_URL + "/v1/web/customerService/getProductsByLevel2AndLevel3CategoriesInBusiness", data, httpOptions).subscribe(
      response => {

        console.log(response);

        this.productList = this.productList.concat(response.product_list);
        for (let i = 0; i < this.productList.length; i++) {
          this.productList[i].urlProductName = this.productList[i].name.replace(/\s+/g, '-').replace(/[^a-zA-Z0-9-]/g, '');
          this.productList[i].productNavigationUrl = this.baseUrl + '/product/' + this.businessCity + '/' + 'Business Offer Products' + '/' + this.businessId + '/' + this.productList[i].category_level3.name + '/' + this.productList[i].product_id + '/' + this.productList[i].urlProductName + '/' + this.category + '/' + this.keyword + '/' + this.index + '/' + this.pageNumber;
        }

        this.totalItems = response.total_count;
        this.config = {
          currentPage: this.pageNumber,
          itemsPerPage: this.pageSize,
          totalItems: this.totalItems
        };

        this.pageStart = 1 + (this.pageNumber - 1) * this.pageSize;

        if ((this.totalItems - this.pageStart) > this.pageSize) {
          this.pageEnd = (this.pageNumber * this.pageSize);
        } else {
          this.pageEnd = this.totalItems;
        }

        this.getBrowseOffersLevelsCategories();
      },
      err => {
        console.log('error' + err);
      }
    );
  }

  getProductList() {
    const productFilters = this.prepareProductFilters();
    const data = {
      business_city: this.currentCity,
      page_number: this.pageNumber,
      page_size: this.pageSize,
      query: 0,
      category: this.category,
      keyword: this.keyword,
      sort_type: this.selectedSortOption,
      productFilters
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    this.api.post<any>(BASE_URL + "/v1/web/customerService/searchProductsInCity", data, httpOptions).subscribe(
      response => {

        this.productList = response.product_list;
        for (let i = 0; i < this.productList.length; i++) {
          this.productList[i].urlProductName = this.productList[i].name.replace(/\s+/g, '-').replace(/[^a-zA-Z0-9-]/g, '');
          this.productList[i].productNavigationUrl = this.baseUrl + '/product/' + this.productList[i].business_city + '/' + 'Search' + '/' + this.productList[i].business_id + '/' + this.productList[i].category_level3.name + '/' + this.productList[i].product_id + '/' + this.productList[i].urlProductName + '/' + this.pageNumber;

        }
        this.totalItems = response.total_count;
        this.config = {
          currentPage: this.pageNumber,
          itemsPerPage: this.pageSize,
          totalItems: this.totalItems
        };
        this.pageStart = 1 + (this.pageNumber - 1) * this.pageSize;
        if ((this.totalItems - this.pageStart) > this.pageSize) {
          this.pageEnd = (this.pageNumber * this.pageSize);
        } else {
          this.pageEnd = this.totalItems;
        }
      },
      err => console.error('error' + err)
    );

  }

  getCityTrendingProductList() {
    const reqData = new HttpParams()
      .set('gender', 'Male');

    this.api.get<any>(BASE_URL + '/v1/web/homePage/getTrendingProductCategories',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
        response => {
          console.log(response);
          this.levelCategories = response;
          this.levelCategories.forEach((element) => {
            element.name = element.category;
          });
          for (let i = 0; i < this.levelCategories.length; i++) {
            this.levelCategories[i].href_link = this.baseUrl + '/products/ct/' + this.currentCity + '/' + 'Trending' + '/' + this.levelCategories[i].name + '/' + i + '/' + '1';
          }
          if (this.levelCategories.length > 1) {
            this.showCategories = true;
          }
          this.getProductsByTrendingCategory();
        },
        err => {
          console.log('error' + err);
        }
      );
  }

  getProductsByTrendingCategory() {

    const reqBody = {
      business_city: this.currentCity,
      page_number: this.pageNumber,
      page_size: this.pageSize,
      category: this.category
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    this.api.post<any>(BASE_URL + "/v1/web/homePage/getTrendingProductsByCategory", reqBody, httpOptions).subscribe(
      response => {


        this.productList = this.productList.concat(response.product_list);
        for (let i = 0; i < this.productList.length; i++) {
          this.productList[i].urlProductName = this.productList[i].name.replace(/\s+/g, '-').replace(/[^a-zA-Z0-9-]/g, '');
          this.productList[i].productNavigationUrl = this.baseUrl + '/product/' + this.productList[i].business_city + '/' + 'Trending' + '/' + this.productList[i].business_id + '/' + this.productList[i].category_level3.name + '/' + this.productList[i].product_id + '/' + this.productList[i].urlProductName + '/' + this.category + '/' + this.index + '/' + this.pageNumber;
        }

        this.totalItems = response.total_count;
        this.config = {
          currentPage: this.pageNumber,
          itemsPerPage: this.pageSize,
          totalItems: this.totalItems
        };

        this.pageStart = 1 + (this.pageNumber - 1) * this.pageSize;

        if ((this.totalItems - this.pageStart) > this.pageSize) {
          this.pageEnd = (this.pageNumber * this.pageSize);
        } else {
          this.pageEnd = this.totalItems;
        }

      }, err => {
        console.error(err);
      }
    );
  }

  getProductsByAllLevels() {
    const productFilters = this.prepareProductFilters();
    const data = {
      city: this.currentCity,
      page_number: this.pageNumber,
      page_size: this.pageSize,
      category: this.category,
      category_level: this.level,
      sort_type: this.selectedSortOption,
      productFilters
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    this.api.post<any>(BASE_URL + "/v1/web/customerService/getProductsByAllCategoryLevels", data, httpOptions).subscribe(
      response => {

        this.productList = response.product_list;
        for (let i = 0; i < this.productList.length; i++) {
          this.productList[i].urlProductName = this.productList[i].name.replace(/\s+/g, '-').replace(/[^a-zA-Z0-9-]/g, '');
          this.productList[i].productNavigationUrl = this.baseUrl + '/product/' + this.productList[i].business_city + '/' + 'Health Products' + '/' + this.productList[i].business_id + '/' + this.productList[i].category_level3.name + '/' + this.productList[i].product_id + '/' + this.productList[i].urlProductName + '/' + this.pageNumber;
        }

        this.totalItems = response.total_count;
        this.config = {
          currentPage: this.pageNumber,
          itemsPerPage: this.pageSize,
          totalItems: this.totalItems
        };

        this.pageStart = 1 + (this.pageNumber - 1) * this.pageSize;

        if ((this.totalItems - this.pageStart) > this.pageSize) {
          this.pageEnd = (this.pageNumber * this.pageSize);
        } else {
          this.pageEnd = this.totalItems;
        }
      },
      err => console.error(err)
    );
  }

  populateBusinessName() {
    if (this.businessId) {
      const params = new HttpParams()
        .set('business_city', this.businessCity)
        .set('business_id', this.businessId)
        .set('customer_id', this.contextData.loggedInCustomerId);
      this.businessDetailsService.getBusinessProfile(params).subscribe(res => {
        this.businessName = res.business_name;
      });
    }
  }

  onQuantityChange(product: any) {
    this.cartChanged$.next();
  }

  private listenToCartChange() {
    this.cartChanged$.pipe(
      debounceTime(1000),
      takeWhile(() => !this.destroyed),
      filter(() => !!this.storeCartBtn),
      tap(() => this.storeCartBtn.refreshBusinessCartCount())
    ).subscribe();
  }

  addSearchListener() {
    this.search$.pipe(
      takeWhile(() => !this.destroyed),
      distinctUntilChanged(),
      switchMap((searchText: string) => {
        this.searchResultsList = [];
        const params = new HttpParams();
        const reqData = params
          .set('keyword', this.searchTxt)
          .set('business_id', this.businessId)
          .set('business_city', this.businessCity)
          .set('size', '20');

        return this.api.get<any>(BASE_URL + '/v1/web/autoComplete/product/getStringsByBusiness',
          { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData });
      })
    ).subscribe((response) => {
      if (response) {
        this.searchResultsList.push(...response);
      }
    });
  }

  handleSearchChange(event) {
    this.search$.next(this.searchTxt);
    if (event.target.id != 'txtSearch') {
      this.searchResultsList = [];
    }
  }

  pageChange(newPage: number) {
    this.showData = false;
    window.location.href = this.paginationRedirectionUrl + '/' + newPage;
  }

  getShortBusinessDetails() {
    const reqData = new HttpParams()
      .set('business_id', this.businessId)
      .set('business_city', this.currentCity);

    this.api.get<any>(BASE_URL + '/v1/web/customerService/getShortBusinessDetails',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData })
      .subscribe(
        response => {
          console.log(response);
          if (response != null) {
            this.oneShellHomeDelivery = response.oneshell_home_delivery;
            if (this.oneShellHomeDelivery) {
              this.getBusinessDeliveryProfile();
            }
          }
        },
        err => {
          console.log('error' + err);
        }
      );
  }

  getBusinessDeliveryProfile() {

    if (this.businessId != null) {
      const reqData = new HttpParams()
        .set('business_city', this.currentCity)
        .set('business_id', this.businessId);

      this.api.get<any>(BASE_URL + '/v1/web/customer/cart/getBusinessDeliveryProfile',
        { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
          response => {
            this.deliveryProfile = response;
            this.checkOffline(this.deliveryProfile);
          },
          err => console.error(err)
        );
    }
  }

  checkOffline(businessItem: any) {

    if (businessItem.is_offline) {
      this.isOffline = true;
    }

    if (businessItem.delivery_start_time != null && businessItem.delivery_end_time != null) {

      const currentDate = new Date();

      const dd = String(currentDate.getDate()).padStart(2, '0');
      const mm = String(currentDate.getMonth() + 1).padStart(2, '0');
      const yyyy = currentDate.getFullYear();

      const dateString = yyyy + '-' + mm + '-' + dd + 'T';

      const startDateTimeString = dateString + businessItem.delivery_start_time + ':00';
      const endDateTimeString = dateString + businessItem.delivery_end_time + ':00';

      const startDateTime = new Date(startDateTimeString);
      const endDateTime = new Date(endDateTimeString);

      if (startDateTime.getTime() > endDateTime.getTime()) {
        if (currentDate.getTime() > startDateTime.getTime()) {
          this.isOffline = false;
        } else {
          this.isOffline = true;
        }
      } else if (endDateTime.getTime() > startDateTime.getTime()) {
        if (currentDate.getTime() > startDateTime.getTime() && currentDate.getTime() < endDateTime.getTime()) {
          this.isOffline = false;
        } else {
          this.isOffline = true;
        }
      }
    }
  }

  onSortOptionsChange() {
    this.refreshProductsBasedOnType();
  }

  openPopup(template: TemplateRef<any>) {
    this.currentActiveModalRef = this.modalService.show(template);
  }

}


enum ProductListType {
  Search = 'Search',
  Trending = 'Trending',
  healthProducts = 'Health Products',
  businessSearch = 'Business Search',
  businessTrending = 'Business Trending',
  businessCategoryProducts = 'Business Category Products',
  businessOfferProducts = 'Business Offer Products'
}
