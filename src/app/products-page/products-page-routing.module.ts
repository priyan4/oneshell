import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FeaturedProdsComponent} from './featured-prods/featured-prods.component';
import {ProductsListComponent} from './products-list/products-list.component';

const routes: Routes = [
  { path: 'p/:type/:city', component: FeaturedProdsComponent },
  { path: 'p/:type/:city/:page', component: FeaturedProdsComponent },
  { path: 'o/c/:city/:type/:businessid/:businesscity/:category/:keyword/:level1category/:index/:page', component: ProductsListComponent },
  { path: 't/:city/:type/:businessid/:businesscity/:keyword/:page', component: ProductsListComponent },
  { path: 'c/:city/:type/:businessid/:businesscity/:displayname/:category/:leve3category/:index/:page', component: ProductsListComponent },
  { path: 'ct/:city/:type/:category/:index/:page', component: ProductsListComponent },
  { path: ':city/:type/:category/:keyword/:page', component: ProductsListComponent },
  { path: ':city/:type/:category', component: ProductsListComponent },
  { path: ':city/:type/:category/:keyword/:level/:page', component: ProductsListComponent },
  { path: ':city/:type/:businessid/:businessname/:category/:keyword/:page', component: ProductsListComponent }
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsPageRoutingModule { }
