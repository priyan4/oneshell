import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsPageRoutingModule } from './products-page-routing.module';
import { FeaturedProdsComponent } from './featured-prods/featured-prods.component';
import {NgxPaginationModule} from 'ngx-pagination';
import {ProductsListComponent} from './products-list/products-list.component';
import {SharedModule} from '@app/shared/shared-module.module';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {CoreModule} from '@app/core/core.module';
import {FormsModule} from '@angular/forms';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import {MatCheckboxModule, MatRadioModule, MatSelectModule} from '@angular/material';


@NgModule({
  declarations: [FeaturedProdsComponent, ProductsListComponent],
    imports: [
        CommonModule,
        ProductsPageRoutingModule,
        NgxPaginationModule,
        SharedModule,
        MatButtonModule,
        MatCardModule,
        CoreModule,
        SharedModule,
        FormsModule,
        MatProgressSpinnerModule,
        MatCheckboxModule,
        MatSelectModule,
        MatRadioModule
    ]
})
export class ProductsPageModule { 
  
}
