import { Injectable } from '@angular/core';
import { environment as env } from '@env/environment';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../../services/http.helper';

const BASE_URL = env.serverUrl;

@Injectable({
  providedIn: 'root'
})
export class ProductDataServiceService {

  constructor(private api: MyHttpClient) { }

  getBrandsByProductsCategory(businessCity: string, category: string) {
    const reqData = new HttpParams()
      .set('business_city', businessCity)
      .set('category', category)
      .set('category_level', this.extractCategoryLevel(category));
    return this.api.get<any>(BASE_URL + '/v1/web/customerService/getBrandsByProductsCategory',
    { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData });
  }

  getBrandsByProductsCategoryInBusiness(businessId: string, businessCity: string, category: string) {
    const reqData = new HttpParams()
      .set('business_id', businessId)
      .set('business_city', businessCity)
      .set('category', category)
      .set('category_level', this.extractCategoryLevel(category));
    return this.api.get<any>(BASE_URL + '/v1/web/customerService/getBrandsByProductsCategoryInBusiness',
    { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData });
  }

  private extractCategoryLevel(category: string) {
    if (category) {
      if (category.includes('level1')) { return 'LEVEL1'; }
      if (category.includes('level2')) { return 'LEVEL2'; }
      if (category.includes('level3')) { return 'LEVEL3'; }
    }
  }
}