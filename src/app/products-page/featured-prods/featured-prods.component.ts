import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { Router } from "@angular/router";
import { environment as env } from '@env/environment';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../../services/http.helper';

const BASE_URL = env.serverUrl;

@Component({
  selector: 'app-featured-prods',
  templateUrl: './featured-prods.component.html',
  styleUrls: ['./featured-prods.component.scss']
})
export class FeaturedProdsComponent implements OnInit {

  prodList: Array<any> = [];
  nextToken: number = 1;
  isFullyLoaded: boolean = false;
  paginationSize: number = 10;
  requestedPage: string = '';
  categories: Array<String> = [];
  city: string;
  type: string;
  customer_id: string;
  config: any;
  pageNumber: number;
  pageStart: number;
  pageSize: number;
  pageEnd: number;
  totalItems: number;

  baseUrl: string = env.webUrl;
  showData: boolean = true;

  constructor(
    private route: ActivatedRoute,
    private api: MyHttpClient,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
  }

  ngOnInit() {
    this.prodList = [];
    this.categories.push("health_care_level1");
    this.customer_id = "c0000000000000";

    this.pageSize = 12;
    this.pageNumber = parseInt(((this.activatedRoute.snapshot.paramMap.get("page") != null) ? this.activatedRoute.snapshot.paramMap.get("page") : '1'));

    this.city = this.activatedRoute.snapshot.paramMap.get("city");
    this.type = this.activatedRoute.snapshot.paramMap.get("type");

    this.getPremiumProductCount();

    if ("General" === this.type) {
      this.getProdADVList();
    } else {
      this.getHealthProdADVList();
    }
  }

  getPremiumProductCount() {

    const params = new HttpParams();
    const reqData = params
      .set("business_city", this.city)
      .set("category", this.type);

    this.api.get<any>(BASE_URL + '/v1/web/homePage/getProductAdvCount',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
        response => {
          this.totalItems = response.total_count;

          this.config = {
            currentPage: this.pageNumber,
            itemsPerPage: this.pageSize,
            totalItems: this.totalItems
          };

          this.pageStart = 1 + (this.pageNumber - 1) * this.pageSize;

          if ((this.totalItems - this.pageStart) > this.pageSize)
            this.pageEnd = (this.pageNumber * this.pageSize);
          else
            this.pageEnd = this.totalItems;
        }
      );
  }

  pageChange(newPage: number) {
    this.showData = false;
    if ("General" === this.type) {
      window.location.href = this.baseUrl + '/products/p/General/' + this.city + '/' + newPage;
    } else {
      window.location.href = this.baseUrl + '/products/p/Health/' + this.city + '/' + newPage;
    }
  }

  getProdADVList() {

    let data: any;

    data = {
      target_audience_city: this.city,
      page_number: this.pageNumber,
      page_size: 12,
      //below is not being used
      customer_gender: "All",
      //below is not being used
      customer_age: 30,
      query: 0,
      customer_id: this.customer_id,
      category: "General",
      is_home_page: false
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    this.api.post<any>(BASE_URL + "/v1/web/homePage/getPremiumProducts", data, httpOptions).subscribe(
      response => {

        console.log(response);
        this.prodList = response.product_list;
        for (let i = 0; i < this.prodList.length; i++) {
          this.prodList[i]['urlProductName'] = this.prodList[i]['name'].replace(/\s+/g, "-").replace(/[^a-zA-Z0-9-]/g, "");
        }

      }, (err) => {
        console.log('error ' + err);
        //show no data found message
      })
  }

  getHealthProdADVList() {
    let data: any;

    data = {
      target_audience_city: this.city,
      page_number: this.pageNumber,
      page_size: 12,
      //below is not being used
      customer_gender: "All",
      //below is not being used
      customer_age: 30,
      query: 0,
      customer_id: this.customer_id,
      category: "Health",
      is_home_page: false
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    this.api.post<any>(BASE_URL + "/v1/web/customer/healthCare/getProductAdv", data, httpOptions).subscribe(
      response => {

        console.log(response);
        this.prodList = this.prodList.concat(response.product_list);
        for (let i = 0; i < this.prodList.length; i++) {
          this.prodList[i]['urlProductName'] = this.prodList[i]['name'].replace(/\s+/g, "-").replace(/[^a-zA-Z0-9-]/g, "");
        }

      }, (err) => {
        console.log('error ' + err);
        //show no data found message
      })
  }
}