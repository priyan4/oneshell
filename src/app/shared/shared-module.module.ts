import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidePanelFiltersComponent } from './side-panel-filters/side-panel-filters.component';
import { Ng5SliderModule } from 'ng5-slider';
import { HorizontalCarouselComponent } from './horizontal-carousel/horizontal-carousel.component';
import { NgxImageCompressService } from 'ngx-image-compress';
@NgModule({
  declarations: [SidePanelFiltersComponent, HorizontalCarouselComponent],
  exports: [SidePanelFiltersComponent, HorizontalCarouselComponent],
  imports: [
    CommonModule,
    Ng5SliderModule
  ],
  providers:[NgxImageCompressService]
})
export class SharedModule { }
