import {Component, OnInit, ViewChild, ElementRef, Input, AfterContentInit,AfterViewInit,HostListener} from '@angular/core';

@Component({
  selector: 'app-horizontal-carousel',
  template: `
    <div class="custom-carousel-container">
      <div *ngIf="!hideCarousel" class="left-arrow-btn" (mousedown)="scrollLeft()" (mouseup) ="scrollRelease()" [ngClass]="{'disable-icon': disablePrevious}">
        <i class="fa fa-chevron-left" aria-hidden="true"></i>
      </div>
      <div *ngIf="!hideCarousel" class="right-arrow-btn" (mousedown)="scrollRight()" (mouseup) ="scrollRelease()">
        <i class="fa fa-chevron-right" aria-hidden="true"></i>
      </div>
      <div class="custom-carousel-inner hide-scroll-bar" #panel>
        <ng-content></ng-content>
      </div>
    </div>
  `,
  styleUrls: ['./horizontal-carousel.component.scss']
})
export class HorizontalCarouselComponent implements OnInit, AfterContentInit,AfterViewInit {
  stopScrolling = true;
  @ViewChild('panel', {static: false}) public panel: ElementRef<any>;
  scrollIntervalId: any;
  @Input() slideIdPrefix: string;
  @Input() defaultIndex: number;
  hideCarousel: boolean = false;
  disablePrevious: boolean = true;

  constructor() { }

  ngOnInit() { }
  ngAfterContentInit(): void {
    if (!this.defaultIndex) { return; }
    setTimeout(() => {
      const activeSlide = document.querySelector(`#${this.slideIdPrefix}${this.defaultIndex}`);
      if (!activeSlide) { return; }
      activeSlide.scrollIntoView({ behavior: 'smooth', block: 'nearest', inline: 'start' });
    }, 1000);
  }
  ngAfterViewInit(): void {
    setTimeout(()=> {
      this.showCarousel();
    })
  }
  @HostListener('document:click')
  @HostListener('window:resize')
  onResize() {
    this.showCarousel();
  }
  scrollLeft() {
    this.stopScrolling = false;
    clearInterval(this.scrollIntervalId);
    this.doScrollLeft();

    this.scrollIntervalId = setTimeout(() => {
      this.stopScrolling = true;
    }, 5000);
  }
  doScrollLeft() {
    this.panel.nativeElement.scrollLeft -= 3;
    this.disablePrevIcon();
    setTimeout(() => {
      if(!this.stopScrolling) {
        console.log('scrollibg left')
        this.doScrollLeft();
      }
    }, 10);
  }
  doScrollRight() {
    this.panel.nativeElement.scrollLeft += 3;
    this.disablePrevIcon();
    setTimeout(() => {
      if(!this.stopScrolling) {
        console.log('scrollibg right')
        this.doScrollRight();
      }
    }, 10);
  }
  scrollRight() {
    this.stopScrolling = false;
    clearInterval(this.scrollIntervalId);

    this.doScrollRight();
    this.scrollIntervalId = setTimeout(() => {
      this.stopScrolling = true;
    }, 5000);
  }
  scrollRelease() {
    this.stopScrolling = true;
  }

  showCarousel() {
    if(this.panel && this.panel.nativeElement && this.panel.nativeElement.scrollWidth <= this.panel.nativeElement.offsetWidth) {
      this.hideCarousel = true;
    }
    else {
      this.hideCarousel = false;
    } 
  }
  disablePrevIcon() {
    if(this.panel.nativeElement.scrollLeft > 0) {
      this.disablePrevious = false;
    }
    else {
      this.disablePrevious = true;
    }
  }

}
