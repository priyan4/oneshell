import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Options, LabelType } from 'ng5-slider';

@Component({
  selector: 'app-side-panel-filters',
  templateUrl: './side-panel-filters.component.html',
  styleUrls: ['./side-panel-filters.component.scss']
})
export class SidePanelFiltersComponent implements OnInit {
  @Output() filterChangeEmitter: EventEmitter<any> = new EventEmitter();
  changedFilter: any = {
    filterType: 'date',
    filterMinValue: new Date(),
    filterMaxValue: new Date(),
    filterValue: ''
  }
  minValue: number = 100;
  maxValue: number = 400;
  options: Options = {
    floor: 0,
    ceil: 500,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          return '<b>Min price:</b> $' + value;
        case LabelType.High:
          return '<b>Max price:</b> $' + value;
        default:
          return '$' + value;
      }
    }
  };
  constructor() { }

  ngOnInit() {
    
  }
  handleMinValueChanged($event: any) {
    console.log('min value changed'+ $event);
    this.changedFilter.filterType = 'price';
    this.changedFilter.filterMinValue = $event;
    this.emitChangeInfilter();

  }
  handleMaxValueChanged($event: any) {
    console.log('max value changed'+ $event);
    this.changedFilter.filterType = 'price';
    this.changedFilter.filterMaxValue = $event;
    this.emitChangeInfilter();
  }

  emitChangeInfilter(): void {
    this.filterChangeEmitter.emit(this.changedFilter);
  }
}
