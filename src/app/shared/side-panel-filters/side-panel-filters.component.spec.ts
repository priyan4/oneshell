import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SidePanelFiltersComponent } from './side-panel-filters.component';

describe('SidePanelFiltersComponent', () => {
  let component: SidePanelFiltersComponent;
  let fixture: ComponentFixture<SidePanelFiltersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidePanelFiltersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidePanelFiltersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
