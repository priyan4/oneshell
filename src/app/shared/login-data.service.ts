import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginDataService {
  private showProfile = new BehaviorSubject<boolean>(false);
  showProfileValue = this.showProfile.asObservable();
  private loginPage = new BehaviorSubject<boolean>(false);
  showLoginPageValue = this.loginPage.asObservable();
  constructor() { }
  isRegisteredUser(registered: boolean) {
    this.showProfile.next(registered);
  }

  showLoginPage(show: boolean) {
    this.loginPage.next(show);
  }
}
