import { Injectable } from '@angular/core';
import { Observable, Subject, BehaviorSubject, from } from 'rxjs';
import { AngularFireStorageReference, AngularFireUploadTask, AngularFireStorage } from 'angularfire2/storage';
import { finalize, map } from 'rxjs/operators';
import { NgxImageCompressService } from 'ngx-image-compress';

@Injectable({
  providedIn: 'root'
})
export class FileUploadService {

  fileRef: AngularFireStorageReference;
  task: AngularFireUploadTask;
  uploadProgress: Observable<number>;
  downloadURL: Observable<string>;
  userDetail: any = {};
  secondaryURL = new Subject<any>();

  constructor(
    private afStorage: AngularFireStorage,
    private imageCompress: NgxImageCompressService
  ) { }


  public compressFile(image, fileName, uploadImage, trackLastUpdatedFile) {
    var orientation = -1;
    this.imageCompress.compressFile(image, orientation, 50, 50).then(
      result => {
        uploadImage.nativeElement.src = result;
        let arr = result.split(','),
          mime = arr[0].match(/:(.*?);/)[1],
          bstr = atob(arr[1]),
          n = bstr.length,
          u8arr = new Uint8Array(n);

        while (n--) {
          u8arr[n] = bstr.charCodeAt(n);
        }
        const imageFile = new File([u8arr], fileName, { type: 'image/jpeg' });
        trackLastUpdatedFile = imageFile;
      });
    return trackLastUpdatedFile;
  }

  public compressMultiplefile(image, fileName, trackLastUpdatedFile) {
    var orientation = -1;
    var data = trackLastUpdatedFile;
    this.imageCompress.compressFile(image, orientation, 50, 50).then(
      result => {
        console.log(result);
        data = result;
      });
    return data;
  }


  public uploadImage(trackLastUpdatedFile): Observable<any> {
    let file: any;
    let data = Date.now();
    let metadata = {
      contentType: 'image/jpeg'
    }
    file = trackLastUpdatedFile;

    file.forEach(element => {
      const filePath = `Prescriptions/medical/${data}`;
      this.fileRef = this.afStorage.ref(filePath);
      this.task = this.afStorage.upload(`Prescriptions/medical/${data}`, element, metadata);
      this.uploadProgress = this.task.percentageChanges();
      this.task.snapshotChanges().pipe(
        finalize(() => {
          this.downloadURL = this.fileRef.getDownloadURL();
          this.downloadURL.subscribe(url => {
            if (url) {
              this.secondaryURL.next({ url });
              return url;
            }
          });
        })).subscribe(url => {
          if (url) {
            console.log(url);
          }
        });
    });
    return this.secondaryURL;
  }
}
