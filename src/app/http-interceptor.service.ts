import { Injectable, Inject, PLATFORM_ID, NgZone } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { TransferState, makeStateKey, StateKey } from '@angular/platform-browser';
import { isPlatformServer } from '@angular/common';
import * as memoryCache from 'memory-cache';

@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorService implements HttpInterceptor {
  constructor(private transferState: TransferState, @Inject(PLATFORM_ID) private platformId: any, private ngZone: NgZone) { }

  public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const key: StateKey<string> = makeStateKey<string>(request.url);
    if (isPlatformServer(this.platformId)) {

      // console.log('inside server::' + key);
      // if (request.method === 'GET') {
      //   const cachedData = memoryCache.get(request.url);
      //   if (cachedData) {
      //     this.transferState.set(makeStateKey(request.url), cachedData);
      //     // console.log('got from tranfer state::' + request.url);
      //     return of(new HttpResponse({ body: cachedData, status: 200 }));
      //   }
      // }

      // console.log(JSON.stringify(request) + "server platform server");
      return next.handle(request).pipe(tap((event) => {
        // console.log('saved to tranfer state::' + request.url);
        this.transferState.set(key, (<HttpResponse<any>>event).body);

        // if (request.method === 'GET') {
        //   this.ngZone.runOutsideAngular(() => {
        //     memoryCache.put(request.url, (<HttpResponse<any>>event).body, 1000 * 1800);
        //     // console.log('saved to cache state::' + request.url);
        //   })
        // }
      }));
    } else {

      // console.log('inside browser::' + key);

      const storedResponse = this.transferState.get<any>(key, null);
      // console.log("stored response", storedResponse);
      // console.log(JSON.stringify(request) + "server platform browser");
      if (storedResponse) {
        // console.log('got from tranfer state::' + request.url);
        const response = new HttpResponse({ body: storedResponse, status: 200 });
        this.transferState.remove(key);
        return of(response);
      } else {
        // console.log('nothing got from tranfer state::' + request.url);
        return next.handle(request);
      }
    }
  }
}