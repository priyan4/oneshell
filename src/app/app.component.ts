import { Component, ChangeDetectorRef, PLATFORM_ID, Inject } from '@angular/core';
import { DeviceLocationService } from '@app/core/device-location/device-location.service';
import { LoginDataService } from './shared/login-data.service';
import { Router } from '@angular/router';
import { response } from 'express';
import { isPlatformBrowser } from '@angular/common';

var localMaster = {};
var localStorage = {
  getItem: (itemName: string) => { return localMaster[itemName] },
  setItem: (name: string, item: any) => { localMaster[name] = item },
  clear: () => { localMaster = {} },
};
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  title = 'OneShell';
  showLoginPage: boolean = false;
  loginType: string;
  deviceInfo = null;
  constructor(
    private _router: Router,
    private dl: DeviceLocationService,
    private data: LoginDataService,
    private cd: ChangeDetectorRef,
    @Inject(PLATFORM_ID) private platformId: any
  ) {
    this.sendUserDetailsInCaseOfNewUser(dl);
  }

  sendUserDetailsInCaseOfNewUser(dl: DeviceLocationService) {

    // if (!localStorage.getItem('customer_id')) {

    //   if (!localStorage.getItem('os') || !localStorage.getItem('browser')) {
    //     this.deviceInfo = dl.getDeviceDetails();

    //     let reqBody = {
    //       os: this.deviceInfo.os,
    //       os_version: this.deviceInfo.os_version,
    //       browser: this.deviceInfo.browser,
    //       browser_version: this.deviceInfo.browser_version,
    //       device: this.deviceInfo.device
    //     };

    //     // this.api
    //     //   .post("/v1/web/customer/web/registerAnonymousUser", reqBody)
    //     //   .subscribe(
    //     //     response => {
    //     //       console.log("registartion success: " + response.customer_id);

    //     //       if (response.success) {
    //     //         localStorage.setItem('customer_id', String(response.customer_id))
    //     //         localStorage.setItem('os', this.deviceInfo.os);
    //     //         localStorage.setItem('os_version', this.deviceInfo.os_version);
    //     //         localStorage.setItem('device', this.deviceInfo.device);
    //     //         localStorage.setItem('browser', this.deviceInfo.browser);
    //     //         localStorage.setItem('browser_version', this.deviceInfo.browser_version);
    //     //       }
    //     //     },
    //     //     err => {
    //     //       console.log("error" + err);
    //     //     }
    //     //   );
    //   }
    // }

    // if (localStorage.getItem('customer_id')) {

    //   console.log("customer id found");

    //   if (!localStorage.getItem('latitude') || !localStorage.getItem('longitude')) {

    //     dl.getLocation().subscribe(coordinates => {

    //       let reqBody = {
    //         latitude: coordinates.coords.latitude,
    //         longitude: coordinates.coords.longitude,
    //         customer_id: localStorage.getItem('customer_id')
    //       };

    //       // this.api
    //       //   .post("/v1/web/customer/web/updateAnonymousUser", reqBody)
    //       //   .subscribe(
    //       //     response => {
    //       //       console.log("updation success: " + response.success);

    //       //       if (response.success) {
    //       //         localStorage.setItem('latitude', coordinates.coords.latitude);
    //       //         localStorage.setItem('longitude', coordinates.coords.longitude);
    //       //       }
    //       //     },
    //       //     err => {
    //       //       console.log("error" + err);
    //       //     }
    //       //   );
    //     });
    //   }
    // }
  }

  ngOnInit() {

    // console.log(this.cookies.get("session-id") + "---------------");
    this.data.showLoginPageValue.subscribe(res => {
      if (res)
        this.loginTypeHandler('login');
    });
    // this.checkuser();
    this.disableRightClick();
  }

  loginTypeHandler(type) {
    this.showLoginPage = true
    this.loginType = type;
    this.cd.detectChanges();
  }
  hideloginPageHandler(event) {
    this.showLoginPage = event;
  }
  private disableRightClick() {
    if (isPlatformBrowser(this.platformId)) {
      $(document).ready(function () {
        $('body').bind('cut copy paste', function (e) { e.preventDefault(); });
        $("body").on("contextmenu", function (e) { return false; });
      });
    }
  }

}
