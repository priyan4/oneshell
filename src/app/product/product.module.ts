import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductRoutingModule } from './product-routing.module';
import {ProductDetailsComponent} from '@app/product/product-details/product-details.component';
import {ProductsPageRoutingModule} from '@app/products-page/products-page-routing.module';
import {NgxPaginationModule} from 'ngx-pagination';
import {SharedModule} from '@app/shared/shared-module.module';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {FormsModule} from '@angular/forms';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatCheckboxModule, MatRadioModule, MatSelectModule} from '@angular/material';
import {CoreModule} from '@app/core/core.module';
import {NgbPopoverModule} from '@ng-bootstrap/ng-bootstrap';
import {OsShareButtonModule} from '@app/os-share-button/os-share-button.module';


@NgModule({
  declarations: [ProductDetailsComponent],
  imports: [
    CommonModule,
    ProductRoutingModule,
    NgxPaginationModule,
    SharedModule,
    MatButtonModule,
    MatCardModule,
    CoreModule,
    FormsModule,
    MatProgressSpinnerModule,
    MatCheckboxModule,
    MatSelectModule,
    MatRadioModule,
    OsShareButtonModule,
    NgbPopoverModule
  ]
})
export class ProductModule { }
