import { Component, ElementRef, OnDestroy, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { environment as env } from '@env/environment';
import { BusinessDetailsService } from '@app/business-page/services/business-details.service';
import { ContextDataService } from '@app/services/context-data.service';
import { CartService } from '@app/services/cart.service';
import { takeWhile, distinctUntilChanged, switchMap, tap, debounceTime, filter } from 'rxjs/operators';
import { Subject, Subscription } from 'rxjs';
import { LoginService } from '@app/login/login.service';
import { StoreCartButtonComponent } from '@app/core/components/store-cart-button/store-cart-button.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DeviceDetectorService } from 'ngx-device-detector';
import { NgForm } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LocationComponent } from '@app/pages/location/location.component';

import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { SelfOrderCartButtonComponent } from '@app/core/components/self-order-cart-button/self-order-cart-button.component';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../../services/http.helper';
import {SOCIAL_MEDIA_SHARE_ICONS} from '../../../icons';
import { FaIconLibrary } from '@fortawesome/angular-fontawesome';

const BASE_URL = env.serverUrl;

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss'],
  host: {
    '(document:click)': 'handleSearchChange($event)',
  },
})
export class ProductDetailsComponent implements OnInit, OnDestroy {

  product: any;
  imagesList: any = [];
  filteredImagesList = [];

  business_city: string;
  business_id: string;
  category: string;
  product_id: string;
  product_name: string;
  city: string;
  businessName: string;
  type = 'Type';
  showBredCrum: boolean;

  sharableSocialMediaPlatforms = ['facebook', 'twitter', 'linkedin', 'google',
    'telegram', 'messenger', 'whatsapp', 'email', 'tumblr'];

  searchTxt = '';
  search$: Subject<any> = new Subject();
  searchResultsList: Array<any> = [];
  url: string;
  typeUrl: string;
  index: number;
  pageNumber: number;
  backTraceCategory: string;
  offerCategoryName: string;
  productMainProperties: any;
  productPriceRange: any;
  selectedProperties: any[] = [];
  variablePriceResponse: any;
  featuredProdsType: string;

  baseUrl: string = env.webUrl;
  addToCartOngoing: boolean;
  isOffline = false;
  deliveryProfile: any;
  isWishListed = false;
  dealModalRef: BsModalRef;
  showDealError = false;

  customer_dellivery_radius: any;
  deliverystatus: string;
  isLocationAvail: boolean;
  deliverstatusaction: string;
  userAddress: string = '';
  message: any;
  subscription: Subscription;
  tableNo: string;
  isSelfOrderAvailable: boolean;
  detailPage :boolean = true;

  private cartChanged$: Subject<any> = new Subject<any>();
  @ViewChild(StoreCartButtonComponent, { static: false }) private storeCartBtn: StoreCartButtonComponent;
  @ViewChild(SelfOrderCartButtonComponent, { static: false }) private selfOrderCartBtn: SelfOrderCartButtonComponent;

  get business_name(): string {
    return this.product && this.product.business_name;
  }
  private destroyed: boolean;

  @ViewChild('selectPropertiesWarning', { static: false }) selectPropertiesWarningTpl: ElementRef;
  shareDealModalRef: BsModalRef;

  @ViewChild('confirmDealDetailsShare', { static: false }) confirmDealDetailsTpl: ElementRef;
  selectPropertiesWarningPopupRef: BsModalRef;

  constructor(private activatedRoute: ActivatedRoute, private api: MyHttpClient,

    private businessDetailsService: BusinessDetailsService,
    private contextData: ContextDataService,
    private modalService: BsModalService,
    private cartService: CartService, private loginService: LoginService,
    private snackBar: MatSnackBar,
    private deviceService: DeviceDetectorService,
    private modalServicengb: NgbModal,
    @Inject(PLATFORM_ID) private platformId: any, library: FaIconLibrary) {
    this.subscription = this.businessDetailsService.getMessage().subscribe(message => {
      if (message) {
        this.placechanged();
      } else {
        console.log('error in Subject');
      }
    });
    library.addIcons(...SOCIAL_MEDIA_SHARE_ICONS);
  }

  ngOnInit() {
    this.business_id = this.activatedRoute.snapshot.paramMap.get('business_id');
    this.business_city = this.activatedRoute.snapshot.paramMap.get('city');
    this.category = this.activatedRoute.snapshot.paramMap.get('category');
    this.product_id = this.activatedRoute.snapshot.paramMap.get('pid');
    this.product_name = this.activatedRoute.snapshot.paramMap.get('name');
    this.type = this.activatedRoute.snapshot.paramMap.get('type');
    this.index = Number(this.activatedRoute.snapshot.paramMap.get('index'));
    this.pageNumber = parseInt(((this.activatedRoute.snapshot.paramMap.get('page') != null) ? this.activatedRoute.snapshot.paramMap.get('page') : '1'));
    this.backTraceCategory = this.activatedRoute.snapshot.paramMap.get('category_type');
    this.offerCategoryName = this.activatedRoute.snapshot.paramMap.get('offer_category_name');
    this.tableNo = this.activatedRoute.snapshot.paramMap.get('tableNo');

    this.isSelfOrderAvailable = this.tableNo != null ? true : false;

    this.featuredProdsType = this.type;

    this.detailPage = true;

    this.getProductDetails();
    this.listenToCartChange();
    if (this.deviceService.isMobile()) {
      this.sharableSocialMediaPlatforms = ['facebook', 'whatsapp', 'telegram', 'messenger',
        'google', 'twitter', 'linkedin', 'email', 'tumblr'];
    }
  }

  getProductDetails() {
    const reqBody = {
      business_id: this.business_id,
      business_city: this.business_city,
      customer_id: this.contextData.customerProfile.customer_id,
      customer_city: this.contextData.customerProfile.customer_city,
      product_id: this.product_id,
      category: this.category,
      business_name: this.product_name
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    this.api.post<any>(BASE_URL + "/v1/web/customerService/getProductDetails", reqBody, httpOptions).subscribe(
      response => {
        this.product = response;

        if (this.product.oneshell_home_delivery && this.product.is_main_properties_available) {
          this.getProductMainProperties();
          this.getProductPriceRange();
        }

        if (this.product.oneshell_home_delivery) {
          this.getBusinessDeliveryProfile();
        }


        this.getProductWished();

        this.product.urlBusinessName = this.product.business_name.replace(/\s+/g, '-').replace(/[^a-zA-Z0-9-]/g, '');
        if (!this.product.urlBusinessName) {
          this.product.urlBusinessName = "";
        }

        if ('Business' === this.type || 'Dynamic Link' === this.type) {
          this.type = null;
          this.businessName = this.product.business_name;
          this.showBredCrum = true;
        } else if ('Business Trending' === this.type) {
          this.type = 'Trending';
          this.showBredCrum = true;
          this.businessName = this.product.business_name;
          this.typeUrl = this.baseUrl + '/products/t/' + this.business_city + '/' + 'Business Trending' + '/' + this.business_id + '/' + this.business_city + '/' + 'Trending Products' + '/' + this.pageNumber;
        } else if ('Business Category Products' === this.type) {
          this.businessName = this.product.business_name;
          this.type = this.product.category_level2.display_name;
          this.showBredCrum = true;
          this.typeUrl = this.baseUrl + '/products/c/' + this.business_city + '/' + 'Business Category Products' + '/' + this.business_id + '/' + this.business_city + '/' + this.product.category_level2.display_name + '/' + this.product.category_level2.name + '/' + this.backTraceCategory + '/' + this.index + '/' + this.pageNumber;
        } else if ('Business Offer Products' === this.type) {
          this.businessName = this.product.business_name;
          this.type = this.backTraceCategory;
          this.showBredCrum = true;
          this.typeUrl = this.baseUrl + '/products/o/c/' + this.business_city + '/' + 'Business Offer Products' + '/' + this.business_id + '/' + this.business_city + '/' + this.offerCategoryName + '/' + this.backTraceCategory + '/' + this.product.category_level1.name + '/' + this.index + '/' + this.pageNumber;
        } else if ('Trending' === this.type) {
          this.type = 'Trending';
          this.showBredCrum = true;
          this.typeUrl = this.baseUrl + '/products/ct/' + this.business_city + '/' + 'Trending' + '/' + this.backTraceCategory + '/' + this.index + '/' + this.pageNumber;
        } else if ('Search' === this.type) {
          this.showBredCrum = false;
        } else if ('Health Products' === this.type) {
          this.showBredCrum = true;
          this.type = 'Healthy Deals';
          this.typeUrl = this.baseUrl + '/products/' + this.business_city + '/' + 'Health Products' + '/' + 'health_care_level1' + '/' + 'Health Products' + '/' + 'LEVEL1' + '/' + this.pageNumber;
        } else if ('Business Search' === this.type) {
          this.showBredCrum = true;
          this.businessName = this.product.business_name;
          this.type = this.backTraceCategory;
          this.typeUrl = this.baseUrl + '/products/' + this.business_city + '/' + 'Business Search' + '/' + this.business_id + '/' + this.businessName + '/' + this.product.category_level3.name + '/' + this.backTraceCategory + '/' + this.pageNumber;
        } else if ('General' === this.type) {
          this.showBredCrum = true;
          this.type = 'Featured';
          this.typeUrl = this.baseUrl + '/products/p/' + this.featuredProdsType + '/' + this.business_city + '/' + this.pageNumber;
        } else if ('Health' === this.type) {
          this.showBredCrum = true;
          this.type = 'Healthy Deals';
          this.typeUrl = this.baseUrl + '/products/p/' + this.featuredProdsType + '/' + this.business_city + '/' + this.pageNumber;
        } else if ('Wish List' === this.type) {
          this.type = 'WishList';
          this.showBredCrum = true;
          this.typeUrl = this.baseUrl + '/profile/wishListBusiness/' + this.business_city + '/' + this.business_id + '/' + this.product.urlBusinessName;
        }


        this.imagesList.push(this.product.image_url);
        console.log(this.imagesList);
        // this.imagesList.splice(0, 0, this.product.image_url);
        Array.prototype.push.apply(this.imagesList, this.product.secondary_image_urls);
        console.log(this.imagesList);
        this.imagesList.filter(url => {
          this.filteredImagesList.push({
            isActive: false,
            imgUrl: url
          });
        });
        if (!this.product.description.length) {
          this.product.description = 'NA';
        }
      },
      err => {
        console.error(err);
      }
    );
  }

  getProductWished() {
    const reqBody = {
      business_id: this.business_id,
      business_city: this.business_city,
      customer_id: this.contextData.customerProfile.customer_id,
      customer_city: this.contextData.customerProfile.customer_city,
      product_id: this.product_id,
      product_category: this.category
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    this.api.post<any>(BASE_URL + "/v1/web/customerService/isCustomerPinnedThisProduct", reqBody, httpOptions).subscribe(
      response => {
        this.isWishListed = response;
      },
      err => {
        console.error(err);
      }
    );
  }

  showAOptions(): boolean {
    const type = this.type && this.type.toLowerCase();
    return ['Business', 'Business Trending', 'Business Category Products', 'Business Offer Products', 'Business Search']
      .map(v => v.toLowerCase())
      .includes(type);
  }

  imageChange(item) {
    this.product.image_url = item;
  }
  selectedItem(item) {
    this.filteredImagesList.forEach(it => it.isActive = false);
    item.isActive = !item.isActive;
  }

  ngOnDestroy(): void {
    this.destroyed = true;
  }

  addSearchListener() {
    this.search$.pipe(
      takeWhile(() => !this.destroyed),
      distinctUntilChanged(),
      switchMap((searchText: string) => {
        this.searchResultsList = [];
        const params = new HttpParams();
        const reqData = params
          .set('keyword', this.searchTxt)
          .set('business_id', this.business_id)
          .set('business_city', this.business_city)
          .set('size', '20');
        this.url = '/v1/web/autoComplete/product/getStringsByBusiness';
        return this.api.get<any>(BASE_URL + this.url,
        { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData });
      })
    ).subscribe((response) => {
      if (response) {
        this.searchResultsList.push(...response);
      }
    });
  }

  handleSearchChange(event) {
    this.search$.next(this.searchTxt);
    if (event.target.id != 'txtSearch') {
      this.searchResultsList = [];
    }
  }

  goBack() {
    window.history.back();
  }

  getProductMainProperties() {
    this.cartService.getProductProperties(this.business_id, this.business_city, this.product.category_level3.name, this.product.product_id, this.selectedProperties).pipe(
      tap(res => this.productMainProperties = res),
      tap(() => {
        this.selectedProperties.forEach((item, index) => {
          if (!item) { return; }
          const mainProperty = this.productMainProperties.find(p => p.title === item.title);
          const propertyValue = mainProperty.property_values.find(pv => pv.value === item.value);
          const isAvailable = propertyValue && propertyValue.is_available;
          if (!isAvailable) {
            this.selectedProperties[index] = undefined;
          }
        });
        this.cleanSelectedProperties();
      })
    ).subscribe();
  }

  getProductPriceRange() {
    this.cartService.getProductPriceRange(this.business_id, this.business_city, this.product.category_level3.name, this.product.product_id).pipe(
      tap(res => this.productPriceRange = res)
    ).subscribe();
  }

  getVariableProductPrice() {
    this.cartService.getVariableProductPrice(this.business_id, this.business_city, this.product.category_level3.name, this.product.product_id, this.selectedProperties).pipe(
      tap(res => this.variablePriceResponse = res)
    ).subscribe();
  }

  onPropertyClicked(title: string, value: string, index: number) {
    if (this.isPropertySelected(title, value)) {
      const i = this.selectedProperties.findIndex(p => p && p.title === title);
      this.selectedProperties[i] = undefined;
    } else {
      const i = this.selectedProperties.findIndex(p => p && p.title === title);
      if (i !== -1) {
        this.selectedProperties[i] = undefined;
      }
      this.selectedProperties.push({ title, value });
    }
    this.cleanSelectedProperties();
    this.getProductMainProperties();
    this.getVariableProductPrice();
  }

  private cleanSelectedProperties() {
    this.selectedProperties = this.selectedProperties.filter(Boolean);
  }

  isPropertySelected(title: string, value: string): boolean {
    return !!this.selectedProperties
      .filter(p => p)
      .filter(p => p.title === title)
      .filter(p => p.value === value).length;
  }


  addToCartWithProperties(product) {
    if (!this.contextData.isUserLoggedIn) {
      this.loginService.openLoginModal();
      return;
    }
    if (!this.variablePriceResponse) {
      this.showSelectPropertiesWarning();
      return;
    }
    this.updateProductQuantity();
  }

  onWishListClicked() {
    if (!this.contextData.isUserLoggedIn) {
      this.loginService.openLoginModal();
      return;
    }
    this.postWishList();
  }

  onWishIconClicked() {
    if (!this.contextData.isUserLoggedIn) {
      this.loginService.openLoginModal();
    } else {
      window.location.href = this.baseUrl + '/profile/wishListBusiness/' + this.product.business_city + '/' + this.product.business_id + '/' + this.product.urlBusinessName + '/' + '1';
    }
  }

  onQuantityChange(product: any) {
    this.cartChanged$.next();
  }

  private listenToCartChange() {
    if (this.isSelfOrderAvailable) {
      this.cartChanged$.pipe(
        debounceTime(1000),
        takeWhile(() => !this.destroyed),
        filter(() => !!this.selfOrderCartBtn),
        tap(() => this.selfOrderCartBtn.refreshBusinessCartCount())
      ).subscribe();
    } else {
      this.cartChanged$.pipe(
        debounceTime(1000),
        takeWhile(() => !this.destroyed),
        filter(() => !!this.storeCartBtn),
        tap(() => this.storeCartBtn.refreshBusinessCartCount())
      ).subscribe();
    }
  }

  private updateProductQuantity() {
    this.addToCartOngoing = true;
    const additionalPropertyList = this.product.additional_property_list ? this.product.additional_property_list.filter(p => p.selected) : [];
    this.product.quantity = 0;
    this.cartService.updateProductQuantity(this.product, 1, this.variablePriceResponse, this.selectedProperties, additionalPropertyList, this.tableNo).pipe(
      tap(() => this.addToCartOngoing = false),
      tap(() => this.cartChanged$.next()),
      tap(() => this.resetProductPropertySelections())
    ).subscribe(res => {
      if (res.success) {
        this.showSuccessMessage('Added 1 Item to Cart');
      }
    }, error => console.error(error));
  }

  private showSelectPropertiesWarning() {
    this.selectPropertiesWarningPopupRef = this.modalService.show(this.selectPropertiesWarningTpl);
  }

  showDealTemplate(template: TemplateRef<any>) {
    if (!this.contextData.isUserLoggedIn) {
      this.dealModalRef = this.modalService.show(template);
    } else {
      this.shareDealModalRef = this.modalService.show(this.confirmDealDetailsTpl);
    }
  }

  hideDealTemplate() {
    this.showDealError = false;
    this.dealModalRef.hide();
  }

  hideShareDealTemplate() {
    this.shareDealModalRef.hide();
  }

  private resetProductPropertySelections() {
    this.variablePriceResponse = undefined;
    this.selectedProperties = [];
    if (this.product.additional_property_list && this.product.additional_property_list.length) {
      this.product.additional_property_list.forEach(p => p.selected = false);
    }
    this.getProductMainProperties();
    this.getVariableProductPrice();
  }

  private showSuccessMessage(message: string) {
    this.snackBar.open(message, '', {
      duration: 2000,
    });
  }

  getBusinessDeliveryProfile() {
    const reqData = new HttpParams()
      .set('business_city', this.business_city)
      .set('business_id', this.business_id);
    this.api.get<any>(BASE_URL + '/v1/web/customer/cart/getBusinessDeliveryProfile',
    { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
      response => {
        this.deliveryProfile = response;
        this.getdistance();
      },
      err => console.error(err)
    );
  }

  checkOffline() {

    if (this.customer_dellivery_radius != undefined && this.customer_dellivery_radius <= this.deliveryProfile.delivery_kms_range) {
      this.isOffline = false;
    } else {
      this.isOffline = true;
    }

    if (!this.isOffline && this.deliveryProfile.is_offline) {
      this.isOffline = true;
    }

    if (!this.isOffline && this.deliveryProfile.delivery_start_time != null && this.deliveryProfile.delivery_end_time != null) {

      const currentDate = new Date();

      const dd = String(currentDate.getDate()).padStart(2, '0');
      const mm = String(currentDate.getMonth() + 1).padStart(2, '0');
      const yyyy = currentDate.getFullYear();

      const dateString = yyyy + '-' + mm + '-' + dd + 'T';

      const startDateTimeString = dateString + this.deliveryProfile.delivery_start_time + ':00';
      const endDateTimeString = dateString + this.deliveryProfile.delivery_end_time + ':00';

      const startDateTime = new Date(startDateTimeString);
      const endDateTime = new Date(endDateTimeString);

      if (startDateTime.getTime() > endDateTime.getTime()) {
        if (currentDate.getTime() > startDateTime.getTime()) {
          this.isOffline = false;
        } else {
          this.isOffline = true;
        }
      } else if (endDateTime.getTime() > startDateTime.getTime()) {
        if (currentDate.getTime() > startDateTime.getTime() && currentDate.getTime() < endDateTime.getTime()) {
          this.isOffline = false;
        } else {
          this.isOffline = true;
        }
      }
    }

    if (this.isOffline) {
      this.deliverystatus = "Currently not delivering to: ";
      this.deliverstatusaction = "CHANGE";
    }
    else {
      this.deliverystatus = " Delivering to: ";
      this.deliverstatusaction = "CHANGE";
    }
    if (!this.isLocationAvail) {
      this.deliverystatus = 'Please provide Delivery location to determine Product /  Service Availability';
      this.deliverstatusaction = 'LOCATION';
      this.isOffline = true;
    }
  }

  checkSelfOrderOffline() {

    if (this.customer_dellivery_radius != undefined && this.customer_dellivery_radius <= this.deliveryProfile.self_order_kms_range) {
      this.isOffline = false;
    } else {
      this.isOffline = true;
    }

    if (this.isOffline) {
      this.deliverystatus = "Currently not Serving to: ";
      this.deliverstatusaction = "CHANGE";
    }
    else {
      this.deliverystatus = " Serving to: ";
      this.deliverstatusaction = "CHANGE";
    }
    if (!this.isLocationAvail) {
      this.deliverystatus = "Please set your current location to place order";
      this.deliverstatusaction = "LOCATION";
      this.isOffline = true;
    }
  }

  onSubmit(form: NgForm) {
    if (form.value.name === '' || form.value.number === '') {
      this.showDealError = true;
    } else {
      this.showDealError = false;
      this.hideDealTemplate();
      this.postDealDetails(form.value.name, form.value.number);
    }
  }

  onShareAlertClicked() {
    this.hideShareDealTemplate();
    this.postDealDetails('', '');
  }

  postDealDetails(name: string, phNo: string) {
    const reqBody = {
      business_city: this.business_city,
      business_id: this.business_id,
      category: this.product.category_level3.name,
      product_id: this.product.product_id,
      customer_name: name,
      customer_phone_number: phNo
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    this.api.post<any>(BASE_URL + "/v1/web/customerService/enquiry/deal/getProductDeal", reqBody, httpOptions).subscribe(
      response => {
        if (response.success) {
          this.snackBar.open('Successfully Submitted Details!', '', {
            duration: 2000,
          });
        }
      },
      err => {
        console.log('error' + err);
      }
    );
  }

  postWishList() {
    const requestData = {
      customer_id: this.contextData.customerProfile.customer_id,
      customer_city: this.contextData.customerProfile.customer_city,
      business_id: this.business_id,
      business_city: this.business_city,
      product_id: this.product.product_id,
      product_category: this.product.category_level3.name,
      customer_name: this.contextData.customerProfile.name,
      primary_image_url: this.product.image_url,
      product_name: this.product.name
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    this.api.post<any>(BASE_URL + "/v1/web/customerService/productPinRequest", requestData, httpOptions).subscribe(
      response => {
        if(response.success) {
        this.isWishListed = true;
      } else {
        this.snackBar.open("Sorry! Couldn't Wish List. Please try in a while ", '', {
          duration: 2000,
        });
      }
      }, (err) => {
        console.log('error ' + err);
        // show no data found message
      });
  }

  openLocationModal() {
    const modalRef = this.modalServicengb.open(LocationComponent);
  }

  getdistance() {
    let localaddress: any;

    if (isPlatformBrowser(this.platformId)) {
      localaddress = JSON.parse(localStorage.getItem("userAddress"));
    }

    if (localaddress) {
      this.isLocationAvail = true;
      if (localaddress.address != null) {
        this.userAddress = localaddress.address;
      }
      let data: any;
      data = {
        from_latitude: localaddress.latitude,
        from_longitude: localaddress.longitude,
        to_latitude: this.product.latitude,
        to_longitude: this.product.longitude
      };

      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      };

      this.api.post<any>(BASE_URL + "/v1/web/customer/maps/getDistance", data, httpOptions).subscribe(
        response => {
          this.customer_dellivery_radius = response;
          if (this.isSelfOrderAvailable) {
            this.checkSelfOrderOffline();
          } else {
            this.checkOffline();
          }
        }, (err) => {
          console.log('error ' + err);
          // show no data found message
        });
    } else {
      this.isLocationAvail = false;
      if (this.isSelfOrderAvailable) {
        this.checkSelfOrderOffline();
      } else {
        this.checkOffline();
      }
    }
  }

  placechanged() {
    this.getdistance();
  }

  onBusinessNameBreadCrumbClick() {
    if(this.isSelfOrderAvailable){
        window.location.href = this.baseUrl + '/business/so/' + this.business_city + '/' + this.business_id + '/'+ this.product.business_name + '/' + this.tableNo;
    } else {
      window.location.href = this.baseUrl + '/business/' + this.business_city + '/' + this.business_id + '/'+ this.product.business_name;
    }
  }

}
