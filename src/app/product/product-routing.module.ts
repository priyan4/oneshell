import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ProductDetailsComponent} from '@app/product/product-details/product-details.component';

const routes: Routes = [
  {
    path: 'selfOrder/:city/:type/:business_id/:category/:pid/:name/:tableNo',
    component: ProductDetailsComponent
  },
  {
    path: ':city/:type/:business_id/:category/:pid/:name',
    component: ProductDetailsComponent
  },
  {
    path: ':city/:type/:business_id/:category/:pid/:name/:page',
    component: ProductDetailsComponent
  },
  {
    path: ':city/:type/:business_id/:category/:pid/:name/:index/:page',
    component: ProductDetailsComponent
  },
  {
    path: ':city/:type/:business_id/:category/:pid/:name/:category_type/:index/:page',
    component: ProductDetailsComponent
  },
  {
    path: ':city/:type/:business_id/:category/:pid/:name/:offer_category_name/:category_type/:index/:page',
    component: ProductDetailsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductRoutingModule { }
