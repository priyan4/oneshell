import { AppComponent } from './app.component';
import { AppModule } from './app.module';
import { NgModule } from '@angular/core';
import { BrowserModule, BrowserTransferStateModule } from '@angular/platform-browser';
// import { NgxQRCodeModule } from 'ngx-qrcode2';
// import { GoogleMapsModule } from '@angular/google-maps'

@NgModule({
     bootstrap: [AppComponent],
     imports: [
            BrowserModule.withServerTransition({appId: 'app-root'}),
            AppModule,
            BrowserTransferStateModule,
          //   NgxQRCodeModule
          //   GoogleMapsModule
     ]
})
export class AppBrowserModule {}
