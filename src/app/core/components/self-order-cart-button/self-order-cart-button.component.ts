import {Component, Input, OnInit} from '@angular/core';
import {CartService} from '@app/services/cart.service';
import {tap} from 'rxjs/operators';
import { environment as env } from '@env/environment';

@Component({
  selector: 'self-order-cart-button',
  template: `
    <a target="_self" [href]="[baseUrl, 'viewCart', businessCity, businessId, businessName, tableNo, 'selfOrder'].join('/')" class="self-order-cart-button"
       [matBadge]="businessCartCount" matBadgeColor="warn" matBadgeSize="small" matBadgeOverlap="true">
      <img src="../../assets/images/in_store_cart.png" alt="cart" class="img-fluid cart-icon">
    </a>
  `,
  styleUrls: ['./self-order-cart-button.component.scss']
})
export class SelfOrderCartButtonComponent implements OnInit {

  @Input()
  businessId: string;
  @Input()
  businessCity: string;
  @Input()
  businessName: string;
  @Input()
  tableNo: string;
  
  baseUrl: string = env.webUrl;
  businessCartCount: number;
  constructor(private cartService: CartService) { }

  ngOnInit() {
    this.populateBusinessCartCount();
  }

  refreshBusinessCartCount() {
    this.populateBusinessCartCount();
  }

  private populateBusinessCartCount() {
    console.log("calling self order cart count")
    this.cartService.getSelfOrderBusinessCartCount(this.businessId, this.businessCity,this.tableNo).pipe(
      tap(count => this.businessCartCount = count)
    ).subscribe();
  }
}
