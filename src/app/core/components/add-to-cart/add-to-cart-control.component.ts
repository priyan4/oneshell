import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CartService } from '@app/services/cart.service';
import { tap } from 'rxjs/operators';
import { LoginService } from '@app/login/login.service';
import { ContextDataService } from '@app/services/context-data.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'add-to-cart-control',
  styleUrls: ['./add-to-cart-control.component.scss'],
  template: `
      <div class="add-to-cart" *ngIf="!isSelfOrder">
          <button (click)="addToCart()" class="add-to-cart--btn os-btn-primary"
                  *ngIf="!product.quantity"
                  [disabled]="addToCartOngoing" [ngClass]="{'disabled': isDisabled}">
              ADD
          </button>
          <div class="add-to-cart__modify" *ngIf="product.quantity" [ngClass]="{'disabled': isDisabled}">
              <button class="add-to-cart__modify--decrement" (click)="decrement()" [disabled]="addToCartOngoing" [ngClass]="{'disabled': isDisabled}">
                  <i class="fa fa-minus" [ngClass]="{'disabled': isDisabled}"></i>
              </button>
              <input class="add-to-cart__modify--input" type="text" [(ngModel)]="product.quantity" readonly>
              <button class="add-to-cart__modify--increment" (click)="increment()" [disabled]="addToCartOngoing" [ngClass]="{'disabled': isDisabled}">
                  <i class="fa fa-plus" [ngClass]="{'disabled': isDisabled}"></i>
              </button>
          </div>
      </div>

      <div class="add-to-cart" *ngIf="isSelfOrder">
          <button (click)="addToCart()" class="add-to-cart--btn os-btn-primary"
                  *ngIf="!product.self_order_quantity"
                  [disabled]="addToCartOngoing" [ngClass]="{'disabled': isDisabled}">
              ADD
          </button>
          <div class="add-to-cart__modify" *ngIf="product.self_order_quantity" [ngClass]="{'disabled': isDisabled}">
              <button class="add-to-cart__modify--decrement" (click)="decrement()" [disabled]="addToCartOngoing" [ngClass]="{'disabled': isDisabled}">
                  <i class="fa fa-minus" [ngClass]="{'disabled': isDisabled}"></i>
              </button>
              <input class="add-to-cart__modify--input" type="text" [(ngModel)]="product.self_order_quantity" readonly>
              <button class="add-to-cart__modify--increment" (click)="increment()" [disabled]="addToCartOngoing" [ngClass]="{'disabled': isDisabled}">
                  <i class="fa fa-plus" [ngClass]="{'disabled': isDisabled}"></i>
              </button>
          </div>
      </div>
      <div class="toaster-msg px-3 py-2" [ngClass]="showToaster ? 'd-block' : 'd-none' " >
      Quantity changed noticed from other user.
      </div>
      `
})
export class AddToCartControlComponent {
  @Input() isDisabled: boolean;
  @Input() product: any;
  @Input() detailPage: boolean = false;
  @Input() isSelfOrder: boolean;
  @Input() tableNo: string;
  @Output() quantityChanged = new EventEmitter<any>();
  showToaster: boolean = false;
  addToCartOngoing: boolean;

  constructor(private cartService: CartService,
    private loginService: LoginService,
    private contextData: ContextDataService) { }

  addToCart() {
    if (this.contextData.isUserLoggedIn) {
      if (!this.detailPage && this.product.is_main_properties_available) {
        window.location.href = this.product.productNavigationUrl ? this.product.productNavigationUrl : this.product.product_href;
      }
      else {
        this.updateProductQuantity(1);
      }
    }
    else {
      this.loginService.openLoginModal();
    }
  }

  increment() {
    if (this.isSelfOrder) {
      this.updateProductQuantity(this.product.self_order_quantity + 1);
    } else {
      this.updateProductQuantity(this.product.quantity + 1);
    }
  }

  decrement() {
    if (this.isSelfOrder) {
      this.updateProductQuantity(this.product.self_order_quantity - 1);
    } else {
      this.updateProductQuantity(this.product.quantity - 1);
    }
  }

  private updateProductQuantity(newQuantity) {
    this.addToCartOngoing = true;
    if (this.isSelfOrder) {
      this.cartService.updateProductQuantityForSelfOrder(this.product, newQuantity, this.tableNo).pipe(
        tap(res => {
          if (!res.success && res.self_order_quantity !== this.product.self_order_quantity) {
            this.product.self_order_quantity = res.self_order_quantity;
            this.showMessage();
          }
          else {
            this.product.self_order_quantity = newQuantity;
          }
        }),
        tap(() => this.quantityChanged.emit(this.product)),
        tap(() => this.addToCartOngoing = false)
      ).subscribe(res => undefined, error => console.error(error));
    } else {
      this.cartService.updateProductQuantity(this.product, newQuantity).pipe(
        tap(res => this.product.quantity = newQuantity),
        tap(() => this.quantityChanged.emit(this.product)),
        tap(() => this.addToCartOngoing = false)
      ).subscribe(res => undefined, error => console.error(error));
    }
  }

  showMessage(): void {
    this.showToaster = true;
    setTimeout(() => {
      this.showToaster = false;
    }, 5000);
  }
}
