import {Component, Input, OnInit} from '@angular/core';
import {CartService} from '@app/services/cart.service';
import {tap} from 'rxjs/operators';
import { environment as env } from '@env/environment';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'store-cart-button',
  template: `
    <a target="_self" [href]="[baseUrl, 'viewCart', businessCity, businessId].join('/')" class="store-cart-btn"
       [matBadge]="businessCartCount" matBadgeColor="warn" matBadgeSize="small" matBadgeOverlap="true">
      <img src="../../assets/images/in_store_cart.png" alt="cart" class="img-fluid cart-icon">
    </a>
  `,
  styleUrls: ['./store-cart-button.component.scss']
})
export class StoreCartButtonComponent implements OnInit {

  @Input()
  businessId: string;
  @Input()
  businessCity: string;
  baseUrl: string = env.webUrl;
  businessCartCount: number;
  constructor(private cartService: CartService) { }

  ngOnInit() {
    this.populateBusinessCartCount();
  }

  refreshBusinessCartCount() {
    this.populateBusinessCartCount();
  }

  private populateBusinessCartCount() {
    this.cartService.getBusinessCartCount(this.businessId, this.businessCity).pipe(
      tap(count => this.businessCartCount = count)
    ).subscribe();
  }
}
