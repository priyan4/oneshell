import { Injectable } from '@angular/core';
import {ContextDataService} from '@app/services/context-data.service';
import {CanActivate} from '@angular/router';
import {UrlNavigationService} from '@app/services/url-navigation.service';


@Injectable({
  providedIn: 'root'
})
export class UserLoggedInGuard implements CanActivate {

  constructor(private contextData: ContextDataService,
              private urlNavigation: UrlNavigationService) { }
  canActivate(): boolean {
    if (this.contextData.isUserLoggedIn) {
      return true;
    }
    this.urlNavigation.homePage();
  }
}
