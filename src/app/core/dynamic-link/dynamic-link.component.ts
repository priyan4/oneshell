import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponent } from "@app/app.component";
import { environment as env } from '@env/environment';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../../services/http.helper';

const BASE_URL = env.serverUrl;

var localMaster = {};
var localStorage = {
  getItem: (itemName: string) => { return localMaster[itemName] },
  setItem: (name: string, item: any) => { localMaster[name] = item },
  clear: () => { localMaster = {} },
};
@Component({
  selector: 'app-dynamic-link',
  templateUrl: './dynamic-link.component.html',
  styleUrls: ['./dynamic-link.component.scss']
})
export class DynamicLinkComponent implements OnInit {

  private type: string;
  private city: string;
  private shareCode: string;

  constructor(private router: ActivatedRoute, private api: MyHttpClient, private _router: Router, private appComponent: AppComponent) { }

  ngOnInit() {

    this.type = this.router.snapshot.paramMap.get("type");
    this.city = this.router.snapshot.paramMap.get("city");
    this.shareCode = this.router.snapshot.paramMap.get("code");

    if (!localStorage.getItem('customer_id')) {
      localStorage.setItem('customer_id', "c000000000000000");
      localStorage.setItem('city', this.city);
    }

    if (this.type === 'p')
      this.productRedirection();
    else if (this.type === 'b')
      this.businessRedirection();
    else if (this.type === 'rd')
      this.businessRedirection();
    else if (this.type === 'r')
      this.realEstateRedirection();
    else if (this.type === 'e')
      this.eventRedirection();

    console.log(this.router.url);
  }

  productRedirection() {

    let reqBody = {
      city: this.city,
      code: this.shareCode,
      customer_id: localStorage.getItem('customer_id'),
      customer_city: localStorage.getItem('city')
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    this.api.post<any>(BASE_URL + "/dl/web/getProductURL", reqBody, httpOptions).subscribe(
      response => {

        if (response.url)
          this._router.navigate(["/" + response.url]);
      },
      err => {
        console.log("error" + err);
      }
    );
  }

  businessRedirection() {

    let reqBody = {
      city: this.city,
      code: this.shareCode,
      customer_id: localStorage.getItem('customer_id'),
      customer_city: localStorage.getItem('city')
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    this.api.post<any>(BASE_URL + "/dl/web/getBusinessURL", reqBody, httpOptions).subscribe(
      response => {


        if (response.url)
          this._router.navigate(["/" + response.url]);
      },
      err => {
        console.log("error" + err);
      }
    );

  }

  realEstateRedirection() {

    let reqBody = {
      city: this.city,
      code: this.shareCode,
      customer_id: localStorage.getItem('customer_id'),
      customer_city: localStorage.getItem('city')
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    this.api.post<any>(BASE_URL + "/dl/web/getRealEstateURL", reqBody, httpOptions).subscribe(
      response => {


        console.log(response);
        if (response.url)
          this._router.navigate(["/" + response.url]);
      },
      err => {
        console.log("error" + err);
      }
    );

  }

  eventRedirection() {

    let reqBody = {
      city: this.city,
      code: this.shareCode,
      customer_id: localStorage.getItem('customer_id'),
      customer_city: localStorage.getItem('city')
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    this.api.post<any>(BASE_URL + "/dl/web/getEventURL", reqBody, httpOptions).subscribe(
      response => {

        console.log(response);
        if (response.url)
          this._router.navigate(["/" + response.url]);
      },
      err => {
        console.log("error" + err);
      }
    );
  }
}