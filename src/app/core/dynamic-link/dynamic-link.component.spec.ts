import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicLinkComponent } from './dynamic-link.component';

describe('DynamicLinkComponent', () => {
  let component: DynamicLinkComponent;
  let fixture: ComponentFixture<DynamicLinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicLinkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
