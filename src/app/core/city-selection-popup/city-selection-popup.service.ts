import {Inject, Injectable, Optional, PLATFORM_ID} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {CitySelectionPopupComponent} from './city-selection-popup/city-selection-popup.component';
import {CitiesService} from "@app/services/cities.service";
import {isPlatformServer} from "@angular/common";

@Injectable({
  providedIn: 'root'
})
export class CitySelectionPopupService {

  private isPopupOpen = false;

  constructor(@Optional() public dialog: MatDialog, private citiesService: CitiesService, @Inject(PLATFORM_ID) private platformId: any) {}

  openPopup(): void {
    if (isPlatformServer(this.platformId)) { return; }
    if (this.isPopupOpen) { return; }
    this.isPopupOpen = true;
    const dialogRef = this.dialog.open(CitySelectionPopupComponent, {
      width: '80vw',
      maxWidth: '600px',
      height: 'auto',
      maxHeight: '80vh',
      position: {
        top: '80px'
      },
      disableClose: !this.citiesService.activeCity
    });

    const subscription = dialogRef.afterClosed().subscribe(result => {
      this.isPopupOpen = false;
      subscription.unsubscribe();
    });
  }
}
