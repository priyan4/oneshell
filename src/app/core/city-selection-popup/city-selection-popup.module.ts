import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CitySelectionPopupComponent } from './city-selection-popup/city-selection-popup.component';
import {FormsModule} from '@angular/forms';
import {MatDialogModule} from '@angular/material/dialog';
import {MatTooltipModule} from '@angular/material/tooltip';
import {NgbTypeaheadModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [CitySelectionPopupComponent],
  imports: [
    CommonModule,
    FormsModule,
    MatDialogModule,
    MatTooltipModule,
    NgbTypeaheadModule
  ],
  entryComponents: [CitySelectionPopupComponent],
  exports: [CitySelectionPopupComponent]
})
export class CitySelectionPopupModule { }
