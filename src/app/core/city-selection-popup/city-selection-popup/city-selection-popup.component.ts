import {Component, OnInit} from '@angular/core';
import { MatDialogRef} from '@angular/material/dialog';
import {CitiesService, City, CityType} from '@app/services/cities.service';
import {debounceTime, distinctUntilChanged, filter, switchMap, tap} from 'rxjs/operators';
import {UrlNavigationService} from '@app/services/url-navigation.service';
import {Observable} from 'rxjs';
import {TitleCasePipe} from '@angular/common';
import {NgbTypeaheadSelectItemEvent} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-city-selection-popup',
  templateUrl: './city-selection-popup.component.html',
  styleUrls: ['./city-selection-popup.component.scss'],
  providers: [TitleCasePipe]
})
export class CitySelectionPopupComponent implements OnInit {

  showAllCities: boolean;
  popularCities: City[] = [];
  otherCities: City[] = [];
  citySearchText: string;
  areaSearchText: string;
  private allCities: City[] = [];
  private readonly numberOfOtherCitiesShownByDefault = 4;

  get otherCitiesFilteredBasedOnShowAllCitiesFlag() {
    return (this.showAllCities || !this.isOtherCitiesSizeHigherThanDefault) ? this.otherCities
      : this.otherCities.slice(0, this.numberOfOtherCitiesShownByDefault);
  }

  get isOtherCitiesSizeHigherThanDefault() {
    return this.otherCities.length > this.numberOfOtherCitiesShownByDefault;
  }

  get activeCityObject(): City {
    return this.allCities.find(c => c.city_name === this.citiesService.activeCity);
  }

  constructor(public dialogRef: MatDialogRef<any>,
              public citiesService: CitiesService,
              private urlNavigation: UrlNavigationService,
              public titleCasePipe: TitleCasePipe) {}

  ngOnInit() {
    this.populateCities();
  }
  onCitySelection(city: City): void {
    this.setLocationDetails(city.city_name);
    this.sendMessage();
  }
  private closeDialogAndNavigateToHome(cityName: string) {
    this.dialogRef.close();
    this.urlNavigation.homePage(cityName);
  }
  private populateCities() {
    this.citiesService.getCities().pipe(
      tap(cities => this.allCities = cities),
      tap(() => this.popularCities = this.allCities.filter(c => c.type === CityType.popular)),
      tap(() => this.otherCities = this.allCities.filter(c => c.type === CityType.other))
    ).subscribe();
  }
  searchCity = (text$: Observable<string>) => text$.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      filter(text => !!text),
      switchMap(term => this.citiesService.searchCities(term))
  )

  searchArea = (text$: Observable<string>) => text$.pipe(
    debounceTime(300),
    distinctUntilChanged(),
    filter(text => !!text),
    switchMap(term => this.citiesService.getLocationsByCitySearch(this.citiesService.activeCity, term))
  )

  citySearchResultFormatter = (city: City): string => this.titleCaseFormatter(city && city.city_name);

  titleCaseFormatter = (name: string): string => this.titleCasePipe.transform(name);

  onCitySelectionFromSearchResults = (searchResult: NgbTypeaheadSelectItemEvent): void => searchResult && searchResult.item && this.onCitySelection(searchResult.item);

  onAreaSelectionFromSearchResults = (searchResult: NgbTypeaheadSelectItemEvent): void => searchResult && searchResult.item && this.onAreaSelection(searchResult.item);

  getUrlToDisplay = (city: City): string => this.citiesService.activeCity === city.city_name ? city.selected_image_url : city.image_url;

  showAreaSelection = (): boolean => (this.citiesService.activeCity && (this.activeCityObject && this.activeCityObject.is_locations_enabled));

  private onAreaSelection(areaName: string) {
    this.setLocationDetails(this.citiesService.activeCity, areaName);
  }

  private setLocationDetails(cityName: string, areaName?: string) {
    this.citiesService.setActiveCity(cityName);
    this.citiesService.setActiveArea(areaName);
    this.closeDialogAndNavigateToHome(cityName);
  }

  sendMessage(): void {
    // send message to subscribers via observable subject
    this.citiesService.sendMessage('City is Selected');
}

}

