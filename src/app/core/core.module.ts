import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthRedirectComponent } from './auth-redirect/auth-redirect.component';
import { DynamicLinkComponent } from './dynamic-link/dynamic-link.component';
import {DetailsFooterMobileModule} from './details-footer-mobile/details-footer-mobile.module';
import { StoreCartButtonComponent } from './components/store-cart-button/store-cart-button.component';
import {MatBadgeModule} from '@angular/material/badge';
import {AddToCartControlComponent} from '@app/core/components/add-to-cart/add-to-cart-control.component';
import { SelfOrderCartButtonComponent } from './components/self-order-cart-button/self-order-cart-button.component';

@NgModule({
  declarations: [
    AuthRedirectComponent,
    DynamicLinkComponent,
    StoreCartButtonComponent,
    AddToCartControlComponent,
    SelfOrderCartButtonComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    DetailsFooterMobileModule,
    FormsModule,
    MatBadgeModule
  ],
  exports: [
    StoreCartButtonComponent,
    AddToCartControlComponent,
    SelfOrderCartButtonComponent
  ]
})
export class CoreModule { }
