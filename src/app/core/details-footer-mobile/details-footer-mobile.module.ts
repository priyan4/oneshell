import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SpamModalComponent} from '@app/core/details-footer-mobile/modals/spam-modal/spam-modal.component';
import {DetailsFooterMobileComponent} from '@app/core/details-footer-mobile/details-footer-mobile.component';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {FormsModule} from '@angular/forms';
import {MatSnackBarModule} from '@angular/material/snack-bar';

@NgModule({
  declarations: [DetailsFooterMobileComponent,
    SpamModalComponent],
  imports: [
    CommonModule,
    MatInputModule,
    MatFormFieldModule,
    FormsModule,
    MatSnackBarModule
  ],
  exports:  [DetailsFooterMobileComponent,
    SpamModalComponent],
  entryComponents: [SpamModalComponent]
})
export class DetailsFooterMobileModule { }
