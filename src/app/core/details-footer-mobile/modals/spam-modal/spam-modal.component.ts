import {Component} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {ContextDataService} from '@app/services/context-data.service';
import {filter} from 'rxjs/operators';
import {MatSnackBar} from '@angular/material/snack-bar';
import { environment as env } from '@env/environment';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../../../../services/http.helper';

const BASE_URL = env.serverUrl;

@Component({
  selector: 'app-spam-modal',
  template: `
    <div class="modal-header">
      <h5><strong>Report Spam</strong></h5>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('close-spam')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <mat-form-field class="w-100">
        <mat-label>Leave a comment</mat-label>
        <textarea [(ngModel)]="additionalComments" matInput placeholder="Leave a comment"></textarea>
      </mat-form-field>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-secondary" (click)="activeModal.dismiss('close-spam')">CANCEL</button>
      <button type="button" class="btn os-btn-primary" (click)="saveSpam()">SUBMIT</button>
    </div>
  `,
  styleUrls: ['./spam-modal.component.scss']
})
export class SpamModalComponent {
  additionalComments: string;
  constructor(private api: MyHttpClient, public activeModal: NgbActiveModal,
              private contextData: ContextDataService,
              private snackBar: MatSnackBar) { }

  saveSpam() {
    const payload = {
      customer_id: this.contextData.customerProfile.customer_id,
      city: this.contextData.customerProfile.customer_city,
      business_id: this.contextData.businessProfile.business_id,
      business_city: this.contextData.businessProfile.business_city,
      comments: this.additionalComments
    };
    
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    this.api.post<any>(BASE_URL + "/v1/customer/spamRequest/reportBusiness", payload, httpOptions).pipe(
      filter(res => res.success)
    ).subscribe(res => {
      this.showSuccessMessage();
      this.activeModal.close();
    });
  }
  showSuccessMessage() {
    this.snackBar.open('Reported Spam', '',{
      duration: 2000,
    });
  }

}

