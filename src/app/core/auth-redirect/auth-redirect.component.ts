import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-auth-redirect',
  templateUrl: './auth-redirect.component.html',
  styleUrls: ['./auth-redirect.component.scss']
})
export class AuthRedirectComponent implements OnInit {

  constructor(private activatedRoute: ActivatedRoute) {
    const routeFragment: Observable<string> = activatedRoute.fragment;
    routeFragment.subscribe(fragment => {
      let token: string = fragment.match(/^(.*?)&/)[1].replace('access_token=', '');
      console.log('login from facebook = '+token);
     // this.tokenService.setToken(token);
    });

  }

  ngOnInit() {
  }

}
