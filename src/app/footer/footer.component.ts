import { Component, OnInit } from '@angular/core';
import { environment as env } from '@env/environment';
import {CitiesService} from '@app/services/cities.service';
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  city: string;
  baseUrl: string = env.webUrl;

  quickLinkFactory = {
    stores: (city) => `/stores/${city}`,
    services: (city) => `/service/${city}`,
    movies: (city) => `/movies/${city}/now_showing`,
    home_services: (city) => `/service/h/${city}`,
    real_estates: (city) => `/realEstate/${city}/buy`,
  };

  constructor(private citiesService: CitiesService) { }

  ngOnInit() {
    this.city = this.citiesService.activeCity;
  }
  openQuickLink(key: string) {
    window.open(this.quickLinkFactory[key](this.citiesService.activeCity), '_self');
  }
}
