 export class ShowHeaderConfigObj {
  routeString: string;
  title: string;
  showMainHeader: boolean;
  showThinHeader: boolean;
  showShareButton: boolean;
  showNotifButton: boolean;
}
// all thin header configuration done below
 export const HeaderConfigList: Array<ShowHeaderConfigObj> = [
  {
    routeString: '/movie',
    showMainHeader: false,
    showThinHeader: false,
    title: 'Movie',
    showShareButton: false,
    showNotifButton: false
  },
  {
    routeString: '/listingPage',
    showMainHeader: false,
    showThinHeader: true,
    title: 'Details',
    showShareButton: false, // demo, change it to false when moving to production
    showNotifButton: false // demo, change it to false when moving to production
  }
];
