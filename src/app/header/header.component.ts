import { CookieService } from 'ngx-cookie-service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, Output, EventEmitter, OnDestroy, Inject, PLATFORM_ID } from '@angular/core';
import { Subject } from 'rxjs';
import { distinctUntilChanged, switchMap, takeWhile } from 'rxjs/operators';
import { Router, Event, NavigationStart, NavigationEnd, NavigationError } from '@angular/router';
import { HeaderConfigList, ShowHeaderConfigObj } from './header.constants';
import { Location } from '@angular/common';
import { environment as env } from '@env/environment';
import { CitiesService } from '@app/services/cities.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ScannerModalComponent } from '@app/scanner/scanner-modal/scanner-modal.component';
import { LoginService } from '@app/login/login.service';
import { ContextDataService } from '@app/services/context-data.service';
import { CartService } from "@app/services/cart.service";
import { CitySelectionPopupService } from "@app/core/city-selection-popup/city-selection-popup.service";
import { UrlNavigationService } from "@app/services/url-navigation.service";
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../services/http.helper';

const BASE_URL = env.serverUrl;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  host: {
    '(document:click)': 'handleSearchChange($event)',
  },
})
export class HeaderComponent implements OnInit, OnDestroy {

  @Output() loginType = new EventEmitter();
  searchResultsList: Array<any> = [];
  activeSearchMenu = 'business';
  searchTxt = '';
  url: string;
  search$: Subject<any> = new Subject();
  activeCity: string;
  registered: boolean;
  showThinHeader: boolean;
  thinHeaderTitle: string;
  showShareButton: boolean;
  showNotifButton: boolean;
  baseUrl: string = env.webUrl;
  private destroyed: boolean;
  orderUrl: string = this.baseUrl + '/orders/general/1';
  cartUrl: string = this.baseUrl + '/viewCart';

  get isUserLoggedIn(): boolean {
    return this.contextData.isUserLoggedIn;
  }
  get customerName(): string {
    return this.contextData.customerProfile && this.contextData.customerProfile.name;
  }
  get completeCartCount$() {
    return this.cartService.completeCartCount$;
  }
  constructor(
    private api: MyHttpClient,
    private router: Router,
    private locationRef: Location,
    @Inject(PLATFORM_ID) private platformId: any,
    private activatedRoute: ActivatedRoute,
    private cookieService: CookieService,
    public citiesService: CitiesService,
    private modalService: NgbModal,
    private loginService: LoginService,
    private contextData: ContextDataService,
    private cartService: CartService,
    private citySelectionPopup: CitySelectionPopupService,
    private urlNavigator: UrlNavigationService
  ) { }

  ngOnInit() {
    this.populateActiveCity();
    this.initRouterEvents();
    this.addSearchListener();
  }

  addSearchListener() {

    this.search$.pipe(
      takeWhile(() => !this.destroyed),
      distinctUntilChanged(),
      switchMap((searchText: string) => {
        this.searchResultsList = [];
        const params = new HttpParams();
        const reqData = params.set('city', this.activeCity)
          .set('keyword', this.searchTxt)
          .set('size', '20');

        if (this.activeSearchMenu === 'product') {
          this.url = '/v1/autoComplete/product/getStrings';
        } else if (this.activeSearchMenu === 'business') {
          this.url = '/v1/autoComplete/business/getStrings';
        }
        return  this.api.get<any>(BASE_URL + this.url,
        { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData });
      })).subscribe((response) => {

        response.forEach(element => {
          if (element.category_type == 'brands') {
            element.keyword_original = element.keyword;
            element.keyword = element.keyword + ' in Brands';
          } else if (element.category_type == 'business_name') {
            element.keyword_original = element.keyword;
            element.keyword = element.keyword + ' in Business';
          } else if (element.category_type == 'category') {
            element.keyword_original = element.keyword;
            element.keyword = element.keyword + ' in Category';
          } else if (element.category_type == 'service') {
            element.keyword_original = element.keyword;
            element.keyword = element.keyword + ' in Services';
          } else if (element.category_type == 'hotelService') {
            element.keyword_original = element.keyword;
            element.keyword = element.keyword + ' in Hotel Service';
          } else if (element.category_type == 'tags') {
            element.keyword_original = element.keyword;
            element.keyword = element.keyword;
          }

          if (this.activeSearchMenu == 'business') {
            let keyword = '';
            if (element.category_type != null && element.category_type === 'category') {
              keyword = element.attribute1;
            } else {
              keyword = element.keyword_original;
            }

            if (keyword === element.keyword_original) {
              element.href_link = `${this.baseUrl}/business/s/${this.activeCity}/${element.category_type}/${keyword && keyword.replace(/[^\w\s]/gi, '')}/1`;
            } else {
              element.href_link = this.baseUrl + "/business/s/" + this.activeCity + '/' + element.category_type + '/' + keyword + '/' + element.keyword_original.replace(/[^\w\s]/gi, '') + '/' + '1';
            }

          } else if (this.activeSearchMenu == 'product') {
            element.href_link = this.baseUrl + "/products/" + this.activeCity + '/' + 'Search' + '/' + element.category + '/' + element.keyword.replace(/[^\w\s]/gi, '') + '/' + '1';
          }
          this.searchResultsList.push(element);
        });
      });
  }

  handleChangeTab(tab: string) {
    this.activeSearchMenu = tab;
    this.searchResultsList = [];
  }
  loginHandler(loginType) {
    this.loginType.emit(loginType);
  }
  handleSearchChange(event) {
    this.search$.next(this.searchTxt);
    if (event.target.id != "searchInputTxt") // or some similar check
    {
      this.searchResultsList = [];
    }
  }
  ngOnDestroy() {
    this.destroyed = true;
  }
  private populateActiveCity() {
    this.activeCity = this.citiesService.activeCity;
    if (!this.activeCity) {
      // setTimeout(() => this.citySelectionPopup.openPopup(), 2000);
    }
  }

  openScannerModal() {
    const modalRef = this.modalService.open(ScannerModalComponent);
  }
  openLoginModal() {
    this.loginService.openLoginModal();
  }
  // thin header functions start
  goBack() {
    this.locationRef.back();
  }
  initRouterEvents() {
    this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationStart) {
        // Show loading indicator
      }
      if (event instanceof NavigationEnd) {
        this.showThinHeader = false;
        // Hide loading indicator
        const url = event.url;
        HeaderConfigList.forEach((conf: ShowHeaderConfigObj) => {
          if (url.indexOf(conf.routeString) >= 0) {
            this.showThinHeader = conf.showThinHeader;
            this.thinHeaderTitle = conf.title;
            this.showShareButton = conf.showShareButton;
            this.showNotifButton = conf.showNotifButton;
          }
        });
      }
      if (event instanceof NavigationError) {
        // Hide loading indicator
        // Present error to user
        console.log(event.error);
      }
    });
  }
  doAction(action: string) {
    switch (action) {
      case 'initiate-share':
        // ok to write small logics or minimal api calls here. if code is huge write a separate service or component and
        //send events through observables in header.service.ts(not created yet)/features's own service
        break;
      case 'show-notif':
        break;
    }
  }
  onCityClick() {
    this.citySelectionPopup.openPopup();
  }

  onOrdersClicked() {
    if (this.contextData.isUserLoggedIn) {
      window.location.href = this.orderUrl;
    } else {
      this.openLoginModal();
    }
  }

  onCartClicked() {
    if (this.contextData.isUserLoggedIn) {
      window.location.href = this.cartUrl;
    } else {
      this.openLoginModal();
    }
  }
}
