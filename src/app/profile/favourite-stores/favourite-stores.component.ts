import { HttpParams, HttpHeaders } from "@angular/common/http";
import { Component, OnInit, ViewChild, ElementRef, TemplateRef } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { environment as env } from '@env/environment';
import { ContextDataService } from '@app/services/context-data.service';
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { NgForm } from '@angular/forms';
import { MatSnackBar } from "@angular/material/snack-bar";
import { MyHttpClient } from '../../services/http.helper';

const BASE_URL = env.serverUrl;

@Component({
  selector: 'app-favourite-stores',
  templateUrl: './favourite-stores.component.html',
  styleUrls: ['./favourite-stores.component.scss']
})
export class FavouriteStoresComponent implements OnInit {

  storeList: Array<any> = [];
  nextToken: number = 1;
  isFullyLoaded: boolean = false;
  paginationSize: number = 10;
  requestedPage: string = '';
  categories: Array<String> = [];
  days: any = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
  city: string;
  baseUrl: string = env.webUrl;

  dealModalRef: BsModalRef;
  showDealError: boolean = false;
  chosenStore: any;

  @ViewChild('confirmDealDetailsShare', { static: false }) confirmDealDetailsTpl: ElementRef;
  shareDealModalRef: BsModalRef;

  constructor(
    private route: ActivatedRoute,
    private api: MyHttpClient,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private contextData: ContextDataService,
    private modalService: BsModalService,
    private snackBar: MatSnackBar
  ) {
  }

  ngOnInit() {
    this.city = this.activatedRoute.snapshot.paramMap.get("city");
    this.getBusinessList();
  }

  getBusinessList() {

    const params = new HttpParams();
    const reqData = params
      .set("customer_id", this.contextData.customerProfile.customer_id)
      .set("city", this.contextData.customerProfile.customer_city)
      .set("next_token", String(this.nextToken));

    this.api.get<any>('/v1/web/customerService/getSubscribedBusiness', 
    { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData })
    
    this.api.get<any>(BASE_URL + '/v1/web/customerService/getSubscribedBusiness',
    { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe((response
    ) => {

      var currentDate = new Date();
      var dayName = this.days[currentDate.getDay()];

      var dd = String(currentDate.getDate()).padStart(2, '0');
      var mm = String(currentDate.getMonth() + 1).padStart(2, '0');
      var yyyy = currentDate.getFullYear();

      var dateString = yyyy + '-' + mm + '-' + dd + 'T';
      let timing: Map<String, any>;

      for (let business of response.business_list) {

        // Rating 
        if (business.number_of_feedbacks != 0) {
          var rating = business.total_ratings / business.number_of_feedbacks;
          business.rating_display_number = rating.toFixed(1);
        } else {
          business.rating_display_number = 0;
        }
        business.rating_display_description = '(' + business.number_of_feedbacks + ' reviews)';

        if (business.oneshell_home_delivery) {
          this.checkOffline(business, dateString);
        } else {
          business.show_offline = false;
        }

        timing = business.business_timings;
        for (let [key, value] of Object.entries(timing)) {
          if (key === dayName) {

            let timingObj = value;

            var startDateTimeString = dateString + timingObj.start_time + ':00';
            var endDateTimeString = dateString + timingObj.end_time + ':00';

            let startDateTime = new Date(startDateTimeString);
            let endDateTime = new Date(endDateTimeString);

            if (timingObj.start_time === '24 hrs') {
              business.is_open = 'Open';
              business.timings = '24 hrs';
            } else if (currentDate.getTime() >= startDateTime.getTime() && currentDate.getTime() <= endDateTime.getTime()) {

              if (timingObj.break_end_time === '- -') {
                business.is_open = 'Open';
              } else {

                var breakStartDateTimeString = dateString + timingObj.break_start_time + ':00';
                var breakEndDateTimeString = dateString + timingObj.break_end_time + ':00';

                let breakStartDateTime = new Date(breakStartDateTimeString);
                let breakEndDateTime = new Date(breakEndDateTimeString);

                if (currentDate.getTime() >= breakEndDateTime.getTime() && currentDate.getTime() <= breakStartDateTime.getTime()) {
                  business.is_open = 'Open';
                } else {
                  business.is_open = 'Closed';
                }
              }
              business.timings = startDateTime.toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true }) + ' - ' + endDateTime.toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
            } else {
              business.is_open = 'Closed';
              business.timings = startDateTime.toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true }) + ' - ' + endDateTime.toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
            }
          }
        }
      }
      console.log(response);
      this.storeList = this.storeList.concat(response.business_list);
      for (let i = 0; i < this.storeList.length; i++) {
        this.storeList[i]['urlBusinessName'] = this.storeList[i]['business_name'].replace(/\s+/g, "-").replace(/[^a-zA-Z0-9-]/g, "");
      }
    }, (err) => {
      console.log('error ' + err);
      //show no data found message
    })
  }

  setHeightOfImage() {
    return (window.innerHeight / 4) + 'px';
  }

  checkOffline(businessItem: any, dateString: any) {

    if (businessItem.is_offline) {
      businessItem.show_offline = true;
    }

    if (businessItem.delivery_start_time != null && businessItem.delivery_end_time != null) {
      var startDateTimeString = dateString + businessItem.delivery_start_time + ':00';
      var endDateTimeString = dateString + businessItem.delivery_end_time + ':00';

      var startDateTime = new Date(startDateTimeString);
      var endDateTime = new Date(endDateTimeString);
      var currentDate = new Date();
    
      if (startDateTime.getTime() > endDateTime.getTime()) {
        if (currentDate.getTime() > startDateTime.getTime()) {
          businessItem.show_offline = false;
        } else {
          businessItem.show_offline = true;
        }
      } else if (endDateTime.getTime() > startDateTime.getTime()) {
        if (currentDate.getTime() > startDateTime.getTime() && currentDate.getTime() < endDateTime.getTime()) {
          businessItem.show_offline = false;
        } else {
          businessItem.show_offline = true;
        }
      }
    }
  }

  onStoreClicked(store: any) {
    window.location.href = this.baseUrl + '/business/' + store.business_city + '/' + store.business_id + '/'+ store.business_name;
  }

  showDealTemplate(template: TemplateRef<any>, store: any, event: any) {
    event.stopImmediatePropagation();
    this.chosenStore = store;
    if (!this.contextData.isUserLoggedIn) {
      this.dealModalRef = this.modalService.show(template);
    } else {
      this.shareDealModalRef = this.modalService.show(this.confirmDealDetailsTpl);
    }
  }

  hideDealTemplate() {
    this.showDealError = false;
    this.dealModalRef.hide();
  }

  hideShareDealTemplate() {
    this.shareDealModalRef.hide();
  }

  onSubmit(form: NgForm) {
    if (form.value.name === "" || form.value.number === "") {
      this.showDealError = true;
    } else {
      this.showDealError = false;
      this.hideDealTemplate();
      this.postDealDetails(form.value.name,form.value.number);
    }
  }

  onShareAlertClicked() {
    this.hideShareDealTemplate();
    this.postDealDetails('','');
  }

  postDealDetails(name: string, phNo: string) {
    const reqBody = {
      business_id: this.chosenStore.business_id,
      business_city: this.chosenStore.business_city,
      customer_name: name,
      customer_phone_number: phNo
    };

    const httpOptions = {
      headers: new HttpHeaders({
          'Content-Type': 'application/json'
      })
  };

    this.api.post<any>('/v1/web/customerService/enquiry/deal/getBusinessDeal', reqBody, httpOptions).subscribe(
      response => {
        if(response.success) {
          this.snackBar.open('Successfully Submitted Details!', '', {
            duration: 2000,
          });
        }
      },
      err => {
        console.error(err);
      }
    );
  }
}