import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileComponent } from './profile.component';
import {ProfileListOptionsComponent} from '@app/profile/profile-list-options/profile-list-options.component';
import {FavouriteStoresComponent} from '@app/profile/favourite-stores/favourite-stores.component';
import {WishListComponent} from '@app/profile/wish-list/wish-list.component';
import {WishListBusinessComponent} from '@app/profile/wish-list-business/wish-list-business.component';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '@app/shared/shared-module.module';
import {NgxPaginationModule} from 'ngx-pagination';
import {MatProgressSpinnerModule} from '@angular/material';
import { NgxQRCodeModule } from 'ngx-qrcode2';

@NgModule({
  declarations: [ProfileComponent, ProfileListOptionsComponent,
    FavouriteStoresComponent, WishListComponent, WishListBusinessComponent],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    FormsModule,
    SharedModule,
    NgxPaginationModule,
    MatProgressSpinnerModule,
    NgxQRCodeModule
  ]
})
export class ProfileModule { }
