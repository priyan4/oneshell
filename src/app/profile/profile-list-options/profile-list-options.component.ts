import { environment as env } from '@env/environment';
import {Component, OnInit, TemplateRef} from '@angular/core';
import {ContextDataService} from "@app/services/context-data.service";
import {BsModalRef, BsModalService} from 'ngx-bootstrap';

@Component({
  selector: 'app-profile-list-options',
  templateUrl: './profile-list-options.component.html',
  styleUrls: ['./profile-list-options.component.scss']
})
export class ProfileListOptionsComponent implements OnInit {

  baseUrl: string = env.webUrl;
  showLogoutDialog = false;
  modalRef: BsModalRef;

  constructor(private contextData: ContextDataService,
              private modalService: BsModalService,) { }

  ngOnInit() {
  }

  showLogout(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  hideLogout() {
    this.showLogoutDialog = false;
  }

  requestForLogout() {
    this.contextData.clearCustomerProfile();
    this.hideLogout();
    window.open(`${this.baseUrl}/`, '_self');
  }

}
