import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { environment as env } from '@env/environment';
import { ContextDataService } from '@app/services/context-data.service';
import { tap } from 'rxjs/operators';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../../services/http.helper';

const BASE_URL = env.serverUrl;

@Component({
  selector: 'app-wish-list-business',
  templateUrl: './wish-list-business.component.html',
  styleUrls: ['./wish-list-business.component.scss']
})
export class WishListBusinessComponent implements OnInit {

  prodList: any;
  businessName: string;
  businessCity: string;
  businessId: string;
  baseUrl: string = env.webUrl;

  config: any;
  pageNumber: number;
  pageStart: number;
  pageSize: number;
  pageEnd: number;
  totalItems: number;
  showData: boolean = true;
  paginationSize: number = 10;

  constructor(private activatedRoute: ActivatedRoute,
    private api: MyHttpClient,
    private router: Router, private contextData: ContextDataService
  ) { }

  ngOnInit() {
    this.businessCity = this.activatedRoute.snapshot.paramMap.get("city");
    this.businessId = this.activatedRoute.snapshot.paramMap.get("businessId");
    this.businessName = this.activatedRoute.snapshot.paramMap.get("businessName");
    this.pageNumber = parseInt(((this.activatedRoute.snapshot.paramMap.get("page") != null) ? this.activatedRoute.snapshot.paramMap.get("page") : '1'));
    this.pageSize = 12;
    this.getProductsWishList();
  }

  getProductsWishList() {
    const params = new HttpParams();
    const reqData = params
      .set('business_city', this.businessCity)
      .set('business_id', this.businessId)
      .set('page_number', "1")
      .set('page_size', "100");

    this.api.get<any>(BASE_URL + '/v1/web/customerService/getPinnedProductsPerBusiness',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
        response => {

          this.prodList = response.product_list;
          if(this.prodList != null && this.prodList.length > 0) {
            this.businessName = this.prodList[0]['business_name'];
          for (let i = 0; i < this.prodList.length; i++) {
            this.prodList[i]['urlProductName'] = this.prodList[i]['name'].replace(/\s+/g, "-").replace(/[^a-zA-Z0-9-]/g, "");
          }

          this.totalItems = response.total_count;

          this.config = {
            currentPage: this.pageNumber,
            itemsPerPage: this.pageSize,
            totalItems: this.totalItems
          };

          this.pageStart = 1 + (this.pageNumber - 1) * this.pageSize;

          if ((this.totalItems - this.pageStart) > this.pageSize)
            this.pageEnd = (this.pageNumber * this.pageSize);
          else
            this.pageEnd = this.totalItems;
        }
        }, (err) => {
          console.log('error ' + err);
          //show no data found message
        })
  }

  pageChange(newPage: number) {
    this.showData = false;
    window.location.href = this.baseUrl + '/profile/wishListBusiness/' + this.businessCity + '/' + this.businessId + '/' + this.businessName + '/' + newPage;
  }

  onDeleteClicked(item: any, event: any) {
    event.stopImmediatePropagation();
    const data = {
      business_id: item.business_id,
      business_city: item.business_city,
      customer_id: this.contextData.customerProfile.customer_id,
      city: this.contextData.customerProfile.customer_city,
      category: item.category_level3.name,
      product_id: item.product_id
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    this.api.post<any>(BASE_URL + "/v1/web/customerService/removePinnedProduct", data, httpOptions).pipe(
      tap(() => this.getProductsWishList())
    ).subscribe(response => undefined, err => console.error(err));
  }

  onDeleteAllClicked() {
    const data = {
      business_id: this.businessId,
      business_city: this.businessCity,
      customer_id: this.contextData.customerProfile.customer_id,
      city: this.contextData.customerProfile.customer_city
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    this.api.post<any>(BASE_URL + "/v1/web/customerService/removePinnedProductByBusiness", data, httpOptions).pipe(
      tap(() => this.getProductsWishList())
    ).subscribe(response => undefined, err => console.error(err));
  }

  onWishItemClicked(item: any) {
    window.location.href = this.baseUrl + '/product/' + item.business_city + '/' + 'Wish List' + '/' + item.business_id + '/' + item.category_level3.name + '/' + item.product_id + '/' + item.urlProductName;
  }
}