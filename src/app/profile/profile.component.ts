import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginDataService } from '@app/shared/login-data.service';
import { ContextDataService } from '@app/services/context-data.service';
import { MatSnackBar } from "@angular/material/snack-bar";
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../services/http.helper';
import { environment as env } from '@env/environment';

const BASE_URL = env.serverUrl;

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  value: any;
  elementType = 'url';
  customer_id: string;
  city: string;
  // valuesss = "https://www.letsboot.com/";

  entry = {
    name: '',
    age: 0,
    email: '',
    gender: '',
    referralCount: 0,
    referralCode: '',
  }
  constructor(private api: MyHttpClient,
    private router: Router,
    private data: LoginDataService,
    private activatedRoute: ActivatedRoute,
    private contextData: ContextDataService,
    private snackBar: MatSnackBar) {

  }

  ngOnInit() {
    this.customer_id = this.contextData.customerProfile.customer_id;
    this.city = this.contextData.customerProfile.customer_city;
    this.getProfile();
  }

  getProfile() {
    const params = new HttpParams();
    const reqData = params
      .set("customer_id", this.customer_id)
      .set("city", this.city);

    this.api.get<any>(BASE_URL + '/v1/web/customer/getCustomerProfile',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
        response => {
          if (response) {
            console.log(response);

            this.entry.name = response.name;
            this.entry.age = response.age;
            this.entry.gender = response.gender;
            this.entry.email = response.email_id;
            this.entry.referralCount = response.number_of_referrals;
            this.entry.referralCode = response.referral_code;

            var data = "CUSTOMER_PROFILE" + "::" + this.customer_id + "::" +
              this.city + "::" + this.contextData.customerProfile.name;

            const reqData_key = params.set('data', data);

            this.api.get<any>(BASE_URL + '/v1/web/customerService/getEncryptedData',
              { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData_key }).subscribe(
                response => {

                  console.log(response);
                  this.value = response.encrypted_data;
                },
                err => {
                  console.log("error" + err);
                }
              );
          }
        },
        err => {
          console.log("error" + err);
        }
      );
  }

  updateProfile(value) {

    let formData = {};

    if (value.name != '' && value.name != null && value.name != undefined) {
      formData['name'] = value.name;
    }
    else {
      formData['name'] = this.contextData.customerProfile.name;
    }
    if (value.email != '' && value.email != null && value.email != undefined) {
      formData['email_id'] = value.email;
    }
    else {
      formData['email_id'] = this.contextData.customerProfile.email_id;
    }
    if (value.age != '' && value.age != null && value.age != undefined) {
      formData['age'] = value.age;
    }
    else {
      formData['age'] = this.contextData.customerProfile.age;
    }

    if (value.gender != '' && value.gender != null && value.gender != undefined) {
      formData['gender'] = value.gender;
    } else {
      formData['gender'] = this.contextData.customerProfile.gender;
    }

    formData['city'] = this.city;
    formData['customer_id'] = this.customer_id;

    const reqData = formData

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    this.api.post<any>(BASE_URL + "/v1/web/customer/update", reqData, httpOptions).subscribe(
      response => {
        console.log(response);
        if (response.success) {
          this.snackBar.open("Profile Submitted Successfully", '', {
            duration: 2000,
          });
          this.getProfile();
        }
      },
      err => {
        this.snackBar.open("Sorry! Something went wrong while submitting Profile. Please try in a while", '', {
          duration: 2000,
        });
        console.log("error" + err);
      }
    );

    console.log(formData);
  }

}