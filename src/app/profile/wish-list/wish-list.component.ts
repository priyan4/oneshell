import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { environment as env } from '@env/environment';
import { ContextDataService } from '@app/services/context-data.service';
import { tap } from 'rxjs/operators';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../../services/http.helper';

const BASE_URL = env.serverUrl;

@Component({
  selector: 'app-wish-list',
  templateUrl: './wish-list.component.html',
  styleUrls: ['./wish-list.component.scss']
})
export class WishListComponent implements OnInit {

  wishList: any;
  baseUrl: string = env.webUrl;

  constructor(private activatedRoute: ActivatedRoute,
    private api: MyHttpClient,
    private router: Router, private contextData: ContextDataService
  ) { }

  ngOnInit() {
    this.getWishListBusinesses();
  }


  getWishListBusinesses() {

    const params = new HttpParams();
    const reqData = params
      .set('customer_id', this.contextData.customerProfile.customer_id)
      .set('city', this.contextData.customerProfile.customer_city)
      .set('page_number', "1")
      .set('page_size', "100");

    this.api.get<any>(BASE_URL + '/v1/web/customerService/getPinnedProductsCountByBusiness',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
        response => {

          this.wishList = response.pinned_product_count_list;
          for (let business of this.wishList) {
            business.urlBusinessName = business.business_name.replace(/\s+/g, "-").replace(/[^a-zA-Z0-9-]/g, "");
          }
          console.log(this.wishList);

        }, (err) => {
          console.log('error ' + err);
          //show no data found message
        })
  }

  onDeleteAllClicked(wishItem: any) {
    const data = {
      business_id: wishItem.business_id,
      business_city: wishItem.business_city,
      customer_id: this.contextData.customerProfile.customer_id,
      city: this.contextData.customerProfile.customer_city
    };
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    this.api.post<any>(BASE_URL + "/v1/web/customerService/removePinnedProductByBusiness", data, httpOptions).pipe(
      tap(() => this.getWishListBusinesses())
    ).subscribe(response => undefined, err => console.error(err));
  }

}