import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {ProfileListOptionsComponent} from '@app/profile/profile-list-options/profile-list-options.component';
import {ProfileComponent} from '@app/profile/profile.component';
import {FavouriteStoresComponent} from '@app/profile/favourite-stores/favourite-stores.component';
import {WishListComponent} from '@app/profile/wish-list/wish-list.component';
import {WishListBusinessComponent} from '@app/profile/wish-list-business/wish-list-business.component';

const routes: Routes = [
  { path: 'options', component: ProfileListOptionsComponent },
  { path: 'updateProfile', component: ProfileComponent },
  { path: 'favouriteBusiness', component: FavouriteStoresComponent },
  { path: 'wishList', component: WishListComponent },
  { path: 'wishListBusiness/:city/:businessId/:businessName/:page', component: WishListBusinessComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule { }
