import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RealEstateComponent} from '@app/real-estate/real-estate.component';
import {RealEstateDetailsComponent} from '@app/real-estate/real-estate-details/real-estate-details.component';


const routes: Routes = [
  { path: ':city/:type', component: RealEstateComponent },
  { path: 'd/:city/:id/:name', component: RealEstateDetailsComponent}
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RealEstateRoutingModule { }
