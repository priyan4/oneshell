import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RealEstateRoutingModule } from './real-estate-routing.module';
import { RealEstateComponent } from './real-estate.component';
import { RealEstateDetailsComponent } from '@app/real-estate/real-estate-details/real-estate-details.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '@app/shared/shared-module.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [RealEstateComponent, RealEstateDetailsComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RealEstateRoutingModule,
    SharedModule,
    NgbModule
  ]
})
export class RealEstateModule { }
