import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PagesService } from '@app/pages/pages.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, Subject } from 'rxjs';
import { distinctUntilChanged, switchMap } from 'rxjs/operators';
import { environment as env } from '@env/environment';
import { Title, Meta } from '@angular/platform-browser';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../services/http.helper';

const BASE_URL = env.serverUrl;

@Component({
  selector: 'app-real-estate',
  templateUrl: './real-estate.component.html',
  styleUrls: ['./real-estate.component.scss'],
  host: {
    '(document:click)': 'handleSearchChange($event)',
  },
})
export class RealEstateComponent implements OnInit {

  realEstateList: Array<any> = [];
  buy: boolean = true;
  rent: boolean = false;
  commercial: boolean = false;
  nextToken: number = 1;
  city: string;
  currentTab: string = 'Buy';
  listState: string = '';
  isFullyLoaded: boolean = false;
  paginationSize: number = 10;

  searchTxt: string = '';
  subscription: Subscription;
  search$: Subject<any> = new Subject();
  searchResultsList: Array<any> = [];
  url: string;
  type: string;
  baseUrl: string = env.webUrl;
  title: string = "";
  description: string = "";
  keywords: string = "";

  cityMapping = new Map();

  constructor(private http: HttpClient, private api: MyHttpClient, private activatedRoute: ActivatedRoute, private _pagesService: PagesService, private _router: Router,
    private metaTagService: Meta,
    private titleService: Title) {
    this.cityMapping.set('ballari', 'bellary/ballari');
    this.cityMapping.set('hosapete', 'hospet/hosapete');
  }

  ngOnInit() {

    this.city = this.activatedRoute.snapshot.paramMap.get("city");
    this.type = this.activatedRoute.snapshot.paramMap.get("type");
    this.getRealEstateList(this.type);
    if (this.type === "buy") {
      this.buy = true;
      this.commercial = false;
      this.rent = false;
    } else if (this.type === "rent") {
      this.rent = true;
      this.buy = false;
      this.commercial = false;
    } else if (this.type === "commercial") {
      this.commercial = true;
      this.rent = false;
      this.buy = false;
    }
    this.addSearchListener();
  }

  addPageMeta() {

    this.title = 'Real Estate Properties in ' + this.cityMapping.get(this.city) + ' - OneShell';

    this.description = 'Property for sale in ' + this.cityMapping.get(this.city) + ', ' +
      'Apartmenst for rent/sale in ' + this.cityMapping.get(this.city) + ', ' +
      'house for rent in  ' + this.cityMapping.get(this.city) + ', ' +
      '1BHK, 2BHK, 3BHK house in  ' + this.cityMapping.get(this.city) + ', ' +
      '1BHK, 2BHK, 3BHK Apartment in ' + this.cityMapping.get(this.city) + ', ' +
      'independent house for sale in ' + this.cityMapping.get(this.city) + ', ' +
      'land for sale in  ' + this.cityMapping.get(this.city) + ', ' +
      'Plots for sale in ' + this.cityMapping.get(this.city) + ', '
    '2 bedroom, 1 bedroom, 3 bedroom house in   ' + this.cityMapping.get(this.city) + ', ' +
      '2 bedroom ,1 bedroom, 3 bedroom Apartment in in ' + this.cityMapping.get(this.city);


    this.metaTagService.addTags([
      { name: 'Title', content: this.title },
      { name: 'description', content: this.description },
      { name: 'keywords', content: this.keywords },
      { name: 'robots', content: 'index, follow' },
      { name: 'author', content: 'OneShell' },
      { charset: 'UTF-8' }
    ]);
    this.titleService.setTitle(this.title);
  }

  getRealEstateList(tab: string): void {

    if (tab != this.listState) {
      this.nextToken = 1;
      this.isFullyLoaded = false;
      this.realEstateList = [];
    }

    let reqBody = {
      city: this.city,
      next_token: String(this.nextToken),
      global_type: tab
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    this.api.post<any>(BASE_URL + "/v1/web/customer/realEstate/getRealEstateList", reqBody, httpOptions).subscribe(
      response => {
        console.log(response);
        this.realEstateList = this.realEstateList.concat(response.real_estate_response);
        for (let i = 0; i < this.realEstateList.length; i++) {
          this.realEstateList[i]['urlPropertyName'] = this.realEstateList[i]['property_name'].replace(/\s+/g, "-").replace(/[^a-zA-Z0-9-]/g, "");
          this.keywords = this.keywords + this.realEstateList[i]['property_name'] + ' in ' + this.cityMapping.get(this.city) + ', ';
        }

        if (Number(response.next_token) > 1)
          this.listState = tab;

        if ((Number(response.next_token) - (this.nextToken)) != this.paginationSize)
          this.isFullyLoaded = true;

        this.nextToken = Number(response.next_token);

        this.addPageMeta();
      }, (err) => {
        console.log('error ' + err);
        //show no data found message
      })
  }

  handleSearchChange(event) {
    console.log(this.searchTxt);
    this.search$.next(this.searchTxt);
    if (event.target.id != "txtSearch") // or some similar check
    {
      this.searchResultsList = [];
    }
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  addSearchListener() {

    this.subscription = this.search$.pipe(distinctUntilChanged(), switchMap((searchText: string) => {

      this.searchResultsList = [];

      const params = new HttpParams();
      const reqData = params
        .set("city", this.city)
        .set("keyword", this.searchTxt)
        .set("size", "20");

      return this.api.get<any>(BASE_URL + '/v1/web/customer/realEstate/getStrings',
        { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData });
    })).subscribe((response) => {
      console.log(response);

      response.forEach(element => {
        this.searchResultsList.push(element);
      });
    })
  }

  // handleSearchResultClicked(result: any) {
  //   console.log(result);
  //   this.searchResultsList = [];

  //   this.nextToken = 1;
  //   this.isFullyLoaded = false;
  //   this.realEstateList = [];

  //   let reqBody = {
  //     city: this.city,
  //     next_token: String(this.nextToken),
  //     keyword: result.keyword,
  //     tag: result.tag
  //   };
  //   this.api.post('/v1/web/customer/realEstate/getRealEstateListBySearch', reqBody).subscribe((response
  //   ) => {
  //     console.log(response);
  //     this.realEstateList = this.realEstateList.concat(response.real_estate_response);

  //     if (Number(response.next_token) > 1)
  //       this.listState = '';

  //     if ((Number(response.next_token) - (this.nextToken)) != this.paginationSize)
  //       this.isFullyLoaded = true;

  //     this.nextToken = Number(response.next_token);
  //   }, (err) => {
  //     console.log('error ' + err);
  //     //show no data found message
  //   })

  // }
}