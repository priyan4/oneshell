import { Component, OnInit } from '@angular/core';
import { PagesService } from '@app/pages/pages.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Title, Meta } from '@angular/platform-browser';
import { environment as env } from '@env/environment';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../../services/http.helper';

const BASE_URL = env.serverUrl;

@Component({
  selector: 'app-real-estate-details',
  templateUrl: './real-estate-details.component.html',
  styleUrls: ['./real-estate-details.component.scss']
})
export class RealEstateDetailsComponent implements OnInit {

  city: string;
  real_estate_id: string;
  realEstateDetails: any = {};
  showViewLessDisclaimer: boolean = true;
  showViewMoreDisclaimer: boolean = false;
  customerViews = 0;
  siteList: any;
  shownGroup = null;
  title: string = "";
  description: string = "";
  keywords: string = "";

  cityMapping = new Map();

  get amenities(): any[] {
    return this.realEstateDetails && this.realEstateDetails.amenities;
  }
  get plans(): any[] {
    return this.realEstateDetails && this.realEstateDetails.plan_response;
  }

  constructor(private _pagesService: PagesService, private activatedRoute: ActivatedRoute, private api: MyHttpClient, private _router: Router,
    private metaTagService: Meta,
    private titleService: Title, ) {
    this.cityMapping.set('ballari', 'bellary/ballari');
    this.cityMapping.set('hosapete', 'hospet/hosapete');
  }

  ngOnInit() {

    this.city = this.activatedRoute.snapshot.paramMap.get("city");
    this.real_estate_id = this.activatedRoute.snapshot.paramMap.get("id");

    this.getRealEstateDetails();

  }
  addPageMeta() {

    this.metaTagService.addTags([
      { name: 'Title', content: this.title },
      { name: 'description', content: this.description },
      { name: 'keywords', content: this.keywords },
      { name: 'robots', content: 'index, follow' },
      { name: 'author', content: 'OneShell' },
      { charset: 'UTF-8' }
    ]);
    this.titleService.setTitle(this.title);
  }

  getRealEstateDetails() {

    let reqBody = {
      city: this.city,
      real_estate_id: this.real_estate_id
    };

    console.log('reqBody ' + reqBody);

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    this.api.post<any>(BASE_URL + "/v1/web/customer/realEstate/getRealEstateDetails", reqBody, httpOptions).subscribe(
      response => {
        this.realEstateDetails = response;
        this.title = response.property_name + ' in ' + this.cityMapping.get(this.city) + ' - OneShell';
        this.description = response.property_description + ', ' + this.cityMapping.get(this.city);
        this.keywords = this.title;

        if (this.realEstateDetails.is_site) {
          this.getSiteDetails();
        }
        this.getCustomerReach();
        this.addPageMeta();
      }, (err) => {
        console.log('error ' + err);
        //show no data found message
      });
  }

  getCustomerReach() {
    const params = new HttpParams();
    const reqData = params
      .set("real_estate_id", this.realEstateDetails.real_estate_id)
      .set("city", this.city);

    this.api.get<any>(BASE_URL + '/v1/web/customer/realEstate/getRealEstateCustomerReach',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
        response => {
          this.customerViews = response;
        },
        err => {
          console.log("error" + err);
        }
      );
  }

  getSiteDetails() {
    const params = new HttpParams();
    const reqData = params
      .set("city", this.city)
      .set("real_estate_id", this.real_estate_id);

    this.api.get<any>(BASE_URL + '/v1/web/customer/realEstate/getSiteDetails',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
        response => {
          this.siteList = response;
        },
        err => {
          console.log("error" + err);
        }
      );
  }

  showComma(index: any, list: Array<any>) {
    if (list) {
      return (index < list.length - 1);
    } else {
      return false;
    }
  }

  onViewMoreClicked() {
    this.showViewLessDisclaimer = false;
    this.showViewMoreDisclaimer = true;
  }

  onViewLessClicked() {
    this.showViewMoreDisclaimer = false;
    this.showViewLessDisclaimer = true;
  }

  onSiteDetailPop(group) {
    if (this.isGroupShown(group)) {
      this.shownGroup = null;
    }
  }

  onSiteDetailClicked(group) {
    if (this.isGroupShown(group)) {
      this.shownGroup = null;
    } else {
      this.shownGroup = group;
    }
  }

  isGroupShown(group) {
    return this.shownGroup === group;
  };

}