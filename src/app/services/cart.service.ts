import { Injectable } from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {ContextDataService} from '@app/services/context-data.service';
import {filter, tap} from 'rxjs/operators';
import { environment as env } from '@env/environment';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../services/http.helper';

const BASE_URL = env.serverUrl;

@Injectable({
  providedIn: 'root'
})
export class CartService {

  private _completeCartCount$ = new Subject<number>();
  completeCartCount$ = this._completeCartCount$.asObservable();

  constructor(private api: MyHttpClient, private contextData: ContextDataService) {
    this.contextData.customerProfile$.pipe(
      filter(profile => !!profile),
      tap(profile => this.refreshCompleteCartCount())
    ).subscribe();
  }

  updateProductQuantityForSelfOrder(product: any, newQuantity, tableNo: string) {
    return this.updateProductQuantity(product, newQuantity, null, null, null, tableNo)
  }

  updateProductQuantity(product: any, newQuantity, variablePriceResponse?: any, selectedProperties?: any, additionalPropertyList?: any, tableNo?: string): Observable<any> {
    let prodCategory: string;
    if (product.category_level3 != null) {
      prodCategory = product.category_level3.name;
    } else {
      prodCategory = product.category;
    }
    var additionalPropList: any;
    if (additionalPropertyList != null) {
      additionalPropList = additionalPropertyList;
    } else if (product.additional_property_list) {
      additionalPropList = product.additional_property_list;
    }
    const reqData = {
      business_id: product.business_id,
      business_city: product.business_city,
      product_id: product.product_id,
      quantity: newQuantity,
      customer_id: this.contextData.customerProfile.customer_id,
      customer_city: this.contextData.customerProfile.customer_city,
      brand_name: product.brand_name,
      name: product.name,
      image_url: product.image_url,
      description: product.description,
      is_out_of_stock: product.is_out_of_stock,
      oneshell_home_delivery: product.oneshell_home_delivery,
      business_name: product.business_name,
      customer_name: this.contextData.customerProfile.name,
      category: prodCategory,
      is_main_properties_available: product.is_main_properties_available,
      actual_price_double: product.actual_price_double,
      offer_price_double: product.offer_price_double,
      actual_discount: product.actual_discount,
      additional_property_list: additionalPropertyList,
      property_identifier: undefined
    } as any;
    if (variablePriceResponse) {
      reqData.variable_product_id = variablePriceResponse.variable_product_id;
      reqData.actual_price_double = variablePriceResponse.actual_price;
      reqData.offer_price_double = variablePriceResponse.offer_price;
      reqData.actual_discount = variablePriceResponse.actual_discount;
    }
    if (selectedProperties && selectedProperties.length) {
      reqData.property_identifier = selectedProperties.map(p => p.value).join('');
    }

    var cartesianProperties: any[] = [];
    if (selectedProperties && selectedProperties.length) {
      for (let p of selectedProperties) {
        var cartesianProp = {
          title: p.title,
          value: p.value
        }
        cartesianProperties.push(cartesianProp);
      }
    }

    reqData.chosen_properties = cartesianProperties;

    if (product.property_identifier != null) {
      reqData.property_identifier = product.property_identifier;
    }
    if (product.variable_product_id != null) {
      reqData.variable_product_id = product.variable_product_id;
    }
    if (product.chosen_properties != null && product.chosen_properties.length > 0) {
      reqData.chosen_properties = product.chosen_properties;
    }
    if (tableNo != null) {
      reqData.table_no = tableNo;
      reqData.is_self_order = true;
      reqData.old_quantity = product.self_order_quantity;
    } else {
      reqData.old_quantity = product.quantity;
    }

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.api.post<any>(BASE_URL + "/v1/web/customer/cart/updateProductQuantity", reqData, httpOptions).pipe(
      tap(() => this.refreshCompleteCartCount())
    );
  }

  getBusinessCartCount(businessId: string, businessCity: string) {
    const reqData = new HttpParams()
      .set('business_id', businessId)
      .set('business_city', businessCity)
      .set('customer_id', this.contextData.customerProfile.customer_id)
      .set('customer_city', this.contextData.customerProfile.customer_city);

    return this.api.get<any>(BASE_URL + '/v1/web/customer/cart/getBusinessCartCount',
    { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData });
  }

  refreshCompleteCartCount() {
    this.getCompleteCartCount().subscribe();
  }

  getCompleteCartCount() {
    const reqData = new HttpParams()
      .set('customer_id', this.contextData.customerProfile.customer_id)
      .set('customer_city', this.contextData.customerProfile.customer_city);

    return this.api.get<any>(BASE_URL + '/v1/web/customer/cart/getCompleteCartCount',
    { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).pipe(
      tap(res => this._completeCartCount$.next(res))
    );
  }

  getBusinessCartProducts(businessId: string, businessCity: string) {
    const requestData = {
      customer_id: this.contextData.customerProfile.customer_id,
      customer_city: this.contextData.customerProfile.customer_city,
      business_id: businessId,
      business_city: businessCity
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.api.post<any>(BASE_URL + "/v1/web/customer/cart/getBusinessCartProducts", requestData, httpOptions);
  }

  getProductsCartSubTotal(businessId: string, businessCity: string) {
    const requestData = {
      customer_id: this.contextData.customerProfile.customer_id,
      customer_city: this.contextData.customerProfile.customer_city,
      business_id: businessId,
      business_city: businessCity
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.api.post<any>(BASE_URL + "/v1/web/customer/cart/getBusinessCartSubTotal", requestData, httpOptions);
  }

  verifyCheckout(businessId: string, businessCity: string): Observable<CheckoutStockResponse[]> {
    const requestData = {
      customer_id: this.contextData.customerProfile.customer_id,
      customer_city: this.contextData.customerProfile.customer_city,
      business_id: businessId,
      business_city: businessCity
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.api.post<any>(BASE_URL + "/v1/web/customer/cart/checkoutPerBusiness", requestData, httpOptions);
  }

  deleteCartProduct(product: any, businessId: string, tableNo?: string) {
    const requestData = {
      customer_id: this.contextData.customerProfile.customer_id,
      business_id: businessId,
      product_id: product.product_id,
      variable_product_id: product.variable_product_id,
      quantity: product.quantity,
      total_price: product.actual_price_double,
      table_no: tableNo
    };

    if (tableNo != null) {
      requestData.quantity = product.self_order_quantity;
    } else {
      requestData.quantity = product.quantity;
    }

    var url = "";
    if (tableNo != null) {
      url = '/v1/web/customer/selfOrder/deleteCartProduct';
    } else {
      url = '/v1/web/customer/cart/deleteCartProduct';
    }

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.api.post<any>(BASE_URL + url, requestData, httpOptions).pipe(
      tap(() => this.refreshCompleteCartCount())
    );
  }

  getCheckoutResponse(businessId: string, businessCity: string) {
    const requestData = {
      customer_id: this.contextData.customerProfile.customer_id,
      customer_city: this.contextData.customerProfile.customer_city,
      business_id: businessId,
      business_city: businessCity
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.api.post<any>(BASE_URL + "/v1/web/customer/cart/getCheckoutResponse", requestData, httpOptions);
  }

  getProductProperties(businessId: string, businessCity: string, productCategory: string, productId: string, properties: any[]) {
    properties = properties.filter(p => p);
    const requestData = {
      business_id: businessId,
      business_city: businessCity,
      category: productCategory,
      product_id: productId,
      properties
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.api.post<any>(BASE_URL + "/v1/web/customer/cart/getProductProperties", requestData, httpOptions);
  }

  getProductPriceRange(businessId: string, businessCity: string, productCategory: string, productId: string) {
    const requestData = {
      business_id: businessId,
      business_city: businessCity,
      category: productCategory,
      product_id: productId
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.api.post<any>(BASE_URL + "/v1/web/customer/cart/getProductPriceRange", requestData, httpOptions);
  }

  getVariableProductPrice(businessId: string, businessCity: string, productCategory: string, productId: string, selectedProperties: any) {
    const properties = selectedProperties.filter(p => p).map(p => p.value);
    const requestData = {
      business_id: businessId,
      business_city: businessCity,
      category: productCategory,
      product_id: productId,
      properties
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.api.post<any>(BASE_URL + "/v1/web/customer/cart/getVariableProductPrice", requestData, httpOptions);
  }

  placeOrder(businessId: string, businessCity: string, businessPhoneNumber: string,
    homeDeliveryCheckoutResponse: any, addressType: string, deliveryAddressUI: any,
    paymentMethod: string, deliveryDate: Date, deliveryTime: string ,FirebaseImageArray :any = []) {
    const requestData = {
      business_id: businessId,
      business_city: businessCity,
      customer_id: this.contextData.customerProfile.customer_id,
      customer_city: this.contextData.customerProfile.customer_city,
      delivery_address: this.convertToAddress(deliveryAddressUI),
      payment_method: paymentMethod,
      partner_city: homeDeliveryCheckoutResponse.partner_city,
      partner_profile_id: homeDeliveryCheckoutResponse.partner_profile_id,
      business_phone_number: businessPhoneNumber,
      address_type: addressType,
      delivery_date: deliveryDate && this.prepareDateString(deliveryDate),
      delivery_time: deliveryTime && this.prepareTImeString(deliveryTime),
      is_schedule_enabled: homeDeliveryCheckoutResponse.is_schedule_enabled,
      is_pick_up_enabled: undefined,
      prescription_image_url: FirebaseImageArray
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.api.post<any>(BASE_URL + "/v1/web/customer/cart/placeOrder", requestData, httpOptions);
  }

  private prepareDateString = (d: Date) => `${d.getDate()}/${d.getMonth() + 1}/${d.getFullYear()}`
  private prepareTImeString = (t: string) => t.replace(':', ' : ').replace('am', 'AM').replace('pm', 'PM');

  private convertToAddress(deliveryAddressUI: any) {
    return {
      house_no: deliveryAddressUI.address.line1,
      house_details: deliveryAddressUI.address.line2,
      area: '',
      street_name: '',
      landmark: '',
      city: '',
      customer_name: deliveryAddressUI.name,
      phone_number: deliveryAddressUI.phoneNumber,
      address_type: '',
      latitude: 0.0,
      longitude: 0.0
    };
  }

  // Self Order services::

  getSelfOrderCartProducts(businessId: string, businessCity: string, tableNo: string) {
    const requestData = {
      business_id: businessId,
      business_city: businessCity,
      table_no: tableNo
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.api.post<any>(BASE_URL + "/v1/web/customer/selfOrder/getBusinessCartProducts", requestData, httpOptions);
  }

  getSelfOrderBusinessCartCount(businessId: string, businessCity: string, tableNo: string) {
    const reqData = new HttpParams()
      .set('business_id', businessId)
      .set('business_city', businessCity)
      .set('table_no', tableNo);

    return this.api.get<any>(BASE_URL + '/v1/web/customer/selfOrder/getBusinessCartCount',
    { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData })
  }

  getSelfOrderProductsSubTotal(businessId: string, businessCity: string, tableNo: string) {
    const requestData = {
      business_id: businessId,
      business_city: businessCity,
      table_no: tableNo
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.api.post<any>(BASE_URL + "/v1/web/customer/selfOrder/getBusinessCartSubTotal", requestData, httpOptions);
  }

  getSelfOrderSummaryResponse(businessId: string, businessCity: string, tableNo: string) {
    const requestData = {
      business_id: businessId,
      business_city: businessCity,
      customer_id: this.contextData.customerProfile.customer_id,
      customer_city: this.contextData.customerProfile.customer_city,
      table_no: tableNo
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.api.post<any>(BASE_URL + "/v1/web/customer/selfOrder/getBillSummaryDetails", requestData, httpOptions);
  }

  getSelfOrderPlacedProducts(businessId: string, businessCity: string, tableNo: string) {
    const requestData = {
      business_id: businessId,
      business_city: businessCity,
      table_no: tableNo
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.api.post<any>(BASE_URL + "/v1/web/customer/selfOrder/getPlacedProducts", requestData, httpOptions);
  }

  getSelfOrderId(businessId: string, businessCity: string,tableNo: string) {
    const requestData = {
      business_id: businessId,
      business_city: businessCity,
      table_no: tableNo
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.api.post<any>(BASE_URL + "/v1/web/customer/selfOrder/getOrderId", requestData, httpOptions);
  }

  getCompletedOrder(businessId: string, businessCity: string, businessPhoneNumber: string,tableno: string, orderId:string,partner_ciy:string,partner_id:string) {
    const requestData = {
      customer_id: this.contextData.customerProfile.customer_id,
      customer_city: this.contextData.customerProfile.customer_city,
      customer_name: this.contextData.customerProfile.name,
      business_id: businessId,
      business_city: businessCity,
      partner_city: partner_ciy,
      partner_profile_id: partner_id,
      business_phone_number: businessPhoneNumber,
      order_id: orderId,
      table_no: tableno
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.api.post<any>(BASE_URL + "/v1/web/customer/selfOrder/completedOrder", requestData, httpOptions);
  }

  placeSelfOrder(businessId: string, businessCity: string, businessPhoneNumber: string,tableno: string, orderId:string,partner_ciy:string,partner_id:string) {
    const requestData = {
      business_id: businessId,
      business_city: businessCity,
      customer_id: this.contextData.customerProfile.customer_id,
      customer_city: this.contextData.customerProfile.customer_city,
      partner_city: partner_ciy,
      partner_profile_id: partner_id,
      business_phone_number: businessPhoneNumber,
      customer_phone_number: this.contextData.customerProfile.phone_number,
      customer_name: this.contextData.customerProfile.name,
      table_no: tableno,
      order_id: orderId
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.api.post<any>(BASE_URL + "/v1/web/customer/selfOrder/placeOrder", requestData, httpOptions);
  }
}

interface CheckoutStockResponse {
  product_id: string;
  properties: string[];
  property_identifier: string;
}
