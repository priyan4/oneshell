import {Inject, Injectable, PLATFORM_ID} from '@angular/core';
import {tap} from 'rxjs/operators';
import {LoginService} from '@app/login/login.service';
import {isPlatformBrowser} from '@angular/common';
import {BehaviorSubject, Observable} from 'rxjs';
import {CustomerProfile} from '@app/models/customer-profile.model';

@Injectable({
  providedIn: 'root'
})
export class ContextDataService {
  private _customerProfile: CustomerProfile;
  private _customerProfile$ =  new BehaviorSubject<CustomerProfile>(undefined);
  private readonly KEY_CUSTOMER_PROFILE = 'customerProfile';
  private readonly DEFAULT_CUSTOMER_ID = 'C0000000000';
  private readonly DEFAULT_CUSTOMER_PROFILE = {} as CustomerProfile;

  private _businessProfile: any;
  private _businessProfile$ = new BehaviorSubject(undefined);

  constructor(private loginService: LoginService, @Inject(PLATFORM_ID) private platformId: any) {
    this.populateCustomerProfile();
  }
  get customerProfile(): CustomerProfile {
    return this._customerProfile || this.DEFAULT_CUSTOMER_PROFILE;
  }
  get customerProfile$(): Observable<CustomerProfile> {
    return this._customerProfile$.asObservable();
  }
  get isUserLoggedIn(): boolean {
    return !!(this._customerProfile && Object.keys(this.customerProfile).length);
  }
  get loggedInCustomerId(): string {
    return (this.customerProfile && this.customerProfile.customer_id) || this.DEFAULT_CUSTOMER_ID;
  }
  private populateCustomerProfile() {
    if (isPlatformBrowser(this.platformId)) {
      const customerProfileStr = localStorage.getItem(this.KEY_CUSTOMER_PROFILE);
      if (customerProfileStr) {
        try {
          const profile = JSON.parse(customerProfileStr) as CustomerProfile;
          this.setCustomerProfile(profile);
        } catch (e) {
        }
      }
    }
  }
  private setCustomerProfile(profile: CustomerProfile) {
    this._customerProfile = profile;
    this._customerProfile$.next(this._customerProfile);
  }
  clearCustomerProfile() {
    localStorage.removeItem(this.KEY_CUSTOMER_PROFILE);
    this.setCustomerProfile(this.DEFAULT_CUSTOMER_PROFILE);
  }
  retrieveCustomerProfileByPhoneNumber(phoneNumber: string) {
    return this.loginService.getCustomerProfileByPhoneNumber(phoneNumber).pipe(
      tap(customer => this.setCustomerProfile(customer)),
      tap(customer => localStorage.setItem(this.KEY_CUSTOMER_PROFILE, JSON.stringify(customer)))
    );
  }

  populateBusinessProfile(bp: any) {
    this._businessProfile = bp;
    this._businessProfile$.next(bp);
  }
  get businessProfile() {
    return this._businessProfile;
  }
  get businessProfile$(): Observable<any> {
    return this._businessProfile$.asObservable();
  }
}
