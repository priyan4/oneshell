import {Inject, Injectable, PLATFORM_ID} from '@angular/core';
import {Observable, of, Subject} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import {isPlatformBrowser} from '@angular/common';
import { environment as env } from '@env/environment';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../services/http.helper';

const BASE_URL = env.serverUrl;

@Injectable({
  providedIn: 'root'
})
export class CitiesService {

  private _cities: string[];
  private readonly KEY_ACTIVE_CITY = 'activeCity';
  private readonly KEY_ACTIVE_AREA = 'activeArea';
  private subject = new Subject<any>();
  get activeCity() {
    if (isPlatformBrowser(this.platformId)) {
      return localStorage.getItem(this.KEY_ACTIVE_CITY);
    }
  }
  get activeArea() {
    if (isPlatformBrowser(this.platformId)) {
      return localStorage.getItem(this.KEY_ACTIVE_AREA);
    }
  }
  constructor(private api: MyHttpClient,
              @Inject(PLATFORM_ID) private platformId: any) { }

  getCities$(lookInMemory = true): Observable<string[]> {
    if (lookInMemory && this._cities) {
      return of(this._cities);
    }
    return this.getCities().pipe(
      map(cities => cities.map(c => c.city_name)),
      map(cities => Array.from(new Set<string>(cities))),
      tap(cities => this._cities = cities)
    );
  }

  getCities(): Observable<City[]> {
    return  this.api.get<any>(BASE_URL + '/v1/customers/getCustomerCities',
    { headers: new HttpHeaders({ 'Content-Type': 'application/json' })});
  }

  searchCities(citySearchText: string) {
    const reqData = new HttpParams()
      .set('keyword', citySearchText);
    return  this.api.get<any>(BASE_URL + '/v1/customers/getCitiesBySearch',
    { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData });
  }

  getLocationByCity(city: string): Observable<string[]> {
    const reqData = new HttpParams()
      .set('city', city);
    return  this.api.get<any>(BASE_URL + '/v1/web/customers/getLocationsByCity',
    { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData });
  };

  getLocationsByCitySearch(city: string, locationSearchText: string) {
    const reqData = new HttpParams()
      .set('city', city)
      .set('keyword', locationSearchText);
    return  this.api.get<any>(BASE_URL + '/v1/customers/getLocationsByCitySearch',
    { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData });
  }

  setActiveCity = (cityName: string): void => this.setOrRemoveItem(this.KEY_ACTIVE_CITY, cityName);
  setActiveArea = (areaName: string): void => this.setOrRemoveItem(this.KEY_ACTIVE_AREA, areaName);

  private setOrRemoveItem(key, value) {
    if (isPlatformBrowser(this.platformId)) {
      if (value) {
        localStorage.setItem(key, value);
      } else {
        localStorage.removeItem(key);
      }
    }
  }
  sendMessage(message: string) {
    this.subject.next({ text: message });
}
getMessage(): Observable<any> {
  return this.subject.asObservable();
}
}

export interface City {
  city_name: string;
  image_url: string;
  is_locations_enabled: boolean;
  selected_image_url: string;
  type: CityType;
}

export enum CityType {
  other = 'other',
  popular = 'popular'
}
