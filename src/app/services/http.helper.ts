import { Injectable, Inject, PLATFORM_ID, Injector, Optional } from "@angular/core";
import { isPlatformServer } from '@angular/common';
import { HttpHeaders, HttpParams, HttpClient, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { REQUEST } from '@nguniversal/express-engine/tokens';

interface IHttpOptions {
    body?: any;
    headers?: HttpHeaders | {
        [header: string]: string | string[];
    };
    params?: HttpParams | {
        [param: string]: string | string[];
    };
    reportProgress?: boolean;
    responseType?: 'arraybuffer' | 'blob' | 'json' | 'text';
    withCredentials?: boolean;
}

@Injectable()
export class MyHttpClient extends HttpClient {

    constructor(
        handler: HttpHandler,
        private injector: Injector,
        @Inject(PLATFORM_ID) private platformId: Object,
        @Optional() @Inject(REQUEST) private req: any
    ) {
        super(handler);
    }

    // `first` is either method or httprequest
    // overwrites `request()` from `HttpClient`
    request(first: string | HttpRequest<any>, url?: string, options: IHttpOptions = {}): Observable<any> {
        // ensures headers properties are not null
        if (!options)
            options = {};
        if (!options.headers)
            options.headers = new HttpHeaders();
        if (typeof first !== "string" && !first.headers)
            first = (first as HttpRequest<any>).clone({ headers: new HttpHeaders() });

        // xhr withCredentials flag
        if (typeof first !== "string")
            first = (first as HttpRequest<any>).clone({
                withCredentials: true,
            });
        options.withCredentials = true;

        // if we are server side, then import cookie header from express
        // if (isPlatformServer(this.platformId)) {
        //         const req: any = this.injector.get('REQUEST');
        //         const rawCookies = !!req.headers['cookie'] ? req.headers['cookie'] : '';

        //         if (typeof first !== "string")
        //             first = (first as HttpRequest<any>).clone({ setHeaders: {'cookie': rawCookies} });
        //         options.headers = (options.headers as HttpHeaders).set('cookie', rawCookies);
        //     }
        if (isPlatformServer(this.platformId)) {
            // const req: any = this.injector.get('REQUEST'); --> StaticInjectorError.  Replaced by import of REQUEST
            const rawCookies = !!this.req.headers['cookie'] ? this.req.headers['cookie'] : '';

            // console.log(JSON.stringify(rawCookies) + "-------cookies in http client");
            if (typeof first !== "string")
                first = (first as HttpRequest<any>).clone({ setHeaders: { 'cookie': rawCookies } });

            if (rawCookies) {
                options.headers = (options.headers as HttpHeaders).set('session-id', rawCookies.substring(11));
            }
            // console.log(JSON.stringify(options) + "options -----" + url)
        }
        return super.request(first as (any), url, options);
    }

}