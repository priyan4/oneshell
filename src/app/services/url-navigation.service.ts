import { Injectable } from '@angular/core';
import {environment as env} from '@env/environment';
import {CitiesService} from "@app/services/cities.service";

@Injectable({
  providedIn: 'root'
})
export class UrlNavigationService {
  private baseUrl = env.webUrl;
  constructor(private citiesService: CitiesService) { }

  reload() {
    window.location.reload();
  }

  homePage(activeCity?: string) {
    activeCity = activeCity || this.citiesService.activeCity || '';
    window.location.href = `${this.baseUrl}/${activeCity}`;
  }

  placeOrderPage(businessCity: string, businessId: string) {
    window.location.href = `${this.baseUrl}/viewCart/${businessCity}/${businessId}/placeOrder`;
  }
}
