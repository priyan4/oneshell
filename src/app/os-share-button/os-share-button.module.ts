import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import {ShareButtonsModule} from '@ngx-share/buttons';
import { FaIconLibrary } from '@fortawesome/angular-fontawesome';
import {SOCIAL_MEDIA_SHARE_ICONS} from '../../icons';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AngularFontAwesomeModule,
    ShareButtonsModule
  ],
  exports: [ShareButtonsModule]
})
export class OsShareButtonModule {
  constructor(library: FaIconLibrary) {
    library.addIcons(...SOCIAL_MEDIA_SHARE_ICONS);
  }
}
