import {
    Component,
    OnInit,
    ViewEncapsulation,
    Output,
    EventEmitter
} from '@angular/core';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../services/http.helper';
import { Router, ActivatedRoute } from '@angular/router';
import { LoginDataService } from '@app/shared/login-data.service';
import { environment as env } from '@env/environment';
import { Title, Meta } from '@angular/platform-browser';
import { ContextDataService } from '@app/services/context-data.service';
import { DeviceDetectorService } from 'ngx-device-detector';
import { CitiesService } from '@app/services/cities.service';
import { CitySelectionPopupService } from "@app/core/city-selection-popup/city-selection-popup.service";
import { UrlNavigationService } from "@app/services/url-navigation.service";
import { HOME } from "@app/constants";
import { Observable } from 'rxjs';

const BASE_URL = env.serverUrl;

@Component({
    selector: 'app-home-page',
    templateUrl: './home-page.component.html',
    styleUrls: ['./home-page.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class HomePageComponent implements OnInit {
    @Output() loginType = new EventEmitter();
    justForYou: any;
    subscribeBusiness: any;
    featuredProducts: any;
    featuredProductsList: Array<string> = [];
    featuredProductsMobList: Array<string> = [];
    bestOffers: any;
    getBannerAdvList: any;
    customerActivities: Array<string> = [];
    cityData: any;
    homeFeatures: any[];

    isCouponsAvailable = false;
    isOffersAvailable = false;
    isMoviesAvailable = false;
    isEventsAvailable = false;
    isActivitiesAvailable = false;
    isHealthAvailable = false;
    isFuncHallAvailable = false;
    isRealEstateAvailable = false;
    isHomeServiceAvailable = false;
    isHomeDeliveryAvailable = false;

    couponDisplayName: string;
    offerDisplayName: string;
    moviesDisplayName: string;
    eventsDisplayName: string;
    activitiesDisplayName: string;
    healthDisplayName: string;
    funsHallsDisplayName: string;
    realEstateDisplayName: string;
    homeServiceDisplayName: string;
    homeDeliveryDisplayName: string;

    bannerSetHeight: any = {
        display: 'block',
        width: '100%',
        height: '100%',
        padding: '0px 15px 0px 15px'
    };

    baseUrl: string = env.webUrl;
    city: string;

    get customerId() {
        return this.contextData.loggedInCustomerId;
    }

    get getHomeFeatureCols() {
        if (this.deviceService.isMobile()) {
            return (this.homeFeatures.length + (this.homeFeatures.length % 2)) / 2;
        }
        return this.homeFeatures.length;
    }


    title = 'OneShell - The Local Connect App, Local Business Search, Local Product Search, Local Professionals Search, City Offers, Health, Movies, Events, Activities, Social, Real Estate, Function Halls, Home Service, Shopping, Home Delivery';
    description = 'OneShell - The Local Connect App, India\'s Real Time Local Search, provides Best City Offers, Happening Local Events, Home Service Booking, Home Delivery from Local stores, Movies, Activities, restaurants, Real Estate and Function Halls. You can also View Products From Local Stores, Offers from Local Businesses, Book Home Service from your Service Providers, View Health Tips From Doctors, Restaurant Menu and Todays Special, Real Estate Site Plans along with Booking Status, Real Estate Floor Plans, Function Halls Availability';
    keywords = 'OneShell, local search, local product search, local store search, local service search, local offer search, local business search, food, grocery, medicines, shopping, movies, hotels, services, health, professionals, real estate, function halls availability, function halls availability, health tips, health deals, events, activities, hotels, automobiles, appointments, coupons, search plus services, local businesses, online yellow pages, Local Search directory, city yellow pages';

    constructor(
        private api: MyHttpClient,
        private router: Router,
        private data: LoginDataService,
        private activatedRoute: ActivatedRoute,
        private metaTagService: Meta, private titleService: Title,
        private contextData: ContextDataService,
        private deviceService: DeviceDetectorService,
        private citiesService: CitiesService,
        private citySelectionPopup: CitySelectionPopupService,
        private urlNavigator: UrlNavigationService) {
    }

    ngOnInit() {
        this.checkForRedirectRequest();
        this.populateActiveCity();
        this.addPageMeta();
        this.getFeaturedBusiness();
        this.getFeaturedProducts();
        this.bestOfferInTown();
        this.getBannerAdv();
        this.getMyScheduleActivities();
        this.getJustForYou();
        this.getHomeFeatures();
    }

    private checkForRedirectRequest() {

        // console.log('inside home page...');
        this.city = this.activatedRoute.snapshot.paramMap.get('city');
        // console.log('inside home page...' + this.city);

        if (this.city === 'pageRedirect') {
            var session_id = this.activatedRoute.snapshot.queryParams['session-id'];

            // console.log('inside home page param exist::' + session_id);

            const params = new HttpParams();
            const reqData = params
                .set('session_id', session_id);

            this.api.get<any>(BASE_URL + '/v1/web/customer/getDefaultCity',
                { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
                    response => {
                        // console.log('response::' + response.default_city);
                        this.city = response.default_city;
                        // console.log('inside home page city modified.....' + this.city);
                    },
                    err => {
                        console.log('error' + err);
                    }
                );
        }
    }

    private populateActiveCity() {
        const activeCity = this.citiesService.activeCity;
        if (this.city === HOME || !this.city) {
            if (activeCity) {
                this.urlNavigator.homePage(activeCity);
            } else {
                // this.citySelectionPopup.openPopup();
                this.checkuser();
            }
        } else {
            this.citiesService.setActiveCity(this.city);
            if (this.city !== activeCity) {
                this.urlNavigator.reload();
            }
        }
    }

    checkuser() {
        this.api.get<any>(BASE_URL + '/v1/web/customer/getUserCityFromCookie').subscribe(
            response => {
                if (!response) {
                    console.log("new user");
                    this.router.navigate(["/" + 'selectcity']);
                }
            },
            err => {
                console.log('error' + err);
                this.router.navigate(["/" + 'selectcity']);
            }
        );
    }

    addPageMeta() {

        this.metaTagService.addTags([
            { name: 'Title', content: this.title },
            { name: 'description', content: this.description },
            { name: 'keywords', content: this.keywords },
            { name: 'robots', content: 'index, follow' },
            { name: 'author', content: 'OneShell' },
            { charset: 'UTF-8' }
        ]);
        this.titleService.setTitle(this.title);
    }

    onResize() {
        const ele = document.getElementsByClassName('banner-img');
        const len = document.getElementsByClassName('banner-img').length;
        for (let i = 0; i < len; i++) {
            (ele[i] as HTMLElement).style.display = 'block';
            (ele[i] as HTMLElement).style.height = '100%';
            (ele[i] as HTMLElement).style.width = '100%';
            (ele[i] as HTMLElement).style.padding = '0px 15px 0px 15px';
        }
    }

    trackBySubscibeBusinessID(business: any): string {
        return business.business_id;
    }

    bannerHeight() {
        if (window.innerWidth < 1024) {
            return window.innerHeight / 4 + 'px';
        } else {
            return 'auto';
        }
    }

    getBannerAdv() {
        const params = new HttpParams();
        const reqData = params
            .set('customer_id', this.customerId)
            .set('target_audience_city', this.city);

        this.api.get<any>(BASE_URL + '/v1/web/homePage/getBannerAdv',
            { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
                response => {
                    this.getBannerAdvList = response.banner_list;
                    // console.log('response.banner_list');
                    // console.log(response.banner_list);

                    for (let i = 0; i < this.getBannerAdvList.length; i++) {
                        if (this.getBannerAdvList[i].business_name != null) {
                            this.getBannerAdvList[i].urlBusinessName = this.getBannerAdvList[i].business_name.replace(/\s+/g, '-').replace(/[^a-zA-Z0-9-]/g, '');
                        }
                        if (this.getBannerAdvList[i].property_name != null) {
                            this.getBannerAdvList[i].urlPropertyName = this.getBannerAdvList[i].property_name.replace(/\s+/g, '-').replace(/[^a-zA-Z0-9-]/g, '');
                        }
                        if (this.getBannerAdvList[i].title != null) {
                            this.getBannerAdvList[i].urlEventName = this.getBannerAdvList[i].title.replace(/\s+/g, '-').replace(/[^a-zA-Z0-9-]/g, '');
                        }
                    }

                    for (const banner of this.getBannerAdvList) {
                        if ('business' === banner.banner_type) {
                            banner.href_link = this.baseUrl + '/business/' + banner.business_city + '/' + banner.business_id + '/' + banner.urlBusinessName;
                        } else if ('event' === banner.banner_type) {
                            banner.href_link = this.baseUrl + '/event/' + banner.city + '/' + banner.event_type + '/' + banner.event_id + '/' + banner.urlEventName;
                        } else if ('real_estate' === banner.banner_type) {
                            banner.href_link = this.baseUrl + '/realEstate/d/' + banner.city + '/' + banner.real_estate_id + '/' + banner.urlPropertyName;
                        }
                    }
                },
                err => {
                    console.log('error' + err);
                }
            );
    }

    getFeaturedBusiness() {

        var customer_id = "c0000000000000";
        if (this.contextData.isUserLoggedIn) {
            customer_id = this.contextData.customerProfile.customer_id;
        }

        let data: any;

        data = {
            business_city: this.city,
            //below is not being used
            customer_gender: "All",
            //below is not being used
            customer_age: 30,
            page_number: 1,
            page_size: 12,
            query: 0,
            customer_id: customer_id,
            category: "General",
            is_home_page: true
        };

        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        };
        this.api.post<any>(BASE_URL + "/v1/web/homePage/getPremiumBusiness", data, httpOptions).subscribe(
            response => {
                this.subscribeBusiness = response.business_list;
                for (let i = 0; i < this.subscribeBusiness.length; i++) {
                    this.subscribeBusiness[i].urlBusinessName = this.subscribeBusiness[i].business_name.replace(/\s+/g, '-').replace(/[^a-zA-Z0-9-]/g, '');
                }

            }, (err) => {
                console.log('error ' + err);
                //show no data found message
            })

    }

    getJustForYou() {
        const params = new HttpParams();
        const reqData = params
            .set('business_city', this.city)
            .set('page_number', '1')
            .set('page_size', '10')
            .set('query', '0');


        this.api.get<any>(BASE_URL + '/v1/web/homePage/getRecommendations',
            { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
                response => {
                    // console.log('response.business_list just for you');
                    // console.log(response.business_list);
                    this.justForYou = response.business_list;
                    for (let i = 0; i < this.justForYou.length; i++) {
                        this.justForYou[i].urlBusinessName = this.justForYou[i].business_name.replace(/\s+/g, '-').replace(/[^a-zA-Z0-9-]/g, '');
                    }
                },
                err => {
                    console.log('error' + err);
                }
            );
    }

    getFeaturedProducts() {

        var customer_id = "c0000000000000";
        if (this.contextData.isUserLoggedIn) {
            customer_id = this.contextData.customerProfile.customer_id;
        }

        let data: any;

        data = {
            target_audience_city: this.city,
            page_number: 1,
            page_size: 12,
            //below is not being used
            customer_gender: "All",
            //below is not being used
            customer_age: 30,
            query: 0,
            customer_id: customer_id,
            category: "General",
            is_home_page: false
        };

        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        };
        this.api.post<any>(BASE_URL + "/v1/web/homePage/getPremiumProducts", data, httpOptions).subscribe(
            response => {
                this.featuredProducts = response;
                this.featuredProductsList = this.featuredProducts.product_list;
                for (let i = 0; i < this.featuredProductsList.length; i++) {
                    this.featuredProductsList[i]['urlBusinessName'] = this.featuredProductsList[i]['business_name'].replace(/\s+/g, "-").replace(/[^a-zA-Z0-9-]/g, "");
                }
                this.featuredProductsMobList = this.featuredProductsList.slice(0, 4);
            },
            err => {
                console.log('error' + err);
            }
        );
    }

    bestOfferInTown() {
        const reqBody = {
            target_audience_city: this.city,
            page_number: '1',
            page_size: '10',
            city: this.city,
            customer_id: this.customerId
        };

        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        };
        this.api
            .post<any>(BASE_URL + '/v1/web/customerService/cityOffers/getCityOffers', reqBody, httpOptions)
            .subscribe(
                response => {
                    this.bestOffers = response.offerResponse;
                    for (let i = 0; i < this.bestOffers.length; i++) {
                        this.bestOffers[i].urlBusinessName = this.bestOffers[i].business_name.replace(/\s+/g, '-').replace(/[^a-zA-Z0-9-]/g, '');
                    }
                },
                err => {
                    console.log('error' + err);
                }
            );
    }

    getMyScheduleActivities() {

        // if (localStorage.getItem('customer_id') && localStorage.getItem('city')) {

        const params = new HttpParams();
        const reqData = params
            .set('customer_id', this.customerId)
            .set('customer_city', this.city);

        this.api.get<any>(BASE_URL + '/v1/web/customer/events/getCustomerActivityDetails',
            { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
                response => {
                    this.customerActivities = response;
                    for (let i = 0; i < this.customerActivities.length; i++) {
                        this.customerActivities[i]['urlBusinessName'] = this.customerActivities[i]['business_name'].replace(/\s+/g, "-").replace(/[^a-zA-Z0-9-]/g, "");
                    }
                    // console.log(response);
                },
                err => {
                    console.log('error' + err);
                }
            );
        // }
    }

    getHomeFeatures() {
        const params = new HttpParams();
        const reqData = params
            .set('city', this.city);

        this.api.get<any>(BASE_URL + '/v1/web/homePage/getFeaturesByCity',
            { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
                response => {
                    this.homeFeatures = response;
                    this.homeFeatures.forEach(feature => {
                        feature.display_name = feature.display_name.replace('<br>', ' ');
                        switch (feature.name) {
                            case 'coupons':
                                this.isCouponsAvailable = true;
                                this.couponDisplayName = feature.display_name;
                                break;
                            case 'offers':
                                this.isOffersAvailable = true;
                                this.offerDisplayName = feature.display_name;
                                break;
                            case 'movies':
                                this.isMoviesAvailable = true;
                                this.moviesDisplayName = feature.display_name;
                                break;
                            case 'events':
                                this.isEventsAvailable = true;
                                this.eventsDisplayName = feature.display_name;
                                break;
                            case 'activities':
                                this.isActivitiesAvailable = true;
                                this.activitiesDisplayName = feature.display_name;
                                break;
                            case 'health':
                                this.isHealthAvailable = true;
                                this.healthDisplayName = feature.display_name;
                                break;
                            case 'real_estate':
                                this.isRealEstateAvailable = true;
                                this.realEstateDisplayName = feature.display_name;
                                break;
                            case 'function_halls':
                                this.isFuncHallAvailable = true;
                                this.funsHallsDisplayName = feature.display_name;
                                break;
                            case 'home_service':
                                this.isHomeServiceAvailable = true;
                                this.homeServiceDisplayName = feature.display_name;
                                break;
                            case 'home_delivery':
                                this.isHomeDeliveryAvailable = true;
                                this.homeDeliveryDisplayName = feature.display_name;
                                break;
                        }
                    });
                }, err => console.error(err)
            );
    }
}
