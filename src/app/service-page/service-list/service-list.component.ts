import {
  Component,
  OnInit
} from "@angular/core";

import { Router, ActivatedRoute } from "@angular/router";
import { LoginDataService } from '@app/shared/login-data.service';
import { environment as env } from '@env/environment';
import { ContextDataService } from '@app/services/context-data.service';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../../services/http.helper';

const BASE_URL = env.serverUrl;

declare var $: any;

@Component({
  selector: 'app-productdetails',
  templateUrl: './service-list.component.html',
  styleUrls: ['./service-list.component.scss']
})
export class ServiceListComponent implements OnInit {

  catagoryObj: any;
  businessesByCategory: any;
  subscribeBusiness: any;
  featuredProducts: any;
  featuredProductsList: any;
  featuredProductsMobList: any;
  bestOffers: any;
  getBannerAdvList: any;
  customerActivities: any;
  recentlyBooked: any;
  nextToken: number = 1;
  isFullyLoaded: boolean = false;
  paginationSize: number = 10;

  currentCity: any;
  customer_id: any;
  category: string;
  categoryDisplayName: string;

  baseUrl: string = env.webUrl;

  config: any;
  pageNumber: number;
  pageStart: number;
  pageSize: number;
  pageEnd: number;
  totalItems: number;
  showData: boolean = true;

  constructor(
    private api: MyHttpClient,
    private router: Router,
    private data: LoginDataService,
    private contextDataService: ContextDataService,
    private activatedRoute: ActivatedRoute) {

  }

  ngOnInit() {
    this.currentCity = this.activatedRoute.snapshot.paramMap.get("city");
    this.category = this.activatedRoute.snapshot.paramMap.get("category");
    this.categoryDisplayName = this.activatedRoute.snapshot.paramMap.get("categorydisplayname");
    this.pageNumber = parseInt(((this.activatedRoute.snapshot.paramMap.get("page") != null) ? this.activatedRoute.snapshot.paramMap.get("page") : '1'));
    this.pageSize = 12;

    this.openLoginPage();
    if (this.contextDataService.isUserLoggedIn) {
      this.getRecentlyBookedServices();
    }
    this.getBusinessesByCategory();
  }

  trackBySubscibeBusinessID(business: any): string {
    return business.business_id;
  }

  // If user press 'call' buttion and not logged in
  openLoginPage() {
    this.activatedRoute.queryParams.subscribe(params => {
      let call = params['call'];
      if (call) {
        this.data.showLoginPage(true);
      }

    });
  }

  getRecentlyBookedServices() {
    const params = new HttpParams();
    const reqData = params
      .set("customer_city", this.contextDataService.customerProfile.customer_city)
      .set("customer_id", this.contextDataService.customerProfile.customer_id)
      .set("city", this.currentCity)
      .set("category_name", this.category);

    this.api.get<any>(BASE_URL + '/v1/web/customer/orders/getCategoryRecentlyBookedServices',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
        response => {
          this.recentlyBooked = response;
          console.log(this.recentlyBooked);
          for (let i = 0; i < this.recentlyBooked.length; i++) {
            this.recentlyBooked[i]['urlBusinessName'] = this.recentlyBooked[i].business_name.replace(/\s+/g, "-").replace(/[^a-zA-Z0-9-]/g, "");
            this.recentlyBooked[i]['category_display_name'] = this.categoryDisplayName;
          }
        },
        err => {
          console.log("error" + err);
        }
      );
  }

  getBusinessesByCategory() {

    const data = {
      city: this.currentCity,
      page_number: this.pageNumber,
      page_size: this.pageSize,
      category_name: this.category
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    this.api.post<any>(BASE_URL + "/v1/web/customer/orders/getBusinessesByCategory", data, httpOptions).subscribe(
      response => {
        this.businessesByCategory = response.business_response_list;
        for (let i = 0; i < this.businessesByCategory.length; i++) {
          this.businessesByCategory[i]['urlBusinessName'] = this.businessesByCategory[i].business_name.replace(/\s+/g, "-").replace(/[^a-zA-Z0-9-]/g, "");
          this.businessesByCategory[i]['category_display_name'] = this.categoryDisplayName;
          this.checkOffline(this.businessesByCategory[i]);
        }

        this.totalItems = response.total_count;
        this.config = {
          currentPage: this.pageNumber,
          itemsPerPage: this.pageSize,
          totalItems: this.totalItems
        };

        this.pageStart = 1 + (this.pageNumber - 1) * this.pageSize;

        if ((this.totalItems - this.pageStart) > this.pageSize)
          this.pageEnd = (this.pageNumber * this.pageSize);
        else
          this.pageEnd = this.totalItems;
      },
      err => {
        console.log("error" + err);
      }
    );
  }

  checkOffline(businessItem: any) {

    if (businessItem.is_offline) {
      businessItem.show_offline = true;
    }

    var currentDate = new Date();

    var dd = String(currentDate.getDate()).padStart(2, '0');
    var mm = String(currentDate.getMonth() + 1).padStart(2, '0');
    var yyyy = currentDate.getFullYear();
    var dateString = yyyy + '-' + mm + '-' + dd + 'T';

    if (businessItem.delivery_start_time != null && businessItem.delivery_end_time != null) {
      var startDateTimeString = dateString + businessItem.delivery_start_time + ':00';
      var endDateTimeString = dateString + businessItem.delivery_end_time + ':00';

      var startDateTime = new Date(startDateTimeString);
      var endDateTime = new Date(endDateTimeString);
      var currentDate = new Date();

      if (startDateTime.getTime() > endDateTime.getTime()) {
        if (currentDate.getTime() > startDateTime.getTime()) {
          businessItem.show_offline = false;
        } else {
          businessItem.show_offline = true;
        }
      } else if (endDateTime.getTime() > startDateTime.getTime()) {
        if (currentDate.getTime() > startDateTime.getTime() && currentDate.getTime() < endDateTime.getTime()) {
          businessItem.show_offline = false;
        } else {
          businessItem.show_offline = true;
        }
      }
    }
  }

  pageChange(newPage: number) {
    this.showData = false;
    window.location.href = this.baseUrl + '/service/h/services/' + this.currentCity + '/' + this.category + '/' + this.categoryDisplayName + '/' + newPage;
  }
}