import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { LoginDataService } from '@app/shared/login-data.service';
import { environment as env } from '@env/environment';
import { ContextDataService } from '@app/services/context-data.service';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../../services/http.helper';

const BASE_URL = env.serverUrl;

declare var $: any;

@Component({
  selector: 'app-home-service',
  templateUrl: './home-service.component.html',
  styleUrls: ['./home-service.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HomeServiceComponent implements OnInit {
  categories: any;
  recentlyBooked: any;
  currentCity: any;
  customer_id: any;

  baseUrl: string = env.webUrl;

  constructor(
    private api: MyHttpClient,
    private router: Router,
    private data: LoginDataService,
    private activatedRoute: ActivatedRoute,
    private contextDataService: ContextDataService) {

  }

  ngOnInit() {
    this.currentCity = this.activatedRoute.snapshot.paramMap.get("city");

    this.openLoginPage();
    if (this.contextDataService.isUserLoggedIn) {
      this.getRecentlyBookedServices();
    }
    this.getAvailableServiceCategoriesByCity();
  }

  trackBySubscibeBusinessID(business: any): string {
    return business.business_id;
  }

  getAvailableServiceCategoriesByCity() {
    const params = new HttpParams();
    const reqData = params
      // .set("customer_city", this.currentCity)
      .set("city", this.currentCity);

    this.api.get<any>(BASE_URL + '/v1/web/customer/orders/getAvailableServiceCategoriesByCity',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
        response => {
          this.categories = response;
          for (let i = 0; i < this.categories.length; i++) {
            let servicesList = this.categories[i].services_list;
            for (let i = 0; i < servicesList.length; i++) {
              servicesList[i]['urlCategorydisplayName'] = servicesList[i]['order_category_display_name'].replace(/\s+/g, "-").replace(/[^a-zA-Z0-9-]/g, "");
            }
          }
          console.log(this.categories);
        },
        err => {
          console.log("error" + err);
        }
      );
  }

  getRecentlyBookedServices() {
    const params = new HttpParams();
    const reqData = params
      .set("customer_city", this.contextDataService.customerProfile.customer_city)
      .set("customer_id", this.contextDataService.customerProfile.customer_id)
      .set("city", this.currentCity);

    this.api.get<any>(BASE_URL + '/v1/web/customer/orders/getRecentlyBookedServices',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
        response => {
          this.recentlyBooked = response;
          console.log(this.recentlyBooked);
          for (let i = 0; i < this.recentlyBooked.length; i++) {
            this.recentlyBooked[i]['urlBusinessName'] = this.recentlyBooked[i].business_name.replace(/\s+/g, "-").replace(/[^a-zA-Z0-9-]/g, "");
          }
        },
        err => {
          console.log("error" + err);
        }
      );
  }

  // If user press 'call' buttion and not logged in
  openLoginPage() {
    this.activatedRoute.queryParams.subscribe(params => {
      let call = params['call'];
      if (call) {
        this.data.showLoginPage(true);
      }

    });
  }
} 