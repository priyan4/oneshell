import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServicePageRoutingModule } from './service-page-routing.module';
import {ServiceComponent} from './service.component';
import {AppointmentBookingComponent} from './appointment-booking/appointment-booking.component';
import {FormsModule} from '@angular/forms';
import {ServiceListComponent} from './service-list/service-list.component';
import {SharedModule} from '@app/shared/shared-module.module';
import {HomeServiceComponent} from './home-service/home-service.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';


@NgModule({
  declarations: [ServiceComponent, AppointmentBookingComponent, ServiceListComponent, HomeServiceComponent],
  imports: [
    CommonModule,
    FormsModule,
    ServicePageRoutingModule,
    SharedModule,
    NgxPaginationModule,
    MatProgressSpinnerModule
  ]
})
export class ServicePageModule { }
