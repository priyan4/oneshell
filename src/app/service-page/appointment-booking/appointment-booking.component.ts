import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ContextDataService } from '@app/services/context-data.service';
import { LoginService } from '@app/login/login.service';
import { environment as env } from '@env/environment';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../../services/http.helper';

const BASE_URL = env.serverUrl;

@Component({
  selector: 'app-appointment-booking',
  templateUrl: './appointment-booking.component.html',
  styleUrls: ['./appointment-booking.component.scss'],
})
export class AppointmentBookingComponent implements OnInit {

  @Output() loginType = new EventEmitter();

  activeCategoryObj: any;


  [x: string]: any;
  catagoryTitle: any;
  modalShowHide = false;
  listAllServices = [];
  addresses = {};
  modalFormShowHide = false;
  requestedPage: String;
  city: string;
  businessId: string;
  businessCity: string;
  businessName: string;
  catagoryObj: any;
  customerId: string;
  categoriesList = [];
  category: string;
  baseUrl: string = env.webUrl;

  model = {
    selectedDate: '',
    selectedTime: '',
    firstName: '',
    comments: '',
    mob: '',
    email: '',
    addr1: '',
    addr2: '',
  };

  errorBln = false;
  errormgs = '';
  todayDate = '';

  constructor(private api: MyHttpClient,
    private router: Router,
    private loginService: LoginService,
    private activatedRoute: ActivatedRoute,
    private contextDataService: ContextDataService) {

    // this.getDefaultAddress();
    // this.getServicesDetails();
  }

  ngOnInit() {

    this.city = this.activatedRoute.snapshot.paramMap.get("city");
    this.businessId = this.activatedRoute.snapshot.paramMap.get("id");
    this.businessCity = this.activatedRoute.snapshot.paramMap.get("businesscity");
    this.businessName = this.activatedRoute.snapshot.paramMap.get("businessname");
    this.catagoryTitle = this.businessName;
    this.customerId = this.activatedRoute.snapshot.paramMap.get("customer_id");
    this.category = this.activatedRoute.snapshot.paramMap.get("categorydisplayname");

    let todayDate = new Date();
    let year = todayDate.getFullYear();
    let month = todayDate.getMonth() + 1;
    let date = todayDate.getDate();
    console.log(year);
    console.log(month);
    console.log(date);
    this.todayDate = year + '-' + month + '-' + date;
    console.log(this.todayDate);

    this.getHomeServiceDetails();
    this.getServicesDetails();
    this.getBusinessProfile();
  }

  getDefaultAddress() {
    const params = new HttpParams();
    const reqData = params
      .set("customer_id", localStorage.getItem('customer_id'))
      .set("city", this.city);

    this.api.get<any>(BASE_URL + '/v1/web/customer/orders/getDefaultAddress',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
        response => {
          this.addresses = response;
        },
        err => {
          console.log("error" + err);
        }
      );
  }

  getServicesDetails() {
    const params = new HttpParams();
    const reqData = params
      .set("business_id", this.businessId)
      .set("business_city", this.city);

    this.api.get<any>(BASE_URL + '/v1/web/customer/orders/getServiceDetails',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
        response => {
          this.listAllServices = response;
          console.log(this.listAllServices);
        },
        err => {
          console.log("error" + err);
        }
      );
  }

  getHomeServiceDetails() {
    const params = new HttpParams();
    const reqData = params
      .set("business_id", this.businessId)
      .set("business_city", this.city);

    this.api.get<any>(BASE_URL + '/v1/web/customer/orders/getBusinessHomeServiceDetails',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
        response => {
          this.catagoryObj = response;
          this.checkOffline(this.catagoryObj);
        },
        err => {
          console.log("error" + err);
        }
      );
  }

  getBusinessProfile() {
    const params = new HttpParams();
    const reqData = params
      .set("business_city", this.businessCity)
      .set("business_id", this.businessId)
      .set("customer_id", this.customerId);

    this.api.get<any>(BASE_URL + '/v1/web/customerService/getBusinessProfile',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
        response => {
          this.categoriesList = response.services_list;
          this.activeCategoryObj = this.category;
        },
        err => {
          console.log("error" + err);
        }
      );
  }

  closeModal() {
    this.modalShowHide = false;
  }

  openModal() {
    this.modalShowHide = true;
  }

  closeFormModal() {
    this.modalFormShowHide = false;
  }

  openFormModal() {
    this.modalFormShowHide = true;
  }

  handleCategoryTabChange() {

  }

  checkOffline(businessItem: any) {

    if (businessItem.is_offline) {
      businessItem.show_offline = true;
    }
    const currentDate = new Date();
    var dd = String(currentDate.getDate()).padStart(2, '0');
    var mm = String(currentDate.getMonth() + 1).padStart(2, '0');
    var yyyy = currentDate.getFullYear();
    var dateString = yyyy + '-' + mm + '-' + dd + 'T';

    if (businessItem.delivery_start_time != null && businessItem.delivery_end_time != null) {
      var startDateTimeString = dateString + businessItem.delivery_start_time + ':00';
      var endDateTimeString = dateString + businessItem.delivery_end_time + ':00';

      var startDateTime = new Date(startDateTimeString);
      var endDateTime = new Date(endDateTimeString);
      if (currentDate.getTime() >= startDateTime.getTime() && currentDate.getTime() <= endDateTime.getTime()) {
        businessItem.show_offline = false;
      } else {
        businessItem.show_offline = true;
      }
    }
  }

  onSubmit(value) {
    if (this.contextDataService.isUserLoggedIn) {
      if (value.selectedDate == undefined || value.selectedDate == '') {
        this.errorBln = true;
        this.errormgs = 'Please Select Date'
      } else if (value.selectedTime == undefined || value.selectedTime == '') {
        this.errorBln = true;
        this.errormgs = 'Please Select Time'
      } else if (value.firstName == undefined || value.firstName == '') {
        this.errorBln = true;
        this.errormgs = 'Please enter name'
      } else if (value.comments == undefined || value.comments == '') {
        this.errorBln = true;
        this.errormgs = 'Please enter comments'
      } else if (value.mob == undefined || value.mob == '') {
        this.errorBln = true;
        this.errormgs = 'Please enter mobile number'
      } else if (value.email == undefined || value.email == '') {
        this.errorBln = true;
        this.errormgs = 'Please enter email'
      } else if (value.addr1 == undefined || value.addr1 == '') {
        this.errorBln = true;
        this.errormgs = 'Please enter address'
      } else {
        this.errorBln = false;
        this.errormgs = '';
        let timePeriod = '';

        let timeSelected = value.selectedTime.split(':');
        let dateSelected = value.selectedDate.split('-');
        let year = dateSelected[0];
        let month = dateSelected[1];
        let date = dateSelected[2];

        let hoursValue = timeSelected[0];

        if (Number(hoursValue) > 12) {
          timePeriod = ' PM'
        } else {
          timePeriod = ' AM';
        }
        var data = this.catagoryObj;

        var deliveryAddress = {};
        deliveryAddress['address_type'] = 'default';
        deliveryAddress['customer_name'] = value.firstName;
        deliveryAddress['house_details'] = value.addr1 + '' + value.addr2;
        deliveryAddress['phone_number'] = Number(value.mob);
        deliveryAddress['house_no'] = 'hu';
        deliveryAddress['landmark'] = '';
        deliveryAddress['latitude'] = Number(localStorage.getItem('latitude'));
        deliveryAddress['longitude'] = Number(localStorage.getItem('longitude'));

        data['delivery_address'] = deliveryAddress;

        data['address_type'] = 'Default';
        data['business_image_url'] = data.image_url;
        data['city'] = JSON.parse(localStorage.getItem('customerProfile')).customer_city;
        data['comments'] = value.comments;
        data['customer_id'] = JSON.parse(localStorage.getItem('customerProfile')).customer_id;
        data['customer_phone_number'] = JSON.parse(localStorage.getItem('customerProfile')).phone_number;
        data['customer_name'] = JSON.parse(localStorage.getItem('customerProfile')).name;
        data['service_requested_date'] = date + '/' + month + '/' + year;
        data['order_category_display_name'] = this.category;
        data['service_requested_time'] = value.selectedTime + timePeriod;
        data['order_title'] = this.catagoryTitle;
        data['order_category'] = this.category;
        data['item_type'] = data.business_name;
        data['business_phone_number'] = Number(data.business_phone_number);
        data['service_start_time'] = "9 AM";
        data['service_end_time'] = "9 PM";

        const reqData = data;
        const httpOptions = {
          headers: new HttpHeaders({
            'Content-Type': 'application/json'
          })
        };

        this.api.post<any>(BASE_URL + "/v1/web/customer/orders/bookAppointment", data, httpOptions).subscribe(
          response => {
            if (response.success) {
              this.modalFormShowHide = false;
              window.location.href = this.baseUrl + '/order/service/details/' + response.order_id + '/' + this.businessId + '/' + this.businessCity;
            } else {
              this.errorBln = true;
              this.errormgs = 'Error';
            }
            console.log(response);
          },
          err => {
            console.error(err);
          }
        );
      }
    } else {
      this.modalFormShowHide = false;
      this.loginService.openLoginModal();
    }
  }
}