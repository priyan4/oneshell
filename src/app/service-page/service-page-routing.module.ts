import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ServiceComponent} from './service.component';
import {AppointmentBookingComponent} from './appointment-booking/appointment-booking.component';
import {ServiceListComponent} from './service-list/service-list.component';
import {HomeServiceComponent} from './home-service/home-service.component';

const routes: Routes = [
  { path: ':city', component: ServiceComponent },
  { path: 'h/:city', component: HomeServiceComponent },
  { path: 'h/services/:city/:category/:categorydisplayname', component: ServiceListComponent },
  { path: 'h/booking/:city/:id/:categorydisplayname/:businesscity/:businessname', component: AppointmentBookingComponent },
  { path: 'h/store/booking/:city/:id/:businesscity/:businessname', component: AppointmentBookingComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServicePageRoutingModule { }
