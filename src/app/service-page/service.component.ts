import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { FrequentCategory, Service } from "./service";
import { environment as env } from '@env/environment';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../services/http.helper';

const BASE_URL = env.serverUrl;

@Component({
  selector: "app-service",
  templateUrl: "./service.component.html",
  styleUrls: ["./service.component.scss"]
})
export class ServiceComponent implements OnInit {
  services: Array<any> = [];
  level2Categories: Array<any> = [];
  frequentCategory: FrequentCategory[];
  city: string;
  baseUrl: string = env.webUrl;

  constructor(private api: MyHttpClient, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.city = this.activatedRoute.snapshot.paramMap.get("city");
    this.getStoresList();
  }

  getStoresList(): void {
    const reqData = new HttpParams()
      .set("category_type", "service")
      .set("city", this.city);;

    this.api.get<any>(BASE_URL + '/v1/web/cache/categories/getCustomerLevelOneAndTwoByCity',
          { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
            response => {
          this.services = response;
          for (let i = 0; i < this.services.length; i++) {
            this.level2Categories = this.services[i].level2_category;
            for (let j = 0; j < this.level2Categories.length; j++) {
                const name = this.level2Categories[j]['display_name'].replace(/\s+/g, '-').replace(/[^a-zA-Z0-9-]/g, '');
                this.level2Categories[j]['urlDisplay_name'] = name;
            }
          }
        },
        err => {
          console.log("error" + err);
        }
      );
  }
}
