import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {StoreRecommendationListingComponent} from './store-recommendation-listing/store-recommendation-listing.component';

const routes: Routes = [
  { path: ':city', component: StoreRecommendationListingComponent },
  { path: ':city/:page', component: StoreRecommendationListingComponent }
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecommendationsPageRoutingModule { }
