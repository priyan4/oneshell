import { Component, OnInit, ViewChild, ElementRef, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { environment as env } from '@env/environment';
import { ContextDataService } from '@app/services/context-data.service';
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { NgForm } from '@angular/forms';
import { MatSnackBar } from "@angular/material/snack-bar";
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../../services/http.helper';

const BASE_URL = env.serverUrl;

@Component({
  selector: 'app-store-recommendation-listing',
  templateUrl: './store-recommendation-listing.component.html',
  styleUrls: ['./store-recommendation-listing.component.scss']
})
export class StoreRecommendationListingComponent implements OnInit {

  storeList: Array<any> = [];
  isFullyLoaded: boolean = false;
  paginationSize: number = 10;
  query: string = '0';
  total_rating: any;
  total_rating_fixed: number;
  total_rating_float: boolean;
  total_rating_total: number;
  days: any = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
  city: string;
  customer_id: string;
  baseUrl: string = env.webUrl;
  config: any;
  pageNumber: number;
  pageStart: number;
  pageSize: number;
  pageEnd: number;
  totalItems: number;
  showData: boolean = true;

  dealModalRef: BsModalRef;
  showDealError: boolean = false;
  chosenStore: any;

  @ViewChild('confirmDealDetailsShare', { static: false }) confirmDealDetailsTpl: ElementRef;
  shareDealModalRef: BsModalRef;

  constructor(
    private route: ActivatedRoute,
    private api: MyHttpClient,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private contextData: ContextDataService,
    private modalService: BsModalService,
    private snackBar: MatSnackBar
  ) {
  }

  ngOnInit() {
    this.storeList = [];

    this.pageSize = 12;

    this.customer_id = "c0000000000000";


    this.city = this.activatedRoute.snapshot.paramMap.get("city");
    this.pageNumber = parseInt(((this.activatedRoute.snapshot.paramMap.get("page") != null) ? this.activatedRoute.snapshot.paramMap.get("page") : '1'));

    this.getBusinessCount();

    this.getBusinessList();
  }

  getBusinessCount() {

    const params = new HttpParams();
    const reqData = params
      .set("business_city", this.city);

    this.api.get<any>(BASE_URL + '/v1/web/homePage/getBusinessCount',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
        response => {

          this.totalItems = response.total_count;

          this.config = {
            currentPage: this.pageNumber,
            itemsPerPage: this.pageSize,
            totalItems: this.totalItems
          };

          this.pageStart = 1 + (this.pageNumber - 1) * this.pageSize;

          if ((this.totalItems - this.pageStart) > this.pageSize)
            this.pageEnd = (this.pageNumber * this.pageSize);
          else
            this.pageEnd = this.totalItems;
        }
      );
  }

  pageChange(newPage: number) {
    this.showData = false;
    window.location.href = this.baseUrl + '/recommendations/' + this.city + "/" + newPage;
  }

  getBusinessList() {

    const params = new HttpParams();
    const reqData = params
      .set("business_city", this.city)
      .set("page_number", String(this.pageNumber))
      .set("page_size", '12')
      .set("query", this.query);

    this.api.get<any>(BASE_URL + '/v1/web/homePage/getRecommendations',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
        response => {

          var currentDate = new Date();
          var dayName = this.days[currentDate.getDay()];

          var dd = String(currentDate.getDate()).padStart(2, '0');
          var mm = String(currentDate.getMonth() + 1).padStart(2, '0');
          var yyyy = currentDate.getFullYear();

          var dateString = yyyy + '-' + mm + '-' + dd + 'T';

          let timing: Map<String, any>;

          for (let business of response.business_list) {

            business.urlBusinessName = business.business_name.replace(/\s+/g, "-").replace(/[^a-zA-Z0-9-]/g, "");

            // Rating 
            if (business.number_of_feedbacks != 0) {
              var rating = business.total_ratings / business.number_of_feedbacks;
              business.rating_display_number = rating.toFixed(1);
            } else {
              business.rating_display_number = 0;
            }
            business.rating_display_description = '(' + business.number_of_feedbacks + ' reviews)';

            if (business.oneshell_home_delivery) {
              this.checkOffline(business, dateString);
            } else {
              business.show_offline = false;
            }

            timing = business.business_timings;
            for (let [key, value] of Object.entries(timing)) {
              if (key === dayName) {

                let timingObj = value;

                var startDateTimeString = dateString + timingObj.start_time + ':00';
                var endDateTimeString = dateString + timingObj.end_time + ':00';

                let startDateTime = new Date(startDateTimeString);
                let endDateTime = new Date(endDateTimeString);

                if (timingObj.start_time === '24 hrs') {
                  business.is_open = 'Open';
                  business.timings = '24 hrs';
                } else if (currentDate.getTime() >= startDateTime.getTime() && currentDate.getTime() <= endDateTime.getTime()) {

                  if (timingObj.break_end_time === '- -') {
                    business.is_open = 'Open';
                  } else {

                    var breakStartDateTimeString = dateString + timingObj.break_start_time + ':00';
                    var breakEndDateTimeString = dateString + timingObj.break_end_time + ':00';

                    let breakStartDateTime = new Date(breakStartDateTimeString);
                    let breakEndDateTime = new Date(breakEndDateTimeString);

                    if (currentDate.getTime() >= breakEndDateTime.getTime() && currentDate.getTime() <= breakStartDateTime.getTime()) {
                      business.is_open = 'Open';
                    } else {
                      business.is_open = 'Closed';
                    }
                  }
                  business.timings = startDateTime.toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true }) + ' - ' + endDateTime.toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
                } else {
                  business.is_open = 'Closed';
                  business.timings = startDateTime.toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true }) + ' - ' + endDateTime.toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
                }

              }
            }
          }

          // console.log(response.business_list)

          this.storeList = this.storeList.concat(response.business_list);

          this.query = String(response.query);

        }, (err) => {
          console.log('error ' + err);
          //show no data found message
        })
  }

  setHeightOfImage() {
    return (window.innerHeight / 4) + 'px';
  }

  repeatRating(rating: any, feedback: any) {
    if (typeof (rating) !== 'undefined') {
      this.total_rating = (rating / feedback).toFixed(1);
      this.total_rating_fixed = parseInt(this.total_rating.toString().split('.')[0]);
      this.total_rating_float = parseInt(this.total_rating.toString().split('.')[1]) > 0;
      if (this.total_rating_float)
        this.total_rating_total = 5 - this.total_rating_fixed - 1;
      else
        this.total_rating_total = 5 - this.total_rating_fixed;
      return true;
    } else {
      return false;
    }
  }

  checkOffline(businessItem: any, dateString: any) {

    if (businessItem.is_offline) {
      businessItem.show_offline = true;
    }

    if (businessItem.delivery_start_time != null && businessItem.delivery_end_time != null) {
      var startDateTimeString = dateString + businessItem.delivery_start_time + ':00';
      var endDateTimeString = dateString + businessItem.delivery_end_time + ':00';

      var startDateTime = new Date(startDateTimeString);
      var endDateTime = new Date(endDateTimeString);
      var currentDate = new Date();

      if (startDateTime.getTime() > endDateTime.getTime()) {
        if (currentDate.getTime() > startDateTime.getTime()) {
          businessItem.show_offline = false;
        } else {
          businessItem.show_offline = true;
        }
      } else if (endDateTime.getTime() > startDateTime.getTime()) {
        if (currentDate.getTime() > startDateTime.getTime() && currentDate.getTime() < endDateTime.getTime()) {
          businessItem.show_offline = false;
        } else {
          businessItem.show_offline = true;
        }
      }
    }
  }

  onStoreClicked(store: any) {
    window.location.href = this.baseUrl + '/business/' + store.business_city + '/' + store.business_id + '/' + store.business_name;
  }

  showDealTemplate(template: TemplateRef<any>, store: any, event: any) {
    event.stopImmediatePropagation();
    this.chosenStore = store;
    if (!this.contextData.isUserLoggedIn) {
      this.dealModalRef = this.modalService.show(template);
    } else {
      this.shareDealModalRef = this.modalService.show(this.confirmDealDetailsTpl);
    }
  }

  hideDealTemplate() {
    this.showDealError = false;
    this.dealModalRef.hide();
  }

  hideShareDealTemplate() {
    this.shareDealModalRef.hide();
  }

  onSubmit(form: NgForm) {
    if (form.value.name === "" || form.value.number === "") {
      this.showDealError = true;
    } else {
      this.showDealError = false;
      this.hideDealTemplate();
      this.postDealDetails(form.value.name, form.value.number);
    }
  }

  onShareAlertClicked() {
    this.hideShareDealTemplate();
    this.postDealDetails('', '');
  }

  postDealDetails(name: string, phNo: string) {
    const reqBody = {
      business_id: this.chosenStore.business_id,
      business_city: this.chosenStore.business_city,
      customer_name: name,
      customer_phone_number: phNo
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    this.api.post<any>(BASE_URL + "/v1/web/customerService/enquiry/deal/getBusinessDeal", reqBody, httpOptions).subscribe(
      response => {

        if (response.success) {
          this.snackBar.open('Successfully Submitted Details!', '', {
            duration: 2000,
          });
        }
      },
      err => {
        console.error(err);
      }
    );
  }

}