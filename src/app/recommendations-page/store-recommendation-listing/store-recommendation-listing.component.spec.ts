import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreRecommendationListingComponent } from './store-recommendation-listing.component';

describe('StoreRecommendationListingComponent', () => {
  let component: StoreRecommendationListingComponent;
  let fixture: ComponentFixture<StoreRecommendationListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StoreRecommendationListingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreRecommendationListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
