import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RecommendationsPageRoutingModule } from './recommendations-page-routing.module';
import {StoreRecommendationListingComponent} from './store-recommendation-listing/store-recommendation-listing.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import {FormsModule} from '@angular/forms';


@NgModule({
  declarations: [StoreRecommendationListingComponent],
  imports: [
    CommonModule,
    RecommendationsPageRoutingModule,
    NgxPaginationModule,
    MatProgressSpinnerModule,
    FormsModule
  ]
})
export class RecommendationsPageModule { }
