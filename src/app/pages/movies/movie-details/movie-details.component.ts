import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { PagesService } from '@app/pages/pages.service';
import { Meta, Title } from '@angular/platform-browser';
import { environment as env } from '@env/environment';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../../../services/http.helper';

const BASE_URL = env.serverUrl;

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.scss']
})

export class MovieDetailsComponent implements OnInit {


  showVideoModalBool: boolean = false;
  theatersList: Array<any> = [];
  city: string;
  movieName: string;
  actualMovieName: string;
  movieData: any = {};
  title: string = "";
  description: string = "";
  keywords: string = "";
  cityMapping = new Map();
  dataPresent = false;

  constructor(private _pagesService: PagesService, private _sanitizer: DomSanitizer,
    private api: MyHttpClient, private _Activatedroute: ActivatedRoute, private _router: Router,
    private metaTagService: Meta,
    private titleService: Title, ) {

    this.cityMapping.set('ballari', 'bellary/ballari');
    this.cityMapping.set('hosapete', 'hospet/hosapete');
  }

  showVideoModal() {
    this.showVideoModalBool = true;
  }
  ngOnInit() {

    this.city = this._Activatedroute.snapshot.paramMap.get("city");
    this.movieName = this._Activatedroute.snapshot.paramMap.get("name");
    this.actualMovieName = this.movieName.replace("-", " ");
    this.getMovieDetail();
    this.getTheaterList();

  }

  addPageMeta() {

    this.metaTagService.addTags([
      { name: 'Title', content: this.title },
      { name: 'description', content: this.description },
      { name: 'keywords', content: this.keywords },
      { name: 'robots', content: 'index, follow' },
      { name: 'author', content: 'OneShell' },
      { charset: 'UTF-8' }
    ]);
    this.titleService.setTitle(this.title);
  }

  getMovieDetail() {
    const params: HttpParams = new HttpParams();
    const reqData = params
      .set("city", this.city)
      .set("movie_name", this.actualMovieName);

    this.api.get<any>(BASE_URL + '/v1/web/customerService/movies/getMovieDetails',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
        response => {
          console.log(response);
          this.movieData = response;
          let data = response;

          this.title = response.movie_name + ' in ' + this.cityMapping.get(this.city) + ' - OneShell';
          this.description = 'movies in ' + this.cityMapping.get(this.city) + ', ' + response.movie_overview;

          this.dataPresent = true;
        }, (err) => {
          console.log('error ' + err);
          //show no data found message
        });
  }
  getTheaterList() {
    const params: HttpParams = new HttpParams();
    const reqData = params
      .set("city", this.city)
      .set("movie_name", this.actualMovieName);

    this.api.get<any>(BASE_URL + '/v1/web/customerService/movies/getTheatersByMovie',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
        response => {
          // console.log(response);
          this.theatersList = response;

          for (let i = 0; i < this.theatersList.length; i++) {
            this.keywords = this.keywords + this.theatersList[i]['theater_name'] + ' in ' + this.cityMapping.get(this.city) + ', ';
          }

          this.addPageMeta();
        }, (err) => {
          console.log('error ' + err);
          //show no data found message
        });
  }

  showComma(index: any, list: Array<any>) {
    if (list) {
      return (index < list.length - 1);
    } else {
      return false;
    }
  }

}
