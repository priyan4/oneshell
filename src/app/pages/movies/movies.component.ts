import { Component, OnInit, PLATFORM_ID, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PagesService } from '@app/pages/pages.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Meta, Title } from '@angular/platform-browser';
import { environment as env } from '@env/environment';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../../services/http.helper';

const BASE_URL = env.serverUrl;


@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.scss']
})
export class MoviesComponent implements OnInit {

  moviesList: Array<any> = [];

  activeTab: string = '';
  nextToken: number = 1;
  listState: string = '';
  isFullyLoaded: boolean = false;
  paginationSize: number = 10;
  baseUrl: string = env.webUrl;
  nowShowing: boolean = true;
  city: string;
  title: string = "";
  description: string = "";
  keywords: string = "";
  cityMapping = new Map();

  constructor(private http: HttpClient, private api: MyHttpClient, private _pagesService: PagesService,
    private _router: Router, private metaTagService: Meta,
    private titleService: Title,
    private activatedRoute: ActivatedRoute,
    @Inject(PLATFORM_ID) private platformId: Object, @Inject('LOCALSTORAGE') private localStorage: any) {
    this.cityMapping.set('ballari', 'bellary/ballari');
    this.cityMapping.set('hosapete', 'hospet/hosapete');
  }

  ngOnInit() {

    this.activeTab = this.activatedRoute.snapshot.paramMap.get("tab");
    this.city = this.activatedRoute.snapshot.paramMap.get("city");

    if (this.activeTab) {
      if (this.activeTab === "now_showing")
        this.nowShowing = true;
      else if (this.activeTab === "up_coming")
        this.nowShowing = false;

    }
    this.getMoviesList();
  }

  addPageMeta() {

    this.title = 'Movies in ' + this.cityMapping.get(this.city) + ' - OneShell';

    this.description = 'movies in ' + this.cityMapping.get(this.city) + ', ' +
      'current movies in ' + this.cityMapping.get(this.city) + ', ' +
      'upcoming movies in ' + this.cityMapping.get(this.city) + ', ' +
      'movie theaters in  ' + this.cityMapping.get(this.city) + ', ' +
      'movie tickets in  ' + this.cityMapping.get(this.city) + ', ' +
      'telugu movie tickets in  ' + this.cityMapping.get(this.city) + ', ' +
      'kanada movie tickets in  ' + this.cityMapping.get(this.city) + ', ' +
      'hindi movie tickets in  ' + this.cityMapping.get(this.city) + ', ' +
      'english movie tickets in  ' + this.cityMapping.get(this.city) + ', ' +
      'Book movie ticket online in  ' + this.cityMapping.get(this.city);

    this.metaTagService.addTags([
      { name: 'Title', content: this.title },
      { name: 'description', content: this.description },
      { name: 'keywords', content: this.keywords },
      { name: 'robots', content: 'index, follow' },
      { name: 'author', content: 'OneShell' },
      { charset: 'UTF-8' }
    ]);
    this.titleService.setTitle(this.title);
  }

  getMoviesList(): void {

    if (this.activeTab != this.listState) {
      this.nextToken = 1;
      this.isFullyLoaded = false;
      this.moviesList = [];
    }

    let reqBody = {
      city: this.city,
      next_token: this.nextToken,
      movie_type: this.nowShowing ? 'NOW_SHOWING' : 'COMING_SOON'
    };
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    this.api.post<any>(BASE_URL + "/v1/web/customerService/movies/getMoviesByCity", reqBody, httpOptions).subscribe(
      response => {

        this.moviesList = this.moviesList.concat(response.movieResponse);
        for (let i = 0; i < this.moviesList.length; i++) {
          this.moviesList[i]['urlMovieName'] = this.moviesList[i]['movie_name'].replace(/\s+/g, "-").replace(/[^a-zA-Z0-9-]/g, "");
          this.keywords = this.keywords + this.moviesList[i]['movie_name'] + ' in ' + this.cityMapping.get(this.city) + ', ';
        }

        if (Number(response.next_token) > 1)
          this.listState = this.activeTab;

        if ((Number(response.next_token) - (this.nextToken)) != this.paginationSize)
          this.isFullyLoaded = true;

        this.nextToken = Number(response.next_token);

        this.addPageMeta();

      }, (err) => {
        console.log('error ' + err);
        //show no data found message
      })
  }
}