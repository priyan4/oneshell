import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Subject, Subscription } from 'rxjs';
import { distinctUntilChanged, switchMap } from 'rxjs/operators';
import { environment as env } from '@env/environment';
import { Title, Meta } from '@angular/platform-browser';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../../services/http.helper';

const BASE_URL = env.serverUrl;

@Component({
  selector: 'app-banquet-halls',
  templateUrl: './banquet-halls.component.html',
  styleUrls: ['./banquet-halls.component.scss'],
  host: {
    '(document:click)': 'handleSearchChange($event)',
  },
})
export class BanquetHallsComponent implements OnInit {

  hallsList: Array<any> = [];
  nextToken: number = 1;
  isFullyLoaded: boolean = false;
  paginationSize: number = 10;
  currentDay: string = "";
  searchTxt: string = '';
  subscription: Subscription;
  search$: Subject<any> = new Subject();
  searchResultsList: Array<any> = [];
  total_rating: any;
  total_rating_fixed: number;
  total_rating_float: boolean;
  total_rating_total: number;
  url: string;
  listState: string = '';
  days: any = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
  baseUrl: string = env.webUrl;
  city: string;

  title: string = "";
  description: string = "";
  keywords: string = "";
  cityMapping = new Map();

  constructor(
    private activatedRoute: ActivatedRoute,
    private api: MyHttpClient,
    private router: Router,
    private metaTagService: Meta,
    private titleService: Title
  ) {
    this.currentDay = [
      "Sunday",
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday"
    ][new Date().getDay()];

    this.cityMapping.set('ballari', 'bellary/ballari');
    this.cityMapping.set('hosapete', 'hospet/hosapete');
  }

  ngOnInit() {
    this.hallsList = [];
    this.city = this.activatedRoute.snapshot.paramMap.get("city");
    this.getHallsList(this.city);
    this.addSearchListener();
  }

  addPageMeta() {

    this.title = 'Function halls in ' + this.cityMapping.get(this.city) + ' - OneShell';

    this.description = 'Banquet Halls in ' + this.cityMapping.get(this.city) + ', ' +
      'Wedding Bands in ' + this.cityMapping.get(this.city) + ', ' +
      'Generators On Hire in ' + this.cityMapping.get(this.city) + ', ' +
      'Caterers in ' + this.cityMapping.get(this.city) + ', ' +
      'Balloon Decorators in ' + this.cityMapping.get(this.city) + ', ' +
      'Flower Decorators in ' + this.cityMapping.get(this.city) + ', ' +
      'Event Organisers in ' + this.cityMapping.get(this.city) + ', ' +
      'Cameras On Hire in ' + this.cityMapping.get(this.city) + ', ' +
      'Decorators in ' + this.cityMapping.get(this.city) + ', ' +
      'Fire Cracker Dealers in ' + this.cityMapping.get(this.city) + ', ' +
      'LED Light Dealers in ' + this.cityMapping.get(this.city) + ', ' +
      'Stage Decorators in ' + this.cityMapping.get(this.city) + ', ' +
      'Video Shooting Services in ' + this.cityMapping.get(this.city) + ', ' +
      'Singers in ' + this.cityMapping.get(this.city) + ', ' +
      'Wedding Photographers in ' + this.cityMapping.get(this.city) + ', ' +
      'Event Management Companies in ' + this.cityMapping.get(this.city) + ', ' +
      'Brahmin Food Caterers in ' + this.cityMapping.get(this.city) + ', ' +
      'Exhibition Organisers in ' + this.cityMapping.get(this.city) + ', ' +
      'Magicians in ' + this.cityMapping.get(this.city) + ', ' +
      'Decoration Material Dealers in ' + this.cityMapping.get(this.city) + ', ' +
      'Balloon Dealers in ' + this.cityMapping.get(this.city) + ', ' +
      'Anchoring Services Horses On Hire in ' + this.cityMapping.get(this.city);

    this.metaTagService.addTags([
      { name: 'Title', content: this.title },
      { name: 'description', content: this.description },
      { name: 'keywords', content: this.keywords },
      { name: 'robots', content: 'index, follow' },
      { name: 'author', content: 'OneShell' },
      { charset: 'UTF-8' }
    ]);
    this.titleService.setTitle(this.title);
  }

  getHallsList(city: string) {

    let data: any;

    data = {
      city: city,
      next_token: 1,
      category: "venue_banquet_hall_level1"
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    this.api.post<any>(BASE_URL + "/v1/web/customer/banquetHalls/getHalls", data, httpOptions).subscribe(
      response => {

        this.hallsList = this.hallsList.concat(response.business_list);

        for (let i = 0; i < this.hallsList.length; i++) {
          this.hallsList[i]['urlBusinessName'] = this.hallsList[i]['business_name'].replace(/\s+/g, "-").replace(/[^a-zA-Z0-9-]/g, "");
          this.keywords = this.keywords + this.hallsList[i]['business_name'] + ' in ' + this.cityMapping.get(this.city) + ', ';
        }
        var currentDate = new Date();
        var dayName = this.days[currentDate.getDay()];

        var dd = String(currentDate.getDate()).padStart(2, '0');
        var mm = String(currentDate.getMonth() + 1).padStart(2, '0');
        var yyyy = currentDate.getFullYear();

        var dateString = yyyy + '-' + mm + '-' + dd + 'T';
        let timing: Map<String, any>;

        for (let business of response.business_list) {
          timing = business.business_timings;
          for (let [key, value] of Object.entries(timing)) {
            if (key === dayName) {

              let timingObj = value;

              if (!(timingObj.start_time === '24 hrs')) {
                var startDateTimeString = dateString + timingObj.start_time + ':00';
                var endDateTimeString = dateString + timingObj.end_time + ':00';

                let startDateTime = new Date(startDateTimeString);
                let endDateTime = new Date(endDateTimeString);

                if (currentDate.getTime() >= startDateTime.getTime() && currentDate.getTime() <= endDateTime.getTime()) {

                  if (timingObj.break_end_time === '- -') {
                    business.is_open = 'Open';
                  } else {

                    var breakStartDateTimeString = dateString + timingObj.break_start_time + ':00';
                    var breakEndDateTimeString = dateString + timingObj.break_end_time + ':00';

                    let breakStartDateTime = new Date(breakStartDateTimeString);
                    let breakEndDateTime = new Date(breakEndDateTimeString);

                    if (currentDate.getTime() >= breakEndDateTime.getTime() && currentDate.getTime() <= breakStartDateTime.getTime()) {
                      business.is_open = 'Open';
                    } else {
                      business.is_open = 'Closed';
                    }
                  }
                } else {
                  business.is_open = 'Closed';
                }
                business.timings = startDateTime.toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true }) + ' - ' + endDateTime.toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });

              } else {
                business.is_open = 'Open';
                business.timings = '24 Hours';
              }
            }
          }
        }

        if ((Number(response.next_token) - (this.nextToken)) != this.paginationSize)
          this.isFullyLoaded = true;

        this.nextToken = Number(response.next_token);

        // prepare meta  data
        this.addPageMeta();
      }, (err) => {
        console.log('error ' + err);
        //show no data found message
      })
  }

  setHeightOfImage() {
    return (window.innerHeight / 4) + 'px';
  }

  repeatRating(rating: any, feedback: any) {
    if (typeof (rating) !== 'undefined') {
      this.total_rating = (rating / feedback).toFixed(1);
      this.total_rating_fixed = parseInt(this.total_rating.toString().split('.')[0]);
      this.total_rating_float = parseInt(this.total_rating.toString().split('.')[1]) > 0;
      if (this.total_rating_float)
        this.total_rating_total = 5 - this.total_rating_fixed - 1;
      else
        this.total_rating_total = 5 - this.total_rating_fixed;
      return true;
    } else {
      return false;
    }
  }

  handleSearchChange(event) {
    this.search$.next(this.searchTxt);
    if (event.target.id != "txtSearch") // or some similar check
    {
      this.searchResultsList = [];
    }
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  addSearchListener() {

    this.subscription = this.search$.pipe(distinctUntilChanged(), switchMap((searchText: string) => {

      this.searchResultsList = [];

      const params = new HttpParams();
      const reqData = params
        .set("city", this.city)
        .set("keyword", this.searchTxt)
        .set("size", "20");

      return this.api.get<any>(BASE_URL + '/v1/web/customer/banquetHalls/getStrings',
        { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData });
    })).subscribe((response) => {

      response.forEach(element => {
        this.searchResultsList.push(element);
      });
    })
  }

  handleSearchResultClicked(result: any) {
    this.searchResultsList = [];

    this.nextToken = 1;
    this.isFullyLoaded = false;
    this.hallsList = [];

    let reqBody = {
      city: localStorage.getItem('activeCity'),
      next_token: String(this.nextToken),
      keyword: result.keyword,
      category: "banquet_hall_level1"
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    this.api.post<any>(BASE_URL + "/v1/web/customer/banquetHalls/getBanquetHallsListBySearch", reqBody, httpOptions).subscribe(
      response => {

        this.hallsList = this.hallsList.concat(response.business_list);

        var currentDate = new Date();
        var dayName = this.days[currentDate.getDay()];

        var dateString = currentDate.getFullYear() + '-' + (currentDate.getMonth() + 1) + '-' + currentDate.getDate() + 'T';
        let timing: Map<String, any>;

        for (let business of response.business_list) {
          timing = business.business_timings;
          for (let [key, value] of Object.entries(timing)) {
            if (key === dayName) {

              let timingObj = value;

              if (!(timingObj.start_time === '24 hrs')) {
                var startDateTimeString = dateString + timingObj.start_time + ':00';
                var endDateTimeString = dateString + timingObj.end_time + ':00';

                let startDateTime = new Date(startDateTimeString);
                let endDateTime = new Date(endDateTimeString);

                if (currentDate.getTime() >= startDateTime.getTime() && currentDate.getTime() <= endDateTime.getTime()) {

                  if (timingObj.break_end_time === '- -') {
                    business.is_open = 'Open';
                  } else {

                    var breakStartDateTimeString = dateString + timingObj.break_start_time + ':00';
                    var breakEndDateTimeString = dateString + timingObj.break_end_time + ':00';

                    let breakStartDateTime = new Date(breakStartDateTimeString);
                    let breakEndDateTime = new Date(breakEndDateTimeString);

                    if (currentDate.getTime() >= breakEndDateTime.getTime() && currentDate.getTime() <= breakStartDateTime.getTime()) {
                      business.is_open = 'Open';
                    } else {
                      business.is_open = 'Closed';
                    }
                  }
                } else {
                  business.is_open = 'Closed';
                }
                business.timings = startDateTime.toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true }) + ' - ' + endDateTime.toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });

              } else {
                business.is_open = 'Open';
                business.timings = '24 Hours';
              }
            }
          }
        }

        if (Number(response.next_token) > 1)
          this.listState = '';

        if ((Number(response.next_token) - (this.nextToken)) != this.paginationSize)
          this.isFullyLoaded = true;

        this.nextToken = Number(response.next_token);
      }, (err) => {
        console.log('error ' + err);
        //show no data found message
      })
  }
}