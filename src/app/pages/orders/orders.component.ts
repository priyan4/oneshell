import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { environment as env } from '@env/environment';
import { ContextDataService } from '@app/services/context-data.service';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../../services/http.helper';

const BASE_URL = env.serverUrl;

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {

  ordersList: Array<any> = [];

  nextToken: number = 1;
  listState: string = '';
  isFullyLoaded: boolean = false;
  paginationSize: number = 10;
  customer_city: String;
  customer_id: any;
  baseUrl: string = env.webUrl;
  city: string;
  url: string;
  type: string;

  cityMapping = new Map();
  config: any;
  pageNumber: number;
  pageStart: number;
  pageSize: number;
  pageEnd: number;
  totalItems: number;
  showData: boolean = true;

  constructor(
    private activatedRoute: ActivatedRoute,
    private api: MyHttpClient,
    private router: Router, private contextData: ContextDataService) {
    this.cityMapping.set('ballari', 'bellary/ballari');
    this.cityMapping.set('hosapete', 'hospet/hosapete');
  }

  ngOnInit() {
    this.city = this.activatedRoute.snapshot.paramMap.get("city");
    this.type = this.activatedRoute.snapshot.paramMap.get("type");
    this.pageNumber = parseInt(((this.activatedRoute.snapshot.paramMap.get("page") != null) ? this.activatedRoute.snapshot.paramMap.get("page") : '1'));
    this.pageSize = 12;
    if ("general" === this.type) {
      this.getAllOrdersList();
    } else {
      this.getHomeServiceOrdersList();
    }
  }

  getHomeServiceOrdersList(): void {

    let reqBody = {
      customer_id: this.contextData.customerProfile.customer_id,
      customer_city: this.contextData.customerProfile.customer_city,
      page_number: this.pageNumber,
      page_size: this.pageSize
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    this.api.post<any>(BASE_URL + "/v1/web/customer/orders/getHomeServiceOrders", reqBody, httpOptions).subscribe(
      response => {

        console.log(response);
        this.ordersList = this.ordersList.concat(response.orderServiceResponses);

        this.totalItems = response.total_count;
        this.config = {
          currentPage: this.pageNumber,
          itemsPerPage: this.pageSize,
          totalItems: this.totalItems
        };

        this.pageStart = 1 + (this.pageNumber - 1) * this.pageSize;

        if ((this.totalItems - this.pageStart) > this.pageSize)
          this.pageEnd = (this.pageNumber * this.pageSize);
        else
          this.pageEnd = this.totalItems;

      }, (err) => {
        console.log('error ' + err);
        //show no data found message
      })
  }

  getAllOrdersList(): void {

    let reqBody = {
      customer_id: this.contextData.customerProfile.customer_id,
      customer_city: this.contextData.customerProfile.customer_city,
      page_number: this.pageNumber,
      page_size: this.pageSize
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    this.api.post<any>(BASE_URL + "/v1/web/customer/serviceOrder/getServiceOrders", reqBody, httpOptions).subscribe(
      response => {

        console.log(response);
        this.ordersList = this.ordersList.concat(response.orderServiceResponses);
        for (let order of this.ordersList) {
          if ("home_delivery" === order.order_type || "self_order" === order.order_type) {
            order.url_redirection = this.baseUrl + '/order/delivery/details/' + order.order_id + '/' + order.business_id + '/' + order.business_city;
          } else {
            order.url_redirection = this.baseUrl + '/order/service/details/' + order.order_id + '/' + order.business_id + '/' + order.business_city;
          }
          var status = "";
          if ("pending" === order.order_status) {
            status = "Order Pending for Approval";
          } else if ("accept_order" === order.order_status) {
            status = "Order Placed";
          } else if ("out_for_delivery" === order.order_status) {
            status = "Out for Delivery";
          } else if ("received_payment" === order.order_status) {
            status = "Out for Delivery";
          } else if ("delivered" === order.order_status) {
            status = "Delivered";
          } else if ("completed" === order.order_status) {
            status = "Completed";
          } else if ("order_rejected" === order.order_status) {
            status = "Order Rejected";
          } else if ("order_cancelled" === order.order_status) {
            status = "Order Cancelled";
          } else if ("in_progress" === order.order_status) {
            status = "In Progress";
          }

          order.order_status_formatted = status;
        }

        this.totalItems = response.total_count;
        this.config = {
          currentPage: this.pageNumber,
          itemsPerPage: this.pageSize,
          totalItems: this.totalItems
        };

        this.pageStart = 1 + (this.pageNumber - 1) * this.pageSize;

        if ((this.totalItems - this.pageStart) > this.pageSize)
          this.pageEnd = (this.pageNumber * this.pageSize);
        else
          this.pageEnd = this.totalItems;


      }, (err) => {
        console.log('error ' + err);
        //show no data found message
      })
  }

  pageChange(newPage: number) {
    this.showData = false;
    window.location.href = this.baseUrl + '/' + 'orders' + '/' + this.type + '/' + newPage;
  }
}