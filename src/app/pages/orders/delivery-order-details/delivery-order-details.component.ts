import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PagesService } from '@app/pages/pages.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ContextDataService } from '@app/services/context-data.service';
import { environment as env } from '@env/environment';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../../../services/http.helper';
import { MatSnackBar } from '@angular/material';

const BASE_URL = env.serverUrl;

@Component({
  selector: 'app-delivery-order-details',
  templateUrl: './delivery-order-details.component.html',
  styleUrls: ['./delivery-order-details.component.scss']
})
export class DeliveryOrderDetailsComponent implements OnInit {

  orderId: string;
  businessId: string;
  businessCity: string;
  orderResponse: any;
  baseUrl: string = env.webUrl;
  cancel_button_show :boolean =true;
  add_info :boolean =false;

  constructor(private activatedRoute: ActivatedRoute, private snackBar: MatSnackBar, private http: HttpClient, private api: MyHttpClient, private _pagesService: PagesService, private router: Router, private contextData: ContextDataService) { }

  ngOnInit() {
    this.orderId = this.activatedRoute.snapshot.paramMap.get("orderId");
    this.businessId = this.activatedRoute.snapshot.paramMap.get("businessId");
    this.businessCity = this.activatedRoute.snapshot.paramMap.get("businessCity");
    this.getOrderDetails();
  }

  getOrderDetails() {

    const params = new HttpParams();
    const reqData = params
      .set("customer_id", this.contextData.customerProfile.customer_id)
      .set("customer_city", this.contextData.customerProfile.customer_city)
      .set("order_id", this.orderId)
      .set("business_id", this.businessId)
      .set("business_city", this.businessCity)

    this.api.get<any>(BASE_URL + '/v1/web/customer/serviceOrder/getServiceOrderDetails',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe((response
      ) => {

        console.log(response);
        this.orderResponse = response;
        var status = "";
        if ("pending" === this.orderResponse.order_status) {
          status = "Order Pending for Approval";
        } else if ("not_accepted" === this.orderResponse.order_status) {
          status = "Order Pending for Approval";
          this.add_info=true;
        } else if ("accept_order" === this.orderResponse.order_status) {
          status = "Order Placed"; 
        } else if ("out_for_delivery" === this.orderResponse.order_status) {
          status = "Out for Delivery"; 
        } else if ("received_payment" === this.orderResponse.order_status) {
          status = "Out for Delivery";
          this.cancel_button_show=false;
        } else if ("delivered" === this.orderResponse.order_status) {
          status = "Delivered";
          this.cancel_button_show=false;
        } else if ("completed" === this.orderResponse.order_status) {
          status = "Completed";
          this.cancel_button_show=false;
        } else if ("order_rejected" === this.orderResponse.order_status) {
          status = "Order Not Accepted";
          this.cancel_button_show=false;
        } else if ("order_cancelled" === this.orderResponse.order_status) {
          status = "Order Cancelled";
          this.cancel_button_show=false;
        } else if ("in_progress" === this.orderResponse.order_status) {
          status = "In Progress";
          this.cancel_button_show=false;
        }
        console.log(this.orderResponse)

        this.orderResponse.order_status_formatted = status;
        this.orderResponse.urlBusinessName = this.orderResponse.business_name.replace(/\s+/g, '-').replace(/[^a-zA-Z0-9-]/g, '');

        var propertyVal = '';
        if (this.orderResponse.products_list != null && this.orderResponse.products_list.length > 0) {
          for (let product of this.orderResponse.products_list) {
            if (product.chosen_properties != null && product.chosen_properties.length > 0) {
              for (var i = 0; i < product.chosen_properties.length; i++) {
                if (i == 0) {
                  propertyVal += product.chosen_properties[i].value;
                } else {
                  propertyVal += ' | ' + product.chosen_properties[i].value;
                }
              }
            }
            if (product.additional_property_list != null && product.additional_property_list.length > 0) {
              for (var i = 0; i < product.additional_property_list.length; i++) {
                if (propertyVal != '') {
                  propertyVal += product.additional_property_list[i].name;
                } else {
                  propertyVal += ' | ' + product.additional_property_list[i].name;
                }
              }
            }
            product.property_value = propertyVal;
          }
        }

      }, (err) => {
        console.log('error ' + err);
        //show no data found message
      })
  }


  cancelOrder(){
    const reqBody = {
      business_id: this.businessId,
      business_city: this.businessCity,
      order_id: this.orderId,
      order_type: this.orderResponse.order_type,
    };
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    this.api.post<any>(BASE_URL + "/v1/web/customer/homeDelivery/cancelOrderRequest", reqBody, httpOptions).subscribe(
      response => {
        if (response.success) {
          this.orderResponse.order_status_formatted="Order Cancelled";
          this.cancel_button_show=false;
        this.getOrderDetails();
        } 
      },
      err => {
        console.log("error" + err);
        this.snackBar.open("Sorry! Something went wrong while cancelling order. Please try in sometime", '', {
          duration: 2000,
        });
      }
    );
  }

}