import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PagesService } from '@app/pages/pages.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ContextDataService } from '@app/services/context-data.service';
import { environment as env } from '@env/environment';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../../../services/http.helper';

const BASE_URL = env.serverUrl;

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.scss']
})
export class OrderDetailsComponent implements OnInit {

  orderId: string;
  businessId: string;
  businessCity: string;
  orderResponse: any;
  baseUrl: string = env.webUrl;

  constructor(private activatedRoute: ActivatedRoute, private http: HttpClient, private api: MyHttpClient, private _pagesService: PagesService, private router: Router, private contextData: ContextDataService) { }

  ngOnInit() {
    this.orderId = this.activatedRoute.snapshot.paramMap.get("orderId");
    this.businessId = this.activatedRoute.snapshot.paramMap.get("businessId");
    this.businessCity = this.activatedRoute.snapshot.paramMap.get("businessCity");
    this.getOrderDetails();
  }

  getOrderDetails() {

    const params = new HttpParams();
    const reqData = params
      .set("customer_id", this.contextData.customerProfile.customer_id)
      .set("customer_city", this.contextData.customerProfile.customer_city)
      .set("order_id", this.orderId)
      .set("business_id", this.businessId)
      .set("business_city", this.businessCity)

      this.api.get<any>(BASE_URL + '/v1/web/customer/serviceOrder/getServiceOrderDetails',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe((response
      ) => {

        console.log(response);
        this.orderResponse = response;
        this.orderResponse.urlBusinessName = this.orderResponse.business_name.replace(/\s+/g, '-').replace(/[^a-zA-Z0-9-]/g, '');

      }, (err) => {
        console.log('error ' + err);
        //show no data found message
      })
  }

}