import { Component, OnInit, ElementRef, ViewChild, NgZone } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MouseEvent, MapsAPILoader } from '@agm/core';
import { } from 'google-maps';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { BusinessDetailsService } from '@app/business-page/services/business-details.service';
declare var google: any;

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss']
})

export class LocationComponent implements OnInit {
  public latitude: number;
  public longitude: number;
  public searchControl: FormControl;
  public zoom: number;
  public fillInAddress: '';
  public f_addr: string;
  public city: string;
  public country: string;
  public postCode: string;
  public state: string;
  public currentaddress: any;
  public formatedaddress: any;
  public houseNo: string;
  public formdirty: boolean;

  @ViewChild('search', { static: true }) public searchElementRef: ElementRef;

  constructor(
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private modalService: NgbModal,
    public activeModal: NgbActiveModal,
    private service: BusinessDetailsService
  ) { }

  mapReady(event) {
    console.log(event);
    this.setCurrentPosition();
  }

  ngOnInit() {
    this.zoom = 15;

    //create search FormControl
    this.searchControl = new FormControl();


    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        componentRestrictions: { country: 'IN' },
        fields: ['geometry', 'formatted_address', 'address_components'],
      }

      );
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          this.formatedaddress = place.formatted_address;
          //set latitude, longitude and zoom
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 12;
          this.autocompleteAddress(place.address_components);
        });
      });
    });


  }

  markerDragEnd($event: MouseEvent) {
    this.latitude = $event.coords.lat;
    this.longitude = $event.coords.lng;
    const geocoder = new google.maps.Geocoder();
    const latlng = new google.maps.LatLng(this.latitude, this.longitude);
    const request = {
      latLng: latlng
    };
    geocoder.geocode(request, (results, status) => {
      this.ngZone.run(() => {
        this.currentaddress = results[0].address_components;
        this.formatedaddress = results[0].formatted_address;
        this.autocompleteAddress(this.currentaddress);
      })
    });
  }

  mapClicked($event: MouseEvent) {
    this.latitude = $event.coords.lat;
    this.longitude = $event.coords.lng;
    const geocoder = new google.maps.Geocoder();
    const latlng = new google.maps.LatLng(this.latitude, this.longitude);
    const request = {
      latLng: latlng
    };

    geocoder.geocode(request, (results, status) => {
      this.ngZone.run(() => {
        this.currentaddress = results[0].address_components;
        this.formatedaddress = results[0].formatted_address;
        this.autocompleteAddress(this.currentaddress);
      })
    });
  }


  // this.recenterMap()
  recenterMap() {
    this.latitude = 36.8392542;
    this.longitude = 10.313922699999999;
  }

  //set current position

  public setCurrentPosition() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 15;
        console.log(this.latitude, this.longitude);
        this.autocompleteonload(this.latitude, this.longitude);
      });
      //
    }
  }

  public autocompleteonload(lat, lng) {
    let geocoder = new google.maps.Geocoder();
    let latlng = new google.maps.LatLng(lat, lng);
    let request = {
      location: latlng
    }
    console.log(request);

    geocoder.geocode(request, (results, status) => {
      if (status == 'OK') {
        this.currentaddress = results[0].address_components;
        this.formatedaddress = results[0].formatted_address;
        this.autocompleteAddress(this.currentaddress);
      }
      else {
        alert('Geocode was not successful for the following reason: ' + status);
      }
    });
    if (this.currentaddress != null || this.currentaddress != undefined) {
      this.autocompleteAddress(this.currentaddress);
    }
  }

  getPosition() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude + (0.0000000000100 * Math.random());
        this.longitude = position.coords.longitude + (0.0000000000100 * Math.random());
      });
    } else {
      alert("Geolocation is not supported by this browser.");
    }
  }

  public autocompleteAddress(result) {
    if (result == undefined) {
      console.log("abc");
    }
    this.state = (result
      .find(item => item.types.indexOf('administrative_area_level_1') > -1)
      .long_name);

    this.country = (result
      .find(item => item.types.indexOf('country') > -1)
      .long_name);

    this.postCode = (result
      .find(item => item.types.indexOf('postal_code') > -1)
      .long_name);


    var newItem = {
      'latitude': this.latitude,
      'longitude': this.longitude,
      'address': this.houseNo + " " + this.formatedaddress
    };

    localStorage.setItem('userAddress', JSON.stringify(newItem));

    Storage.prototype.setObject = function (userAddress, newItem) {
      this.setItem(userAddress, JSON.stringify(newItem));
    }

    Storage.prototype.getObject = function (userAddress) {
      var value = this.getItem(userAddress);
      console.log(JSON.parse(value));
      return value && JSON.parse(value);
    }

  }

  sendMessage(): void {
    // send message to subscribers via observable subject
    if (this.houseNo) {
      var newItem = {
        'latitude': this.latitude,
        'longitude': this.longitude,
        'address': this.houseNo + " " + this.formatedaddress
      };

      localStorage.setItem('userAddress', JSON.stringify(newItem));

      Storage.prototype.setObject = function (userAddress, newItem) {
        this.setItem(userAddress, JSON.stringify(newItem));
      }
      this.service.sendMessage('location');
      this.activeModal.close();
    }
    else {
      this.formdirty = true;
    }
  }
}
