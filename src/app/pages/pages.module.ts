import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MoviesComponent } from './movies/movies.component';
import { MovieDetailsComponent } from './movies/movie-details/movie-details.component';
import { SharedModule } from './../shared/shared-module.module';
import { RouterModule } from '@angular/router';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxImageZoomModule } from 'ngx-image-zoom';
import { HttpClientJsonpModule, HttpClientModule } from '@angular/common/http';
import { BanquetHallsComponent } from './banquet-halls/banquet-halls.component';
import { CouponsComponent } from './coupons/coupons.component';
import { EventsDetailsComponent } from './events/events-details/events-details.component';
import { EventsComponent } from './events/events.component';
import { HealthDepartmentsComponent } from './health-departments/health-departments.component';
import { HealthComponent } from './health/health.component';
import { OrderDetailsComponent } from './orders/order-details/order-details.component';
import { OrdersComponent } from './orders/orders.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { StoreComponent } from './store/store.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { ShareDirective } from '@app/share/share.directive';
import { OffersComponent } from './offers/offers.component';
import { NewStoresComponent } from './new-stores/new-stores.component';
import {StaticPageWrapperComponent} from './static-page-wrapper/static-page-wrapper.component';
import { HomeDeliveryBrowseCategoriesComponent
} from './home-delivery/home-delivery-browse-categories/home-delivery-browse-categories.component';
import {MatButtonModule} from '@angular/material/button';
import {CoreModule} from '@app/core/core.module';
import { DeliveryOrderDetailsComponent } from './orders/delivery-order-details/delivery-order-details.component';
import {MatCardModule, MatFormFieldModule, MatSelectModule} from '@angular/material';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { LocationComponent } from './location/location.component';
import { AgmCoreModule } from '@agm/core';
import { } from 'google-maps';

@NgModule({
  declarations: [MoviesComponent, StoreComponent, MovieDetailsComponent,
    EventsComponent, EventsDetailsComponent, CouponsComponent, HealthComponent,
    NotFoundComponent, BanquetHallsComponent, OrdersComponent, OrderDetailsComponent, HealthDepartmentsComponent,
    ShareDirective, OffersComponent, NewStoresComponent,
    StaticPageWrapperComponent,
    HomeDeliveryBrowseCategoriesComponent,
    DeliveryOrderDetailsComponent, LocationComponent],

  imports: [
    BrowserAnimationsModule,
    ModalModule.forRoot(),
    CommonModule,
    SharedModule,
    RouterModule,
    NgxPaginationModule,
    CarouselModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgxImageZoomModule.forRoot(),
    HttpClientModule,       // (Required) For share counts
    HttpClientJsonpModule,  // (Optional) Add if you want tumblr share counts
    MatButtonModule,
    CoreModule,
    MatCardModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    MatFormFieldModule,
    MatSelectModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBhU34uK5dLXDUM1PDVWBWJ6A1CBYCBcKw',
      libraries: ['geometry','places']
    }),
  ],

  providers: [ ],
  exports: [LocationComponent],
  entryComponents: [LocationComponent]
})
export class PagesModule { }
