import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment as env } from '@env/environment';
import { ActivatedRoute } from '@angular/router';
import { Title, Meta } from '@angular/platform-browser';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../../services/http.helper';

const BASE_URL = env.serverUrl;

@Component({
  selector: 'app-activities',
  templateUrl: './activities.component.html',
  styleUrls: ['./activities.component.scss']
})
export class ActivitiesComponent implements OnInit {
  activitiesList: Array<any> = [];
  activitiesCategoryList: Array<string> = [];
  activeTab: string = '';

  nextToken: number = 1;
  listState: string = '';
  isFullyLoaded: boolean = false;
  baseUrl: string = env.webUrl;

  title: string = "";
  description: string = "";
  keywords: string = "";

  cityMapping = new Map();
  city: string;

  constructor(private http: HttpClient, private api: MyHttpClient,
    private metaTagService: Meta,
    private titleService: Title,
    private activatedRoute: ActivatedRoute) {

    this.cityMapping.set('ballari', 'bellary/ballari');
    this.cityMapping.set('hosapete', 'hospet/hosapete');

  }
  ngOnInit() {
    this.activeTab = '';
    this.city = this.activatedRoute.snapshot.paramMap.get("city");
    this.activeTab = this.activatedRoute.snapshot.paramMap.get("type");
    console.log(this.activeTab);
    this.getactivitiesCategoryList(this.city);
    this.getactivitiesList(this.activeTab);

  }

  addPageMeta() {

    this.title = 'Happening activities in ' + this.cityMapping.get(this.city) + ' - OneShell';

    this.description = 'food and drink in ' + this.cityMapping.get(this.city) + ', ' +
      'Tourist Attractions in ' + this.cityMapping.get(this.city) + ', ' +
      // 'Divine Events in ' + this.cityMapping.get(this.city) + ', ' +
      // 'Events in  ' + this.cityMapping.get(this.city) + ', ' +
      // 'Exhibitions in  ' + this.cityMapping.get(this.city) + ', ' +
      // 'Devotional Events in ' + this.cityMapping.get(this.city) + ', ' +
      // 'Temple Events in ' + this.cityMapping.get(this.city) + ', ' +
      // 'Performances in  ' + this.cityMapping.get(this.city) + ', ' +
      // 'Food Events in ' + this.cityMapping.get(this.city) + ', ' +
      // 'Workshops in   ' + this.cityMapping.get(this.city) + ', ' +
      // 'Party Events in ' + this.cityMapping.get(this.city) + ', ' +
      // 'Conference Events in ' + this.cityMapping.get(this.city) + ', ' +
      // 'Education Events in ' + this.cityMapping.get(this.city);


      this.metaTagService.addTags([
        { name: 'Title', content: this.title },
        { name: 'description', content: this.description },
        { name: 'keywords', content: this.keywords },
        { name: 'robots', content: 'index, follow' },
        { name: 'author', content: 'OneShell' },
        { charset: 'UTF-8' }
      ]);
    this.titleService.setTitle(this.title);
  }

  getactivitiesList(tab: string): void {
    if (tab != this.listState) {
      this.nextToken = 1;
      this.isFullyLoaded = false;
      this.activitiesList = [];
    }
    let reqBody = {
      next_token: this.nextToken,
      activity_type: tab,
      target_audience_city: this.city
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    this.api.post<any>(BASE_URL + "/v1/customer/activities/getActivities", reqBody, httpOptions).subscribe(
      response => {
        console.log(response);
        this.activitiesList = this.activitiesList.concat(response.activities_list);

        for (let i = 0; i < this.activitiesList.length; i++) {
          this.activitiesList[i]['urlTitleName'] = this.activitiesList[i]['title'].replace(/\s+/g, "-").replace(/[^a-zA-Z0-9-]/g, "");
          this.keywords = this.keywords + this.activitiesList[i]['title'] + ' in ' + this.cityMapping.get(this.city) + ', ';
        }

        if (Number(response.next_token) > 1)
          this.listState = tab;

        this.addPageMeta();
      }, (err) => {
        console.log('error ' + err);
        //show no data found message
      })
  }

  getactivitiesCategoryList(city: string): void {

    const params = new HttpParams();
    const reqData = params.set("city", city);

    this.api.get<any>(BASE_URL + '/v1/customer/activities/getActivityCategories',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
        response => {
         
          this.activitiesCategoryList = response;
          
        }, (err) => {
          console.log('error ' + err);
          //show no data found message
        })
  }

  handleChangeInfilter($event: any): void {
    console.log("handle change events %O", $event);
  }


  isActive(tabName: string): boolean {
    return (tabName === this.activeTab);
  }
}