import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { PagesService } from '@app/pages/pages.service';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { LoginDataService } from '@app/shared/login-data.service';
import { Title, Meta } from '@angular/platform-browser';
import { environment as env } from '@env/environment';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../../../services/http.helper';

const BASE_URL = env.serverUrl;

@Component({
  selector: 'app-activities-details',
  templateUrl: './activities-detail.component.html',
  styleUrls: ['./activities-detail.component.scss'],
})
export class ActivitiesDetailComponent implements OnInit {

  activities_id: string;
  target_audience_city: string;
  activities_type: string;
  activitiesData: any = {};
  isAttendanceEnabled: boolean = false;
  showAttendanceDialog: boolean = false;
  isRegisteredUser: boolean = true;
  peopleCount: any;
  enableAttendance: boolean = false;
  display_date: String;

  title: string = "";
  description: string = "";
  keywords: string = "";

  cityMapping = new Map();

  constructor(private _pagesService: PagesService, private _sanitizer: DomSanitizer, private api: MyHttpClient, private _Activatedroute: ActivatedRoute, private _router: Router, private datePipe: DatePipe, private data: LoginDataService,
    private activatedRoute: ActivatedRoute, private metaTagService: Meta,
    private titleService: Title) { 
  }

  ngOnInit() {
    console.log('details works');
    this.activities_id = this.activatedRoute.snapshot.paramMap.get("id");
    this.activities_type = this.activatedRoute.snapshot.paramMap.get("type");
    this.target_audience_city = this.activatedRoute.snapshot.paramMap.get("city");

    this.getactivitiesDetails();  
  }

  addPageMeta() {

    this.metaTagService.addTags([
      { name: 'Title', content: this.title },
      { name: 'description', content: this.description },
      { name: 'keywords', content: this.keywords },
      { name: 'robots', content: 'index, follow' },
      { name: 'author', content: 'OneShell' },
      { charset: 'UTF-8' }
    ]);
    this.titleService.setTitle(this.title);
  }

  getactivitiesDetails() {
    const params = new HttpParams();
    const reqData = params
      .set("activity_id", this.activities_id)
      .set("activity_type", this.activities_type)
      .set("target_audience_city", this.target_audience_city);
    
      this.api.get<any>(BASE_URL + '/v1/customer/activities/getActivityDetails',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
        response => {
     
      this.activitiesData = response;

      this.title = this.activitiesData.title + ' in ' + this.cityMapping.get(this.target_audience_city) + ' - OneShell';
      this.description = this.activitiesData.description;
      this.keywords = this.activitiesData.title + ' in ' + this.cityMapping.get(this.target_audience_city) + ', ' + this.activitiesData.venue_name + ' in ' + this.cityMapping.get(this.target_audience_city);

      if (response.is_recurring) {
        this.activitiesData.display_date = 'Occurs Every ' + response.days.toString();
      } else if (response.start_date === this.activitiesData.end_date) {
        var dateSplit = response.start_date.split("/");
        this.activitiesData.display_date = this.datePipe.transform(dateSplit[2] + "-" + dateSplit[1] + '-' + dateSplit[0], "EE, d MMM y");
      } else {
        var startDateSplit = response.start_date.split("/");
        var endDateSplit = response.end_date.split("/");

        this.activitiesData.display_date = this.datePipe.transform(startDateSplit[2] + "-" + startDateSplit[1] + '-' + startDateSplit[0], "EE, d ")
          + ' - ' + this.datePipe.transform(endDateSplit[2] + "-" + endDateSplit[1] + '-' + endDateSplit[0], "EE, d MMM y");
      }

      this.addPageMeta();

    }, (err) => {
      console.log('error ' + err);
      //show no data found message
    });
  }

  showComma(index: any, list: Array<any>) {
    if (list) {
      return (index < list.length - 1);
    } else {
      return false;
    }
  }
}