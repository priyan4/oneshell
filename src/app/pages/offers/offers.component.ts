import { Component, OnInit, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PagesService } from '@app/pages/pages.service';
import { Title, Meta } from '@angular/platform-browser';
import { DOCUMENT } from '@angular/common';
import { environment as env } from '@env/environment';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../../services/http.helper';

const BASE_URL = env.serverUrl;

@Component({
  selector: 'app-offers',
  templateUrl: './offers.component.html',
  styleUrls: ['./offers.component.scss']
})
export class OffersComponent implements OnInit {

  offersList: Array<any> = [];
  offersCategoryList: any[] = [];

  activeCategory: string = '';
  nextToken: number = 1;
  listState: string = '';
  isFullyLoaded: boolean = false;
  paginationSize: number = 10;
  activeCategoryObj: any;
  city: string = 'ballari';
  target_audience_city: string;
  customer_id: any;
  baseUrl: string = env.webUrl;
  title: string = "";
  description: string = "";
  keywords: string = "";

  cityMapping = new Map();

  get selectedCategoryFromRoute(): string {
    return this.activatedRoute.snapshot.paramMap.get('type');
  }

  constructor(private http: MyHttpClient, private api: MyHttpClient, private _pagesService: PagesService,
    private _router: Router, private activatedRoute: ActivatedRoute, private metaTagService: Meta, private titleService: Title,
    @Inject(DOCUMENT) private document: Document) {
    this.cityMapping.set('ballari', 'bellary/ballari');
    this.cityMapping.set('hosapete', 'hospet/hosapete');

  }

  ngOnInit() {

    this.target_audience_city = this.activatedRoute.snapshot.paramMap.get("city");
    this.activeCategory = this.activatedRoute.snapshot.paramMap.get("type");
    this.activeCategory = decodeURIComponent(this.activeCategory);
    this.city = this.target_audience_city;
    this.activeCategoryObj = this.baseUrl + '/offers/' + this.target_audience_city + `/${this.selectedCategoryFromRoute || 'ALL'}`;
    this.customer_id = "c0000000000000";
    this.getOffersCategories();
    this.getOffersList(this.activeCategory);
  }

  addPageMeta() {

    this.title = 'Top Offers in ' + this.cityMapping.get(this.city) + ' - OneShell';

    this.description = 'Top City Offers in ' + this.cityMapping.get(this.city) + ', ' +
      'Grocery Offers in ' + this.cityMapping.get(this.city) + ', ' +
      'Super Market Offers in ' + this.cityMapping.get(this.city) + ', ' +
      'Restaurant Offers in ' + this.cityMapping.get(this.city) + ', ' +
      'Food Offers in ' + this.cityMapping.get(this.city) + ', ' +
      'Apparels Offers in ' + this.cityMapping.get(this.city) + ', ' +
      'Clothing Offers in ' + this.cityMapping.get(this.city) + ', ' +
      'Fashion Offers in ' + this.cityMapping.get(this.city) + ', ' +
      'Women Fashion Offers in ' + this.cityMapping.get(this.city) + ', ' +
      'Men Fashion Offers in ' + this.cityMapping.get(this.city) + ', ' +
      'Opticals Offers in ' + this.cityMapping.get(this.city) + ', ' +
      'Spectacles Offers in ' + this.cityMapping.get(this.city) + ', ' +
      'Spa Offers in ' + this.cityMapping.get(this.city) + ', ' +
      'Saloon Offers in ' + this.cityMapping.get(this.city) + ', ' +
      'Jewellery Offers in ' + this.cityMapping.get(this.city) + ', ' +
      'Hardware Offers in ' + this.cityMapping.get(this.city) + ', ' +
      'Clinic Offers in ' + this.cityMapping.get(this.city) + ', ' +
      'Health Offers in ' + this.cityMapping.get(this.city) + ', ' +
      'Furniture Offers in ' + this.cityMapping.get(this.city) + ', ' +
      'Toys, Games and Baby Care Offers in ' + this.cityMapping.get(this.city);


    this.metaTagService.addTags([
      { name: 'Title', content: this.title },
      { name: 'description', content: this.description },
      { name: 'keywords', content: this.keywords },
      { name: 'robots', content: 'index, follow' },
      { name: 'author', content: 'OneShell' },
      { charset: 'UTF-8' }
    ]);
    this.titleService.setTitle(this.title);
  }

  getOffersList(tab: string) {

    let reqBody = {
      target_audience_city: this.city,
      next_token: this.nextToken,
      customer_id: this.customer_id,
      page_number: "1",
      page_size: "50",
      city: this.city,
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    this.api.post<any>(BASE_URL + "/v1/web/customerService/cityOffers/getCityOffers", reqBody, httpOptions).subscribe(
      response => {

        if (tab != 'ALL') {
          this.offersList = response.offerResponse.filter(offer => offer.offer_category.offer_category_name == tab);
        } else {
          this.offersList = response.offerResponse;
        }

        for (let i = 0; i < this.offersList.length; i++) {
          this.offersList[i]['urlBusinessName'] = this.offersList[i]['business_name'].replace(/\s+/g, "-").replace(/[^a-zA-Z0-9-]/g, "");
          this.keywords = this.keywords + this.offersList[i]['business_name'] + ' in ' + this.cityMapping.get(this.city) + ', ';
          this.keywords = this.keywords + this.offersList[i]['offer_title'] + ' in ' + this.cityMapping.get(this.city) + ', ';
        }

        if (Number(response.next_token) > 1)
          this.listState = this.activeCategory;

        if ((Number(response.next_token) - (this.nextToken)) != this.paginationSize)
          this.isFullyLoaded = true;

        this.nextToken = Number(response.next_token);

        this.addPageMeta();

      }, (err) => console.error(err)
    );
  }
  handleOffersTabChange(): void {
    this.document.location.href = this.activeCategoryObj;
  }
  getOffersCategories(): void {

    const OFFER_CATEGORY_ALL = {
      offer_display_name: 'ALL',
      offer_category_name: 'ALL'
    };
    this.offersCategoryList.unshift(OFFER_CATEGORY_ALL);
    console.log(this.offersCategoryList);
    const params = new HttpParams();
    const reqData = params.set("target_audience_city", this.city);

    this.api.get<any>(BASE_URL + '/v1/web/customerService/cityOffers/getCityOfferCategories',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
        response => {
          console.log(this.offersCategoryList);
          Array.prototype.push.apply(this.offersCategoryList, response);
          // this.offersCategoryList.push(response);
          console.log(this.offersCategoryList);
        }, (err) => console.error(err)
      );
  }

  handleVisitStoreClicked(selectedList: any) {
    console.log(selectedList);
    // this.location = "offersDetails/" + selectedList['business_id'] + '/' + selectedList['business_name'];
  }
}