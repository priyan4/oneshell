import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HealthDepartmentsComponent } from './health-departments.component';

describe('HealthDepartmentsComponent', () => {
  let component: HealthDepartmentsComponent;
  let fixture: ComponentFixture<HealthDepartmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HealthDepartmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HealthDepartmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
