import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PagesService } from '@app/pages/pages.service';
import { ActivatedRoute, Router } from '@angular/router';
import { environment as env } from '@env/environment';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../../services/http.helper';

const BASE_URL = env.serverUrl;

@Component({
  selector: 'app-health-departments',
  templateUrl: './health-departments.component.html',
  styleUrls: ['./health-departments.component.scss']
})
export class HealthDepartmentsComponent implements OnInit {

  healthLevel2Categories: Array<any> = [];
  city: string;
  baseUrl: string = env.webUrl;

  constructor(private http: HttpClient, private api: MyHttpClient, private _pagesService: PagesService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.city = this.activatedRoute.snapshot.paramMap.get("city");
    this.getDepartments();
  }

  getDepartments() {
    const params = new HttpParams();
    const reqData = params
      .set("level1_category", "health_care_level1");

    this.api.get<any>(BASE_URL + '/v1/web/cache/categories/getCustomerLevel2ByLevel1',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
        response => {

          console.log(response);
          this.healthLevel2Categories = response;

        }, (err) => {
          console.log('error ' + err);
          //show no data found message
        })
  }

}