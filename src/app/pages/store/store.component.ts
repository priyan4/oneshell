import { Component, OnInit } from "@angular/core";
import { FrequentCategory } from "./store";
import { ActivatedRoute, Router } from "@angular/router";
import { environment as env } from '@env/environment';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../../services/http.helper';

const BASE_URL = env.serverUrl;

@Component({
  selector: "app-store",
  templateUrl: "./store.component.html",
  styleUrls: ["./store.component.scss"]
})
export class StoreComponent implements OnInit {
  stores: Array<any> = [];
  level2Categories: Array<any> = [];
  frequentCategory: FrequentCategory[];
  store: any;
  city: string;
  baseUrl: string = env.webUrl;

  constructor(private api: MyHttpClient, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.city = this.activatedRoute.snapshot.paramMap.get("city");

    this.getStoresList();
    // this.getFrequentCategories();
  }

  getStoresList(): void {
    const reqData = new HttpParams().set("category_type", "store").set("city", this.city);
  
    this.api.get<any>(BASE_URL + '/v1/web/cache/categories/getCustomerLevelOneAndTwoByCity',
          { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
            response => {
          this.stores = response;

          for (let i = 0; i < this.stores.length; i++) {
            this.level2Categories = this.stores[i].level2_category;
            for (let j = 0; j < this.level2Categories.length; j++) {
                const name = this.level2Categories[j]['display_name'].replace(/\s+/g, '-').replace(/[^a-zA-Z0-9-]/g, '');
                this.level2Categories[j]['urlDisplay_name'] = name;
            }
          }
        },
        err => {
          console.log("error" + err);
        }
      );
  }
}
