export interface Store {
  level1_category: {
    display_name: string;
    image_url: string;
    level: string;
    name: string;
  };
  level2_category: {
    category_type: string;
    display_name: string;
    image_url: string;
    level: string;
    name: string;
  };
}
export interface FrequentCategory {
  display_name: string;
  image_url: string;
  name: string;
  sort_id: number;
}
export interface AdvertiseListing {
  category_type: string;
  display_name: string;
  image_url: string;
  level: string;
  name: string;
}
