import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { BehaviorSubject } from "rxjs";

import * as CryptoJS from 'crypto-js';

@Injectable({
  providedIn: "root"
})
export class PagesService {
  
selectedMovieData={name:'movie',url:'urlvalue ',data:null};
  private movieDataSource: BehaviorSubject<any> = new BehaviorSubject(
    this.selectedMovieData
  );
  private storeServiceDataSource: BehaviorSubject<any> = new BehaviorSubject(
    this.selectedMovieData
  );
  currentMovieData = this.movieDataSource.asObservable();

  constructor() {}
  changeMovieData(newData: any) {
    this.movieDataSource.next(newData);
  }
  getStoreServiceList(storeService: any) {
    this.storeServiceDataSource.next(storeService);
  }

   //The set method is use for encrypt the value.
   set(keys, value){
    var key = CryptoJS.enc.Utf8.parse(keys);
    var iv = CryptoJS.enc.Utf8.parse(keys);
    var encrypted = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(value.toString()), key,
    {
        keySize: 128 / 8,
        iv: iv,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7
    });

    return encrypted.toString();
  }

  //The get method is use for decrypt the value.
  get(keys, value){
    var key = CryptoJS.enc.Utf8.parse(keys);
    var iv = CryptoJS.enc.Utf8.parse(keys);
    var decrypted = CryptoJS.AES.decrypt(value, key, {
        keySize: 128 / 8,
        iv: iv,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7
    });

    return decrypted.toString(CryptoJS.enc.Utf8);
  }
}
