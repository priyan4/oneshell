import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaticPageWrapperComponent } from './static-page-wrapper.component';

describe('StaticPageWrapperComponent', () => {
  let component: StaticPageWrapperComponent;
  let fixture: ComponentFixture<StaticPageWrapperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaticPageWrapperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaticPageWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
