import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ActivatedRoute} from '@angular/router';
import {filter, tap} from 'rxjs/operators';

@Component({
  selector: 'app-static-page-wrapper',
  templateUrl: './static-page-wrapper.component.html'
})
export class StaticPageWrapperComponent implements OnInit {
  htmlData: any;
  constructor(private http: HttpClient, private route: ActivatedRoute) { }
  ngOnInit() {
    this.route.paramMap.pipe(
      filter(param => !!param.get('page')),
      tap(param => this.retrieveHtml(param.get('page')))
    ).subscribe();
  }
  private retrieveHtml(fileName: string) {
    const headers = new HttpHeaders({
      'Content-Type':  'text/plain',
    });
    this.http.get(`../../assets/${fileName}`,  {
      headers: headers,
      responseType: 'text'
    }).subscribe(res => this.htmlData = res);
  }
}
