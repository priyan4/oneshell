import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewStoresComponent } from './new-stores.component';

describe('NewStoresComponent', () => {
  let component: NewStoresComponent;
  let fixture: ComponentFixture<NewStoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewStoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewStoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
