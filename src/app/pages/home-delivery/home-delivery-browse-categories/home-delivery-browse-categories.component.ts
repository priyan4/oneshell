import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Subject, Subscription } from 'rxjs';
import { distinctUntilChanged, switchMap } from 'rxjs/operators';
import { environment as env } from '@env/environment';
import { Title, Meta } from '@angular/platform-browser';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../../../services/http.helper';

const BASE_URL = env.serverUrl;

@Component({
  selector: 'app-home-delivery-browse-categories',
  templateUrl: './home-delivery-browse-categories.component.html',
  styleUrls: ['./home-delivery-browse-categories.component.scss']
})
export class HomeDeliveryBrowseCategoriesComponent implements OnInit {

  categoryList: any;
  searchTxt: string = '';
  subscription: Subscription;
  search$: Subject<any> = new Subject();
  searchResultsList: Array<any> = [];
  baseUrl: string = env.webUrl;
  city: string;
  url: string;

  title: string = "";
  description: string = "";
  keywords: string = "";
  cityMapping = new Map();

  constructor(
    private activatedRoute: ActivatedRoute,
    private api: MyHttpClient,
    private router: Router,
    private metaTagService: Meta,
    private titleService: Title) {
    this.cityMapping.set('ballari', 'bellary/ballari');
    this.cityMapping.set('hosapete', 'hospet/hosapete');
  }

  ngOnInit() {
    this.city = this.activatedRoute.snapshot.paramMap.get("city");
    console.log("New Calling Categories");
    this.getCategoriesList(this.city);
    this.addSearchListener();
  }

  addPageMeta() {
    this.title = 'Home Delivery Categories in ' + this.cityMapping.get(this.city) + ' - OneShell';

    this.metaTagService.addTags([
      { name: 'Title', content: this.title },
      { name: 'description', content: this.description },
      { name: 'keywords', content: this.keywords },
      { name: 'robots', content: 'index, follow' },
      { name: 'author', content: 'OneShell' },
      { charset: 'UTF-8' }
    ]);
    this.titleService.setTitle(this.title);
  }

  getCategoriesList(city: string) {
    const params = new HttpParams();

    let reqData;

    reqData = params.set("city", this.city);

    this.api.get<any>(BASE_URL + '/v1/web/customer/homeDelivery/getHomeDeliveryCategories',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
        response => {

          this.categoryList = response.categories;
          console.log(this.categoryList);

          for (let i = 0; i < this.categoryList.length; i++) {
            const name = this.categoryList[i]['display_name'].replace(/\s+/g, '-').replace(/[^a-zA-Z0-9-]/g, '');
            this.categoryList[i]['urlDisplay_name'] = name;
          }

        }, (err) => {
          console.log('error ' + err);
          //show no data found message
        })
  }

  setHeightOfImage() {
    return (window.innerHeight / 4) + 'px';
  }

  handleSearchChange(event) {
    console.log(this.searchTxt);
    this.search$.next(this.searchTxt);
    if (event.target.id != "txtSearch") // or some similar check
    {
      this.searchResultsList = [];
    }
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  addSearchListener() {

    this.subscription = this.search$.pipe(distinctUntilChanged(), switchMap((searchText: string) => {

      this.searchResultsList = [];

      const params = new HttpParams();
      const reqData = params
        .set("city", this.city)
        .set("keyword", this.searchTxt)
        .set("size", "20");

      return this.api.get<any>(BASE_URL + '/v1/customer/homeDelivery/getStrings',
        { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData });
    })).subscribe((response) => {
      console.log(response);

      response.forEach(element => {
        this.searchResultsList.push(element);
      });
    })
  }

  handleSearchResultClicked(result: any) {
    console.log(result);
    this.router.navigate(['/storeListing'], { state: { selectedTag: result.tag, keyword: result.keyword, requestedPage: 'SearchPage' } });
  }

}