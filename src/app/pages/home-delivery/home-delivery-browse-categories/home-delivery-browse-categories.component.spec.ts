import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeDeliveryBrowseCategoriesComponent } from './home-delivery-browse-categories.component';

describe('HomeDeliveryBrowseCategoriesComponent', () => {
  let component: HomeDeliveryBrowseCategoriesComponent;
  let fixture: ComponentFixture<HomeDeliveryBrowseCategoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeDeliveryBrowseCategoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeDeliveryBrowseCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
