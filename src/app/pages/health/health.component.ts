// import * as $ from 'jquery';
import { AfterViewInit, Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { LoginDataService } from '@app/shared/login-data.service';
import { Renderer2 } from '@angular/core';
import { environment as env } from '@env/environment';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../../services/http.helper';

const BASE_URL = env.serverUrl;

declare var $: any;

@Component({
  selector: 'app-health',
  templateUrl: './health.component.html',
  styleUrls: ['./health.component.scss']
})
export class HealthComponent implements OnInit, AfterViewInit {
  subscribeBusiness: any;
  featuredProducts: any;
  featuredProductsList: any;
  featuredProductsMobList: any;
  healthTips: any;
  categories: Array<String> = [];
  bannerAdvList: any;
  customer_id: any;
  city: any;

  healthTipColors = ['#9dddee', '#aceade', '#d2b6e5', '#fbcad2', '#f0f4a4', '#ffcebe', '#ccd2e7', '#bbf3aa', '#debdcd', '#d9cfba'];

  bannerSetHeight: any = {
    display: "block",
    width: "100%",
    height: "100%",
    padding: "0px 15px 0px 15px"
  };

  baseUrl: string = env.webUrl;

  @ViewChild("homeBannerImage", { static: false }) bannerImage: ElementRef;
  @ViewChildren('allHealthTipes') allHealthTips: QueryList<any>;
  constructor(private api: MyHttpClient,
    private router: Router,
    private data: LoginDataService,
    private activatedRoute: ActivatedRoute, private rd: Renderer2) { }

  ngOnInit() {

    this.customer_id = 'c000000000000000'
    this.city = this.activatedRoute.snapshot.paramMap.get("city");
    this.categories.push("health_care_level1");

    this.getBannerAdv();
    this.getFeaturedBusiness();
    this.getFeaturedProducts();
    this.getHealthTips();
  }

  ngAfterViewInit() {
    // $(document).ready(function () {
    //   $(".carousel-inner").on('swipeleft', { passive: false }, function (e) {
    //     console.log("swipe left");
    //     $(".carousel-inner").carousel("next");
    //   });
    //   $(".carousel-inner").on('swiperight', { passive: false }, function (e) {
    //     console.log("swipe right");
    //     $(".carousel-inner").carousel("prev");
    //   });
    // });
    this.allHealthTips.changes.subscribe(t => {
      // this.changeHealthTipBackground();
    })

  }

  onResize() {
    var ele = document.getElementsByClassName("banner-img");
    var len = document.getElementsByClassName("banner-img").length;
    for (var i = 0; i < len; i++) {
      (<HTMLElement>ele[i]).style.display = "block";
      (<HTMLElement>ele[i]).style.height = "100%";
      (<HTMLElement>ele[i]).style.width = "100%";
      (<HTMLElement>ele[i]).style.padding = "0px 15px 0px 15px";
    }
  }

  bannerHeight() {
    if (window.innerWidth < 1024) {
      return window.innerHeight / 4 + "px";
    } else {
      return "auto";
    }
  }

  getBannerAdv() {
    const params = new HttpParams();
    const reqData = params
      .set("customer_id", this.customer_id)
      .set("target_audience_city", this.city)
      .set("categories", JSON.stringify(this.categories));

    this.api.get<any>(BASE_URL + '/v1/web/customer/healthCare/getBannerAdv',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
        response => {
          this.bannerAdvList = response;
          for (let i = 0; i < this.bannerAdvList.length; i++) {
            if (this.bannerAdvList[i]['business_name'] != null) {
              this.bannerAdvList[i]['urlBusinessName'] = this.bannerAdvList[i]['business_name'].replace(/\s+/g, "-").replace(/[^a-zA-Z0-9-]/g, "");
            }
            if (this.bannerAdvList[i]['title'] != null) {
              this.bannerAdvList[i]['urlEventName'] = this.bannerAdvList[i]['title'].replace(/\s+/g, "-").replace(/[^a-zA-Z0-9-]/g, "");
            }
          }
          for (let banner of this.bannerAdvList) {
            if ("business" === banner.banner_type) {
              banner.href_link = this.baseUrl + '/business/' + banner.business_city + '/' + banner.business_id + '/' + banner.urlBusinessName;
            } else if ("event" === banner.banner_type) {
              banner.href_link = this.baseUrl + '/event/' + banner.city + '/' + banner.event_type + '/' + banner.event_id + '/' + banner.urlEventName;
            }
          }
        },
        err => {
          console.log("error" + err);
        }
      );
  }

  getFeaturedBusiness() {
    const params = new HttpParams();
    const reqData = params
      .set("business_city", this.city)
      .set("next_token", "1")
      .set("is_home_page", "true")
      .set("customer_id", this.customer_id)
      .set("customer_gender", "Male")
      .set("customer_age", "30")
      .set("query", "0")
      .set("categories", JSON.stringify(this.categories));

    this.api.get<any>(BASE_URL + '/v1/web/customer/healthCare/getBusinessAdv',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
        response => {
          this.subscribeBusiness = response.business_list;
          for (let i = 0; i < this.subscribeBusiness.length; i++) {
            this.subscribeBusiness[i]['urlBusinessName'] = this.subscribeBusiness[i]['business_name'].replace(/\s+/g, "-").replace(/[^a-zA-Z0-9-]/g, "");
          }
        },
        err => {
          console.log("error" + err);
        }
      );
  }

  getFeaturedProducts() {
    let data: any;

    data = {
      target_audience_city: this.city,
      page_number: 1,
      page_size: 12,
      //below is not being used
      customer_gender: "All",
      //below is not being used
      customer_age: 30,
      query: 0,
      customer_id: this.customer_id,
      category: "Health",
      is_home_page: false
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    this.api.post<any>(BASE_URL + "/v1/web/customer/healthCare/getProductAdv", data, httpOptions).subscribe(
      response => {
        this.featuredProducts = response;
        for (let i = 0; i < this.featuredProducts.length; i++) {
          this.featuredProducts[i]['urlBusinessName'] = this.featuredProducts[i]['business_name'].replace(/\s+/g, "-").replace(/[^a-zA-Z0-9-]/g, "");
        }
        this.featuredProductsList = this.featuredProducts.product_list;
        this.featuredProductsMobList = this.featuredProductsList.slice(0, 4);
      },
      err => {
        console.log("error" + err);
      }
    );
  }

  getHealthTips() {
    const params = new HttpParams();
    const reqData = params
      .set("business_city", this.city);

    this.api.get<any>(BASE_URL + '/v1/web/customer/healthCare/getHealthTips',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
        response => {
          this.healthTips = response;
          for (let i = 0; i < this.healthTips.length; i++) {
            this.healthTips[i]['urlBusinessName'] = this.healthTips[i]['clinic_name'].replace(/\s+/g, "-").replace(/[^a-zA-Z0-9-]/g, "");
          }
          if (this.healthTips.length > 0) {
            for (let i = 0; i < this.healthTips.length; i++) {
              this.healthTips[i]['background'] = this.healthTipColors[Math.floor(Math.random() * this.healthTipColors.length)];
            }
          }
        },
        err => {
          console.log("error" + err);
        }
      );
  }

  // changeColour() {
  //   let ranCl = 
  //   return (ranCl);
  // }

  // changeHealthTipBackground() {
  //   // console.log($(".health-tip-bg"));
  //   // if ($(".health-tip-bg").length != 0) {
  //     // var healthTipColors = ['#9dddee', '#aceade', '#d2b6e5', '#fbcad2', '#f0f4a4', '#ffcebe', '#ccd2e7', '#bbf3aa', '#debdcd', '#d9cfba'];
  //     for(let i=0; i<= this.healthTips.length;i++){
  //       this.rd.setStyle(this.healthTips[i],'background-color',this.healthTipColors[i]);
  //     }

  //     // $(".health-tip-bg").each(function (i, obj) {
  //     //   console.log(i);
  //     //   console.log(obj);
  //     //   $(this).css('background', healthTipColors[i]);
  //     //   $(this).children().css('background', healthTipColors[i]);
  //     // });
  //   // }
  // }
}
