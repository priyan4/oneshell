import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { PagesService } from '@app/pages/pages.service';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { LoginDataService } from '@app/shared/login-data.service';
import { Title, Meta } from '@angular/platform-browser';
import { environment as env } from '@env/environment';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../../../services/http.helper';

const BASE_URL = env.serverUrl;

@Component({
  selector: 'app-events-details',
  templateUrl: './events-details.component.html',
  styleUrls: ['./events-details.component.scss'],
})
export class EventsDetailsComponent implements OnInit {

  @Output() loginType = new EventEmitter();

  event_id: string;
  target_audience_city: string;
  event_type: string;
  eventData: any = {};
  isAttendanceEnabled: boolean = false;
  showAttendanceDialog: boolean = false;
  isRegisteredUser: boolean = true;
  peopleCount: any;
  enableAttendance: boolean = false;
  display_date: String;

  title: string = "";
  description: string = "";
  keywords: string = "";

  cityMapping = new Map();

  constructor(private _pagesService: PagesService, private _sanitizer: DomSanitizer, private api: MyHttpClient, private _Activatedroute: ActivatedRoute, private _router: Router, private datePipe: DatePipe, private data: LoginDataService,
    private activatedRoute: ActivatedRoute, private metaTagService: Meta,
    private titleService: Title) {

    this.cityMapping.set('ballari', 'bellary/ballari');
    this.cityMapping.set('hosapete', 'hospet/hosapete');
  }

  ngOnInit() {

    this.event_id = this.activatedRoute.snapshot.paramMap.get("id");
    this.event_type = this.activatedRoute.snapshot.paramMap.get("type");
    this.target_audience_city = this.activatedRoute.snapshot.paramMap.get("city");

    this.getEventDetails();

    if (this.eventData.track_attendance && localStorage.getItem("isRegisteredUser"))
      this.getEventAttendance();
    else
      this.enableAttendance = false;

  }

  addPageMeta() {

    this.metaTagService.addTags([
      { name: 'Title', content: this.title },
      { name: 'description', content: this.description },
      { name: 'keywords', content: this.keywords },
      { name: 'robots', content: 'index, follow' },
      { name: 'author', content: 'OneShell' },
      { charset: 'UTF-8' }
    ]);
    this.titleService.setTitle(this.title);
  }

  getEventDetails() {

    let reqBody = {
      event_id: this.event_id,
      event_type: this.event_type,
      target_audience_city: this.target_audience_city
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    this.api.post<any>(BASE_URL + "/v1/web/customer/events/getEventDetails", reqBody, httpOptions).subscribe(
      response => {
        console.log(response);
        this.eventData = response;

        this.title = this.eventData.title + ' in ' + this.cityMapping.get(this.target_audience_city) + ' - OneShell';
        this.description = this.eventData.description;
        this.keywords = this.eventData.title + ' in ' + this.cityMapping.get(this.target_audience_city) + ', ' + this.eventData.venue_name + ' in ' + this.cityMapping.get(this.target_audience_city);

        if (response.is_recurring) {
          this.eventData.display_date = 'Occurs Every ' + response.days.toString();
        } else if (response.start_date === this.eventData.end_date) {
          var dateSplit = response.start_date.split("/");
          this.eventData.display_date = this.datePipe.transform(dateSplit[2] + "-" + dateSplit[1] + '-' + dateSplit[0], "EE, d MMM y");
        } else {
          var startDateSplit = response.start_date.split("/");
          var endDateSplit = response.end_date.split("/");

          this.eventData.display_date = this.datePipe.transform(startDateSplit[2] + "-" + startDateSplit[1] + '-' + startDateSplit[0], "EE, d ")
            + ' - ' + this.datePipe.transform(endDateSplit[2] + "-" + endDateSplit[1] + '-' + endDateSplit[0], "EE, d MMM y");
        }

        if (this.eventData.track_attendance && localStorage.getItem("isRegisteredUser")) {
          this.getEventAttendance();
        } else if (this.eventData.track_attendance) {
          this.enableAttendance = true;
        }

        this.addPageMeta();

      }, (err) => {
        console.log('error ' + err);
        //show no data found message
      });
  }

  showComma(index: any, list: Array<any>) {
    if (list) {
      return (index < list.length - 1);
    } else {
      return false;
    }
  }

  showAttendanceModal() {
    if (localStorage.getItem("username")) {
      this.showAttendanceDialog = true;
    } else {
      this.data.showLoginPage(true);
    }
  }

  hideAttendanceModal() {
    this.showAttendanceDialog = false;
  }

  requestForSchedule() {
    this.hideAttendanceModal();

    localStorage.setItem("isRegisteredUser", 'true');

    if (localStorage.getItem("isRegisteredUser")) {

      let reqBody = {

        customer_id: localStorage.getItem("customer_id"),
        customer_city: localStorage.getItem("city"),
        customer_phone_number: localStorage.getItem("username"),
        event_id: this.eventData.event_id,
        event_name: this.eventData.title,
        track_attendance: this.eventData.track_attendance,
        event_start_date: this.eventData.start_date,
        event_end_date: this.eventData.end_date,
        event_timing_description: this.eventData.timing_description,
        recurring_days: this.eventData.days,
        is_recurring: this.eventData.is_recurring,
        number_of_peoples_count: this.peopleCount
      };

      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      };

      this.api.post<any>(BASE_URL + "/v1/web/customer/events/postEventAttendance", reqBody, httpOptions).subscribe(
        response => {

          console.log(response);
          if (response)
            this.enableAttendance = false;
          else
            this.enableAttendance = true;

        }, (err) => {
          console.log('error ' + err);
          //show no data found message
        });

    } else {
      this.isRegisteredUser = false;
      this.enableAttendance = false;
    }
  }

  getEventAttendance() {
    const params = new HttpParams();
    const reqData = params
      .set("customer_id", localStorage.getItem("customer_id"))
      .set("customer_city", localStorage.getItem("city"))
      .set("event_id", this.eventData.event_id);

    this.api.get<any>(BASE_URL + '/v1/web/customer/web/events/getEventAttendance',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(
        response => {

          console.log(response);

          if (response)
            this.enableAttendance = false;
          else
            this.enableAttendance = true;
        },
        err => {
          console.log("error" + err);
        }
      );
  }

  onExhibitorClicked(exhibitorDetail: any) {

    this._router.navigate(["/listingPage"], { state: { data: exhibitorDetail } });

  }
}