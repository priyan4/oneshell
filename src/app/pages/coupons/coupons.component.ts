import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PagesService } from '@app/pages/pages.service';
import { ActivatedRoute, Router } from "@angular/router";
import { ContextDataService } from '@app/services/context-data.service';
import { LoginService } from '@app/login/login.service';
import { environment as env } from '@env/environment';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../../services/http.helper';

const BASE_URL = env.serverUrl;

@Component({
  selector: 'app-coupons',
  templateUrl: './coupons.component.html',
  styleUrls: ['./coupons.component.scss']
})
export class CouponsComponent implements OnInit {

  value = "testes";
  couponData: any;
  couponsList: Array<any> = [];
  couponsStagingCouponsList: Array<any> = [];
  couponsStagingList: Array<any> = [];

  activeTab: string = '';
  nextToken: number = 1;
  listState: string = '';
  isFullyLoaded: boolean = false;
  paginationSize: number = 10;

  activeCoupons: boolean = true;
  city: string;
  showCouponDetailsPopUp: boolean = false;
  couponsPerBusness: Array<any> = [];
  elementType = 'url';
  customer_id: string;

  baseUrl: string = env.webUrl;

  constructor(private http: HttpClient, private api: MyHttpClient, private _pagesService: PagesService, private _router: Router, private activatedRoute: ActivatedRoute, private contextData: ContextDataService, private _loginService: LoginService) { }

  ngOnInit() {

    this.customer_id = "c0000000000000";
    this.activeTab = this.activatedRoute.snapshot.paramMap.get("type");
    this.city = this.activatedRoute.snapshot.paramMap.get("city");

    if (this.contextData.isUserLoggedIn) {
      this.customer_id = this.contextData.customerProfile.customer_id;
      if (this.activeTab === 'active') {
        this.getCouponsList();
        this.activeCoupons = true;
      } else {
        this.getRedeemedCouponsList();
        this.activeCoupons = false;
      }
    } else {
      this._loginService.openLoginModal();
    }
  }

  getCouponsList(): void {

    if (this.activeTab != this.listState) {
      this.nextToken = 1;
      this.isFullyLoaded = false;
      this.couponsList = [];
    }

    const params = new HttpParams();

    let reqData;

    reqData = params
      .set("customer_id", this.customer_id)
      .set("city", this.contextData.customerProfile.customer_city);
    
    this.api.get<any>(BASE_URL + '/v1/web/notification/customer/getCoupons',
    { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe((response
    ) => {

      console.log(response);

      this.couponsStagingList = response['customer_coupons'];

      for (let business_name in this.couponsStagingList) {

        this.couponsStagingCouponsList = this.couponsStagingList[business_name];

        console.log(this.couponsStagingCouponsList);

        this.couponsStagingCouponsList.forEach(function (obj) {
          obj.business_name = business_name;
        });
        this.couponsList = this.couponsList.concat(this.couponsStagingCouponsList);
      }

      if (Number(response.next_token) > 1)
        this.listState = this.activeTab;

      if ((Number(response.next_token) - (this.nextToken)) != this.paginationSize)
        this.isFullyLoaded = true;

      this.nextToken = Number(response.next_token);

    }, (err) => {
      console.log('error ' + err);
      //show no data found message
    })

  }

  getRedeemedCouponsList(): void {

    if (this.activeTab != this.listState) {
      this.nextToken = 1;
      this.isFullyLoaded = false;
      this.couponsList = [];
    }

    const params = new HttpParams();

    let reqData;

    reqData = params
      .set("customer_id", this.customer_id)
      //.set("customer_id", "c615725225179321")
      .set("city", "ballari");

    this.api.get<any>(BASE_URL + '/v1/notification/customer/getRedeemedCoupons',
    { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe((response
    ) => {

      console.log(response);

      this.couponsStagingList = response['customer_coupons'];

      for (let business_name in this.couponsStagingList) {

        this.couponsStagingCouponsList = this.couponsStagingList[business_name];

        console.log(this.couponsStagingCouponsList);

        this.couponsStagingCouponsList.forEach(function (obj) {
          obj.business_name = business_name;
        });
        this.couponsList = this.couponsList.concat(this.couponsStagingCouponsList);
      }

      if (Number(response.next_token) > 1)
        this.listState = this.activeTab;

      if ((Number(response.next_token) - (this.nextToken)) != this.paginationSize)
        this.isFullyLoaded = true;

      this.nextToken = Number(response.next_token);

    }, (err) => {
      console.log('error ' + err);
      //show no data found message
    })

  }

  onCouponDetailsClicked(business_name: any) {
    this.showCouponDetailsPopUp = true;
    const params = new HttpParams();

    this.couponsPerBusness = this.couponsStagingList[business_name];

    for (let i = 0; i < this.couponsPerBusness.length; i++) {

      var data = "COUPON_REDEMPTION" + "::" + this.couponsPerBusness[i].coupon_code + "::" + localStorage.getItem('customer_id')
        + "::" + localStorage.getItem('city') + "::" + localStorage.getItem('name')
        + "::" + this.couponsPerBusness[i].business_id + "::" + this.couponsPerBusness[i].business_city;// Whatever you need to encode in the QR code

      const reqData_key = params.set('data', data);

      this.api.get<any>(BASE_URL + '/v1/customerService/getEncryptedData',
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData_key }).subscribe(
        response => {

          console.log(response);
          this.couponData.qrCode = response.encrypted_data;

          console.log(this.couponData);
        },
        err => {
          console.log("error" + err);
        }
      );

    }

    console.log(this.couponsPerBusness);

  }

  closeDetailsFormModal() {
    this.showCouponDetailsPopUp = false;
  }
}