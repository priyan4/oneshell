import { REQUIRED_MSG } from '../../constants/form-validation-message.constants';
import { FormValidationErrors } from '../../interfaces/form-validation-error.interface';
import { Title } from '@angular/platform-browser';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {
  Component, OnInit
} from '@angular/core';
import { environment as env } from '@env/environment';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {ContextDataService} from '@app/services/context-data.service';
import {filter, switchMap, tap} from 'rxjs/operators';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../../services/http.helper';
import { Observable, throwError, of } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';

const BASE_URL = env.serverUrl;

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

  loginForm: FormGroup;
  baseUrl: string = env.webUrl;
  isSubmitted: boolean;
  formValidationErrors: FormValidationErrors;
  username: string;
  passwordConfirm: string;
  referralCode: string;
  showPasswordDetailsContainer: boolean;
  showInvalidCredentials: boolean;
  activeUserType: string;

  constructor(private formBuilder: FormBuilder, private title: Title, private api: MyHttpClient, private activeModal: NgbActiveModal,
              private contextData: ContextDataService) {
    this.oneTimeInitialization();
    this.createForm();
  }

  ngOnInit() {
    this.initialization();
    this.showPasswordDetailsContainer = false;
  }

  private oneTimeInitialization() {
    this.title.setTitle('Oneshell - Login');
    this.formValidationErrors = {
      username: [
        {
          type: 'required',
          message: REQUIRED_MSG
        }
      ],
      password: [
        {
          type: 'required',
          message: REQUIRED_MSG
        }
      ]
    };
  }

  private createForm() {
    this.loginForm = this.formBuilder.group({
      username: this.formBuilder.control(null, [Validators.required]),
      password: this.formBuilder.control(null, [Validators.required])
    });
  }

  private initialization() {
    this.isSubmitted = false;
  }

  handleLoginClicked() {
    this.isSubmitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    const username = this.loginForm.value.username;
   this.doLogin('user', username, this.loginForm.value.password).pipe(
      tap(res => {
        if (!res.login_success) {
          this.showInvalidCredentials = true;
        }
      }),
      filter(res => res.login_success),
      switchMap(() =>  this.contextData.retrieveCustomerProfileByPhoneNumber(username)),
      tap(() => {
        this.activeModal.dismiss('logged-in');
        window.location.reload();
      })
    ).subscribe(() => undefined, () => this.showInvalidCredentials = true);
  }

public doLogin(userType: string, username: string, password: string): Observable<any> {
  this.activeUserType = userType;
  switch (userType) {
    case 'guest':
      return this.doGuestLogin('user');

    case 'user':
      return this.doUserLogin(username, password);
  }
}

public doUserLogin(username, password) {
  const body: LoginData = {
    username, password
  };

  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  return  this.api.post<any>(BASE_URL + "/v1/web/customer/login", body, httpOptions);
}

public doGuestLogin(username) {
  let headerOptions = {
    headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', 'Basic ' + btoa('id:YB6^wd@xS6xdH6q8'))
      .set('Accept', '*/*')
  };
  const body = new HttpParams()
    .set('username', username)
    .set('password', 'CW#JPGr2UG6fNt^L')
    .set('grant_type', 'password');

  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  return this.api.post<any>(BASE_URL + "/oauth/token", body, httpOptions)
    .flatMap((data: any) => {
      console.log(data);
      this.loginSuccess(data);
      return of('success');
    }).catch(err => { console.log(err); return of('fail') });
}

loginSuccess(data) {
  console.log('in login success method');
  console.log(data);
}
}

interface LoginData {
  username: string;
  password: string;
}