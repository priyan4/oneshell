import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { LoginModalComponent } from './login-modal/login-modal.component';
import {LoginPageComponent} from './login-page/login-page.component';
import {RouterModule} from '@angular/router';


@NgModule({
  declarations: [LoginModalComponent, LoginPageComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule
  ],
  entryComponents: [LoginModalComponent]
})
export class LoginModule { }
