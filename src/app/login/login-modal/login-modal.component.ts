import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login-modal',
  template:  `
    <app-login-page></app-login-page>
  `,
  styleUrls: ['./login-modal.component.scss']
})
export class LoginModalComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
