import { Injectable } from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {LoginModalComponent} from './login-modal/login-modal.component';
import {Observable} from 'rxjs';
import {CustomerProfile} from '@app/models/customer-profile.model';
import { environment as env } from '@env/environment';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../services/http.helper';

const BASE_URL = env.serverUrl;

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  constructor(private modalService: NgbModal, private api: MyHttpClient) { }
  openLoginModal() {
    this.modalService.open(LoginModalComponent);
  }
  getCustomerProfileByPhoneNumber(phoneNumber: string): Observable<CustomerProfile> {
    const params = new HttpParams();
    const reqData = params.set('phone_number', phoneNumber);
    return this.api.get<any>(BASE_URL + '/v1/web/customer/getCustomerProfileByPhoneNumber',
    { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData });
  }
}