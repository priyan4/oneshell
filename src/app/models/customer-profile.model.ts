export interface CustomerProfile {
    customer_id: string;
    customer_city: string;
    name: string;
    email_id: string;
    gender: 'Male' | 'Female';
    age: number;
    phone_number: number;
    categories_subscribed: any[];
}
