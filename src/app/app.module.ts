import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PagesModule } from './pages/pages.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { SharedModule } from './shared/shared-module.module';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { PopoverModule } from 'ngx-bootstrap';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BsDropdownModule } from 'ngx-bootstrap';
import { DeviceDetectorModule } from 'ngx-device-detector';
import { CoreModule } from '@app/core/core.module';
import { DatePipe, TitleCasePipe } from '@angular/common';
import { HomePageComponent } from './home-page/home-page.component';
import { HeaderComponent } from './header/header.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpInterceptorService } from './http-interceptor.service';
import { CookieService } from 'ngx-cookie-service';
import { FooterComponent } from './footer/footer.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import {ScannerModule} from '@app/scanner/scanner.module';
import { ActivitiesComponent } from './pages/activities/activities.component';
import { ActivitiesDetailComponent } from './pages/activities/activities-detail/activities-detail.component';
import {LoginModule} from '@app/login/login.module';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatBadgeModule} from '@angular/material/badge';

import {CitySelectionPopupModule} from '@app/core/city-selection-popup/city-selection-popup.module';
import { MyHttpClient } from './services/http.helper';
import { StaticPageComponent } from './static-page/static-page.component';
import * as $ from 'jquery';
import { AngularFireModule } from 'angularfire2';
import { AngularFireStorageModule } from 'angularfire2/storage';
import {firebase} from '../environments/firebase'
// import { BaseRedirectionComponent } from './base-redirection/base-redirection.component';

@NgModule({
  declarations: [AppComponent, HomePageComponent, HeaderComponent,
    FooterComponent, ContactUsComponent,
    ActivitiesComponent, ActivitiesDetailComponent, StaticPageComponent],
    imports: [
        ModalModule.forRoot(),
        BrowserModule.withServerTransition({appId: 'app-root'}),
        AppRoutingModule,
        PagesModule,
        HttpClientModule,
        SharedModule,
        AngularFontAwesomeModule,
        CarouselModule,
        PopoverModule.forRoot(),
        FormsModule,
        BsDropdownModule.forRoot(),
        DeviceDetectorModule.forRoot(),
        CoreModule,
        ReactiveFormsModule,
        NgbModule,
        ScannerModule,
        LoginModule,
        MatGridListModule,
        MatBadgeModule,
        CitySelectionPopupModule,
        AngularFireModule.initializeApp(firebase),
        AngularFireStorageModule,
        CitySelectionPopupModule
    ],
  providers: [DatePipe,TitleCasePipe,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true
    },
    { provide: 'LOCALSTORAGE', useFactory: getLocalStorage },
    CookieService, MyHttpClient
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
var localMaster = {};
export function getLocalStorage() {

  return (typeof window !== 'undefined') ? window.localStorage : {
    getItem: (itemName: string) => { return localMaster[itemName] },
    setItem: (name: string, item: any) => { localMaster[name] = item },
    clear: () => { localMaster = {} },
  };
}
