import { SignupComponent } from './components/signup/signup.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UserNotLoggedInGuard} from '@app/core/guard/user-not-logged-in-guard.service';

const routes: Routes = [
  {
    path: 'signup',
    component: SignupComponent,
    canActivate: [UserNotLoggedInGuard]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthenticationRoutingModule { }
