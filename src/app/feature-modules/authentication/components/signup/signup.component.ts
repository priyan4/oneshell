import { Router } from '@angular/router';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Component, OnInit, AfterViewInit, OnDestroy, Inject, PLATFORM_ID } from '@angular/core';
import * as firebase from 'firebase';
import {debounceTime, filter, takeWhile, tap} from 'rxjs/operators';
import { environment as env } from '@env/environment';
import { isPlatformBrowser } from '@angular/common';
import {CitiesService} from '@app/services/cities.service';
import {ContextDataService} from '@app/services/context-data.service';
import {LoginService} from '@app/login/login.service';
import {CustomerProfile} from '@app/models/customer-profile.model';
import {Subject} from 'rxjs';
import {MatSnackBar} from '@angular/material/snack-bar';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { MyHttpClient } from '../../../../services/http.helper';

const BASE_URL = env.serverUrl;

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit, AfterViewInit, OnDestroy {

  currentFlowStatus: string;
  username: string;
  showInvalidCredentials: boolean;
  windowRef: any;
  isOTPVerified: boolean;
  cities: any = [];
  locations: any = [];
  signUpForm: FormGroup;
  passwordForm: FormGroup;
  gendersList: string[] = ['Male', 'Female'];
  baseUrl = env.webUrl;
  invalidOtp: boolean;
  private destroyed: boolean;
  dataForResetPasswordOrLoginContainer: CustomerProfile;
  private sendOtpStatus$: Subject<boolean> = new Subject<boolean>();
  sendOtpFailure: boolean;
  otpValue: string;
  getPhoneNumberSubmit$: Subject<any> = new Subject<any>();
  get window() {
    return window;
  }

  constructor(private api: MyHttpClient, private title: Title, private formBuilder: FormBuilder, private router: Router,
              @Inject(PLATFORM_ID) private platformId: any, private citiesService: CitiesService,
              private contextData: ContextDataService,
              private loginService: LoginService,
              private snackBar: MatSnackBar) {
    this.createForm();
  }

  ngOnInit() {
    this.title.setTitle('Oneshell - Signup');
    this.initialization();
    this.populateCities();
    this.makeSubscriptions();
  }
  ngAfterViewInit() {
    this.initializeFireBase();
    this.windowRef.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
    this.windowRef.recaptchaVerifier
      .render()
      .then(widgetId => {
        this.windowRef.recaptchaWidgetId = widgetId;
      });
  }
  ngOnDestroy() {
    this.destroyed = true;
  }
  private initializeFireBase() {
    /* REPLACE CONFIG DETAILS BELOW WITH YOURS */
    const firebaseConfig = {
      apiKey: 'AIzaSyD9JgMFdSMc3qWz211cr45-U54ud4sBtEw',
      projectId: 'oneshell-d3a18'
    };
    if (!firebase.apps.length) {
      firebase.initializeApp(firebaseConfig);
    }
  }

  private createForm() {
    this.signUpForm = this.formBuilder.group({
      city: this.formBuilder.control(null, [Validators.required]),
      location: this.formBuilder.control(null, [Validators.required]),
      gender: this.formBuilder.control(null, [Validators.required]),
      name: this.formBuilder.control(null, [Validators.required]),
      age: this.formBuilder.control(null,
          [Validators.required, Validators.pattern('[0-9]*'),
          Validators.maxLength(3)]),
      email: this.formBuilder.control(null, [Validators.email]),
      password: this.formBuilder.control(null,
          [Validators.required, Validators.minLength(6)]),
      passwordConfirm: this.formBuilder.control(null, [Validators.required]),
      referralCode: this.formBuilder.control(null)
    });
    this.passwordForm = this.formBuilder.group({
      password: this.formBuilder.control(null,
        [Validators.required, Validators.minLength(6)]),
      passwordConfirm: this.formBuilder.control(null, [Validators.required]),
    });
  }
  private initialization() {
    this.username = null;
    this.showInvalidCredentials = false;
    if (isPlatformBrowser(this.platformId)) {
      this.windowRef = this.window;
    }
    this.currentFlowStatus = SIGNUP_FLOW_STATE.GET_PHONE_NUMBER;
  }
  private makeSubscriptions() {
    if (this.signUpForm) {
      this.signUpForm.controls.city.valueChanges.pipe(
        takeWhile(() => !this.destroyed),
        tap(() => this.getLocationsByCity())
      ).subscribe();
    }
    this.getPhoneNumberSubmit$.pipe(
      debounceTime(1000),
      takeWhile(() => !this.destroyed)
    ).subscribe(() => {
      this.sendOTP();
      this.sendOtpStatus$.asObservable().subscribe(status => {
        if (status) {
          this.currentFlowStatus = SIGNUP_FLOW_STATE.OTP_VERIFICATION;
        } else {
          this.sendOtpFailure = true;
        }
      });
    });
  }
  checkUserName() {
    const params = new HttpParams();
    const reqData = params.set('phone_number', this.username);
    
    this.api.get<any>(BASE_URL + '/v1/web/customer/getCustomerProfileByPhoneNumber',
    { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).pipe(
      tap(response => {
        if (!response) {
          this.populateCities();
          this.showSignUpDetailsContainer();
        }
      }),
      filter(response => !!response),
      tap(response => {
       if (response.is_password_exist) {
          this.handleResetPasswordOrLoginScenario(response);
        } else {
         this.showResetPasswordContainer();
       }
      })).subscribe(() => undefined, err => {
        console.log('error' + err);
      }
    );
  }
  openLoginModal() {
    this.router.navigate(['']).then(() => this.loginService.openLoginModal());
  }
  showResetPasswordContainer() {
    this.currentFlowStatus = SIGNUP_FLOW_STATE.RESET_PASSWORD;
  }
  private handleResetPasswordOrLoginScenario(data: CustomerProfile) {
    this.dataForResetPasswordOrLoginContainer = data;
    this.currentFlowStatus = SIGNUP_FLOW_STATE.RESET_PASSWORD_OR_LOGIN;
  }
  verifyOTP(formValid: boolean) {
    if (!formValid) {
      return;
    }
    this.windowRef.confirmationResult.confirm(this.otpValue).then(() => {
      this.onOtpVerificationSuccess();
    }).catch(error => {
      console.log(error);
      this.invalidOtp = true;
    });
  }
  private onOtpVerificationSuccess() {
    this.isOTPVerified = true;
    this.checkUserName();
  }
  private sendOTP() {
    const appVerifier = this.windowRef.recaptchaVerifier;
    firebase.auth().signInWithPhoneNumber('+91' + this.username, appVerifier).then(result => {
      this.windowRef.confirmationResult = result;
      this.sendOtpStatus$.next(true);
    }).catch(error => {
      console.log(error);
      this.sendOtpStatus$.next(false);
    });
  }
  private populateCities() {
    this.citiesService.getCities$(false).pipe(
      tap(cities => this.cities = cities)
    ).subscribe();
  }
  private showSignUpDetailsContainer() {
    this.currentFlowStatus = SIGNUP_FLOW_STATE.SIGNUP;
  }
  submitSignUpDetails(): void {
    if (this.signUpForm.valid) {
      const phoneNumber = this.username;
      const formValue = this.signUpForm.value;
      const reqBody = {
        phone_number: Number(phoneNumber),
        city: formValue.city,
        location: formValue.location,
        name: formValue.name,
        age: Number(formValue.age),
        password: formValue.password,
        gender: formValue.gender,
        email_id: formValue.email,
        referral_code: formValue.referralCode
      };

      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      };
  
      this.api.post<any>(BASE_URL + "/v1/web/customer/register", reqBody, httpOptions)
        .subscribe(response => {
            if (response.customer_id) {
              this.contextData.retrieveCustomerProfileByPhoneNumber(phoneNumber).subscribe(() => {
                this.showSuccessMessage('You account is successfully registered.');
                setTimeout(() => this.router.navigate(['']), 1000);
              });
            }
          },
          err => {
            console.log('error' + err);
          }
        );
    } else {
      Object.values(this.signUpForm.controls)
        .forEach(value => value.markAsTouched());
    }
  }
  private getLocationsByCity() {
    const params = new HttpParams();
    const reqData = params.set('city', this.signUpForm.controls.city.value);

    this.api.get<any>(BASE_URL + '/v1/web/customers/getLocationsByCity',
    { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), params: reqData }).subscribe(response => {
      this.locations = response;
    }, err => {
      console.log('error' + err);
    });
  }

  isFieldInvalid(fieldName: string, form: FormGroup) {
    if (form) {
      const field = form.controls[fieldName];
      return field && field.touched && field.invalid;
    }
    return true;
  }

  showPasswordError(fieldName: string, form: FormGroup) {
    if (form) {
      const field = form.controls[fieldName];
      return field && field.touched &&
        (form.value.password !== form.value.passwordConfirm);
    }
  }

  updatePassword() {
    if (this.passwordForm.valid && (this.passwordForm.value.password === this.passwordForm.value.passwordConfirm)) {
      const password = this.passwordForm.value.password;
      this.contextData.retrieveCustomerProfileByPhoneNumber(this.username).subscribe(customerProfile => {
        const reqBody = {
          username: this.username,
          password,
          city: customerProfile.customer_city,
          customer_id: customerProfile.customer_id
        };
        
        const httpOptions = {
          headers: new HttpHeaders({
            'Content-Type': 'application/json'
          })
        };
    
        this.api.post<any>(BASE_URL + "/v1/web/customer/updatePassword", reqBody, httpOptions).subscribe(
          response => {
            if (response) {
              localStorage.setItem('isRegisteredUser', 'true');
              this.showSuccessMessage('Password updated successfully');
              setTimeout(() => this.router.navigate(['']), 1000);
            }
          }
        );
      });
    } else {
      Object.values(this.passwordForm.controls)
        .forEach(value => value.markAsTouched());
    }
  }

  private showSuccessMessage(message: string) {
    this.snackBar.open(message, '', {
      duration: 2000,
    });
  }
}

enum SIGNUP_FLOW_STATE {
  GET_PHONE_NUMBER = 'getPhoneNumber',
  OTP_VERIFICATION = 'otpVerification',
  RESET_PASSWORD_OR_LOGIN = 'resetPasswordOrLogin',
  RESET_PASSWORD = 'resetPassword',
  SIGNUP = 'getSignUpDetails'
}
